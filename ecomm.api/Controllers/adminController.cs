﻿using AttributeRouting.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ecomm.model.repository;
using ecomm.util.entities;

namespace ecomm.api.Controllers
{
    public class adminController : ApiController
    {
        [POST("user/admin/postCompanyEntry/")]
        public string postCompanyEntry(COMPANY cf)
        {
            admin_repository ar = new admin_repository();
            return ar.CompanyEntry(cf);
        }


        [POST("user/admin/postUserEntry/")]
        public string postUserEntry(user_security us)
        {
            ecomm.model.repository.admin_repository ar = new ecomm.model.repository.admin_repository();
            return ar.UserEntry(us);
        }

        [POST("user/admin/postCompanyDetails/")]
        public List<COMPANY> postCompanyDetails(COMPANY cf)
        {
            admin_repository ar = new admin_repository();
            return ar.get_comp_details(cf);
        }

        [POST("user/admin/postPopulateCompany/")]
        public List<COMPANY> postPopulateCompany()
        {
            admin_repository ar = new admin_repository();
            return ar.PopulateCompany();
        }


        [POST("/category/updateenqstatus")]
        public string postupdatepenquirystatus(enq_upd oeu)
        {
            ecomm.model.repository.category_repository ar = new model.repository.category_repository();
            return ar.update_prod_enquire_status(oeu);

        }

        [POST("/category/updategenenqstatus")]
        public string postupdatepgenenquirystatus(gen_enq_upd ge)
        {
            ecomm.model.repository.category_repository ar = new model.repository.category_repository();
            return ar.update_gen_enquire_status(ge);

        }


        [POST("user/admin/postCategoryEntry/")]
        public string postCategoryEntry(CATEGORY cate)
        {
            admin_repository ar = new admin_repository();
            return ar.CategoryEntry(cate);
        }

        [POST("user/admin/postProductEntry/")]
        public string postProductEntry(PRODUCT prod)
        {
            admin_repository ar = new admin_repository();
            return ar.ProductEntry(prod);
        }
    }
}
