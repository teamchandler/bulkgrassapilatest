﻿using AttributeRouting.Web.Http;
using ecomm.util.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ecomm.model;
using ecomm.model.repository;

namespace ecomm.api.Controllers
{
    public class registrationController : ApiController
    {
        [POST("user/registration/submit_registration/")]
        public string submit_registration(registration reg)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();

            //if (rr.RegistrationCheck(reg))
            //{

            return rr.SubmitRegistrationInfo(reg);
            //}
            //else
            //{

            //    return "Invalid Email";

            //}


        }

        [POST("user/registration/update_registration_info/")]
        public string update_registration_info(registration reg)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.UpdateRegistrationInfo(reg);
           
        }


        [POST("user/registration/ResendActCode/")]
        public string ResendActCode(registration reg)
        {
            registration_repository rr = new model.repository.registration_repository();
            return rr.ResendActCode(reg);
        }

        [POST("user/registration/postFeedback/")]
        public string postFeedback(Feedback feed)
        {
            ecomm.model.repository.registration_repository reg = new model.repository.registration_repository();
            return reg.postFeedback(feed);

        }

        [POST("user/registration/ResetPassword/")]
        public string ResetPassword(registration reg)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.CheckEmailToReset(reg);
        }

        [POST("/user/registration/postContactUs/")]
        public string postContactUs(Contact cnt)
        {
            registration_repository reg = new registration_repository();
            return reg.postContactUs(cnt);
        }

        [POST("/user/registration/postGrassCamp/")]
        public string postGrassCamp(GrassCamp grasscmp)
        {
            registration_repository reg = new registration_repository();
            return reg.postGrassCamp(grasscmp);
        }

        [GET("/user/registration/GetGrassCampData/{comments}")]
        public List<GrassCamp> GetGrassCampDatap(string comments)
        {
            registration_repository reg = new registration_repository();
            return reg.GetGrassCampData(comments);
        }

        [GET("/user/registration/GetNewsletterData/{email}")]  //atul 14/7/2015
        public List<Newsletter> GetNewsletterData(string email)
        {
            registration_repository reg = new registration_repository();
            return reg.GetNewsletterData(email);
        }

        [POST("/user/registration/postNewsletter/")]           //atul 14/7/2015
        public string postNewsletter(Newsletter nwl)
        {
            registration_repository reg = new registration_repository();
            return reg.postNewsletter(nwl);
        }

       [POST("user/registration/activate_registration/")]
       public string activate_registration(registration_active reg_ac)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.ActiveRegistration(reg_ac);
        }

        [POST("user/registration/user_login/")]
        public List<registration> user_login(user_login user_log)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();

            return rr.UserLogin(user_log);
        }

        [POST("user/registration/checkEmailGuid/")]
        public string checkEmailGuid(registration_active reg)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.checkEmailGuid(reg);
        }

        [POST("user/registration/SaveNewPassword/")]
        public string SaveNewPassword(registration reg)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.SaveNewPassword(reg);
        }

        [POST("user/registration/ChangePassword/")]
        public string ChangePassword(registration reg)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.ChangePassword(reg);
        }

        [POST("/user/postSendUserCredential/")]
        public string postSendUserCredential(user_credential uc)
        {
            List<OutCollection> oc = new List<OutCollection>();
            List<String> emails = new List<String>();
            emails = uc.emailids;
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.SendUserCredentials(emails);
        }



        [POST("user/registration/insert_enquire_info/")]
        public string insert_enquire_info(enquire oe)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.insert_enquire_info(oe);

        }

        [POST("user/registration/insert_product_enquire_info/")]
        public string insert_product_enquire_info(product_enquire pe)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.insert_product_enquire_info(pe);

        }

        [POST("user/registration/update_product_enquire_info/")]
        public string update_product_enquire_info(product_enquire pe)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.update_product_enquire_info(pe);

        }

        [POST("user/registration/send_mail_for_sample_tshirt/")]
        public string send_mail_for_sample_tshirt(GrassCamp gc)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.send_mail_for_sample_tshirt(gc);

        }

        [POST("user/registration/update_gen_enquire_info/")]
        public string update_gen_enquire_info(enquire enq)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.update_gen_enquire_info(enq);

        }

        [POST("/user/postSendUserPromoCredential/")]
        public string postSendUserPromoCredential(user_credential uc)
        {
            List<OutCollection> oc = new List<OutCollection>();
            List<String> emails = new List<String>();
            emails = uc.emailids;
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.SendPromoWithCredentials(emails);
        }


        [POST("/user/postSendUserPromo/")]
        public string postSendUserPromo(user_credential uc)
        {
            List<OutCollection> oc = new List<OutCollection>();
            List<String> emails = new List<String>();
            emails = uc.emailids;
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.SendPromo(emails);
        }
        //sends only transactional sms; Diwakar 6/2/2014
        [POST("/user/postSendSmsTransactional/")]
        public string postSendSmsTransactional(user_credential uc)
        {
            List<OutCollection> oc = new List<OutCollection>();
            List<String> emails = new List<String>();
            emails = uc.emailids;
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.SendTRNSsms(emails);
        }



        [POST("/user/postRegPassword/")]
        public string postRegPassword(user_credential uc)
        {
            List<OutCollection> oc = new List<OutCollection>();
            List<String> emails = new List<String>();
            emails = uc.emailids;
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.RegPassword(emails);
        }


        [POST("/user/postMigrateStore")]
        public string MigrateStore(migrate_store ms)
        {
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();

                oc = cr.Upload_Cust_Data(ms.store.Trim(), ms.firstname.ToString().Trim(), ms.lastname.ToString().Trim(), ms.emailid.ToString().Trim(), ms.pointsloaded.ToString().Trim(), ms.pointsredeemed.ToString().Trim(), ms.shippingaddress.ToString().Trim());
                if (oc.Count > 0)
                    return oc[0].strParamValue;
                else
                    return "No Response";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [POST("/user/postUploadCustPoints")]
        public string postUploadCustPoints(migrate_store ms)
        {
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
                oc = cr.Upload_Cust_Points(ms.emailid.ToString().Trim(), ms.pointsloaded.ToString().Trim());
                if (oc.Count > 0)
                    return oc[0].strParamValue;
                else
                    return "No Response";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [GET("/user/registration/get_product_enquire_info?email={user_id}")]
        public List<enquiry_info> get_product_enquire_info(string email)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.get_product_enquire_info(email);

        }


        [GET("/user/registration/get_general_enquire_info?email={user_id}")]
        public List<general_enquiry_info> get_general_enquire_info(string email)
        {
            ecomm.model.repository.registration_repository rr = new model.repository.registration_repository();
            return rr.get_general_enquire_info(email);

        }

        [GET("/admin/getUserPin/{strUserId}")]
        public string getUserPin(string strUserId)
        {

            try
            {
                ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
                return cr.UserPin(strUserId);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [GET("/admin/getChangeUserPin/{strUserId}")]
        public string getChangeUserPin(string strUserId)
        {

            try
            {
                ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
                return cr.ChangeUserPin(strUserId);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [POST("/user/postSalesTarget")]
        public string SalesTarget(sales_target st)
        {
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();

                oc = cr.Upload_Sales_Target(st);
                if (oc.Count > 0)
                    return oc[0].strParamValue;
                else
                    return "No Response";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [POST("/user/postLavaData")]
        public string ImportLavaData(Lava_Data ld)
        {
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();

                oc = cr.ImportLavaData(ld);
                if (oc.Count > 0)
                    return oc[0].strParamValue;
                else
                    return "No Response";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [POST("/user/postXoloData")]
        public string ImportXoloData(Xolo_Data ld)
        {
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();

                oc = cr.ImportXoloData(ld);
                if (oc.Count > 0)
                    return oc[0].strParamValue;
                else
                    return "No Response";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [POST("/user/postPumaData")]
        public string ImportPumaData(Puma_data ld)
        {
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();

                oc = cr.ImportPumaData(ld);
                if (oc.Count > 0)
                    return oc[0].strParamValue;
                else
                    return "No Response";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        [POST("/user/postSalesTargetMonthWise")]
        public string SalesTargetMonthWise(sales_target st)
        {
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();

                oc = cr.Upload_Sales_Target_MonthWise(st);
                if (oc.Count > 0)
                    return oc[0].strParamValue;
                else
                    return "No Response";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        [POST("/user/postSalesTargetMonthlyLava")]
        public string SalesTargetMonthlyLava(sales_target_monthly_lava stm)
        {
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();

                oc = cr.Upload_Sales_Target_Monthly_Lava(stm);
                if (oc.Count > 0)
                    return oc[0].strParamValue;
                else
                    return "No Response";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

    }
}