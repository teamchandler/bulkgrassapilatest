﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using ecomm.util.entities;
using System.Web;
using System.IO;



namespace ecomm.api.Controllers
{
    public class categoryController : ApiController
    {

        [GET("category/cat_list")]
        public List<category> get_cat_list()
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_cat_list();
        }

        [GET("category/brand_list/{Flag}")]
        public List<brand> get_brand_list(string Flag)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_brand_list(Flag);
        }



        //[GET("category/product_list")]
        //public List<product> get_product_list()
        //{
        //    ecomm.model.repository.category_repository cr = new model.repository.category_repository();
        //    return cr.get_product_list();
        //}

        // Modified By Hassan To Reduce The Time

        [GET("category/product_list")]
        public List<product> get_product_list()
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_product_list_less_time();
        }



        [GET("category/parent_product_list")]
        public List<product> get_parent_product_list()
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_parent_product_list();
        }

        [GET("category/get_parent_prod_list")]
        public List<product> get_parent_prod_list()
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_parent_prod_list();
        }


        [GET("category/cat_list_for_product")]
        public List<category> get_cat_list_for_product()
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_cat_list_for_product();
        }

        [GET("category/cat_list_tree")]
        public List<category> get_cat_list_tree()
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_cat_list_tree();
        }


        [GET("category/parent_cat_list/{level}")]
        public List<category> get_parent_cat_list(string level)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_parent_cat_list(level);
        }

        [GET("category/cat_filter_list")]
        public List<category> get_cat_filter_list()
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_cat_filter_list();
        }

        [GET("category/cat_list_to_copy/{cat_id}")]
        public List<category> get_cat_list_to_copy(string cat_id)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_cat_list_to_copy(cat_id);
        }


        [GET("category/cat_details/{cat_id}")]
        public List<category> get_cat_details(string cat_id)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_cat_details(cat_id);
        }
        [GET("category/brand_details/{brand_name}")]
        public List<brand> get_brand_details(string brand_name)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_brand_details(brand_name);
        }

        [GET("category/get_cat_features/{cat_id}")]
        public List<category> get_cat_features(string cat_id)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_cat_features(cat_id);
        }



        [GET("category/cat_filters_details/{cat_id}")]
        public List<category> get_cat_filters_details(string cat_id)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_cat_filters_details(cat_id);
        }

        [GET("category/get_cat_imgvidurls/{cat_id}")]
        public List<category> get_cat_imgvidurls(string cat_id)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_cat_imgvidurls(cat_id);
        }

        [GET("category/get_prod_imgvidurls/{prod_id}")]
        public List<product> get_prod_imgvidurls(string prod_id)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_prod_imgvidurls(prod_id);
        }

        [GET("/category/admin/store_cat_list/{StoreCompId}")]
        public List<category> get_store_cat_list(string StoreCompId)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_store_cat_list(StoreCompId);
        }



        [GET("/category/admin/store_Brand_list/{StoreCompId}")]
        public List<brand> get_store_Brand_list(string StoreCompId)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_store_Brand_list(StoreCompId);
        }


        [GET("/category/admin/get_store_config/{StoreCompId}")]
        public List<store_config> get_store_config(string StoreCompId)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_store_config(StoreCompId);
        }

        [GET("/category/admin/get_order_data/{Order_Id}")]
        public ordadmin get_order_data(long Order_Id)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.get_order_data(Order_Id);
        }

      

        [GET("/category/admin/store_Sku_list/{StoreCompId}")]
        public List<product> get_store_Sku_list(string StoreCompId)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_store_Sku_list(StoreCompId);
        }




        [POST("category/admin/insert_catagory/")]
        public string insert_catagory(category cat)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.insert_catagory(cat);
        }
        [POST("category/admin/insert_brand/")]
        public string insert_brand(brand brnd)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.insert_brand(brnd);
        }
        [POST("category/admin/update_brand/")]
        public string update_brand(brand brnd)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.update_brand(brnd);
        }

        [POST("/category/update_order/")]
        public string update_order(ordadmin o)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.update_order(o);

        }




        [POST("/category/update_order/invoice")]
        public string update_order_invoice(ordadmin o)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.update_order_invoice(o);
        }
        [POST("/category/update_order_for_spldisc/")]
        public string give_spl_discount(spl_discount spd)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.give_spl_discount(spd);

        }

      

        [POST("category/admin/insert_product/")]
        public string insert_product(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.insert_product(prod);
        }


        [POST("category/admin/insert_store_config/")]
        public string insert_store_config(store_config sc)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.insert_store_config(sc);
        }



        [POST("/category/admin/Prod_List_App/")]
        public List<product> ProdListApp(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.ProdListApp(prod);
        }


        [POST("/category/admin/Prod_For_Approval/")]
        public string ProdForApproval(prodapp prodapp)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.ProdForApproval(prodapp);
        }


        [POST("category/admin/deal_setup/")]
        public string deal_setup(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.deal_setup(prod);
        }


        [POST("category/admin/insert_child_product/")]
        public string insert_child_product(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.insert_child_product(prod);
        }

        [POST("category/admin/update_product/")]
        public string update_product(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.update_product(prod);
        }

        [POST("category/admin/update_prod_price_list/")]
        public string update_prod_price_list(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.update_prod_price_list(prod);
        }

        [POST("category/admin/update_prod_stock_list/")]
        public string update_prod_stock_list(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.update_prod_stock_list(prod);
        }

        

        [POST("category/admin/update_prod_cat/")]
        public string update_prod_cat_info(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.update_prod_cat_info(prod);
        }

        [POST("category/admin/update_cat_filter/")]
        public string update_cat_filter(category cate)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.update_cat_filter(cate);
        }

        [POST("category/admin/upload_cat_imgvid/")]
        public string upload_cat_imgvid(category cate)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.upload_cat_imgvid(cate);
        }
        [POST("category/admin/update_cat_imgvid/")]
        public string update_cat_imgvid(category cate)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.update_cat_imgvid(cate);
        }

        [POST("category/admin/update_prod_imgvid/")]
        public string update_prod_imgvid(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.update_prod_imgvid(prod);
        }


        [POST("category/admin/upload_prod_imgvid/")]
        public string upload_prod_imgvid(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.upload_prod_imgvid(prod);
        }


        [POST("category/admin/del_cat_filter/")]
        public string dele_cat_filter(category cate)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.del_cat_filter(cate);
        }

        [POST("product/del_product_feature/")]
        public string dele_product_feature(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.dele_product_feature(prod);
        }

        [POST("product/del_product_stock/")]
        public string del_product_stock(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.dele_product_stock(prod);
        }


        [GET("category/product_details/{prod_id}")]
        public List<product> get_product_details(string prod_id)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_product_details(prod_id);
        }


        [GET("category/chkChildProdExist/{prod_id}")]
        public bool get_ChildProdExistResult(string prod_id)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_ChildProdExistResult(prod_id);
        }


        [GET("category/child_product_details/{prod_id}")]
        public List<product> get_child_product_details(string prod_id)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_child_product_details(prod_id);
        }


        [GET("category/product_features/{prod_id}")]
        public List<product> get_product_features(string prod_id)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_product_features(prod_id);
        }
        [GET("category/admin/Deal_History/")]
        public List<deals> get_deal_history(deals dl)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_deal_history(dl);
        }


        [POST("category/insert_filter/")]
        public string insert_filter(copyfilter catecopyfilter)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.insert_filter(catecopyfilter);
        }


        [POST("category/admin/update_catagory/")]
        public string update_catagory(category cat)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.update_catagory(cat);
        }


        [POST("category/cat_file_Upload/")]
        public string cat_file_Upload(string file)
        {
            return "hello";
        }

        [GET("category/get_child_product/{parent_prod}")]
        public List<product> get_child_product(string parent_prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_child_product(parent_prod);
        }

        [POST("category/del_child_product/")]
        public string del_child_product(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.del_child_product(prod);
        }

        [POST("category/upd_child_product/")]
        public string upd_child_product(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.upd_child_product(prod);
        }

        [POST("category/del_deal/")]
        public string del_deal(deals dl)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.del_deal(dl);
        }


        [POST("category/admin/update_prod_features/")]
        public string update_prod_features(product prod)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.update_prod_features(prod);
        }


        [POST("category/admin/admin_login/")]
        public login_msg admin_login(user_security us)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.admin_login(us);
        }

        [GET("/category/admin/Order_History?OrderStore={OrderStore}&OrderType={OrderType}&OrderFrmDt={OrderFrmDt}&OrderToDt={OrderToDt}")]
        public List<ordadmin> getOrderHistory(string OrderStore, int OrderType, string OrderFrmDt, string OrderToDt)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.getOrderHistory(OrderStore, OrderType, OrderFrmDt, OrderToDt);
        }

        [GET("/category/admin/getOrder_Export_To_Excel?OrderStore={OrderStore}&OrderType={OrderType}&OrderFrmDt={OrderFrmDt}&OrderToDt={OrderToDt}")]
        public string getOrder_Export_To_Excel(string OrderStore, int OrderType, string OrderFrmDt, string OrderToDt)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.Order_Export_To_Excel(OrderStore, OrderType, OrderFrmDt, OrderToDt);
        }

        [GET("/category/admin/getOrder_Export_To_Excel_invoice_raised?OrderStore={OrderStore}&OrderType={OrderType}&OrderFrmDt={OrderFrmDt}&OrderToDt={OrderToDt}")]
        public string getOrder_Export_To_Excel_invoice_raised(string OrderStore, int OrderType, string OrderFrmDt, string OrderToDt)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.Order_Export_To_Excel_invoice_raised(OrderStore, OrderType, OrderFrmDt, OrderToDt);
        }


        [GET("/category/admin/getShippedOrder_Export_To_Excel?OrderStore={OrderStore}&OrderType={OrderType}")]
        public string getShippedOrder_Export_To_Excel(string OrderStore, int OrderType)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.Order_Shipped_Export_To_Excel(OrderStore, OrderType);
        }


        [GET("/category/admin/getOrderTrackRecord?OrderStore={OrderStore}&OrderType={OrderType}")]
        public List<ordadmin> getOrderTrackRecord(string OrderStore, int OrderType)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.getOrderTrackRecord(OrderStore, OrderType);
        }



        [GET("/category/admin/getSendTrackReport?OrderStore={OrderStore}&OrderType={OrderType}")]
        public string getSendTrackReport(string OrderStore, int OrderType)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.getSendTrackReport(OrderStore, OrderType);
        }


        [GET("/category/admin/getWillShipTrackReport?OrderStore={OrderStore}&OrderType={OrderType}")]
        public List<ordadmin> getWillShipTrackReport(string OrderStore, int OrderType)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.getWillShipTrackReport(OrderStore, OrderType);
        }

        [GET("/category/admin/getWillShipReport?OrderStore={OrderStore}&OrderType={OrderType}")]
        public string getWillShipReport(string OrderStore, int OrderType)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.getWillShipReport(OrderStore, OrderType);
        }

        [GET("/category/admin/getWillShipReportAfterMaxShip?OrderStore={OrderStore}&OrderType={OrderType}")]
        public string getWillShipReportAfterMaxShip(string OrderStore, int OrderType)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.getWillShipReportAfterMaxShip(OrderStore, OrderType);
        }


        [POST("api/category/admin/postCustPointFile/")]
        public List<OutCollection> postCustPointFile()
        {
            List<OutCollection> oc = new List<OutCollection>();

            //HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;

            if (httpRequest.Files.Count > 0)
            {
                var docfiles = new List<string>();
                foreach (string file in httpRequest.Files)
                {

                    var postedFile = httpRequest.Files[file];
                    var UploadDataType = httpRequest.Form["UploadDataType"];

                    StreamReader csvreader = new StreamReader(httpRequest.Files[file].InputStream);

                    while (!csvreader.EndOfStream)
                    {

                        string line;
                        string[] values = new string[10];
                        try
                        {
                            line = csvreader.ReadLine();
                            values = line.Split(';');

                            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
                            oc = cr.Upload_Cust_points(values[0].ToString(), values[1].ToString());
                        }
                        catch
                        {
                            string strFileName = "";
                            strFileName = System.Configuration.ConfigurationSettings.AppSettings["ErrorCSV"].ToString() + "\\" + Guid.NewGuid() + ".csv";
                            System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);
                            sw.WriteLine("Email Ed,Error Message");
                            string strLine = "";
                            strLine = values[0].ToString() + "," + values[1].ToString();
                            sw.WriteLine(strLine);
                            sw.Close();

                        }
                    }
                }

            }
            else
            {

            }
            //return oc;
            return oc;
        }


        [POST("api/category/admin/postMigrateStoreFile/")]
        public List<OutCollection> postMigrateStoreFile()
        {
            List<OutCollection> oc = new List<OutCollection>();

            //HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;

            if (httpRequest.Files.Count > 0)
            {
                var docfiles = new List<string>();
                foreach (string file in httpRequest.Files)
                {

                    var postedFile = httpRequest.Files[file];
                    var UploadDataType = httpRequest.Form["UploadDataType"];

                    StreamReader csvreader = new StreamReader(httpRequest.Files[file].InputStream);
                    int i = 0;
                    while (!csvreader.EndOfStream)
                    {
                        i = i + 1;
                        if (i > 1)
                        {
                            try
                            {

                                var line = csvreader.ReadLine();
                                var values = line.Split(';');

                                ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
                                oc = cr.Upload_Cust_Data(values[0].ToString().Trim(), values[1].ToString().Trim(), values[2].ToString().Trim(), values[3].ToString().Trim(), values[4].ToString().Trim(), values[5].ToString().Trim(), values[6].ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                ;

                            }
                        }
                    }
                }

            }
            else
            {

            }
            //return oc;
            return oc;
        }


        [GET("/category/getUser_For_Rights/{company}")]
        public List<user_security> getUser_For_Rights(string company)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.getUser_For_Rights(company);
        }

        [GET("/category/getUserRights?USER_ID={USER_ID}&FLAG={FLAG}")]
        public List<user_rigths> getUserRights(long USER_ID, long FLAG)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.getUserRights(USER_ID, FLAG);
        }


        [POST("category/admin/postAssignRights/")]
        public List<OutCollection> postAssignRights(user_assign_rigths uar)
        {
            ecomm.model.repository.checkoutrepository cr = new model.repository.checkoutrepository();
            return cr.postAssignRights(uar);
        }

        [POST("category/admin/postDeleteAssignRights/")]
        public List<OutCollection> postDeleteAssignRights(user_assign_rigths uar)
        {
            ecomm.model.repository.checkoutrepository cr = new model.repository.checkoutrepository();
            return cr.postDeleteAssignRights(uar);
        }


        [GET("/admin/getuser_assign_rights?USER_NAME={USER_NAME}&Comp_ID={Comp_ID}")]
        public List<user_rigths> getuser_assign_rights(string USER_NAME, long Comp_ID)
        {
            ecomm.model.repository.checkoutrepository cr = new model.repository.checkoutrepository();
            return cr.getuser_assign_rights(USER_NAME, Comp_ID);
        }

        [GET("/category/getUserIntimateData?company={company}&pagenum={pagenum}")]
        public List<registration> getUserIntimateData(string company, long pagenum)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.getUserIntimateData(company, pagenum, "");
        }

        [GET("/category/getUserDatePageWise?company={company}&pagenum={pagenum}&useremail={useremail}")]
        public List<registration> getUserDatePageWise(string company, long pagenum, string useremail)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.getUserIntimateData(company, pagenum, useremail);
        }



        [POST("category/admin/postExportCustList/")]
        public string postExportCustList(expo_cust_grid ecg)
        {
            ecomm.model.repository.checkoutrepository cr = new model.repository.checkoutrepository();
            return cr.postExportCustList(ecg);
        }



       [GET("/category/getExportDailySalesReport?company={company}&ReportFromDate={ReportFromDate}&ReportToDate={ReportToDate}")]
        public string getExportDailySalesReport(string company, string ReportFromDate, string ReportToDate)
        {
          ecomm.model.repository.checkoutrepository cr = new model.repository.checkoutrepository();
            return cr.getExportDailySalesReport(company, ReportFromDate, ReportToDate);
        }

       [GET("/category/exportprodenquiry?company={company}&name={name}&status={status}")]
       public string getExportProdEnqReport(string company, string name, string status)
       {
           ecomm.model.repository.checkoutrepository cr = new model.repository.checkoutrepository();
           return cr.getExportProdEnqReport(company, name, status);
       }

       [GET("/category/exportgrasscamp?CampaignType={CampaignType}")]
       public string getExportGrassCamp(string CampaignType)
       {
           ecomm.model.repository.checkoutrepository cr = new model.repository.checkoutrepository();
           return cr.getExportGrassCamp(CampaignType);
       }

       [GET("/category/exportnewsletter?email={email}")]
       public string getNewsletterData(string email)
       {
           ecomm.model.repository.checkoutrepository cr = new model.repository.checkoutrepository();
           return cr.getNewsletterData(email);
       }

        [GET("/category/getExportCustPointBalReport?company={company}&email_id={email_id}")]
        public string getExportCustPointBalReport(string company, string email_id)
        {
            ecomm.model.repository.checkoutrepository cr = new model.repository.checkoutrepository();
            return cr.getExportCustPointBalReport(company, email_id);
        }




        [GET("/category/getProdSearchList?prodsku={prodsku}&prodid={prodid}&prodName={prodName}&prodbrand={prodbrand}")]
        public List<product> getProdSearchList(string prodsku, string prodid, string prodName, string prodbrand)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.getProdSearchList(prodsku, prodid, prodName, prodbrand);
        }



        [GET("admin/promo_list/")]
        public List<promo> get_promo_list(string promo_id)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.get_promo_list(promo_id);
        }


        [POST("admin/save_promo/")]
        public string postsave_promo(promo op)
        {
            ecomm.model.repository.checkoutrepository cr = new model.repository.checkoutrepository();
            return cr.save_promo(op);
        }

        [POST("admin/transfer_promo/")]
        public string posttransfer_promo(promo op)
        {
            ecomm.model.repository.checkoutrepository cr = new model.repository.checkoutrepository();
            return cr.transfer_promo(op);
        }


        [GET("/category/getwarehouse")]
        public List<warehouse> getwarehouse()
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.get_warehouse();
        }


          [POST("/category/warehouse/insert")]
        public string insert_warehouse(warehouse wire)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.insert_warehouse(wire);
        }

        [POST("/category/warehouse/update")]
        public string update_warehouse(warehouse wire)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.update_warehouse(wire);
        }

        [GET("/get/warehouse/details/{warehouse_name}")]
        public List<warehouse> getwarehouse_details(string warehouse_name)
        {
            ecomm.model.repository.category_repository cr = new model.repository.category_repository();
            return cr.getwarehouse_details(warehouse_name);
        }

        [POST("/category/populateprodenquiry")]
        public List<product_enquire> populateprodenquiry(product_enquire prod)
        {
            ecomm.model.repository.category_repository ar = new model.repository.category_repository();
            return ar.search_prod_enquire_data(prod);
        }

        [POST("/category/populategenenquiry")]
        public List<enquire> populategenenquiry(enquire enqury)
        {
            ecomm.model.repository.category_repository ar = new model.repository.category_repository();
            return ar.search_gen_enquire_data(enqury);
        }


    }
}
