﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using ecomm.util.entities;
using System.Web;
using System.IO;

namespace ecomm.api.Controllers
{
    public class sales_trackerController : ApiController
    {
        [GET("tracker/emp_tree/{emp_id}")]
        public airtel_emp_tree get_airtel_emp_tree(string emp_id )
        {
            ecomm.model.repository.sales_tracker_repository s = new model.repository.sales_tracker_repository();
            return s.get_airtel_emp_tree(emp_id);
        }
        [GET("tracker/monthly_tracker/{emp_id}")]
        public List<airtel_monthly_tracker> get_airtel_monthly_tracker(string emp_id)
        {
            ecomm.model.repository.sales_tracker_repository s = new model.repository.sales_tracker_repository();
            return s.get_airtel_monthly_sales_tracker(emp_id);
        }

        [GET("tracker/sales_achievement_data?company={company}&emp={emp_id}")]
        public List<sales_achievement_data> get_sales_achievement_data(string company, string emp_id)
        {
            ecomm.model.repository.sales_tracker_repository s = new model.repository.sales_tracker_repository();
            return s.get_sales_achievement_data(company, emp_id);
        }

        [GET("tracker/work_info?emp={emp_id}")]
        public work_profile get_work_profile(string emp_id)
        {
            ecomm.model.repository.sales_tracker_repository s = new model.repository.sales_tracker_repository();
            return s.get_work_profle(emp_id);
        }

        [GET("tracker/m_profile?emp={emp_id}")]
        public my_profile get_my_profile(string emp_id)
        {
            ecomm.model.repository.sales_tracker_repository s = new model.repository.sales_tracker_repository();
            return s.get_my_profle(emp_id);
        }
    }
}
