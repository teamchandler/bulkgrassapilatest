﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class work_profile
    {
        public string fullname { get; set; }
        public string branch_name { get; set; }
        public string branch_address { get; set; }
        public string manager_name { get; set; }

        public string emp_id { get; set; }
        public string Bu { get; set; }
        public string Outlet_code { get; set; }
        public string Outlet_Name { get; set; }
        public string Fos_Dse_Code { get; set; }
        public string Tm_Name { get; set; }
        public string Tm_code { get; set; }
        public string city { get; set; }
        public string District { get; set; }

    }
}
