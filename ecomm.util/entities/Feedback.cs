﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ecomm.util.entities
{
    public class Feedback
    {
        public long feedback_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email_id { get; set; }
        public string feedback_choise { get; set; }
        public string feedback_comment { get; set; }
        public string company { get; set; }

    }


    public class enquiry_detail
    {
        public string id { get; set; }
        public string name { get; set; }
        public string size { get; set; }
        public string sku { get; set; }
        public int block_number { get; set; }
        public int ord_qty { get; set; }
        public string hashKey { get; set; }
    }

    public class enquiry_info
    {
        public long id { get; set; }
        public long quantity { get; set; }
        public string logo_needed { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string city { get; set; }
        public string company_name { get; set; }
        public string website { get; set; }
        public DateTime create_ts { get; set; }
        public string enquire_from { get; set; }
        public string size_quantity { get; set; }
        public string created_by { get; set; }
        public long status { get; set; }
        public string enquire_status { get; set; }

    }

    public class general_enquiry_info
    {
        public long id { get; set; }
        public string contact_as { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string operating_city { get; set; }
        public string company_name { get; set; }
        public string message { get; set; }
        public string website { get; set; }
        public DateTime create_ts { get; set; }
        public string general_information { get; set; }
        public string product_code { get; set; }
        public string additional_remarks { get; set; }
        public string enquire_from { get; set; }

        public string created_by { get; set; }
        public long status { get; set; }
        public string enquire_status { get; set; }

    }
}