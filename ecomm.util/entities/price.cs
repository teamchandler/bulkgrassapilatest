﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class price
    {
        public double min_qty { get; set; }
        public double max_qty { get; set; }
        public double mrp { get; set; }
        public double list { get; set; }
        public double discount { get; set; }
        public double min { get; set; }
        public double final_offer { get; set; }
        public double final_discount { get; set; }
        public double ka_shipping { get; set; }
        public double other_shipping { get; set; }

    }
}
