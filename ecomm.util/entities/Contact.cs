﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ecomm.util.entities
{
    public class Contact
    {

        public long contact_us_id { get; set; }
        public string name { get; set; }
        public string email_id { get; set; }
        public string telephone { get; set; }
        public string comments { get; set; }
        public string company { get; set; }


    }

    public class GrassCamp
    {


        public long id { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
        public string landline { get; set; }
        public string city { get; set; }
        public string company_name { get; set; }
        public string company_email { get; set; }
        public string designation { get; set; }
        public string address { get; set; }
        public string pincode { get; set; }
        public string product_name { get; set; }
        public string product_id { get; set; }
        public string product_qty { get; set; }
        public string comments { get; set; }
        public DateTime created_ts { get; set; }


    }

    public class Newsletter
    {


        public long id { get; set; }
        public string name { get; set; }
        public string email_id { get; set; }
        public string telephone { get; set; }
        public string company { get; set; }
        public string designation { get; set; }
       
        public DateTime created_ts { get; set; }


    }
}
