﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class size
    {
        public string id { get; set; }
        public string size_name { get; set; }
        public string sku { get; set; }
        public int stock { get; set; }
    }
}
