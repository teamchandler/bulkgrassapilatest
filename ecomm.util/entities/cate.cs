﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ecomm.util.entities
{
    public class cate
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string id { get; set; }
        public string Name { get; set; }
        public string displayname { get; set; }
        public string ParentId { get; set; }
        public string description { get; set; }
        public int active { get; set; }
        public List<filter> filters { get; set; }
        public string special { get; set; }
        public List<storedate> store_defn { get; set; }
        public string Shipping { get; set; }
        public int min_ship { get; set; }
        public int max_ship { get; set; }
    }
}
