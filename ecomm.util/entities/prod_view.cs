﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ecomm.util.entities
{
    public class prod_view
    {
        public string _id { get; set; }
        public string user_id { get; set; }
        //public string id { get; set; }
        public string sku { get; set; }
        public string brand { get; set; }
        public string Name { get; set; }
        public price price { get; set; }
        public string Express { get; set; }
        public DateTime view_ts { get; set; }
    }
}
