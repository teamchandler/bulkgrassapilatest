﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class COMPANY
    {
        public long id { get; set; }
        public string name { get; set; }
        public string displayname { get; set; }
        public string contactname { get; set; }
        public string phonenumber { get; set; }
        public string address { get; set; }
        public string emailid { get; set; }
        public string logo { get; set; }
        public decimal points_ratio { get; set; }
        public decimal point_range_max { get; set; }
        public decimal point_range_min { get; set; }
        public string root_url_1 { get; set; }
        public string root_url_2 { get; set; }
        public decimal shipping_charge { get; set; }
        public decimal min_order_for_free_shipping { get; set; }


        public int sales_tracking { get; set; }
        public string track_cycle { get; set; }
        public int validity { get; set; }

    }
     public class CATEGORY
    {
     public long   CAT_ID	              { get; set; }
     public string CAT_NAME	              { get; set; }
     public string CAT_DISPLAY_NAME       { get; set; }
     public string CAT_DESCRIPTION	      { get; set; }
     public long   PARENT_CAT_ID	          { get; set; }
     public string CAT_IMG_URL            { get; set; }

    }

    

       public class PRODUCT
    {
        public long   PRODUCT_ID         { get; set; }
        public string PRODUCT_NAME       { get; set; }
        public string PRODUCT_DISP_NAME  { get; set; }
        public string PRODUCT_DESC       { get; set; }
        public decimal MRP               { get; set; }
        public decimal LIST_PRICE        { get; set; }
        public decimal MIN_SALE_PRICE    { get; set; }
        public string PRODUCT_IMG_URL    { get; set; }
    }
}
