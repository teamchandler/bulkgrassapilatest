﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
   public class parent_cart_items
    {
        public string Express { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public List<category_id> cat_id { get; set; }
        public string description { get; set; }
        public List<features> feature { get; set; }
        public string id { get; set; }
        public List<imgurl> image_urls { get; set; }
        public string parent_cat_id { get; set; }
        public string point { get; set; }
        public List<price> price { get; set; }
        public string shortdesc { get; set; }
        public string sku { get; set; }
        public string stock { get; set; }
        public object video_urls { get; set; }     
       
    }
}
