﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class user_shipping
    {
        public int id { get; set; }
        public string user_id { get; set; }
        public string shipping_name { get; set; }
        public string pincode { get; set; }
        public string address { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string mobile_number { get; set; }
        public string name { get; set; }
        public string store { get; set; }
    }

    public class bulk_shipping
    {

        public int id { get; set; }
        public string user_id { get; set; }
        public string billing_name { get; set; }
        public string address { get; set; }
        public string locality { get; set; }
        public string city { get; set; }
        public string company { get; set; }
        public string pin { get; set; }
        public string state { get; set; }
        public string mobile_no { get; set; }
        public string sales_person_name { get; set; }
        public string sales_person_mobile { get; set; }
        public string store { get; set; }
        public double shipping_amount { get; set; }
        public double cart_total { get; set; }
      
    }


}
