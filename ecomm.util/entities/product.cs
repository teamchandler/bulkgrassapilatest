﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class product : prod
    {
        public List<imgurl> image_urls { get; set; }
        public List<url> video_urls { get; set; }
        public List<adurl> ad_urls { get; set; }
        public List<features> feature { get; set; }
        public string parent_cat_id { get; set; }
        public List<category_id> cat_id { get; set; }
        public double block_number { get; set; }
       
    }

    public class mongoproduct : prod
    {
        public List<imgurl> image_urls { get; set; }
        public List<url> video_urls { get; set; }
        public List<adurl> ad_urls { get; set; }
        public List<features> feature { get; set; }
        public string parent_cat_id { get; set; }
        public List<category_id> cat_id { get; set; }
        public string block_number { get; set; }
        public List<stock_list> stock { get; set; }

    }
}
