﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
namespace ecomm.util.entities
{
    public class user_cart
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string email_id { get; set; }
        public List<cart_item> cart_items { get; set; }
        public user_cart()
        {
            List<cart_item> cart_items_list = new List<cart_item>();
            cart_items = cart_items_list;
        }
    }

    public class bulk_cart
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string email_id { get; set; }
        public string cart_items { get; set; }

    }

}
