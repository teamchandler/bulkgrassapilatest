﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class user_order
    {
        public int order_id { get; set; }
        public string order_display_id { get; set; }
        public string store { get; set; }
        public string cart_id { get; set; }
        public string user_id { get; set; }
        public int shipping_info_id { get; set; }
        public string discount_code { get; set; }
        public double total_amount { get; set; }
        public int points { get; set; }
        public decimal paid_amount { get; set; }
        public int status { get; set; }
        public string payment_info { get; set; }
        public string rejection_reason { get; set; }
        public DateTime create_ts { get; set; }
        public DateTime modify_ts { get; set; }
        public DateTime pay_sent_ts { get; set; }
        public DateTime pay_conf_ts { get; set; }
        public double shipping_amount { get; set; }
    }

    /// <summary>
    /// User Point/Vouchers
    /// </summary>
    public class user_point
    {
        public string order_id { get; set; }
        public string redeem_point { get; set; }
        public double amount { get; set; }
    }
}
