﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ecomm.util.entities
{
   
    	 public class prod
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string id { get; set; }
        public string sku { get; set; }
        public List<bulk_stock> stock { get; set; }
        public int active { get; set; }
        public string parent_prod { get; set; }
        public string brand { get; set; }
        public string point { get; set; }
        public string Name { get; set; }
        public string Name_value { get; set; }
        public string description { get; set; }
        public string shortdesc { get; set; }
        public List<price> price { get; set; }
        public string size { get; set; }
        public DateTime create_date { get; set; }
        public string Express { get; set; }
        public int have_child { get; set; }
        public int Reorder_Level { get; set; }

        public double min_order_qty { get; set; }
    }
	


    public class bulk_stock
    {
        public string warehouse { get; set; }
        public double stock { get; set; }
        public double blocked_stock { get; set; }


    }
}
