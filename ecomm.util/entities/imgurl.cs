﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class imgurl
    {
        public string display_name { get; set; }
        public string link { get; set; }
        public string thumb_link { get; set; }
        public string zoom_link { get; set; }
    }
}
