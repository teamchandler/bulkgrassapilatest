﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;
using ecomm.util.entities;
using ecomm.dal;
using System.Data;
using System.Security.Cryptography;
using ecomm.util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace ecomm.model.repository
{
    public class registration_repository
    {
       
        private decimal convertToDecimal(object o)
        {
            try
            {
                if (o != null)
                {
                    return decimal.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString().Trim();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        /// <summary>
        /// GetAccountActivationMailBody : Get the Activation Email Body
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string GetAccountActivationMailBody(string userId, string guid, string strCompany)
        {
            string mailBody = string.Empty;
            string strCompanyURL = "";
            string strdisplayname = "";
            string strLogo = "";

            try
            {
                mailBody += "<html xmlns='http://www.w3.org/1999/xhtml'>";
                mailBody += "<head>";
                mailBody += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                mailBody += "<style type='text/css'>";
                mailBody += "* {";
                mailBody += "padding:0px;";
                mailBody += "margin:0px;";
                mailBody += "}";
                mailBody += "</style>";
                mailBody += "</head>";
                mailBody += "<body>";
                mailBody += "<div style='width: 100%; position: relative; float: left'>";
                mailBody += "<div style='width:700px; line-height:20px; margin:0 auto; color:#494949; font-family:Arial, Helvetica, sans-serif; font-size: 14px;'>";
                mailBody += "<div style='float:left; height:70px; width:100%; background-color:#fdfdfd;'> ";
                mailBody += "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
                mailBody += "<tr>";
                mailBody += "<td background='[email_bg]' height='70'><a style='outline:none' target='_blank'  href='http://www.annectos.in'> <img style='border:none;' src='[email_logo]' alt='annectos' width='222' height='54' /></a></td>";
                mailBody += "</tr>";
                mailBody += "</table>";
                mailBody += "</div>";
                mailBody += "<div style='float:left;  width:100%; background-color:#ffffff;'>";
                mailBody += "<div style='width:100%; font-size:24px; color:#1c66ca; height:75px; float:left; text-align:center;'>";
                mailBody += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                mailBody += "<tr>";
                mailBody += "<td height='75' align='center' valign='middle'>Account Activation</td>";
                mailBody += "</tr>";
                mailBody += "</table>";
                mailBody += "</div>";
                mailBody += "<div style='width:100%; float:left;'>";
                mailBody += "<div style='width:620px; margin:0 auto;'>";
                mailBody += "<div style='width:100%; float:left;'>";
                mailBody += "CONFIRM BY VISITING THE LINK BELOW:<br />";
                mailBody += "<span style='font-weight:bold; color:#0000ff;'><a href='[url]' style='font-weight: bold; font-size: 14px; color: #686464;'>[url]</a></span><br />";
                mailBody += "<br />";
                mailBody += "Click the link above to give us permission to open a annectoś Account, in your name.It’s fast and easy! If you cannot click the";
                mailBody += "full URL above, please copy and paste it into your web browser. By clicking the link you confirm your identiy.<br />";
                mailBody += "<br />";
                mailBody += "If you do not want access to online, simply ignore this message.<br />";
                mailBody += "<br />";
                mailBody += "When you click the link, your subscription will be confirmed.<br />";
                mailBody += "<br /><br />";
                mailBody += "<span style='color:#2c2b2b; font-weight:bold;'>Users:</span> <br />";
                mailBody += "Note, you need to activate the link or copy and paste the link directly into your browser window.<br />";
                mailBody += "<br /> Important:<br />";
                mailBody += "If the confirmation email above does not arrive in your inbox within a few minutes, check your “Spam” or “Bulk Folder”.Our message may have been filtered there.<br />";
                mailBody += "<br />";
                mailBody += "If you do find the email there, it’s important that you tag as “Not Spam” or “Mark as Safe.” This will tell your email provider not to filter our messages from annectoś.com.<br />";
                mailBody += "<br />";
                mailBody += "<span style='color:#2c2b2b; font-weight:bold;'>Whitelist our email address</span>";
                mailBody += "<br />";
                mailBody += "Be sure to whitelist “info@annectos.in” This will ensure that our messages arrive safely in your inbox and do not get filtered out.<br />";
                mailBody += "<br />";
                mailBody += "Regards,<br />";
                mailBody += "<span style='color:#0f0f0f;'> Team GRASS </span>";
                mailBody += "</div>";
                mailBody += "<div style='width:100%; float:left; text-align:center; font-size:12px; border-top:2px  solid #b41919; margin-top:50px; padding-top:25px; padding-bottom:25px; '>";
                mailBody += "Copyright &copy; 1999-2001 Annectos.All rights reserved.<br />";
                mailBody += "<span style='color:#373737;'>http://grass.shopinbulk.in/</span>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</body>";
                mailBody += "</html>";


                string siteurl = ConfigurationSettings.AppSettings["SiteUrl"].ToString();


                DataAccess da = new DataAccess();
                DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", strCompany));
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                    strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    siteurl = strCompanyURL;
                }


               if (string.IsNullOrEmpty(guid))
                   siteurl += "index.html#/login/activation/" + userId;
               else
                   siteurl += "index.html#/login/activation/" + userId + "/" + guid;

                string email_bg = ConfigurationSettings.AppSettings["email_bg"].ToString();
                string email_logo = ConfigurationSettings.AppSettings["email_logo"].ToString();

                mailBody = mailBody.Replace("[email_bg]", email_bg);
                mailBody = mailBody.Replace("[email_logo]", strLogo);
                mailBody = mailBody.Replace("[url]", siteurl);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return mailBody;
        }

        /// <summary>
        /// Submit Registration Information
        /// </summary>
        /// <param name="reg"></param>
        /// <param name="oc"></param>
        /// <returns></returns>
        /// 

        public string GetResetPasswordMailBody(string userId, string guid, string strCompany)
        {

            string mailBody = string.Empty;

            try
            {
                mailBody += "<html xmlns='http://www.w3.org/1999/xhtml'>";
                mailBody += "<head>";
                mailBody += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                mailBody += "<style type='text/css'>";
                mailBody += "* {";
                mailBody += "padding:0px;";
                mailBody += "margin:0px;";
                mailBody += "}";
                mailBody += "</style>";
                mailBody += "</head>";
                mailBody += "<body>";
                mailBody += "<div style='width: 100%; position: relative; float: left'>";
                mailBody += "<div style='width:700px; line-height:20px; margin:0 auto; color:#494949; font-family:Arial, Helvetica, sans-serif; font-size: 14px;'>";
                mailBody += "<div style='float:left; height:70px; width:100%; background-color:#fdfdfd;'> ";
                mailBody += "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
                mailBody += "<tr>";
                mailBody += "<td background='[email_bg]' height='70'><a style='outline:none' target='_blank'  href='http://www.annectos.in'> <img style='border:none;' src='[email_logo]' alt='annectos' width='122px' height='86px' /></a></td>";
                mailBody += "</tr>";
                mailBody += "</table>";
                mailBody += "</div>";
                mailBody += "<div style='float:left;  width:100%; background-color:#ffffff;'>";
                mailBody += "<div style='width:100%; font-size:24px; color:#1c66ca; height:75px; float:left; text-align:center;'>";
                mailBody += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                mailBody += "<tr>";
                mailBody += "<td height='75' align='center' valign='middle'>Reset Password</td>";
                mailBody += "</tr>";
                mailBody += "</table>";
                mailBody += "</div>";
                mailBody += "<div style='width:100%; float:left;'>";
                mailBody += "<div style='width:620px; margin:0 auto;'>";
                mailBody += "<div style='width:100%; float:left;'>";
                mailBody += "CONFIRM BY VISITING THE LINK BELOW:<br />";
                mailBody += "<span style='font-weight:bold; color:#0000ff;'><a href='[url]' style='font-weight: bold; font-size: 14px; color: #686464;'>[url]</a></span><br />";
                mailBody += "<br />";
                mailBody += "Click the link above to reset the password of your Annectos Account, in your name.It’s fast and easy! If you cannot click the";
                mailBody += "full URL above, please copy and paste it into your web browser. By clicking the link you confirm your identiy.<br />";
                mailBody += "<br />";
                mailBody += "Be sure to whitelist “info@annectos.in” This will ensure that our messages arrive safely in your inbox and do not get filtered out.<br />";
                mailBody += "<br />";
                mailBody += "Regards,<br />";
                mailBody += "<span style='color:#0f0f0f;'>Team GRASS </span>";
                mailBody += "</div>";
                mailBody += "<div style='width:100%; float:left; text-align:center; font-size:12px; border-top:2px  solid #b41919; margin-top:50px; padding-top:25px; padding-bottom:25px; '>";
                mailBody += "Copyright &copy; 1999-2001 Annectos.All rights reserved.<br />";
                mailBody += "<span style='color:#373737;'>http://grass.shopinbulk.in/</span>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</body>";
                mailBody += "</html>";


                string siteurl = "";
                string email_logo = "";
                string strdisplayname = "";
                DataAccess da = new DataAccess();

                DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", userId));

                DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", strCompany));
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    siteurl = dtCompany.Rows[0]["root_url_1"].ToString();
                    strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                    email_logo = dtCompany.Rows[0]["logo"].ToString();
                }


               

                if (string.IsNullOrEmpty(guid))
                    siteurl += "index.html#/login/reset_password/" + userId;
                else
                    siteurl += "index.html#/login/reset_password/" + userId + "/" + guid;

                string email_bg = ConfigurationSettings.AppSettings["email_bg"].ToString();
                //email_logo = ConfigurationSettings.AppSettings["email_logo"].ToString();

                mailBody = mailBody.Replace("[email_bg]", email_bg);
                mailBody = mailBody.Replace("[email_logo]", email_logo);
                mailBody = mailBody.Replace("[url]", siteurl);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return mailBody;

        }


        public string SubmitRegistrationInfo(registration reg)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            string strComp = reg.company;
            try
            {
                guid = Guid.NewGuid().ToString();
                int i = da.ExecuteSP("bulkhome_registration_Insert", ref oc
                                , da.Parameter("_first_name", reg.first_name)
                                , da.Parameter("_last_name", reg.last_name)
                                , da.Parameter("_gender", reg.gender)
                                , da.Parameter("_email_id", reg.email_id)
                                , da.Parameter("_mobile_no", reg.mobile_no)
                                , da.Parameter("_office_no", "0796")
                                , da.Parameter("_street", reg.street)
                                , da.Parameter("_city", reg.city)
                                , da.Parameter("_state", reg.state)
                                , da.Parameter("_country", reg.country)
                                , da.Parameter("_zipcode", reg.zipcode)
                                , da.Parameter("_password", reg.password)
                                , da.Parameter("_status", 2)
                                , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_company", reg.company)
                                , da.Parameter("_guid", guid)
                                , da.Parameter("_expiry_date", System.DateTime.Today.AddHours(24).ToLocalTime())
                                , da.Parameter("_action_flag", 1)
                                , da.Parameter("_outmsg", String.Empty, true)
                                );

                if (i == 1)
                {
                    i = 0;

                    if (oc[0].strParamValue.ToString() == "Your Registration Has Been Successfully Completed")
                    {
                        //Send Email for Account Activation
                        helper_repository hr = new helper_repository();
                        string mailfrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString();
                        string mailpwd = System.Configuration.ConfigurationSettings.AppSettings["MailPwd"].ToString();
                    //    i = hr.SendMail(reg.email_id, "", "annectoś: Account Activation", GetAccountActivationMailBody(reg.email_id, guid, strComp));
                    }
                    /*
                    oc[0].strParamValue Return Value May Be:                   
                     * 1. Your Registration Has Been Successfully Completed.
                     * 2. Your Account Activation Is Pending.
                     * 3. This Email Is Already Registered!.
                     * 4. Your Account Is Suspended/Terminated.
                     * 5. Your Account Is Inactive                    
                     */
                    return oc[0].strParamValue.ToString();

                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }

        public string UpdateRegistrationInfo(registration reg)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";

            try
            {
                guid = Guid.NewGuid().ToString();
                int i = da.ExecuteSP("update_user_information", ref oc
                                , da.Parameter("_first_name", reg.first_name)
                                , da.Parameter("_last_name", reg.last_name)
                                , da.Parameter("_gender", reg.gender)
                                , da.Parameter("_email_id", reg.email_id)
                                , da.Parameter("_mobile_no", reg.mobile_no)
                                , da.Parameter("_office_no", "0796")
                                , da.Parameter("_street", reg.street)
                                , da.Parameter("_city", reg.city)
                                , da.Parameter("_address", reg.address)
                                , da.Parameter("_state", reg.state)
                                , da.Parameter("_country", reg.country)
                                , da.Parameter("_zipcode", reg.zipcode)
                                , da.Parameter("_password", reg.password)
                                , da.Parameter("_status", 1)
                                , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_company", reg.company)
                                , da.Parameter("_guid", guid)
                                , da.Parameter("_expiry_date", System.DateTime.Today.AddHours(24).ToLocalTime())
                                , da.Parameter("_action_flag", 1)
                                , da.Parameter("_outmsg", String.Empty, true)
                                );

                if (i == 1)
                {

                    return oc[0].strParamValue.ToString();

                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }

        public string ResendActCode(registration reg)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            try
            {
                guid = Guid.NewGuid().ToString();
                int i = da.ExecuteSP("user_registration_Insert", ref oc
                                , da.Parameter("_first_name", reg.first_name)
                                , da.Parameter("_last_name", reg.last_name)
                                , da.Parameter("_gender", reg.gender)
                                , da.Parameter("_email_id", reg.email_id)
                                , da.Parameter("_mobile_no", reg.mobile_no)
                                , da.Parameter("_office_no", "0796")
                                , da.Parameter("_street", reg.street)
                                , da.Parameter("_city", reg.city)
                                , da.Parameter("_state", reg.state)
                                , da.Parameter("_country", reg.country)
                                , da.Parameter("_zipcode", reg.zipcode)
                                , da.Parameter("_password", reg.password)
                                , da.Parameter("_status", 1)
                                , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_guid", guid)
                                , da.Parameter("_expiry_date", System.DateTime.Today.AddHours(24).ToLocalTime())
                                , da.Parameter("_action_flag", 2)
                                , da.Parameter("_outmsg", String.Empty, true)
                                );

                if (i == 1)
                {
                    i = 0;
                    //Send Email for Account Activation
                    helper_repository hr = new helper_repository();
                    string mailfrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString();
                    string mailpwd = System.Configuration.ConfigurationSettings.AppSettings["MailPwd"].ToString();
                    i = hr.SendMail(reg.email_id, "", "annectoś: Account Activation", GetAccountActivationMailBody(reg.email_id, guid, ""));

                    /*
                    oc[0].strParamValue Return Value May Be:                   
                     * 1. Your Registration Has Been Successfully Completed.
                     * 2. Your Account Activation Is Pending.
                     * 3. This Email Is Already Registered!.
                     * 4. Your Account Is Suspended/Terminated.
                     * 5. Your Account Is Inactive                    
                     */
                    return oc[0].strParamValue.ToString();
                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }


        }



        private string CreateRandomPassword(int PassLength)
        {
            try
            {
                string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
                Random randNum = new Random();
                char[] chars = new char[PassLength];
                int allowedCharCount = _allowedChars.Length;
                for (int i = 0; i < PassLength; i++)
                {
                    chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
                }
                string strNewRandomPass = new string(chars);

                return strNewRandomPass;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
        }


        public string CheckEmailToReset(registration reg)
        {
            string CheckResult = "";

            CheckResult = EmailToReset(reg);

            if (CheckResult == "Valid Email")
            {
                return ResetPassword(reg);
            }
            else
            {
                return CheckResult;
            }

        }

        public string EmailToReset(registration reg)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            try
            {

                //string strNewPassword = CreateRandomPassword(10);

                guid = Guid.NewGuid().ToString();

                int i = da.ExecuteSP("user_password_reset", ref oc
                                , da.Parameter("_email_id", reg.email_id)
                                , da.Parameter("_newpassword", guid)
                                , da.Parameter("_reset_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_action_flag", 1)
                                , da.Parameter("_outmsg", String.Empty, true)
                                );

                return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }
        }


        public string ResetPassword(registration reg)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            try
            {

                //string strNewPassword = CreateRandomPassword(10);

                guid = Guid.NewGuid().ToString();

                int i = da.ExecuteSP("user_password_reset", ref oc
                                , da.Parameter("_email_id", reg.email_id)
                                , da.Parameter("_newpassword", guid)
                                , da.Parameter("_reset_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_action_flag", 2)
                                , da.Parameter("_outmsg", String.Empty, true)
                                );

                if (i == 1)
                {
                    i = 0;
                    //Send Email for Account Activation
                    helper_repository hr = new helper_repository();
                    string mailfrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString();
                    string mailpwd = System.Configuration.ConfigurationSettings.AppSettings["MailPwd"].ToString();
                    i = hr.SendMail(reg.email_id, "", "Annectos: Password Reset ", GetResetPasswordMailBody(reg.email_id, guid, reg.company));

                    /*
                    oc[0].strParamValue Return Value May Be:                   
                     * 1. Your Registration Has Been Successfully Completed.
                     * 2. Your Account Activation Is Pending.
                     * 3. This Email Is Already Registered!.
                     * 4. Your Account Is Suspended/Terminated.
                     * 5. Your Account Is Inactive                    
                     */
                    return oc[0].strParamValue.ToString();
                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }


        public string ActiveRegistration(registration_active reg_ac)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            string Msg = "";

            try
            {

                int i = da.ExecuteSP("user_registration_activate", ref oc
                                , da.Parameter("_email_id", reg_ac.email_id)
                                , da.Parameter("_guid", reg_ac.guid)
                                , da.Parameter("_activation_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_outmsg", String.Empty, true)
                                );


                if (i == 1)
                {
                    Msg = oc[0].strParamValue;
                }
                else
                    Msg = "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

            return Msg;
        }


        public string checkEmailGuid(registration_active reg_ac)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string Msg = "";

            try
            {

                int i = da.ExecuteSP("user_pwd_reset_check", ref oc
                                , da.Parameter("_email_id", reg_ac.email_id)
                                , da.Parameter("_guid", reg_ac.guid)
                                , da.Parameter("_activation_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_outmsg", String.Empty, true)
                                );



                Msg = oc[0].strParamValue;

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

            return Msg;
        }

        // public string Feedback(Feedback feed)
        //{
        //    DataAccess da = new DataAccess();
        //    List<OutCollection> oc = new List<OutCollection>();

        //    try
        //    {

        //        int i = da.ExecuteSP("Save_Feedback", ref oc
        //            , da.Parameter("first_name", feed.first_name)
        //            , da.Parameter("emailid", feed.emailid)
        //            , da.Parameter("last_name", feed.last_name)
        //            , da.Parameter("Comments", feed.Comments)
        //            , da.Parameter("Attachment", feed.Attachment)
        //            , da.Parameter("Choices", feed.Choices)
        //            , da.Parameter("_outmsg", String.Empty, true)
        //            );
        //        if (i > 0)
        //        {
        //            return "Feedback Saved Successfully";
        //        }
        //    }
        //    catch (Exception ex)
        //    { return "failed"; }
        //    return "";
        //}
        public string SaveNewPassword(registration reg)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string Msg = "";

            try
            {

                int i = da.ExecuteSP("user_password_reset", ref oc
                               , da.Parameter("_email_id", reg.email_id)
                               , da.Parameter("_newpassword", reg.password)
                               , da.Parameter("_reset_date", System.DateTime.Now.ToLocalTime())
                               , da.Parameter("_action_flag", 9)
                               , da.Parameter("_outmsg", String.Empty, true)
                               );


                Msg = oc[0].strParamValue.ToString();

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

            return Msg;
        }

        public string ChangePassword(registration reg)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string Msg = "";

            try
            {

                int i = da.ExecuteSP("change_password", ref oc
                               , da.Parameter("_user_id", reg.email_id)
                               , da.Parameter("_company", reg.company)
                               , da.Parameter("_old_password", reg.password)
                               , da.Parameter("_new_password", reg.new_password)
                               , da.Parameter("_outmsg", String.Empty, true)
                               );


                Msg = oc[0].strParamValue.ToString();

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

            return Msg;
        }

        public string insert_enquire_info(enquire oe)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";

            try
            {
                guid = Guid.NewGuid().ToString();
                int i = da.ExecuteSP("enquire_insert", ref oc
                    , da.Parameter("_id", 0)
                    , da.Parameter("_contact_as", oe.contact_as)
                    , da.Parameter("_name", oe.name)
                    , da.Parameter("_email", oe.email)
                    , da.Parameter("_operating_city", oe.operating_city)
                    , da.Parameter("_company_name", oe.company_name)
                    , da.Parameter("_mobile_no", oe.mobile_no)
                    , da.Parameter("_website", oe.website)
                    , da.Parameter("_general_information", oe.general_information)
                    , da.Parameter("_product_code", oe.product_code)
                    , da.Parameter("_additional_remarks", oe.additional_remarks)
                    , da.Parameter("_enquire_from", oe.enquire_from)
                    , da.Parameter("_message", oe.message)
                    , da.Parameter("_created_by", oe.created_by)
                    , da.Parameter("_create_ts", System.DateTime.Now.ToLocalTime())

                    , da.Parameter("_outmsg", String.Empty, true)
                                );

                if (i == 1)
                {

                    string email_body = "";
                    string Thank_you_mail_body = "";
                    email_body = GetEnquireMailBody(oe, "grass");

                    product_enquire pe = new product_enquire();
                    pe.name = oe.name;
                    Thank_you_mail_body = GetThankYouMailBody(pe, "grass");

                    string email_id = "";
                    string user_id = oe.email.ToString();
                    helper_repository hr = new helper_repository();
                    email_id = ConfigurationSettings.AppSettings["MailReplyTo"].ToString();
                    string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();


                    if (email_id.Length > 0 && email_body.Length > 0)
                    {
                        hr.SendMail(email_id, bcclist, "Product Enquiry: Grass", email_body);
                        hr.SendMail(user_id, bcclist, "Product Enquiry: Grass", Thank_you_mail_body);
                    }

                    return "send email";


                    //return oc[0].strParamValue.ToString();

                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }

        public string GetEnquireMailBody(enquire oe, string strCompany)
        {
            string mailBody = string.Empty;
            string strCompanyURL = "";
            string strdisplayname = "";
            string strLogo = "";




            try
            {
                mailBody += "<html xmlns='http://www.w3.org/1999/xhtml'>";
                mailBody += "<head>";
                mailBody += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                mailBody += "<style type='text/css'>";
                mailBody += "* {";
                mailBody += "padding:0px;";
                mailBody += "margin:0px;";
                mailBody += "}";
                mailBody += "</style>";
                mailBody += "</head>";
                mailBody += "<body>";
                mailBody += "<div style='width: 100%; position: relative; float: left'>";
                mailBody += "<div style='width:700px; line-height:20px; margin:0 auto; color:#494949; font-family:Arial, Helvetica, sans-serif; font-size: 14px;'>";
                mailBody += "<div style='float:left; height:70px; width:100%; background-color:#fdfdfd;'> ";
                mailBody += "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
                mailBody += "<tr>";
                //    mailBody += "<td background='[email_bg]' height='70'><a style='outline:none' target='_blank'  href='http://www.annectos.in'> <img style='border:none;' src='[email_logo]' alt='annectos' width='222'  margin-top='20px' height='54' /></a></td>";
                mailBody += "</tr>";
                mailBody += "</table>";
                mailBody += "</div>";
                mailBody += "<div style='float:left;  width:100%; background-color:#ffffff;'>";
                mailBody += "<div style='width:100%; font-size:24px; color:#1c66ca; height:75px; float:left; text-align:center;'>";
                mailBody += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                mailBody += "<tr>";
                mailBody += "<td><img style='border:none; margin-right:-150px;' src='[email_logo]' alt='annectos' width='122px'  margin-top='20px' height='86px' /></a></td><td height='75' align='center' valign='middle'>Grass Enquiry</td>";
                mailBody += "</tr>";
                mailBody += "</table>";
                mailBody += "</div>";
                mailBody += "<div style='width:100%; float:left;'>";
                mailBody += "<div style='width:620px; margin:0 auto;'>";
                mailBody += "<div style='width:100%; float:left;'>";
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "Hi,<br />";
                mailBody += "<br />";
                mailBody += "A new enquiry has been created for the below mentioned product by the below mentioned user.<br />";
                mailBody += "<br />";
                mailBody += "<b>General Information</b>: [GeneralInfo].<br />";
                //  mailBody += "<br />";
                mailBody += "<b>Product Code</b>: [ProductCode].<br />";
                //   mailBody += "<br />";
                mailBody += "<b>Additional Remarks</b>:[AdditionalRemarks]<br />";
                //   mailBody += "<br />";               

                mailBody += "<br /><br />";
                mailBody += "<span style='color:#2c2b2b; font-weight:bold;'>User Details:</span> <br />";
                mailBody += "<br />";
                mailBody += "<b>Name</b>: [UserName].<br />";
                mailBody += "<b>Mobile</b>: [UserMobile].<br />";
                mailBody += "<b>Email</b>: [UserEmail].<br />";
                mailBody += "<b>City</b>: [UserCity].<br />";
                mailBody += "<b>Company Name</b>: [UserCompanyName].<br />";

                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "Regards,<br />";
                mailBody += "<span style='color:#0f0f0f;'>Team GRASS</span>";
                mailBody += "</div>";
                mailBody += "<div style='width:100%; float:left; text-align:center; font-size:12px; border-top:2px  solid #b41919; margin-top:50px; padding-top:25px; padding-bottom:25px; '>";
                mailBody += "Copyright &copy; 2014 Annectos.All rights reserved.<br />";
                mailBody += "<span style='color:#373737;'>http://grass.shopinbulk.in/</span>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</body>";
                mailBody += "</html>";


                string siteurl = ConfigurationSettings.AppSettings["SiteUrl"].ToString();


                DataAccess da = new DataAccess();
                DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", strCompany));
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                    strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    siteurl = strCompanyURL;
                }




                string email_bg = ConfigurationSettings.AppSettings["email_bg"].ToString();
                string email_logo = ConfigurationSettings.AppSettings["email_logo"].ToString();

                mailBody = mailBody.Replace("[email_bg]", email_bg);
                strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";
                mailBody = mailBody.Replace("[email_logo]", strLogo);


                mailBody = mailBody.Replace("[GeneralInfo]", oe.general_information);
                mailBody = mailBody.Replace("[ProductCode]", oe.product_code);
                mailBody = mailBody.Replace("[AdditionalRemarks]", oe.additional_remarks);
                //mailBody = mailBody.Replace("[qty]", pe.quantity);

                mailBody = mailBody.Replace("[UserName]", oe.name);
                mailBody = mailBody.Replace("[UserMobile]", oe.mobile_no);
                mailBody = mailBody.Replace("[UserEmail]", oe.email);
                mailBody = mailBody.Replace("[UserCity]", oe.operating_city);
                mailBody = mailBody.Replace("[UserCompanyName]", oe.company_name);


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return mailBody;
        }


        public List<enquiry_info> get_product_enquire_info(string email_id)
        {

            DataAccess da = new DataAccess();
            // string enquire_data = "";
            // string status;
            // string id;
            List<enquiry_info> lst = new List<enquiry_info>();
            DataTable dt = da.ExecuteDataTable("get_bulk_enquire_info", da.Parameter("_user_id", email_id));
            if (dt != null && dt.Rows.Count > 0)
            {
                //enquiry_info ei = new enquiry_info();

                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    ei.id = dt.Rows[i]["id"].ToString();
                //    ei.status = dt.Rows[i]["status"].ToString();
                //    ei.size_quantity = dt.Rows[i]["size_quantity"].ToString();
                //    lst.Add(ei);

                //}
            //    if (oei.size_quantity

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    enquiry_info oei = new enquiry_info();
                    oei.id = convertToLong(dt.Rows[i]["id"]);
                    oei.quantity = convertToLong(dt.Rows[i]["quantity"]);
                    oei.logo_needed = convertToString(dt.Rows[i]["logo_needed"]);
                    oei.name = convertToString(dt.Rows[i]["name"]);
                    oei.mobile = convertToString(dt.Rows[i]["mobile"]);
                    oei.email = convertToString(dt.Rows[i]["email"]);
                    oei.city = convertToString(dt.Rows[i]["city"]); ;
                    oei.company_name = convertToString(dt.Rows[i]["company_name"]); ;
                    oei.website = convertToString(dt.Rows[i]["website"]); ;
                    //oei.create_ts   =convertToString(dt.Rows[i]["create_ts"]);;
                    oei.enquire_from = convertToString(dt.Rows[i]["enquire_from"]); ;
                    oei.size_quantity = convertToString(dt.Rows[i]["size_quantity"]); ;
                    oei.created_by = convertToString(dt.Rows[i]["created_by"]); ;
                    oei.status = convertToLong(dt.Rows[i]["status"]); ;
                    oei.enquire_status = convertToString(dt.Rows[i]["enquire_status"]);
                    lst.Add(oei);
                }
              //  List<enquiry_detail> ed = JsonConvert.DeserializeObject<List<enquiry_detail>>(enquire_data);
             //   JsonConvert.DeserializeObject<List<cart_items>>(dtResult.Rows[i]["cart_data"].ToString();

                //  return ed;
            }
            return lst;


        }

        public List<general_enquiry_info> get_general_enquire_info(string email_id)
        {

            DataAccess da = new DataAccess();
            // string enquire_data = "";
            // string status;
            // string id;
            List<general_enquiry_info> lst = new List<general_enquiry_info>();
            DataTable dt = da.ExecuteDataTable("get_bulk_general_enquire_info", da.Parameter("_user_id", email_id));
            if (dt != null && dt.Rows.Count > 0)
            {
                //enquiry_info ei = new enquiry_info();

                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    ei.id = dt.Rows[i]["id"].ToString();
                //    ei.status = dt.Rows[i]["status"].ToString();
                //    ei.size_quantity = dt.Rows[i]["size_quantity"].ToString();
                //    lst.Add(ei);

                //}

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    general_enquiry_info oei = new general_enquiry_info();
                    oei.id = convertToLong(dt.Rows[i]["id"]);
                    oei.contact_as = convertToString(dt.Rows[i]["contact_as"]);
                    oei.name = convertToString(dt.Rows[i]["name"]);
                    oei.mobile = convertToString(dt.Rows[i]["mobile"]);
                    oei.email = convertToString(dt.Rows[i]["email"]);
                    oei.operating_city = convertToString(dt.Rows[i]["operating_city"]);
                    oei.company_name = convertToString(dt.Rows[i]["company_name"]);
                    oei.website = convertToString(dt.Rows[i]["website"]);
                    oei.message = convertToString(dt.Rows[i]["message"]);
                    oei.product_code = convertToString(dt.Rows[i]["product_code"]);
                    //oei.create_ts   =convertToString(dt.Rows[i]["create_ts"]);;
                    oei.enquire_from = convertToString(dt.Rows[i]["enquire_from"]);
                    oei.general_information = convertToString(dt.Rows[i]["general_information"]);
                    oei.additional_remarks = convertToString(dt.Rows[i]["additional_remarks"]);
                    oei.created_by = convertToString(dt.Rows[i]["created_by"]);
                    oei.status = convertToLong(dt.Rows[i]["status"]);
                    oei.enquire_status = convertToString(dt.Rows[i]["enquire_status"]);
                    lst.Add(oei);
                }
                // List<enquiry_detail> ed = JsonConvert.DeserializeObject<List<enquiry_detail>>(enquire_data);
                //JsonConvert.DeserializeObject<List<cart_items>>(dtResult.Rows[i]["cart_data"].ToString();

                //  return ed;
            }
            return lst;


        }

        public string insert_product_enquire_info(product_enquire pe)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            //string guid = "";

            
            try
            {
                //   guid = Guid.NewGuid().ToString();
                int i = da.ExecuteSP("bulk_prod_enquire_insert", ref oc
                                    , da.Parameter("_id", 0)
                                    , da.Parameter("_quantity", pe.quantity)
                                    , da.Parameter("_logo_needed", pe.logo)
                                    , da.Parameter("_name", pe.name)
                                    , da.Parameter("_mobile", pe.mobile_no)
                                    , da.Parameter("_email", pe.email_id)
                                    , da.Parameter("_city", pe.city)
                                    , da.Parameter("_company_name", pe.company_name)
                                    , da.Parameter("_website", pe.website)
                                    , da.Parameter("_enquire_from", pe.enquire_from)
                                    , da.Parameter("_size_quantity", pe.size_quantity)
                                     ,da.Parameter("_total_enq_qty", pe.total_enq_qty)
                                    , da.Parameter("_created_by", pe.created_by)
                                    , da.Parameter("_create_ts", System.DateTime.Now.ToLocalTime())
                                    , da.Parameter("_outmsg", String.Empty, true)
                                                );

                if (i == 1)
                {

                   
                    string Thank_you_mail_body = "";
                    Thank_you_mail_body = GetThankYouMailBody(pe, "grass");

                    string email_body = "";
                    string user_id = pe.email_id;
                    email_body = GetProdEnquireMailBody(pe, "grass");

                    string email_id = "";
                    helper_repository hr = new helper_repository();
                    email_id = ConfigurationSettings.AppSettings["MailReplyTo"].ToString();
                    string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();


                    if (email_id.Length > 0 && email_body.Length > 0)
                    {
                        hr.SendMail(email_id, bcclist, "Product Enquiry: Grass", email_body);
                        hr.SendMail(user_id, bcclist, "Product Enquiry: Grass", Thank_you_mail_body);


                        //sms_message = sms_message.Replace("##OrderNo##", o.order_id.ToString());
                        //sms_message = sms_message.Replace("##Courier##", o.shipping_info.ToString());
                        //sms_message = sms_message.Replace("##TrackNo##", o.courier_track_no.ToString());

                        //strMobileNo = rr.getUserMobileNo(email_id);
                        //if (rr.CheckMobileNo(strMobileNo, ref MobNo) != "Failure")
                        //{
                        //    string resp = hp.SendSMS(sms_user_id, sms_pwd, MobNo, sms_ssid, sms_message, "TRNS");
                        //}
                    }

                    return "send email";

                    return oc[0].strParamValue.ToString();

                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }

        public string update_product_enquire_info(product_enquire pe)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            //string guid = "";


            try
            {
                //   guid = Guid.NewGuid().ToString();
                int i = da.ExecuteSP("update_bulk_prodenquire_for_comments", ref oc
                                    , da.Parameter("_comments", pe.comments)
                                    , da.Parameter("_prodenqid", pe.id)

                                                );

            }


            //    if (i == 1)
            //    {

            //        string email_body = "";
            //        email_body = GetProdEnquireMailBody(pe, "grass");

            //        string email_id = "";
            //        helper_repository hr = new helper_repository();
            //        email_id = ConfigurationSettings.AppSettings["MailReplyTo"].ToString();
            //        string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();




            //        return oc[0].strParamValue.ToString();

            //    }
            //    else
            //        return oc[0].strParamValue.ToString();
            //}
            //catch (Exception ex)
            //{
            //    ExternalLogger.LogError(ex, this, "#");
            //    return "Failed"; ;
            //}

            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

            return "Succesfully updation";
        }

        public string update_gen_enquire_info(enquire enq)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            //string guid = "";


            try
            {
                //   guid = Guid.NewGuid().ToString();
                int i = da.ExecuteSP("update_bulk_genenquire_for_comments", ref oc
                                    , da.Parameter("_comments", enq.comment)
                                    , da.Parameter("_enqid", enq.id)

                                                );

            }


            //    if (i == 1)
            //    {

            //        string email_body = "";
            //        email_body = GetProdEnquireMailBody(pe, "grass");

            //        string email_id = "";
            //        helper_repository hr = new helper_repository();
            //        email_id = ConfigurationSettings.AppSettings["MailReplyTo"].ToString();
            //        string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();




            //        return oc[0].strParamValue.ToString();

            //    }
            //    else
            //        return oc[0].strParamValue.ToString();
            //}
            //catch (Exception ex)
            //{
            //    ExternalLogger.LogError(ex, this, "#");
            //    return "Failed"; ;
            //}

            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

            return "Succesfully updation";
        }

        public string send_mail_for_sample_tshirt(GrassCamp gc)
        {

            try
            {
                string mailBody = string.Empty;
                string email_id = gc.company_email;
                string name = gc.name;
                string strMobileNo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";
                string poweredbyLogo = "";
                string strLogo = "";
                string strCompanyUrl = "";
                string strStoreName = "";
                string cart_data = "";





                mailBody = @"
<html>
<head><title>EDM</title></head>
<body>
	
	<table cellpadding='0' cellspacing='0' border='0' width='600' bgcolor='#ffffff'>
		<tr>
		  <td align='center'>	
	
	<table cellpadding='0' cellspacing='0' border='0' width='600'>
		<tr>
		  <td align='center'>	
	
	
<table cellpadding='0' cellspacing='0' border='0' width='600'>
    <tr>
        <td align='left'>
            <a href='http://grass.shopinbulk.in/'><img src='https://s3.amazonaws.com/grass-reseller/edm/header1.jpg' width='600' height='auto' style='padding-left: 0; padding-right: 0; display: block; border: none;'></a>
        </td>
    </tr>
</table>

<table cellpadding='0' cellspacing='0' border='0' width='600' bgcolor='#ffffff'>
    <tr>
        <td align='left' colspan='5'>
        <p style='font-family: Arial, Helvetica, sans-serif; color:#333333;padding-left: 15px; padding-right: 15px;font-size: 15px; line-height: 20px; text-align: left;line-height:25px;'>

        <strong>Hi ##FIRST_NAME##,</strong><br><br>

		Thank You for connecting with us. We have a brand new premium T-Shirt.
		Just for you, as a gift.<br>

		GRASS is the brand for the brands, founded with the mission to bring awesomeness to apparel branding. To date we have branded more than 2.5 million pieces of apparel for more than 2000 Indian and Global clients.<br><br>

		Please click on the below link to select your Sample T-Shirt. Do ensure to choose the size you require as well. 
		<strong>http://www.grassapparels.in/samplecampaign/#/main/track/#EMAIL#</strong><br><br>

		 
		For any assistance<br>
		Call us: + 91 9686202046 | 9972334590<br> 
		Mail us: customerfirst@grassapparels.in<br>

		Sincerely,<br>
		Grass Program Center<br>        	
        </p>
            
        </td>
    </tr>
</table>

	<table>
	<tr>
	    <td>
	        <img src='https://s3.amazonaws.com/grass-reseller/edm/footer.jpg' width='600' border='0' height='auto' style='display: block; border: none;'>
	    </td>
	</tr>
    </table>


			</td>
		  </tr>
		</table>
	   </td>
	  </tr>
	</table>
</body>
</html> ";

                //    strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";
                //   poweredbyLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/annectostemp.png";



                //mailBody = mailBody.Replace("##PUMA_LINK##", "http://grass.shopinbulk.in");
                mailBody = mailBody.Replace("##FIRST_NAME##", name);
                mailBody = mailBody.Replace("#EMAIL#", email_id);
                //// mailBody = mailBody.Replace("##ORDER_ID##", OrderID);
                //// mailBody = mailBody.Replace("##DELAY_DAY##", DelayDays);
                //mailBody = mailBody.Replace("##POWERED_BY##", poweredbyLogo);
                //// mailBody = mailBody.Replace("##TOTAL_AMT##", convertToString(finalamt));
                //mailBody = mailBody.Replace("##FIRST_NAME##", oe.name);
                ////mailBody = mailBody.Replace("##CANCEL_REASON##", cancel_reason);
                //// mailBody = mailBody.Replace("##SHIP_VALUE##", convertToString(shipmentvalue));




                //mailBody = mailBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");

                helper_repository hr = new helper_repository();

                string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();
                hr.SendMail(email_id, bcclist, "GRASS Promo Apparels: Sample T-Shirt", mailBody);

                return "send email";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }


        }

        public string GetThankYouMailBody(product_enquire oe, string strCompany)
        {

            try
            {
                string mailBody = string.Empty;

                string strMobileNo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";
                string poweredbyLogo = "";
                string strLogo = "";
                string strCompanyUrl = "";
                string strStoreName = "";
                string cart_data = "";

                payment_repository pr = new payment_repository();

                //    DataTable dt = pr.GetDataFromOrder(Convert.ToInt64(OrderID));
                //    DataTable dtShip = pr.GetDataFromUserID(Convert.ToInt64(OrderID));

                //if (dt == null || dt.Rows.Count == 0)
                //    return "No Such order";

                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    email_id = dt.Rows[i]["user_id"].ToString();
                //    strStoreName = Convert.ToString(dt.Rows[i]["store"].ToString().Trim());
                //    break;
                //}

                //if ((dtShip == null) || (dtShip.Rows.Count == 0))
                //{
                //    return "No Shipping Information";
                //}
                //else
                //{
                //    //ShippingName = dtShip.Rows[0]["name"].ToString();
                //    //ShippingAddress = dtShip.Rows[0]["address"].ToString();
                //    //ShippingCity = dtShip.Rows[0]["city"].ToString();
                //    //ShippingState = dtShip.Rows[0]["state"].ToString();
                //    //ShippingPin = dtShip.Rows[0]["pincode"].ToString();
                //    //strMobileNo = dtShip.Rows[0]["mobile_number"].ToString();

                //    ShippingName = dtShip.Rows[0]["billing_name"].ToString();
                //    ShippingAddress = dtShip.Rows[0]["address"].ToString();
                //    ShippingCity = dtShip.Rows[0]["city"].ToString();
                //    ShippingState = dtShip.Rows[0]["state"].ToString();
                //    ShippingPin = dtShip.Rows[0]["pin"].ToString();
                //    strMobileNo = dtShip.Rows[0]["mobile_no"].ToString();

                //}

                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    email_id = dt.Rows[i]["user_id"].ToString();
                //    cart_data = dt.Rows[i]["cart_data"].ToString();


                //}

                DataTable dtCompany = pr.GetCompanyInfo(strStoreName);
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
                }

                //  update_product_stock(warehouse, order_status, JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString()));



                mailBody = @"
                        <html>
                        <body bgcolor='#8d8e90'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#8d8e90'>
                        <tr>
                        <td><table width='800' border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' align='center'>
                        <tr>
                        <td style='border-bottom:5px solid #f1613b; padding: 10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>              
                        <td width='144'><a href= '##PUMA_LINK##' target='_blank'><img src='##PUMA_LOGO##'  border='0' alt=''/></a></td>
                        <td width='393'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td height='46' align='right' valign='middle'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='67%' align='left'><font style='font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#68696a; font-size:18px; text-transform:uppercase'></td>                         
                        <td width='4%'>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr>                  
                        </table></td>
                        </tr>
                        </table></td>
                        </tr>
                        <tr>
                        <td align='center'>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='5%'>&nbsp;</td>
                        <td width='90%' align='left' valign='top'><font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:24px'><strong><em>Hi ##FIRST_NAME##,</em></strong></font><br /><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>               
                        Thank you for your query. Our customer support team shall get in touch with you within the next 24 business hours.</font><br /><br /> 
                                                             
						 <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>               
                        If you have any questions, please feel free to contact us at customerfirst@grassapparels.in or by phone at +91 9686202046 /9972334590 (9:30 am - 6:30 pm, Mon – Fri)";


                mailBody += @"<br /><br />
                                                
                        </font></td>             
                        </tr>             
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>     
                        <td style='padding: 10px;' align='left' valign='top'>
                        <font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'>
                        <strong>Regards</strong> ,<br/>
                        <strong>Team GRASS</strong>            
                        </font>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>               
                        <tr>
                        <td align='center'><font style='font-family:Helvetica, Arial, sans-serif; color:#231f20; font-size:12px'>Copyright &copy; 2014 Annectos.All rights reserved.</font></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align='center'>
                        <font style=' font-family:Helvetica, Arial, sans-serif; color:#f1613b; font-size:12px; text-transform:uppercase'><a href='http://grass.shopinbulk.in' style='color:#f1613b; text-decoration:none'target='_blank' >http://grass.shopinbulk.in</a></font>
                        </td>
                        </tr>
                        <tr>
                        <td style='padding: 10px;padding-left:600px' align='center'><a href= '####' target='_blank'><img src='http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/annectostemp.png'  border='0' alt=''/></a></td>                        
                        </tr>
                        </table></td>
                        </tr> 
                        </table>
                        </body>
                        </html> ";

                strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";
                poweredbyLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/annectostemp.png";



                mailBody = mailBody.Replace("##PUMA_LINK##", "http://grass.shopinbulk.in");
                mailBody = mailBody.Replace("##PUMA_LOGO##", strLogo);
                // mailBody = mailBody.Replace("##ORDER_ID##", OrderID);
                // mailBody = mailBody.Replace("##DELAY_DAY##", DelayDays);
                mailBody = mailBody.Replace("##POWERED_BY##", poweredbyLogo);
                // mailBody = mailBody.Replace("##TOTAL_AMT##", convertToString(finalamt));
                mailBody = mailBody.Replace("##FIRST_NAME##", oe.name); 
                //mailBody = mailBody.Replace("##CANCEL_REASON##", cancel_reason);
                // mailBody = mailBody.Replace("##SHIP_VALUE##", convertToString(shipmentvalue));




                mailBody = mailBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");

                return mailBody;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }


        }

        private string ConvertStringArrayToString(List<string> lst)
        {
            //
            // Concatenate all the elements into a StringBuilder.
            //
            StringBuilder builder = new StringBuilder();
            foreach (string value in lst)
            {
                builder.Append(value);
                builder.Append(' ');
            }
            return builder.ToString();
        }

        private string ConvertIntArrayToString(List<string> lst)
        {
            //
            // Concatenate all the elements into a StringBuilder.
            //
            StringBuilder builder = new StringBuilder();
            foreach (string value in lst)
            {
                builder.Append(value);
                builder.Append(' ');
            }
            return builder.ToString();
        }


       //public string GetProdEnquireMailBody(product_enquire pe, string strCompany)
       //{
       //    string mailBody = string.Empty;
       //    string strCompanyURL = "";
       //    string strdisplayname = "";
       //    string strLogo = "";
       //    string prod_size = "";
       //    string size = "";
       //    string p_size = "";
       //    string p_qnty;
       //    string pr_size;
       //    string pr_qnty;
       //    List<string> lst_size = new List<string>();
       //    List<string> lst_qnty = new List<string>();


       //    //int[] product_qnty = null ;
       //    //for (int i = 0; i < 100; i++)
       //    //{
       //    //   Array.Resize(ref product_qnty, product_qnty.Length +1);
       //    //}

       //    prod_size = pe.size_quantity.ToString();

       //    List<object> sizes = JsonConvert.DeserializeObject<List<object>>(prod_size.ToString());


       //    for (int i = 0; i < sizes.Count; i++)
       //    {
       //        size = sizes[i].ToString();

       //        List<product_details> lst = JsonConvert.DeserializeObject<List<product_details>>(prod_size.ToString());

       //        if (lst[i].ord_qty != "" && lst[i].ord_qty != null)
       //        {
       //            p_size = lst[i].size;
       //            p_qnty = lst[i].ord_qty;
       //            lst_size.Add(p_size);
       //            lst_qnty.Add(p_qnty);
       //        }

       //    }

       //    pr_size = ConvertStringArrayToString(lst_size);
       //    pr_qnty = ConvertIntArrayToString(lst_qnty);

       //    try
       //    {
       //        mailBody += "<html xmlns='http://www.w3.org/1999/xhtml'>";
       //        mailBody += "<head>";
       //        mailBody += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
       //        mailBody += "<style type='text/css'>";
       //        mailBody += "* {";
       //        mailBody += "padding:0px;";
       //        mailBody += "margin:0px;";
       //        mailBody += "}";
       //        mailBody += "</style>";
       //        mailBody += "</head>";
       //        mailBody += "<body>";
       //        mailBody += "<div style='width: 100%; position: relative; float: left'>";
       //        mailBody += "<div style='width:700px; line-height:20px; margin:0 auto; color:#494949; font-family:Arial, Helvetica, sans-serif; font-size: 14px;'>";
       //        mailBody += "<div style='float:left; height:70px; width:100%; background-color:#fdfdfd;'> ";
       //        mailBody += "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
       //        mailBody += "<tr>";
       //        //    mailBody += "<td background='[email_bg]' height='70'><a style='outline:none' target='_blank'  href='http://www.annectos.in'> <img style='border:none;' src='[email_logo]' alt='annectos' width='222'  margin-top='20px' height='54' /></a></td>";
       //        mailBody += "</tr>";
       //        mailBody += "</table>";
       //        mailBody += "</div>";
       //        mailBody += "<div style='float:left;  width:100%; background-color:#ffffff;'>";
       //        mailBody += "<div style='width:100%; font-size:24px; color:#1c66ca; height:75px; float:left; text-align:center;'>";
       //        mailBody += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
       //        mailBody += "<tr>";
       //        mailBody += "<td><img style='border:none; margin-right:-150px;' src='[email_logo]' alt='annectos' width='122px'  margin-top='20px' height='86px' /></a></td><td height='75' align='left' valign='middle'>Product Enquiry</td>";
       //        mailBody += "</tr>";
       //        mailBody += "</table>";
       //        mailBody += "</div>";
       //        mailBody += "<div style='width:100%; float:left;'>";
       //        mailBody += "<div style='width:620px; margin:0 auto;'>";
       //        mailBody += "<div style='width:100%; float:left;'>";

       //        mailBody += "Hi,<br />";
       //        mailBody += "A new enquiry has been created for the below mentioned product by the below mentioned user.<br />";
       //        mailBody += "<br />";
       //        mailBody += "<b>Product Name</b>: [ProdName].<br />";
       //        //  mailBody += "<br />";
       //        mailBody += "<b>Product ID</b>: [ProdId].<br />";
       //        //   mailBody += "<br />";
       //        mailBody += "<b>Size</b>: [size]<br />";
       //        //   mailBody += "<br />";
       //        mailBody += "<b>Quantity</b>:[qty]<br />";
       //        if (pe.logo == "Y")
       //        {
       //            mailBody += "<b>Logo Needed</b>: Yes.<br />";

       //        }
       //        else
       //        {

       //            mailBody += "<b>Logo Needed</b>: No.<br />";
       //        }

       //        mailBody += "<br /><br />";
       //        mailBody += "<span style='color:#2c2b2b; font-weight:bold;'>User</span> <br />";
       //        mailBody += "<b>Name</b>: [UserName].<br />";
       //        mailBody += "<b>Mobile</b>: [UserMobile].<br />";
       //        mailBody += "<b>Email</b>: [UserEmail].<br />";
       //        mailBody += "<b>City</b>: [UserCity].<br />";
       //        mailBody += "<b>Company Name</b>: [UserCompanyName].<br />";
       //        mailBody += "<b>Website</b>: [UserWebsite].<br />";
       //        mailBody += "<br />";
       //        mailBody += "<br />";
       //        mailBody += "Regards,<br />";
       //        mailBody += "<span style='color:#0f0f0f;'>Team annectoś</span>";
       //        mailBody += "</div>";
       //        mailBody += "<div style='width:100%; float:left; text-align:center; font-size:12px; border-top:2px  solid #b41919; margin-top:50px; padding-top:25px; padding-bottom:25px; '>";
       //        mailBody += "Copyright &copy; 2014 Annectos.All rights reserved.<br />";
       //        mailBody += "<span style='color:#373737;'>http://grass.shopinbulk.in/</span>";
       //        mailBody += "</div>";
       //        mailBody += "</div>";
       //        mailBody += "</div>";
       //        mailBody += "</div>";
       //        mailBody += "</div>";
       //        mailBody += "</div>";
       //        mailBody += "</body>";
       //        mailBody += "</html>";


       //        string siteurl = ConfigurationSettings.AppSettings["SiteUrl"].ToString();


       //        DataAccess da = new DataAccess();
       //        DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", strCompany));
       //        if (dtCompany != null && dtCompany.Rows.Count > 0)
       //        {
       //            strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
       //            strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
       //            strLogo = dtCompany.Rows[0]["logo"].ToString();
       //            siteurl = strCompanyURL;
       //        }




       //        string email_bg = ConfigurationSettings.AppSettings["email_bg"].ToString();
       //        string email_logo = ConfigurationSettings.AppSettings["email_logo"].ToString();

       //        mailBody = mailBody.Replace("[email_bg]", email_bg);
       //        strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";
       //        mailBody = mailBody.Replace("[email_logo]", strLogo);


       //        mailBody = mailBody.Replace("[ProdName]", pe.prod_name);
       //        mailBody = mailBody.Replace("[ProdId]", pe.prod_id);
       //        //mailBody = mailBody.Replace("[qty]", pe.quantity);
       //        mailBody = mailBody.Replace("[size]", pr_size);
       //        mailBody = mailBody.Replace("[qty]", pr_qnty);
       //        mailBody = mailBody.Replace("[UserName]", pe.name);
       //        mailBody = mailBody.Replace("[UserMobile]", pe.mobile_no);
       //        mailBody = mailBody.Replace("[UserEmail]", pe.email_id);
       //        mailBody = mailBody.Replace("[UserCity]", pe.city);
       //        mailBody = mailBody.Replace("[UserCompanyName]", pe.company_name);
       //        mailBody = mailBody.Replace("[UserWebsite]", pe.website);

       //    }
       //    catch (Exception ex)
       //    {
       //        ExternalLogger.LogError(ex, this, "#");
       //    }
       //    return mailBody;
       //}


        //public registration UserLogin(user_login user_log)
        //{
        //DataAccess da = new DataAccess();
        //List<OutCollection> oc = new List<OutCollection>();

        //string Msg = "";
        //string token_id = "";
        //login_msg return_msg = new login_msg();

        //try
        //{
        //    token_id = Guid.NewGuid().ToString();
        //    int i = da.ExecuteSP("user_login", ref oc
        //                    , da.Parameter("_email_id", user_log.email_id)
        //                    , da.Parameter("_password", user_log.password)
        //                    , da.Parameter("_login_date", System.DateTime.Now.ToLocalTime())
        //                    , da.Parameter("_token_id", token_id)
        //                    , da.Parameter("_status", 1)
        //                    , da.Parameter("_outmsg", String.Empty, true)
        //                    );


        //    if (i == 1)
        //    {
        //        Msg = token_id;
        //    }
        //    else
        //    {
        //        Msg = oc[0].strParamValue;
        //    }
        //    return_msg.type = "success";
        //    return_msg.value = Msg;

        //}
        //catch (Exception ex)
        //{
        //    return_msg.type = "system_error";
        //    return_msg.value = ex.Message;
        //}

        ////return Msg;
        //return return_msg;
        //}

        public string GetProdEnquireMailBody(product_enquire pe, string strCompany)
        {
            string mailBody = string.Empty;
            string strCompanyURL = "";
            string strdisplayname = "";
            string strLogo = "";
            string logo = "";
            string prod_size = "";
            string size = "";
            string p_size = "";
            string p_qnty;
            string pr_size;
            string pr_qnty;
            string poweredbyLogo = "";
            List<string> lst_size = new List<string>();
            List<string> lst_qnty = new List<string>();


            //int[] product_qnty = null ;
            //for (int i = 0; i < 100; i++)
            //{
            //   Array.Resize(ref product_qnty, product_qnty.Length +1);
            //}

            prod_size = pe.size_quantity.ToString();

            List<object> sizes = JsonConvert.DeserializeObject<List<object>>(prod_size.ToString());


            for (int i = 0; i < sizes.Count; i++)
            {
                size = sizes[i].ToString();

              //  List<product_enquire_with_product_details> lst = JsonConvert.DeserializeObject<List<product_enquire_with_product_details>>(prod_size.ToString());
                List<product_details> lst = JsonConvert.DeserializeObject<List<product_details>>(prod_size.ToString());

                if (lst[i].ord_qty != "" && lst[i].ord_qty != null)
                {
                    p_size = lst[i].size;
                    p_qnty = lst[i].ord_qty;
                    lst_size.Add(p_size);
                    lst_qnty.Add(p_qnty);
                }

            }

            pr_size = ConvertStringArrayToString(lst_size);
            pr_qnty = ConvertIntArrayToString(lst_qnty);

            string quantity = pr_qnty.Substring(0, pr_qnty.Length - 1);
            string product_size = pr_size.Substring(0, pr_size.Length - 1);

            try
            {
                //mailBody += "<html xmlns='http://www.w3.org/1999/xhtml'>";
                //mailBody += "<head>";
                //mailBody += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                //mailBody += "<style type='text/css'>";
                //mailBody += "* {";
                //mailBody += "padding:0px;";
                //mailBody += "margin:0px;";
                //mailBody += "}";
                //mailBody += "</style>";
                //mailBody += "</head>";
                //mailBody += "<body>";
                //mailBody += "<div style='width: 100%; position: relative; float: left'>";
                //mailBody += "<div style='width:700px; line-height:20px; margin:0 auto; color:#494949; font-family:Arial, Helvetica, sans-serif; font-size: 14px;'>";
                //mailBody += "<div style='float:left; height:70px; width:100%; background-color:#fdfdfd;'> ";
                //mailBody += "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
                //mailBody += "<tr>";
                ////    mailBody += "<td background='[email_bg]' height='70'><a style='outline:none' target='_blank'  href='http://www.annectos.in'> <img style='border:none;' src='[email_logo]' alt='annectos' width='222'  margin-top='20px' height='54' /></a></td>";
                //mailBody += "</tr>";
                //mailBody += "</table>";
                //mailBody += "</div>";
                //mailBody += "<div style='float:left;  width:100%; background-color:#ffffff;'>";
                //mailBody += "<div style='width:100%; font-size:24px; color:#1c66ca; height:75px; float:left; text-align:center;'>";
                //mailBody += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                //mailBody += "<tr>";
                //mailBody += "<td><img style='border:none; margin-right:-150px;' src='[email_logo]' alt='annectos' width='222'  margin-top='20px' height='54' /></a></td><td height='75' align='center' valign='middle'>Product Enquiry</td>";
                //mailBody += "</tr>";
                //mailBody += "</table>";
                //mailBody += "</div>";
                //mailBody += "<div style='width:100%; float:left;'>";
                //mailBody += "<div style='width:620px; margin:0 auto;'>";
                //mailBody += "<div style='width:100%; float:left;'>";

                //mailBody += "Hi,<br />";
                //mailBody += "A new enquiry has been created for the below mentioned product by the below mentioned user.<br />";
                //mailBody += "<br />";
                //mailBody += "<b>Product Name</b>: [ProdName].<br />";
                ////  mailBody += "<br />";
                //mailBody += "<b>Product ID</b>: [ProdId].<br />";
                ////   mailBody += "<br />";
                //mailBody += "<b>Size</b>: [size]<br />";
                ////   mailBody += "<br />";
                //mailBody += "<b>Quantity</b>:[qty]<br />";
                //mailBody += "<b>Total Quantity</b>:[total_qty]<br />";

                //if (pe.logo == "Y")
                //{
                //    mailBody += "<b>Logo Needed</b>: Yes.<br />";

                //}
                //else
                //{

                //    mailBody += "<b>Logo Needed</b>: No.<br />";
                //}

                //mailBody += "<br /><br />";
                //mailBody += "<span style='color:#2c2b2b; font-weight:bold;'>User</span> <br />";
                //mailBody += "<b>Name</b>: [UserName].<br />";
                //mailBody += "<b>Mobile</b>: [UserMobile].<br />";
                //mailBody += "<b>Email</b>: [UserEmail].<br />";
                //mailBody += "<b>City</b>: [UserCity].<br />";
                //mailBody += "<b>Company Name</b>: [UserCompanyName].<br />";
                //mailBody += "<b>Website</b>: [UserWebsite].<br />";
                //mailBody += "<br />";
                //mailBody += "<br />";
                //mailBody += "Regards,<br />";
                //mailBody += "<span style='color:#0f0f0f;'>Team Annectos</span>";
                //mailBody += "</div>";
                //mailBody += "<div style='width:100%; float:left; text-align:center; font-size:12px; border-top:2px  solid #b41919; margin-top:50px; padding-top:25px; padding-bottom:25px; '>";
                //mailBody += "Copyright &copy; 2014 Annectos.All rights reserved.<br />";
                //mailBody += "<span style='color:#373737;'>http://shopinbulk.in/</span>";
                //mailBody += "</div>";
                //mailBody += "</div>";
                //mailBody += "</div>";
                //mailBody += "</div>";
                //mailBody += "</div>";
                //mailBody += "</div>";
                //mailBody += "</body>";
                //mailBody += "</html>";


                if (pe.logo == "Y")
                {
                    logo = "yes";

                }
                else
                {
                    logo = "no";

                }


                mailBody = @"
                         <html > 
                    <body>       
            <table style='border: 1px solid #ccc'  width='700' align='center' border='0' cellpadding='0' cellspacing='0'>
            	<tr>
            		<td  style='padding: 10px; border-bottom:5px solid #f1613b'>
                  <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                     <tr>
                        <td><img style='border:none; margin-right:-150px;' src='http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png' alt='annectos' width='122px'  margin-top='20px' height='86px' /></a></td>
                        <td height='75' align='center' style='font-family:Helvetica, Arial, sans-serif; ' valign='middle'>Grass Enquiry</td>
                     </tr>
                  </table>
              </td>
              </tr>
              <tr>
              <td style='padding: 10px; font-size:13px; line-height:22px; font-family:Helvetica, Arial, sans-serif;  border-bottom:5px solid #f1613b '>
               <table style='border-collapse: collapse' cellpadding='0' cellspacing='0' width='100%'>
               <tbody>
               <tr>
                  <td colspan='5' style='color: #333333; font-family:&quot;arial&quot; ; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold'>[ProdName]</td>
               </tr>
               <tr>
                 
                  <td style='color: #666666;  font-family:&quot;arial&quot; ; border:1px solid #ccc; font-size: 13px; padding: 5px; text-align: center;'>
                    Product ID
                     <div style='margin-top: 10px;  color: #333333'>[ProdId]</div>
                  </td>
                  <td style='color: #666666;  font-family:&quot;arial&quot; ; border:1px solid #ccc; font-size: 13px;  text-align: center;'>
                     <div style=' padding: 5px;;'>
                        Size 
                        <div style='margin-top: 10px; color: #333333;'>[size]</div>
                     </div>
                  </td>
                  <td style='color: #666666;  font-family:&quot;arial&quot; ; font-size: 13px; border:1px solid #ccc; text-align: center;'>
                     <div style=' padding: 5px;'>
                         Quantity
                        <div style='margin-top: 10px; color: #333333'>[qty]</div>
                     </div>
                  </td>
                   <td style='color: #666666;  font-family:&quot;arial&quot; ; font-size: 13px; border:1px solid #ccc; text-align: center;'>
                     <div style=' padding: 5px;'>
                        Total Quantity
                        <div style='margin-top: 10px; color: #333333'>[total_qty]</div>
                     </div>
                  </td>
                  <td style='color: #666666;  font-family:&quot;arial&quot; ; border:1px solid #ccc; font-size: 13px;  text-align: center;'>
                     <div style='padding: 5px;'>
                        Logo Needed 
                        <div style='margin-top: 10px;' > [logo_needed] </div>
                     </div>
                  </td>
                 
               </tr>                       
            </tbody>

				</table>
                     </td>
            	</tr> 
            	<tr> 
            		<td style='width:100%; font-family:Helvetica, Arial, sans-serif; line-height:20px; font-size:14px; color:#333333;float:left; padding: 5px; border-bottom:5px solid #f1613b'>
            			User Details:</span> <br /><b>Name</b>:[UserName]<br /><b>Mobile</b>:[UserMobile] <br /><b>Email</b>: [UserEmail]<br /><b>City</b>:[UserCity]<br /><b>Company Name</b>:[UserCompanyName] .<br /><b>Website</b>:[UserWebsite] .<br /><br /><br /><strong> Regards,</strong><br />Team GRASS
            		</td>
            	</tr>
                  <tr>
              <td style='padding: 10px; font-family:Helvetica, Arial, sans-serif; '>    
                <div style='width:100%; float:left; text-align:center; font-size:12px;   padding-top:25px; padding-bottom:25px; '>Copyright &copy; 2014 Annectos.All rights reserved.<br /><span style='color:#373737;'>http://grass.shopinbulk.in/</span></div>                                
               </td>
            </tr>
          <tr>
           <td style='padding: 10px;padding-left:600px;' align='center'><a href= '####' target='_blank'><img src='http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/annectostemp.png'  border='0' alt=''/></a></td>                       
            </tr>
				
     </table>
   </body>
     </html> ";





                string siteurl = ConfigurationSettings.AppSettings["SiteUrl"].ToString();


                DataAccess da = new DataAccess();
                DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", strCompany));
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                    strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    siteurl = strCompanyURL;
                }




                string email_bg = ConfigurationSettings.AppSettings["email_bg"].ToString();
                string email_logo = ConfigurationSettings.AppSettings["email_logo"].ToString();

                mailBody = mailBody.Replace("[email_bg]", email_bg);
                poweredbyLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/annectostemp.png";
              strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/bulkgrass.png";
                mailBody = mailBody.Replace("[email_logo]", strLogo);

                mailBody = mailBody.Replace("[PoweredBy]", poweredbyLogo);
                mailBody = mailBody.Replace("[ProdName]", pe.prod_name);
                mailBody = mailBody.Replace("[ProdId]", pe.prod_id);
                //mailBody = mailBody.Replace("[qty]", pe.quantity);
                mailBody = mailBody.Replace("[size]", product_size);
                mailBody = mailBody.Replace("[qty]", quantity);
                mailBody = mailBody.Replace("[total_qty]", convertToString(pe.total_enq_qty));
                mailBody = mailBody.Replace("[logo_needed]", logo);
                mailBody = mailBody.Replace("[UserName]", pe.name);
                mailBody = mailBody.Replace("[UserMobile]", pe.mobile_no);
                mailBody = mailBody.Replace("[UserEmail]", pe.email_id);
                mailBody = mailBody.Replace("[UserCity]", pe.city);
                mailBody = mailBody.Replace("[UserCompanyName]", pe.company_name);
                mailBody = mailBody.Replace("[UserWebsite]", pe.website);

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return mailBody;
        }


        public List<registration> UserLogin(user_login user_log)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string Msg = "";
            string token_id = "";
            login_msg return_msg = new login_msg();
            List<registration> SL = new List<registration>();
            DataTable dtResult = new DataTable();

            try
            {
                string[] strout = new string[0];

                token_id = Guid.NewGuid().ToString();

                dtResult = da.ExecuteDataTable("user_login"
                                , da.Parameter("_email_id", user_log.email_id)
                                , da.Parameter("_password", user_log.password)
                                , da.Parameter("_login_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_token_id", token_id)
                                , da.Parameter("_status", 1)
                                , da.Parameter("_company", user_log.company)
                                , da.Parameter("_outmsg", "")
                                );




                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        registration oreg = new registration();
                        if (dtResult.Rows[i]["message"].ToString() != "Invalid Email Or Password")
                        {
                            oreg.first_name = dtResult.Rows[i]["first_name"].ToString();
                            oreg.last_name = dtResult.Rows[i]["last_name"].ToString();
                            oreg.gender = dtResult.Rows[i]["gender"].ToString();
                            oreg.mobile_no = dtResult.Rows[i]["mobile_no"].ToString();
                            oreg.email_id = dtResult.Rows[i]["email_id"].ToString();
                            oreg.status = Convert.ToInt32(dtResult.Rows[i]["status"].ToString());
                            oreg.total_point = dtResult.Rows[i]["total_point"].ToString();
                            oreg.max_point_range = convertToDecimal(dtResult.Rows[i]["max_point_range"].ToString());
                            oreg.min_point_range = convertToDecimal(dtResult.Rows[i]["min_point_range"].ToString());
                            oreg.company = dtResult.Rows[i]["company"].ToString();
                            oreg.token_id = token_id;
                        }
                        oreg.message = dtResult.Rows[i]["message"].ToString();
                        SL.Add(oreg);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return SL;
        }

        public Boolean RegistrationCheck(registration reg)
        {
            List<COMPANY> cmplist = new List<COMPANY>();
            cmplist = get_comp_details();

            if (cmplist.Count() == 0)
            {
                return false;
            }
            else
            {
                int match = 0;
                for (var i = 0; i < cmplist.Count(); i++)
                {
                    var inputemail = reg.email_id.ToString();
                    var charPos = inputemail.IndexOf('@') + 1;
                    var checkdomain = inputemail.Substring(charPos, (inputemail.Length - charPos));

                    if (cmplist[i].emailid.Contains(checkdomain))
                    {
                        match++;
                        break;
                    }
                }
                if (match == 0)
                {
                    return false;
                }
                else
                {
                    return true;

                }

            }
        }
        public string postFeedback(Feedback feed)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string Msg = "";

            try
            {

                int i = da.ExecuteSP("user_feedback_insert", ref oc
                    , da.Parameter("_feedback_id", feed.feedback_id)
                    , da.Parameter("_first_name", feed.first_name)
                    , da.Parameter("_last_name", feed.last_name)
                    , da.Parameter("_email_id", feed.email_id)
                    , da.Parameter("_feedback_choise", feed.feedback_choise)
                    , da.Parameter("_feedback_comment", feed.feedback_comment)
                    , da.Parameter("_company", feed.company)
                    , da.Parameter("_created_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_outmsg", String.Empty, true)
                    );
                if (i > 0)
                {
                    Msg = oc[0].strParamValue.ToString();
                }
                else
                {
                    Msg = "Record Not Inserted";

                }
            }
            catch (Exception ex)
            {
                Msg = "failed";
            }
            return Msg;
        }


        public string postContactUs(Contact cnt)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string Msg = "";

            try
            {
                int i = da.ExecuteSP("contact_us_insert", ref oc
                    , da.Parameter("_contact_us_id", cnt.contact_us_id)
                    , da.Parameter("_name", cnt.name)
                    , da.Parameter("_email_id", cnt.email_id)
                    , da.Parameter("_telephone", cnt.telephone)
                    , da.Parameter("_comments", cnt.comments)
                    , da.Parameter("_company", cnt.company)
                    , da.Parameter("_created_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_outmsg", String.Empty, true)
                    );
                if (i > 0)
                {
                    Msg = oc[0].strParamValue.ToString();
                }
                else
                {
                    Msg = "Record Not Inserted";

                }
            }
            catch (Exception ex)
            {
                Msg = "failed";
            }
            return Msg;
        }

        public string postGrassCamp(GrassCamp grasscmp)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string Msg = "";

            try
            {
                int i = da.ExecuteSP("grass_camp_insert", ref oc
                    , da.Parameter("_id", grasscmp.id)
                    , da.Parameter("_name", grasscmp.name)
                    , da.Parameter("_mobile", grasscmp.mobile)
                    , da.Parameter("_landline", grasscmp.landline)
                    , da.Parameter("_city", grasscmp.city)
                    , da.Parameter("_company_name", grasscmp.company_name)
                    , da.Parameter("_company_email", grasscmp.company_email)
                    , da.Parameter("_designation", grasscmp.designation)
                    , da.Parameter("_address", grasscmp.address)
                    , da.Parameter("_pincode", grasscmp.pincode)
                    , da.Parameter("_product_name", grasscmp.product_name)
                    , da.Parameter("_product_id", grasscmp.product_id)
                    , da.Parameter("_product_qty", grasscmp.product_qty)
                    , da.Parameter("_comments", grasscmp.comments)
                    , da.Parameter("_created_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_outmsg", String.Empty, true)
                    );
                if (i > 0)
                {
                    Msg = "Record  Inserted Successfully";
                }
                else
                {
                    Msg = "Record Not Inserted";

                }
            }
            catch (Exception ex)
            {
                Msg = "failed";
            }
            return Msg;
        }



        public string postNewsletter(Newsletter nwl)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string Msg = "";

            try
            {
                int i = da.ExecuteSP("newsletter_insert", ref oc
                    , da.Parameter("_name", nwl.name)
                    , da.Parameter("_email_id", nwl.email_id)
                    , da.Parameter("_telephone", nwl.telephone)
                    , da.Parameter("_company", nwl.company)
                    , da.Parameter("_designation", nwl.designation)
                    , da.Parameter("_created_ts", System.DateTime.Now.ToLocalTime())
                   
                    );
                if (i > 0)
                {
                    Msg = "Record  Inserted Successfully";
                }
                else
                {
                    Msg = "Record Not Inserted";

                }
            }
            catch (Exception ex)
            {
                Msg = "failed";
            }
            return Msg;
        }

        public List<GrassCamp> GetGrassCampData(string comments)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            DataTable dtResult = new DataTable();
            List<GrassCamp> GC = new List<GrassCamp>();
            string Msg = "";

            try
            {
                dtResult = da.ExecuteDataTable("get_grass_camp_data"
                    , da.Parameter("_comments", comments)
                    );
                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        GrassCamp gcd = new GrassCamp();

                        gcd.id = Convert.ToInt32(dtResult.Rows[i]["id"].ToString());
                        gcd.name = dtResult.Rows[i]["name"].ToString();
                        gcd.mobile = dtResult.Rows[i]["mobile"].ToString();
                        gcd.landline = dtResult.Rows[i]["landline"].ToString();
                        gcd.city = dtResult.Rows[i]["city"].ToString();
                        gcd.company_name = dtResult.Rows[i]["company_name"].ToString();
                        gcd.company_email = dtResult.Rows[i]["company_email"].ToString();
                        gcd.designation = dtResult.Rows[i]["designation"].ToString();
                        gcd.address = dtResult.Rows[i]["address"].ToString();
                        gcd.pincode = dtResult.Rows[i]["pincode"].ToString();
                        gcd.product_name = dtResult.Rows[i]["product_name"].ToString();
                        gcd.product_id = dtResult.Rows[i]["product_id"].ToString();
                        gcd.product_qty = dtResult.Rows[i]["product_qty"].ToString();
                        gcd.created_ts = Convert.ToDateTime(dtResult.Rows[i]["created_ts"].ToString());
                        gcd.comments = comments;

                        GC.Add(gcd);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return GC;
        }



        public List<Newsletter> GetNewsletterData(string email)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            DataTable dtResult = new DataTable();
            List<Newsletter> NL = new List<Newsletter>();
            string Msg = "";

            if (email == "''")
            {
                email = string.Empty;
            }
            try
            {
                dtResult = da.ExecuteDataTable("get_newsletter_data"
                    , da.Parameter("_email", email)
                    );
                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        Newsletter nwl = new Newsletter();

                        nwl.id = Convert.ToInt32(dtResult.Rows[i]["_id"].ToString());
                        nwl.name = dtResult.Rows[i]["name"].ToString();
                        nwl.email_id = dtResult.Rows[i]["email_id"].ToString();
                        nwl.telephone = dtResult.Rows[i]["telephone"].ToString();
                        nwl.company = dtResult.Rows[i]["company"].ToString();
                        nwl.designation = dtResult.Rows[i]["designation"].ToString();
                        nwl.created_ts = Convert.ToDateTime(dtResult.Rows[i]["created_ts"].ToString());
                     

                        NL.Add(nwl);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return NL;
        }
        
        //sends only transactional sms; Diwakar 6/2/2014
        public string SendTRNSsms(List<String> strEmails)
        {
            DataAccess da = new DataAccess();

            string strdisplayname = "";
            string strCompanyURL = "";
            string strMobileNo = "";
            try
            {
                for (int i = 0; i < strEmails.Count; i++)
                {
                    DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", strEmails[i]));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        strMobileNo = dt.Rows[0]["Mobile_No"].ToString();
                        DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", dt.Rows[0]["company"].ToString()));
                        if (dtCompany != null && dtCompany.Rows.Count > 0)
                        {
                            strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                            strdisplayname = convertToString(dtCompany.Rows[0]["root_url_2"]);
                            // strLogo = dtCompany.Rows[0]["logo"].ToString();
                        }

                        helper_repository hp = new helper_repository();

                        string sms_user_id = ConfigurationSettings.AppSettings["SMSNONPROMOLOGIN"].ToString();
                        string sms_pwd = ConfigurationSettings.AppSettings["SMSNONPROMOPWD"].ToString();

                        string sms_message = "";

                        if (dt.Rows[0]["company"].ToString() == "xolo")
                        {
                            sms_message = ConfigurationSettings.AppSettings["SMS_USER_TRNS_MSG3"].ToString();
                        }
                        else
                        {
                            sms_message = ConfigurationSettings.AppSettings["SMS_USER_TRNS_MSG1"].ToString();
                        }
                        string sms_ssid = ConfigurationSettings.AppSettings["SMSTRNID"].ToString();

                        sms_message = sms_message.Replace("##COMPANY##", strdisplayname);
                        sms_message = sms_message.Replace("##LOGIN##", strEmails[i].ToString());
                        sms_message = sms_message.Replace("##PWD##", dt.Rows[0]["password"].ToString());
                        sms_message = sms_message.Replace("##POINTS##", dt.Rows[0]["points"].ToString());
                        sms_message = sms_message.Replace("##COMP_URL##", strCompanyURL);


                        string ErroMsg = "";
                        int MobNoLength = 0;

                        long number1 = 0;

                        if (strMobileNo != null)
                        {
                            bool canConvert = long.TryParse(strMobileNo, out number1);
                            if (canConvert == false)
                            {
                                ErroMsg = "Mobile Number Is Not In Correct Format";
                                ExternalLogger.LogInfo(ErroMsg, this, "#");
                                return "Failure";
                            }

                            MobNoLength = strMobileNo.Length;

                            if (MobNoLength != 10)
                            {
                                ErroMsg = "Mobile Number Should Contain 10 Digits";
                                ExternalLogger.LogInfo(ErroMsg, this, "#");
                                return "Failure";
                            }
                            else
                            {
                                strMobileNo = "91" + strMobileNo;

                            }
                        }


                        if (!string.IsNullOrEmpty(strMobileNo))
                        {
                            string resp = hp.SendSMS(sms_user_id, sms_pwd, strMobileNo, sms_ssid, sms_message, "TRNS");
                        }
                        //strMobileNo
                    }
                }
            }

            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failure";
            }

            return "Success";

        }

        public string getUserEMailAddress(string Email_Id)
        {
            DataAccess da = new DataAccess();
            string EmailAddress = "";

            if (Email_Id.Contains("@"))
            {
                EmailAddress = Email_Id;
            }
            else
            {
                EmailAddress = ConfigurationSettings.AppSettings["MailReplyToLava"].ToString();


                DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", Email_Id));
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["EmailAddress"] != null)
                    {
                        if (dt.Rows[0]["EmailAddress"].ToString().Contains("@"))
                        {
                            EmailAddress = dt.Rows[0]["EmailAddress"].ToString();
                        }

                    }
                }

            }
            return EmailAddress.Trim();
        }

        public string getUserMobileNo(string Email_Id)
        {
            DataAccess da = new DataAccess();
            string strMobileNo = "";
            DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", Email_Id));
            if (dt != null && dt.Rows.Count > 0)
            {
                strMobileNo = dt.Rows[0]["Mobile_No"].ToString();
            }
            return strMobileNo;
        }

        public List<COMPANY> get_comp_details()
        {

            DataAccess da = new DataAccess();
            List<COMPANY> lcomp = new List<COMPANY>();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("company_details"
                                 , da.Parameter("_comp_id", 0)
                                 , da.Parameter("_entry_mode", "A")
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        COMPANY comp = new COMPANY();
                        comp.id = Convert.ToInt32(dtResult.Rows[i]["id"].ToString());
                        comp.name = dtResult.Rows[i]["name"].ToString();
                        comp.emailid = dtResult.Rows[i]["emailid"].ToString();
                        lcomp.Add(comp);
                    }
                }



            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return lcomp;
        }

        /// <summary>
        /// This will send the User Credentials by email and update the status flag for login
        /// </summary>
        /// <param name="strEmails"></param>
        /// <returns></returns>
        public string SendUserCredentials(List<String> strEmails)
        {
            DataAccess da = new DataAccess();
            string strSubject = "Welcome to annectoś rewards portal";
            string strBody = "";
            string strCompanyURL = "";
            string strdisplayname = "";
            string strStoreName = "";
            string strLogo = "";
            string strMobileNo = "";
            store_message osm = new store_message();
            try
            {
                for (int i = 0; i < strEmails.Count; i++)
                {
                    DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", strEmails[i]));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        strMobileNo = dt.Rows[0]["Mobile_No"].ToString();
                        DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", dt.Rows[0]["company"].ToString()));
                        if (dtCompany != null && dtCompany.Rows.Count > 0)
                        {
                            strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                            strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                            strStoreName = dtCompany.Rows[0]["name"].ToString();
                            strLogo = dtCompany.Rows[0]["logo"].ToString();
                        }
                        /*
                        strBody="Hello " + dt.Rows[0]["first_name"].ToString() + "," + dt.Rows[0]["last_name"].ToString();
                        strBody += "<br/>";
                        strBody += "<br/>";
                        strBody +="Welcome to Annectos' rewards portal. Below is your sign in credential and information:";
                        strBody += "<br/>";
                        strBody += "<br/>";
                        strBody +="User Id  - " + strEmails[i];
                        strBody += "<br/>";
                        strBody +="Password - " +  dt.Rows[0]["password"].ToString();
                        strBody += "<br/>";
                        strBody +="Your point balance as per our record " + dt.Rows[0]["points"].ToString();
                        strBody += "<br/>";
                        strBody += "If you feel any discrepancy - please contact our support or your HR Representative.";
                        strBody += "<br/>";
                        */
                        /*************************************/
                        strSubject = "Welcome to " + strdisplayname;// +" Rewards platform"; Changed by Rahul on Dec 1
                        strBody = "<html>";
                        strBody += "<head>";
                        strBody += "</head>";
                        strBody += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                        strBody += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                        strBody += "    <tr>";
                        strBody += "        <td style='background: #f5f5f5'>";
                        strBody += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                        strBody += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                        strBody += "<div style='padding: 5px;'>";
                        strBody += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                        strBody += "		            <tr>";
                        strBody += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                        //strBody += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg'</td>";
                        strBody += "			            <td style='width: 300px;;'></td>";
                        strBody += "			            <td style='width: 35px;' ><a href='#'></a></td>";
                        strBody += "			            <td style='width: 35px;' ><a href='#'></a></td>";
                        strBody += "			            <td style='width: 35px;'><a href='#'></a></td>";
                        strBody += "		            </tr>";
                        strBody += "		            </table>";
                        strBody += "		            </div>";
                        strBody += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                        strBody += "			            <table cellpadding='0' cellspacing='0'  >";
                        strBody += "		            </table>";
                        strBody += "		            </div>";
                        strBody += "            </div>";
                        strBody += "        </td>";
                        strBody += "    </tr>";
                        strBody += "    <tr>";
                        strBody += "        <td style='background: #fff; padding: 10px;font-family: 'Trebuchet MS'; font-size: 13px'>";
                        strBody += "        Dear " + dt.Rows[0]["first_name"].ToString() + " " + dt.Rows[0]["last_name"].ToString() + ", <br /><br />";
                        strBody += "";
                        // changed by rahul on 1 Dec 2013
                        strBody += "        Welcome to " + strdisplayname + ". Please Login to the portal using the following link - " + strCompanyURL + ". Below is your sign in credential and information: <br /><br />";
                        strBody += "        <div style='font-family: 'Trebuchet MS'; font-size: 12px; color: #333'>";
                        strBody += "        User Id - " + strEmails[i] + "<br/>";
                        strBody += "        Password - " + dt.Rows[0]["password"].ToString() + "<br/>";
                        strBody += "        <br/>";
                        strBody += "        (please do not share your login details or password with anyone else. )<br/>";
                        strBody += "        <br/>";
                        strBody += "        The points balance in your account is " + dt.Rows[0]["points"].ToString() + "<br/>";
                        strBody += "        <br/>";
                        strBody += "        For further assistance call us on +91 9686202046 / 9972334590 or mail us at  <br/>";
                        strBody += "        <br/>";
                        strBody += "        Assuring you of our best services always. <br/>";
                        strBody += "        <br/>";
                        strBody += "        ** Please use Google Chrome for exploring through the portal. <br/>";
                        strBody += "        <br/>";
                        strBody += "        Sincerely<br/>";
                        strBody += "        <br/>";
                        strBody += "        Team GRASS ";
                        strBody += "        </div>";
                        strBody += "        </td>";
                        strBody += "        </tr>";
                        strBody += "        </table>";
                        strBody += "        </td>";
                        strBody += "        </tr>";
                        strBody += "<tr>";
                        strBody += "        <td style='background: #fff; padding: 10px;font-family: 'Trebuchet MS'; font-size: 13px'>";
                        strBody += "        </td>";
                        strBody += "</tr>";
                        strBody += "</table>";
                        strBody += "</body>";
                        strBody += "</html>";

                        strBody = strBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                        /*************************************/


                        helper_repository hp = new helper_repository();
                        string strbcc_email = ConfigurationSettings.AppSettings["bcc_email"].ToString();
                        string sms_user_id = ConfigurationSettings.AppSettings["SMSNONPROMOLOGIN"].ToString();
                        string sms_pwd = ConfigurationSettings.AppSettings["SMSNONPROMOPWD"].ToString();
                        string sms_ssid = ConfigurationSettings.AppSettings["SMSSSID"].ToString();
                        string sms_message = ConfigurationSettings.AppSettings["SMS_USER_ACC_CREATION_MSG"].ToString();


                        //This Code is applicable only for airtel

                        if (strStoreName == "airtel")
                        {
                            strBody = "";
                            strSubject = "";

                            osm = get_store_message(strStoreName, "welcome");
                            if (osm != null)
                            {
                                strBody = osm.Msg_content;
                                strSubject = osm.Subject;
                            }

                            strBody = strBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");

                            strBody = strBody.Replace("#USER_FIRST_NAME#", dt.Rows[0]["first_name"].ToString());
                            strBody = strBody.Replace("#USER_LAST_NAME#", dt.Rows[0]["last_name"].ToString());
                            strBody = strBody.Replace("#COMPANY_LOGO#", strLogo);
                            strBody = strBody.Replace("#COMPANY_URL#", strCompanyURL);
                            strBody = strBody.Replace("#USER_EMAIL#", strEmails[i].ToString());
                            strBody = strBody.Replace("#USER_PASSWORD#", dt.Rows[0]["password"].ToString());
                            strBody = strBody.Replace("#PONTS_BALANCE#", dt.Rows[0]["points"].ToString());
                        }
                        //   This is for XoloMan

                        else if (strStoreName == "xolo")
                        {
                            strBody = "";
                            strSubject = "";

                            osm = get_store_message(strStoreName, "welcome");
                            if (osm != null)
                            {
                                strBody = osm.Msg_content;
                                strSubject = osm.Subject;
                            }

                            strBody = strBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");

                            strBody = strBody.Replace("#USER_FIRST_NAME#", dt.Rows[0]["first_name"].ToString());
                            strBody = strBody.Replace("#USER_LAST_NAME#", dt.Rows[0]["last_name"].ToString());
                            strBody = strBody.Replace("#COMPANY_LOGO#", strLogo);
                            strBody = strBody.Replace("#COMPANY_URL#", strCompanyURL);
                            strBody = strBody.Replace("#USER_EMAIL#", strEmails[i].ToString());
                            strBody = strBody.Replace("#USER_PASSWORD#", dt.Rows[0]["password"].ToString());
                            strBody = strBody.Replace("#PONTS_BALANCE#", dt.Rows[0]["points"].ToString());


                        }
                        else if (strStoreName == "grass")
                        {
                            strBody = "";
                            strSubject = "";

                            osm = get_store_message(strStoreName, "welcome");
                            if (osm != null)
                            {
                                strBody = osm.Msg_content;
                                strSubject = osm.Subject;
                            }
                            strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";
                            strBody = strBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");

                            strBody = strBody.Replace("#USER_FIRST_NAME#", dt.Rows[0]["first_name"].ToString());
                            strBody = strBody.Replace("#USER_LAST_NAME#", dt.Rows[0]["last_name"].ToString());
                            strBody = strBody.Replace("#COMPANY_LOGO#", strLogo);
                            strBody = strBody.Replace("#COMPANY_URL#", strCompanyURL);
                            strBody = strBody.Replace("#USER_EMAIL#", strEmails[i].ToString());
                            strBody = strBody.Replace("#USER_PASSWORD#", dt.Rows[0]["password"].ToString());
                            strBody = strBody.Replace("#PONTS_BALANCE#", dt.Rows[0]["points"].ToString());


                        }

                        if (hp.SendMail(getUserEMailAddress(strEmails[i].ToString()), "", strbcc_email, strSubject, strBody) == 1)
                        {
                            int j = da.ExecuteSP("auto_activate_users", da.Parameter("_email_id", strEmails[i]));

                            if (!string.IsNullOrEmpty(strMobileNo))
                            {
                                string MobNo = strMobileNo;
                                if (CheckMobileNo(strMobileNo, ref MobNo) != "Failure")
                                {
                                    string resp = hp.SendSMS(sms_user_id, sms_pwd, MobNo, sms_ssid, sms_message);
                                }
                            }
                            //strMobileNo
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failure";
            }

            return "Success";

        }

        public string CheckMobileNo(string strMobileNo, ref string MobNo)
        {

            string ErroMsg = "";
            int MobNoLength = 0;

            long number1 = 0;

            if (strMobileNo != null)
            {
                bool canConvert = long.TryParse(strMobileNo, out number1);
                if (canConvert == false)
                {
                    ErroMsg = "Mobile Number Is Not In Correct Format";
                    ExternalLogger.LogInfo(ErroMsg, this, "#");
                    return "Failure";
                }

                MobNoLength = strMobileNo.Length;

                if (MobNoLength != 10)
                {
                    ErroMsg = "Mobile Number Should Contain 10 Digits";
                    ExternalLogger.LogInfo(ErroMsg, this, "#");
                    return "Failure";
                }
                else
                {
                    strMobileNo = "91" + strMobileNo;
                    MobNo = strMobileNo;
                    return strMobileNo;
                }
            }
            else
            {
                return "Failure";
            }

        }

        public string SendPromoWithCredentials(List<String> strEmails)
        {
            DataAccess da = new DataAccess();
            string strSubject = "";
            string strBody = "";
            string strCompanyURL = "";
            string strdisplayname = "";
            string strStoreName = "";
            string strLogo = "";
            string strMobileNo = "";
            store_message osm = new store_message();
            try
            {
                for (int i = 0; i < strEmails.Count; i++)
                {
                    DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", strEmails[i]));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        strMobileNo = dt.Rows[0]["Mobile_No"].ToString();
                        DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", dt.Rows[0]["company"].ToString()));

                        if (dtCompany != null && dtCompany.Rows.Count > 0)
                        {
                            strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                            strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                            strStoreName = dtCompany.Rows[0]["name"].ToString();
                            strLogo = dtCompany.Rows[0]["logo"].ToString();
                        }

                        osm = get_store_message(strStoreName, "promo");
                        if (osm != null)
                        {
                            strBody = osm.Msg_content;
                            strSubject = osm.Subject;
                        }

                        strBody = strBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");

                        strBody = strBody.Replace("#USER_FIRST_NAME#", dt.Rows[0]["first_name"].ToString());
                        strBody = strBody.Replace("#USER_LAST_NAME#", dt.Rows[0]["last_name"].ToString());
                        strBody = strBody.Replace("#COMPANY_DISPLAY_NAME#", strdisplayname);
                        strBody = strBody.Replace("#COMPANY_URL#", strCompanyURL);
                        strBody = strBody.Replace("#USER_EMAIL#", strEmails[i].ToString());
                        strBody = strBody.Replace("#USER_PASSWORD#", dt.Rows[0]["password"].ToString());
                        strBody = strBody.Replace("#PONTS_BALANCE#", dt.Rows[0]["points"].ToString());

                        /*************************************/


                        helper_repository hp = new helper_repository();
                        string strbcc_email = ConfigurationSettings.AppSettings["bcc_email"].ToString();
                        string sms_user_id = ConfigurationSettings.AppSettings["SMSNONPROMOLOGIN"].ToString();
                        string sms_pwd = ConfigurationSettings.AppSettings["SMSNONPROMOPWD"].ToString();
                        string sms_ssid = ConfigurationSettings.AppSettings["SMSSSID"].ToString();
                        string sms_message = ConfigurationSettings.AppSettings["SMS_USER_ACC_CREATION_MSG"].ToString();
                        if (hp.SendMail(getUserEMailAddress(strEmails[i].ToString()), "", strbcc_email, strSubject, strBody) == 1)
                        {
                            int j = da.ExecuteSP("auto_activate_users", da.Parameter("_email_id", strEmails[i]));

                            if (!string.IsNullOrEmpty(strMobileNo))
                            {
                                string resp = hp.SendSMS(sms_user_id, sms_pwd, strMobileNo, sms_ssid, sms_message);
                            }
                            //strMobileNo
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failure";
            }

            return "Success";

        }

        public string SendPromo(List<String> strEmails)
        {
            DataAccess da = new DataAccess();
            string strSubject = "";
            string strBody = "";
            string strCompanyURL = "";
            string strStoreName = "";
            string strdisplayname = "";
            string strLogo = "";
            string strMobileNo = "";
            store_message osm = new store_message();
            try
            {
                for (int i = 0; i < strEmails.Count; i++)
                {
                    DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", strEmails[i]));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        strMobileNo = dt.Rows[0]["Mobile_No"].ToString();
                        DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", dt.Rows[0]["company"].ToString()));

                        if (dtCompany != null && dtCompany.Rows.Count > 0)
                        {
                            strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                            strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                            strStoreName = dtCompany.Rows[0]["name"].ToString();
                            strLogo = dtCompany.Rows[0]["logo"].ToString();
                        }

                        osm = get_store_message(strStoreName, "promo");
                        if (osm != null)
                        {
                            strBody = osm.Msg_content;
                            strSubject = osm.Subject;
                        }

                        strBody = strBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");

                        strBody = strBody.Replace("#USER_FIRST_NAME#", dt.Rows[0]["first_name"].ToString());
                        strBody = strBody.Replace("#USER_LAST_NAME#", dt.Rows[0]["last_name"].ToString());
                        strBody = strBody.Replace("#COMPANY_DISPLAY_NAME#", strdisplayname);
                        strBody = strBody.Replace("#COMPANY_URL#", strCompanyURL);
                        strBody = strBody.Replace("#USER_EMAIL#", strEmails[i].ToString());
                        strBody = strBody.Replace("#USER_PASSWORD#", dt.Rows[0]["password"].ToString());
                        strBody = strBody.Replace("#PONTS_BALANCE#", dt.Rows[0]["points"].ToString());
                        /*************************************/


                        helper_repository hp = new helper_repository();
                        string strbcc_email = ConfigurationSettings.AppSettings["bcc_email"].ToString();
                        string sms_user_id = ConfigurationSettings.AppSettings["SMSNONPROMOLOGIN"].ToString();
                        string sms_pwd = ConfigurationSettings.AppSettings["SMSNONPROMOPWD"].ToString();
                        string sms_ssid = ConfigurationSettings.AppSettings["SMSSSID"].ToString();
                        string sms_message = ConfigurationSettings.AppSettings["SMS_USER_ACC_CREATION_MSG"].ToString();
                        if (hp.SendMail(getUserEMailAddress(strEmails[i].ToString()), "", strbcc_email, strSubject, strBody) == 1)
                        {
                            int j = da.ExecuteSP("auto_activate_users", da.Parameter("_email_id", strEmails[i]));

                            if (!string.IsNullOrEmpty(strMobileNo))
                            {
                                string resp = hp.SendSMS(sms_user_id, sms_pwd, strMobileNo, sms_ssid, sms_message);
                            }
                            //strMobileNo
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failure";
            }

            return "Success";

        }

        public store_message get_store_message(string Store_Id, string Msg_Type)
        {

            DataAccess da = new DataAccess();
            store_message osm = new store_message();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("get_store_message"
                                 , da.Parameter("_store_id", Store_Id)
                                 , da.Parameter("_msg_type", Msg_Type)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {

                        osm.Msg_id = convertToLong(dtResult.Rows[i]["Msg_id"]);
                        osm.Store_id = convertToString(dtResult.Rows[i]["Store_id"]);
                        osm.Msg_content = convertToString(dtResult.Rows[i]["Msg_content"]);
                        osm.Msg_type = convertToString(dtResult.Rows[i]["Msg_type"]);
                        osm.Subject = convertToString(dtResult.Rows[i]["Subject"]);

                    }
                }



            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return osm;
        }


        public string RegPassword(List<String> strEmails)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                for (int i = 0; i < strEmails.Count; i++)
                {

                    string Pwd = "";

                    Pwd = GetUniquePassword();
                    int j = da.ExecuteSP("user_password_reset", ref oc
                                       , da.Parameter("_email_id", strEmails[i].ToString())
                                       , da.Parameter("_newpassword", Pwd)
                                       , da.Parameter("_reset_date", System.DateTime.Now.ToLocalTime())
                                       , da.Parameter("_action_flag", 9)
                                       , da.Parameter("_outmsg", String.Empty, true)
                                       );
                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failure";
            }

            return "Success";

        }

        private string GetUniquePassword()
        {
            int maxSize = 7;
            char[] chars = new char[62];
            string a;
            a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            int size = maxSize;
            byte[] data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("X2"));
            }
            return sb.ToString();

        }

    }
}
