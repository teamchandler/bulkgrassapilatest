﻿using ecomm.dal;
using ecomm.util;
using ecomm.util.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace ecomm.model.repository
{
    public class payment_repository
    {
        string strMailSubCompany = "";

        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != "") && (o.ToString() != "BsonNull"))
                {
                    return o.ToString();
                }
                else if (o.ToString() == "BsonNull")
                {
                    return "";
                }
                else { return ""; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        private double convertToDouble(object o)
        {
            try
            {
                if (o != null && o.ToString() != "")
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        private bool check_field(BsonDocument doc, string field_name)
        {
            if (doc.Contains(field_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        /*************Payment Gateway Pre & Post Actions************/

        public string GetPartialShippingStatusBody(string OrderID, string shipdate, string CourServProv, string CourTrackNo, string CourTrackLink, double dCourierCost, int Status, List<partial_prod_id_qty> partial_order_prods, ref string email_id, ref string PartSubject)
        {

            try
            {
                string mailBody = string.Empty;
                string cart_data = "";
                double shipmentvalue = 0;
                double discount = 0;
                double finalamt = 0;
                double paidamt = 0;
                string vouchers = "";
                Int64 points = 0;
                double pointamt = 0;
                double voucheramt = 0;
                double PromoOffer = 0;
                string discount_rule = "";
                string discount_code = "";
                string strShippingInfo = "";
                string strStoreName = "";
                string strLogo = "";
                string poweredbyLogo = "";

                string strMobileNo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";
                string shippingAmount = "";
                string ShippingThreshold = "0";// ConfigurationSettings.AppSettings["shipping_threshold"].ToString();
                string strCompanyUrl = "";
                shippingAmount = "0";// ConfigurationSettings.AppSettings["shipping_amount"].ToString();

                DataTable dt = GetDataFromOrder(Convert.ToInt64(OrderID));
                DataTable dtShip = GetDataFromUserID(Convert.ToInt64(OrderID));
                checkoutrepository cr = new checkoutrepository();

                if (dt == null || dt.Rows.Count == 0)
                    return "No Such order";

                if ((dtShip == null) || (dtShip.Rows.Count == 0))
                {
                    return "No Shipping Information";
                }
                else
                {
                    ShippingName = dtShip.Rows[0]["billing_name"].ToString();
                    ShippingAddress = dtShip.Rows[0]["address"].ToString();
                    ShippingCity = dtShip.Rows[0]["city"].ToString();
                    ShippingState = dtShip.Rows[0]["state"].ToString();
                    ShippingPin = dtShip.Rows[0]["pin"].ToString();
                    strMobileNo = dtShip.Rows[0]["mobile_no"].ToString();
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    email_id = dt.Rows[i]["user_id"].ToString();
                    cart_data = dt.Rows[i]["cart_data"].ToString();
                    paidamt = convertToDouble(dt.Rows[i]["paid_amount"]);
                    points = Convert.ToInt64(dt.Rows[i]["points"].ToString()); ;
                    pointamt = convertToDouble(dt.Rows[i]["points_amount"]); ;
                    vouchers = dt.Rows[i]["egift_vou_no"].ToString();
                    voucheramt = convertToDouble(dt.Rows[i]["egift_vou_amt"]);
                    strShippingInfo = Convert.ToString(dt.Rows[i]["shipping_info"]);
                    strStoreName = Convert.ToString(dt.Rows[i]["store"].ToString().Trim());
                    if (dt.Rows[i]["discount_code"] != null)
                    {
                        discount_code = dt.Rows[i]["discount_code"].ToString();
                    }
                    break;
                }


                if (discount_code != null || discount_code != "")
                {

                    DataTable dtDiscInfo = GetDiscountCodeInfo(discount_code);

                    if (dtDiscInfo == null || dtDiscInfo.Rows.Count > 0)
                    {
                        for (int p = 0; p < dtDiscInfo.Rows.Count; p++)
                        {
                            discount_rule = dtDiscInfo.Rows[p]["rule"].ToString();
                        }

                    }

                    disc_rule odrulelist = JsonConvert.DeserializeObject<disc_rule>(discount_rule.ToString());

                    if (odrulelist != null)
                    {
                        PromoOffer = convertToDouble(odrulelist.discount_value);
                    }
                }

                DataTable dtPartialOrderResult = GetPartialOrderInfo(Convert.ToInt64(OrderID));
                DataTable dtCompany = GetCompanyInfo(strStoreName);
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
                    strMailSubCompany = dtCompany.Rows[0]["displayname"].ToString();

                    if (dtCompany.Rows[0]["shipping_charge"] == null)
                        shippingAmount = "0";
                    else
                        shippingAmount = dtCompany.Rows[0]["shipping_charge"].ToString();

                    if (dtCompany.Rows[0]["min_order_for_free_shipping"] == null)
                        ShippingThreshold = "0";
                    else
                        ShippingThreshold = dtCompany.Rows[0]["min_order_for_free_shipping"].ToString();

                    if (shippingAmount == "")
                        shippingAmount = "0";

                    if (ShippingThreshold == "")
                        ShippingThreshold = "0";


                }



                if (cart_data.Length == 0)
                    return "Invalid Order";


                //List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());
                List<cart_item> ordlist = cr.cart_items_to_item(JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString()));

                mailBody = @"
                        <html>
                        <body >
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td><table width='800' border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' align='center'>
                        <tr>
                        <td style='border-bottom:5px solid ##EMAIL_TEMP_COLOR##; padding: 10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>              
                        <td width='144'><a href= '##COMP_LINK##' target='_blank'><img src='##COMP_LOGO##'  border='0' alt=''/></a></td>
                        <td width='393'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td height='46' align='right' valign='middle'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='67%' align='left'><font style='font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#68696a; font-size:18px; text-transform:uppercase'></td>                         
                        <td width='4%'>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr>                  
                        </table></td>
                        </tr>
                        </table></td>
                        </tr>
                        <tr>
                        <td align='center'>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>

                       <tr>
                  <td style='background: #fff; padding: 10px;  font-family:'arial' ; font-size: 13px'>
                   Hi  ##FIRST_NAME##, <br /><br />We would like to inform you that part of your  ##COMP_LINK## order has been dispatched from our warehouse. You will receive this part of your order in the next few days.<br /><br /> </td> </tr>
                   <tr>
                  <td align='left' style='background: #fff; padding: 10px;  font-family:'arial' ; font-size: 13px'>
                  <table   border='0' cellspacing='0' cellpadding='0' >
                  	<tr>
                  <td   style='font-size: 13px; color: #333; border-right: 1px dotted #ccc; padding-right: 50px;' >
                  <b>Shipping Details:</b>    <br />        Order No :" + OrderID + "<br />  Shipping Date :" + shipdate + "<br /> Courier Service Provider :" + CourServProv + "<br /> Courier Track No :" + CourTrackNo + "<br /> Courier Track Link :" + CourTrackLink + "</td>";

                mailBody += "<td style='font-size: 13px; color: #333 ;  padding-left: 50px; ' >";
                mailBody += "<b>Shipping Address:</b>    <br />Name:" + ShippingName + "<br />Address: " + ShippingAddress + "<br /> City:" + ShippingCity + "<br /> State:" + ShippingState + "<br /> Mobile:" + strMobileNo + "<br /> ";
                mailBody += "</td>";
                mailBody += "</tr>";
                mailBody += "</table>";
                mailBody += "</td>";
                mailBody += "</tr>";

                mailBody += @"<br /><br />
                                             
                        </font></td>             
                        </tr>  						
                        <tr>
        	            <td style='padding: 10px;'>
        		        <table style='border-collapse: collapse' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100.0%'>
                        <tbody>";

                int k = 0;


                foreach (cart_item ord in ordlist)
                {

                    for (int s = 0; s < partial_order_prods.Count(); s++)
                    {

                        if ((ord.id.ToString() == partial_order_prods[s].id.ToString()) && (ord.sku.ToString() == partial_order_prods[s].sku.ToString()))
                        {

                            k = k + 1;

                            mailBody += "  <tr>";
                            mailBody += "  <td colspan='6'  style= 'font-family:Helvetica, Arial, sans-serif; padding-bottom:10px; padding-top:10px;   font-size:12px; text-transform:uppercase'>";
                            mailBody += "  <strong>" + k.ToString() + " Product Code: " + ord.id + "-" + ord.name + "";
                            mailBody += "  </td>   ";
                            mailBody += "  <td style='padding:0cm 0cm 0cm 0cm'></td>";
                            mailBody += "  </tr>";
                            mailBody += "  <tr>";
                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'></span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:red'><img src='" + ord.image.thumb_link + "' /></span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";

                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Size: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + partial_order_prods[s].selected_size.ToString() + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";

                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Order Quantity: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + ord.quantity.ToString() + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";

                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Shipping Quantity: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + partial_order_prods[s].shipping_qty.ToString() + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";

                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>MRP: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + ord.mrp + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";


                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Discount: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + ord.discount + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";

                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Unit Price: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + ord.final_offer + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";


                            if (ord.quantity == 0)
                            {
                                ord.final_offer = 0;

                            }
                            else
                            {

                                mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif; padding: 5px;' >";
                                mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                                mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Sub Total:</span></p>";
                                mailBody += "  <div style='margin-top:7.5pt'>";
                                mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + (ord.final_offer * convertToInt(partial_order_prods[s].shipping_qty.ToString())) + "</span></p>";
                                mailBody += "  </div> ";
                                mailBody += "  </div> ";
                                mailBody += "  </td>  ";
                                mailBody += "  </tr>  ";
                            }

                            shipmentvalue = shipmentvalue + Convert.ToDouble(ord.final_offer * convertToInt(partial_order_prods[s].shipping_qty.ToString()));
                            discount = discount + Convert.ToDouble(ord.discount);
                            finalamt = shipmentvalue;

                        }
                    }

                }

                if (finalamt > Convert.ToDouble(ShippingThreshold))
                {
                    shippingAmount = "Free";
                }
                else


                    finalamt = finalamt + Convert.ToDouble(shippingAmount);



                mailBody += @" </td>   
                       </tr>
                     <tr>                            
                     <td>&nbsp;</td>
                    </tr>  
                   <tr>
                  <td style='text-align: right; border-top: 1px dotted #ccc; font-family:'arial' ;  padding:10px 0px;' colspan='5' >
                     <div style=' width:100%; float:left;  padding:5px 0px '>
                        <div style='float: right;  text-align:left'>
                           <div style='font-weight: bold;  padding-right:20px; font-size:12px; color:#333333;'>Shipment Value </div>
						   <div style='font-weight: bold;  padding-right:20px; font-size:12px; color:#333333;'>Amount paid   </div>
                           
                        </div>
                     </div>
                  </td>
                  <td style='text-align: right; border-top: 1px dotted #ccc;'>
                     <div style=' padding:5px 0px '>
                        <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>##SHIP_VALUE##</div>
						<div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>##TOTAL_AMT##</div>
                       
                     </div>
                  </td>
                  </tr>
                   </tbody>
                      </table>
                        </td>
                        </tr>";



                mailBody += @"<br /><br />
                                             
                        </font></td>             
                        </tr>  						
                        <tr>
        	            <td style='padding: 10px;'>
        		        <table style='border-collapse: collapse' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100.0%'>
                        <tbody>";
                int ppc = 0;
                if (dtPartialOrderResult != null && dtPartialOrderResult.Rows.Count > 0)
                {
                    int x = 0;
                    for (int l = 0; l < dtPartialOrderResult.Rows.Count; l++)
                    {
                        if (convertToInt(dtPartialOrderResult.Rows[l]["item_status"]) == 0)
                        {
                            if (x == 0)
                            {
                                mailBody += "  <tr>";
                                mailBody += "  <td colspan='6'  style= 'font-family:Helvetica, Arial, sans-serif; padding-bottom:10px; padding-top:10px;   font-size:12px; text-transform:uppercase'>";
                                mailBody += "  <strong>" + " Pending Items:  ";
                                mailBody += "  </td>   ";
                                mailBody += "  <td style='padding:0cm 0cm 0cm 0cm'></td>";
                                mailBody += "  </tr>";
                                mailBody += "  <tr>";
                                x++;
                            }

                            ppc = ppc + 1;

                            int remain_qty = (int)convertToDouble(dtPartialOrderResult.Rows[l]["quantity"].ToString()) - convertToInt(dtPartialOrderResult.Rows[l]["del_qty"]);

                            mailBody += "  <tr>";
                            mailBody += "  <td colspan='6'  style= 'font-family:Helvetica, Arial, sans-serif; padding-bottom:10px; padding-top:10px;   font-size:12px; text-transform:uppercase'>";
                            mailBody += "  <strong>" + ppc.ToString() + " Product Code: " + convertToString(dtPartialOrderResult.Rows[l]["id"]) + "-" + convertToString(dtPartialOrderResult.Rows[l]["name"]) + "";
                            mailBody += "  </td>   ";
                            mailBody += "  <td style='padding:0cm 0cm 0cm 0cm'></td>";
                            mailBody += "  </tr>";
                            mailBody += "  <tr>";
                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'></span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:red'><img src='" + convertToString(dtPartialOrderResult.Rows[l]["thumb_link"]) + "' /></span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";

                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Size: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + convertToString(dtPartialOrderResult.Rows[l]["selected_size"]) + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";

                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Order Quantity: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + convertToString(dtPartialOrderResult.Rows[l]["quantity"]) + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";

                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Remaining Quantity: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + remain_qty.ToString() + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";

                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>MRP: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + convertToString(dtPartialOrderResult.Rows[l]["mrp"]) + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";


                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Discount: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + convertToString(dtPartialOrderResult.Rows[l]["discount"]) + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";

                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Unit Price: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + convertToString(dtPartialOrderResult.Rows[l]["final_offer"]) + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";

                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Sub Total: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + convertToString(convertToDouble(dtPartialOrderResult.Rows[l]["final_offer"]) * remain_qty) + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";

                        }
                    }

                }


                mailBody += @" </tbody>
                      </table>
                        </td>
                        </tr>";



                mailBody += @"<tr>                            
                        <td>&nbsp;</td>
                        </tr>                      
                        <td>&nbsp;</td>
                        </tr>  
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>             
                        
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>";




                mailBody += @"<br />
                                                
                        </font></td>             
                        </tr>             
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid ##EMAIL_TEMP_COLOR##;'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>     
                        <td style='padding: 10px;' align='left' valign='top'>
                        <font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'>
                        <strong>Regards</strong> ,<br/>
                        <strong>Team GRASS</strong>            
                        </font>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>               
                        <tr>
                        <td align='center'><font style='font-family:Helvetica, Arial, sans-serif; color:#231f20; font-size:12px'>Copyright &copy; 2014 Annectos.All rights reserved.</font></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align='center'>
                        <font style=' font-family:Helvetica, Arial, sans-serif; color:##EMAIL_TEMP_COLOR##; font-size:12px; text-transform:uppercase'><a href='##COMP_LINK##' style='color:##EMAIL_TEMP_COLOR##; text-decoration:none'target='_blank' >##COMP_LINK##</a></font>
                        </td>
                        </tr>
                        <tr>
                        <td style='padding: 10px;padding-left:600px' align='center'><a href= '####' target='_blank'><img src='##POWERED_LOGO##'  border='0' alt=''/></a></td>                       
                        </tr>
                        </table></td>
                        </tr> 
                        </table>
                        </body>
                        </html> ";


                //strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/pumaenq.png";
                //poweredbyLogo = "##POWERED_LOGO##";

                string email_temp_color = ConfigurationSettings.AppSettings["EMAIL_TEMP_COLOR"].ToString();

                poweredbyLogo = ConfigurationSettings.AppSettings["POWERED_BY"].ToString();

                mailBody = mailBody.Replace("##COMP_LINK##", strCompanyUrl);
                mailBody = mailBody.Replace("##COMP_LOGO##", strLogo);
                mailBody = mailBody.Replace("##ORDER_ID##", OrderID);
                mailBody = mailBody.Replace("##TOTAL_AMT##", convertToString(finalamt));
                mailBody = mailBody.Replace("##FIRST_NAME##", ShippingName);
                mailBody = mailBody.Replace("##SHIP_VALUE##", convertToString(shipmentvalue));
                mailBody = mailBody.Replace("##POWERED_LOGO##", poweredbyLogo);
                mailBody = mailBody.Replace("##EMAIL_TEMP_COLOR##", email_temp_color);

                PartSubject = PartSubject.Replace("##COMP_URL##", strCompanyUrl);
                PartSubject = PartSubject.Replace("http://", "");

                mailBody = mailBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return mailBody;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public DataTable GetPartialOrderInfo(Int64 OrderID)
        {
            try
            {
                DataAccess da = new DataAccess();
                DataTable dt = da.ExecuteDataTable("get_partial_order_info",
                         da.Parameter("_order_id", OrderID)
                        );
                return dt;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new DataTable();
            }
        }


        public string GetShippingStatusBody(string OrderID, string shipdate, string CourServProv, string CourTrackNo, string CourTrackLink, double dCourierCost, int Status, ref string email_id)
        {

            try
            {
                string mailBody = string.Empty;
                string cart_data = "";
                double shipmentvalue = 0;
                double discount = 0;
                double finalamt = 0;
                double paidamt = 0;
                string vouchers = "";
                Int64 points = 0;
                double pointamt = 0;
                double voucheramt = 0;
                double PromoOffer = 0;
                string discount_rule = "";
                string discount_code = "";
                string strShippingInfo = "";
                string strStoreName = "";
                string strLogo = "";
                string poweredbyLogo = "";

                string strMobileNo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";
                string shippingAmount = "";
                string ShippingThreshold = "0";// ConfigurationSettings.AppSettings["shipping_threshold"].ToString();
                string strCompanyUrl = "";
                shippingAmount = "0";// ConfigurationSettings.AppSettings["shipping_amount"].ToString();

                DataTable dt = GetDataFromOrder(Convert.ToInt64(OrderID));
                DataTable dtShip = GetDataFromUserID(Convert.ToInt64(OrderID));
                checkoutrepository cr = new checkoutrepository();

                if (dt == null || dt.Rows.Count == 0)
                    return "No Such order";

                if ((dtShip == null) || (dtShip.Rows.Count == 0))
                {
                    return "No Shipping Information";
                }
                else
                {
                    //ShippingName = dtShip.Rows[0]["name"].ToString();
                    //ShippingAddress = dtShip.Rows[0]["address"].ToString();
                    //ShippingCity = dtShip.Rows[0]["city"].ToString();
                    //ShippingState = dtShip.Rows[0]["state"].ToString();
                    //ShippingPin = dtShip.Rows[0]["pincode"].ToString();
                    //strMobileNo = dtShip.Rows[0]["mobile_number"].ToString();

                    ShippingName = dtShip.Rows[0]["shipping_name"].ToString();
                    ShippingAddress = dtShip.Rows[0]["address"].ToString();
                    ShippingCity = dtShip.Rows[0]["city"].ToString();
                    ShippingState = dtShip.Rows[0]["state"].ToString();
                    ShippingPin = dtShip.Rows[0]["pin"].ToString();
                    strMobileNo = dtShip.Rows[0]["mobile_no"].ToString();
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    email_id = dt.Rows[i]["user_id"].ToString();
                    cart_data = dt.Rows[i]["cart_data"].ToString();
                    paidamt = convertToDouble(dt.Rows[i]["paid_amount"]);
                    points = Convert.ToInt64(dt.Rows[i]["points"].ToString()); ;
                    pointamt = convertToDouble(dt.Rows[i]["points_amount"]); ;
                    vouchers = dt.Rows[i]["egift_vou_no"].ToString();
                    voucheramt = convertToDouble(dt.Rows[i]["egift_vou_amt"]);
                    strShippingInfo = Convert.ToString(dt.Rows[i]["shipping_info"]);
                    strStoreName = Convert.ToString(dt.Rows[i]["store"].ToString().Trim());
                    if (dt.Rows[i]["discount_code"] != null)
                    {
                        discount_code = dt.Rows[i]["discount_code"].ToString();
                    }
                    break;
                }


                if (discount_code != null || discount_code != "")
                {

                    DataTable dtDiscInfo = GetDiscountCodeInfo(discount_code);

                    if (dtDiscInfo == null || dtDiscInfo.Rows.Count > 0)
                    {
                        for (int p = 0; p < dtDiscInfo.Rows.Count; p++)
                        {
                            discount_rule = dtDiscInfo.Rows[p]["rule"].ToString();
                        }

                    }

                    disc_rule odrulelist = JsonConvert.DeserializeObject<disc_rule>(discount_rule.ToString());

                    if (odrulelist != null)
                    {
                        PromoOffer = convertToDouble(odrulelist.discount_value);
                    }
                }


                DataTable dtCompany = GetCompanyInfo(strStoreName);
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
                    strMailSubCompany = dtCompany.Rows[0]["displayname"].ToString();

                    if (dtCompany.Rows[0]["shipping_charge"] == null)
                        shippingAmount = "0";
                    else
                        shippingAmount = dtCompany.Rows[0]["shipping_charge"].ToString();

                    if (dtCompany.Rows[0]["min_order_for_free_shipping"] == null)
                        ShippingThreshold = "0";
                    else
                        ShippingThreshold = dtCompany.Rows[0]["min_order_for_free_shipping"].ToString();

                    if (shippingAmount == "")
                        shippingAmount = "0";

                    if (ShippingThreshold == "")
                        ShippingThreshold = "0";


                }



                if (cart_data.Length == 0)
                    return "Invalid Order";


                //List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());
                List<cart_item> ordlist = cr.cart_items_to_item(JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString()));

                mailBody += "<html>";
                mailBody += "<head>";
                mailBody += "</head>";
                mailBody += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                mailBody += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #f5f5f5'>";
                mailBody += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                mailBody += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                mailBody += "<div style='padding: 5px;'>";
                mailBody += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                mailBody += "		            <tr>";
                mailBody += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                //mailBody += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                mailBody += "			            <td style='width: 300px;;'></td>";
                //mailBody += "			            <td style='width: 35px;' ><a href='https://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                //mailBody += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                //mailBody += "			            <td style='width: 35px;'><a href='http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                mailBody += "		            </tr>";
                mailBody += "		            </table>";
                mailBody += "		            </div>";
                mailBody += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                mailBody += "			            <table cellpadding='0' cellspacing='0'  >";
                mailBody += "		            <tr>";
                //mailBody += "		            <td> <img src='img/emails/shiped.png' /></td>";
                //mailBody += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'> Free Shipping for purchase above 500</td>";
                mailBody += "		            </tr>";
                mailBody += "		            </table>";
                mailBody += "		            </div>";
                mailBody += "            </div>";
                mailBody += "        </td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        Hi " + ShippingName + ", <br /><br />";
                mailBody += "";
                mailBody += "        We would like to inform you that your " + strCompanyUrl + " order has been shipped from our warehouse. You will receive your order over the next few days.<br /><br />";
                mailBody += "        <div style='font-size: 12px; color: #333'>";
                mailBody += "        <b>Shipping Details:</b>    <br />";
                mailBody += "        Order No : " + OrderID + "<br />";

                /*
                mailBody += "        Shipment ID : " + OrderID + "  <br />";
                mailBody += "        Courier Service Provider : annectos logistics<br />";    
                mailBody += "        Tracking Number : AW0001022536 <br />     <br />"; 
                 */



                mailBody += "        Shipping Date : " + shipdate + "  <br />";
                mailBody += "        Courier Service Provider : " + CourServProv + "  <br />";
                mailBody += "        Courier Track No : " + CourTrackNo + "           <br />";
                mailBody += "        Courier Track Link : " + CourTrackLink + "       <br />";
                //mailBody += "        Courier Cost : " + dCourierCost + "       <br />";




                mailBody += "        <b>Shipping Address:</b>    <br />";
                mailBody += "        " + ShippingName + " <br />";
                mailBody += "        " + ShippingAddress + " <br />";
                mailBody += "        " + ShippingCity + "<br />    ";
                mailBody += "        " + ShippingState + " - " + ShippingPin + "<br />";
                mailBody += "        Mobile No : " + strMobileNo + "<br />     <br /> ";
                mailBody += "        </div>";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "        <tr>";
                mailBody += "        <td style='background: #fff; padding: 0px 10px;  font-size: 16px;color:#3b3a3a; font-weight:bold '>";
                mailBody += "        <div style='border-top:1px dotted #cccccc; border-bottom:1px dotted #cccccc; padding-top: 5px; padding-bottom: 5px;'>";
                mailBody += "        Order No. #" + OrderID;
                mailBody += "        </div>";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        <table cellpadding='0' cellspacing='0' width='100%' >";

                int k = 0;
                //Loop for Product - Start
                foreach (cart_item ord in ordlist)
                {
                    k = k + 1;

                    mailBody += "            <tr>";
                    mailBody += "             <td colspan='6' style='color: #333333; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold' >" + k.ToString() + " Product Code: " + ord.sku + " - " + ord.name + "</td>	";
                    mailBody += "            </tr>";


                    mailBody += "            <tr>";
                    mailBody += "             <td style='text-align: center; padding-left: 10px; padding-right: 10px;'  ><img src='" + ord.image.thumb_link + "' /></td>	";
                    mailBody += "             <td style='color: #666666; font-size: 13px; padding-left: 10px; padding-right: 10px; text-align: center;'>Size <div style='margin-top: 10px; color: #333333'>" + ord.sizes[0].size_name + "</div></td>";
                    mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Quantity <div style='margin-top: 10px; color: #333333; '>" + ord.quantity + "</div></div></td>";
                    mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Unit Price <div style='margin-top: 10px; color: #333333'>" + ord.final_offer + "</div></div></td>";
                    mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Discount <div style='margin-top: 10px; color: #ff0000'>" + ord.discount + "</div></div></td> ";

                    if (ord.quantity == 0)
                    {
                        ord.final_offer = 0;

                    }
                    else
                    {

                        mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'> <div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Sub Total <div style='margin-top: 10px; color: #333333'>" + ord.quantity + " * " + ord.final_offer + " = "+(ord.final_offer * ord.quantity) + "</div></div></td>";
                        mailBody += "            </tr>";

                    }
                  

                    shipmentvalue = shipmentvalue + Convert.ToDouble(ord.final_offer*ord.quantity);
                    discount = discount + Convert.ToDouble(ord.discount);
                    finalamt = shipmentvalue;
                }
                if (finalamt > Convert.ToDouble(ShippingThreshold))
                {
                    shippingAmount = "Free";
                }
                else
                    finalamt = finalamt + Convert.ToDouble(shippingAmount);




                //Loop for Product - End 
                /*
                mailBody += "            <tr>";
                mailBody += "             <td colspan='6' style='color: #333333; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold' >2. Product Code: 00328 - MeeMee Baby Carrier (Black)</td>	";
                mailBody += "            </tr>";
            
                mailBody += "            <tr>";
                mailBody += "             <td style='text-align: center; padding-left: 10px; padding-right: 10px;'  ><img src='img/emails/product2.png' /></td>	";
                mailBody += "             <td style='color: #666666; font-size: 13px; padding-left: 10px; padding-right: 10px; text-align: center;'>Size <div style='margin-top: 10px; color: #333333'>-</div></td>";
                mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Quantity <div style='margin-top: 10px; color: #333333; '>1</div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Unit Price <div style='margin-top: 10px; color: #333333'>1200 </div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Discount <div style='margin-top: 10px; color: #ff0000'>201</div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'> <div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Sub Total <div style='margin-top: 10px; color: #333333'>999</div></div></td>";
                mailBody += "            </tr>";
                 */
                mailBody += "            <tr>";
                mailBody += "	            <td style='text-align: right; padding:10px 0px;' colspan='5' >";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; width:100%; float:left;  padding:5px 0px '>";
                mailBody += "	            <div style='float: right; text-align:left'>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Shipment Value </div>";
                //mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Discount   </div> ";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Shipping   </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Points Redeemed   </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Gift Voucher    </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Promo Code    </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Cash/Card Paid  </div>";
                mailBody += "		            </div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "	            <td style='text-align: right'>";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; padding:5px 0px '>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>" + shipmentvalue.ToString() + "</div>";
                //mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#ff0000; '>" + discount.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + shippingAmount.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + points.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + voucheramt.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + PromoOffer.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + paidamt.ToString() + "</div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "            </tr>";
                mailBody += "            <tr>";
                mailBody += "	            <td valign='top' colspan='5' >";
                mailBody += "	            <div style='float: right; text-align:left; border-top: 1px dotted #ccc; width: 110px; padding-left: 40px; padding-top:5px;'>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Amount paid   </div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "	            <td  valign='top'  style='text-align: right'>";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; padding:5px 0px '>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>" + finalamt.ToString() + "</div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "            </tr>";
                /*
                mailBody += "            <tr>";
                mailBody += "	            <td valign='top' style='font-size: 12px; padding-top:20px; color: #333333' colspan='6' >";
                mailBody += "	            <b>Shipping Address:</b>    <br />";
                mailBody += ShippingName + " <br />    ";
                mailBody += ShippingAddress + ",  <br />    ";
                mailBody += ShippingCity + " <br />    ";
                mailBody += ShippingState + " - " + ShippingPin + " <br />     <br />";
                //mailBody += "You can track or manage your order at any time by going to <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/myaccounts.php?view=myorders</a>. If you need any assistance or have any questions, feel free to contact us by using the web form at <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/contactus</a> or call us at +91 9686202046 | +91 9972334590";
                mailBody += "If you need any assistance or have any questions, feel free to contact us by using the web form at <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/contactus</a> or call us at +91 9686202046 | +91 9972334590";
                mailBody += "	            </td>";
                mailBody += "	            </tr>";
                */
                mailBody += "        </table>";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "<tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";

                //Promotion/Marketting
                /*
                mailBody += "        <div style='font-size: 18px; font-weight:bold; color:#333333; text-transform:uppercase; line-height:30px; border-bottom: 1px solid #ccc'>You may also consider</div>";
                mailBody += "        <div style='padding-top: 10px'>";
                mailBody += "            <table cellspacing='0' cellpadding='5' style='width: 100%'>";
                mailBody += "	            <tr>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer1.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer2.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer3.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "	            <tr>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>Rooptex Kurthi</div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>Grass Black Crew Neck ";
                mailBody += "Sporty T Shirt</div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>US Polo Assn Blue Shirt</div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "	            <tr>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "            </table>";
                mailBody += "        </div>";
                 */
                mailBody += "        </td>";
                mailBody += "</tr>";
                /*mailBody += "<tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        <table style='width: 100%' cellpadding='0' cellspacing='0'>";
                mailBody += "            <tr>";
                mailBody += "	            <td align='left'><a href='#'><img src='img/emails/add1.png' /></a></td>";
                mailBody += "	            <td align='right'><a href='#'><img src='img/emails/add2.png' /></a></td>";
                mailBody += "            </tr>";
                mailBody += "        </table>";
                mailBody += "        </td>";
                mailBody += "        </tr>";*/
                mailBody += "</table>";
                /*mailBody += "<table cellpadding='0' cellspacing='0' width='500' align='center'  style=' font-size: 12px; color: #666666'>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px;' align='center'>customerfirst@annectos.in   call us at +91 9686202046 | +91 9972334590";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px; vertical-align: top;' align='center'>Connect With Us <a href='#'><img width='18' height='18' src='img/emails/facebook.png' /></a>  <a href='#'><img width='18' height='18' src='img/emails/twitter.png' /></a>  <a href='#'><img width='18' height='18' src='img/emails/linkedin.png' /></a>";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px; font-size: 10px;' align='center'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. All Rights Reserved. www.annectos.in";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "</table>";*/
                mailBody += "</body>";
                mailBody += "</html>";


                mailBody = mailBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return mailBody;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string GetDelayStatusBody(string OrderID, string delay_reason, string DelayDays, int Status, ref string email_id)
        {

            try
            {
                string strBodyEmail = string.Empty;


                string strMobileNo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";

                string strLogo = "";
                string strCompanyUrl = "";
                string strStoreName = "";

                DataTable dt = GetDataFromOrder(Convert.ToInt64(OrderID));
                DataTable dtShip = GetDataFromUserID(Convert.ToInt64(OrderID));

                if (dt == null || dt.Rows.Count == 0)
                    return "No Such order";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    email_id = dt.Rows[i]["user_id"].ToString();
                    strStoreName = Convert.ToString(dt.Rows[i]["store"].ToString().Trim());
                    break;
                }

                if ((dtShip == null) || (dtShip.Rows.Count == 0))
                {
                    return "No Shipping Information";
                }
                else
                {
                    ShippingName = dtShip.Rows[0]["name"].ToString();
                    ShippingAddress = dtShip.Rows[0]["address"].ToString();
                    ShippingCity = dtShip.Rows[0]["city"].ToString();
                    ShippingState = dtShip.Rows[0]["state"].ToString();
                    ShippingPin = dtShip.Rows[0]["pincode"].ToString();
                    strMobileNo = dtShip.Rows[0]["mobile_number"].ToString();

                }

                DataTable dtCompany = GetCompanyInfo(strStoreName);
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
                }




                strBodyEmail += "<html>";
                strBodyEmail += "<head>";
                strBodyEmail += "</head>";
                strBodyEmail += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                strBodyEmail += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #f5f5f5'>";
                strBodyEmail += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                strBodyEmail += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                strBodyEmail += "<div style='padding: 5px;'>";
                strBodyEmail += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                //strBodyEmail += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                strBodyEmail += "			            <td style='width: 300px;;'></td>";
                //strBodyEmail += "			            <td style='width: 35px;' ><a href='https://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                //strBodyEmail += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                //strBodyEmail += "			            <td style='width: 35px;'><a href='http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                strBodyEmail += "			            <table cellpadding='0' cellspacing='0'  >";
                strBodyEmail += "		            <tr>";
                //strBodyEmail += "		            <td> <img src='img/emails/shiped.png' /></td>";
                //strBodyEmail += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'> Free Shipping for purchase above 500</td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "            </div>";
                strBodyEmail += "        </td>";
                strBodyEmail += "    </tr>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "        Hi " + ShippingName + ", <br /><br />";
                strBodyEmail += "";
                strBodyEmail += "        <div style='font-size: 12px; color: #333'>";
                strBodyEmail += "Your order number :" + OrderID + " has been delayed by " + DelayDays + " days. <br />";
                strBodyEmail += "Reason : " + delay_reason + "<br />";
                strBodyEmail += "<br/>";
                strBodyEmail += "Our customer service representative will call you within the next business day to update you on the order status.";
                strBodyEmail += "<br/>";
                strBodyEmail += "If you have any questions, please feel free to contact us at customerfirst@grassapparels.in or by phone at +91 9686202046 /9972334590 (9:30am - 9:30 pm, Mon – Fri)";
                strBodyEmail += "<br/>";
                strBodyEmail += "Regards,";
                strBodyEmail += "<br/>";
                strBodyEmail += "<br/>";
                strBodyEmail += "annectoś Support Team";
                strBodyEmail += "</div>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "<tr>";
                strBodyEmail += "<td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "</table>";

                strBodyEmail += "</body>";
                strBodyEmail += "</html>";

                strBodyEmail = strBodyEmail.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return strBodyEmail;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }

        }

        public string GetCancelStatusBody(string OrderID, string cancel_reason, string order_status,string warehouse, int Status, ref string email_id)
        {

            try
            {
                string mailBody = string.Empty;

                string strMobileNo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";
                string poweredbyLogo = "";
                string strLogo = "";
                string strCompanyUrl = "";
                string strStoreName = "";
                string cart_data = "";

                DataTable dt = GetDataFromOrder(Convert.ToInt64(OrderID));
                DataTable dtShip = GetDataFromUserID(Convert.ToInt64(OrderID));

                if (dt == null || dt.Rows.Count == 0)
                    return "No Such order";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    email_id = dt.Rows[i]["user_id"].ToString();
                    strStoreName = Convert.ToString(dt.Rows[i]["store"].ToString().Trim());
                    break;
                }

                if ((dtShip == null) || (dtShip.Rows.Count == 0))
                {
                    return "No Shipping Information";
                }
                else
                {
                    //ShippingName = dtShip.Rows[0]["name"].ToString();
                    //ShippingAddress = dtShip.Rows[0]["address"].ToString();
                    //ShippingCity = dtShip.Rows[0]["city"].ToString();
                    //ShippingState = dtShip.Rows[0]["state"].ToString();
                    //ShippingPin = dtShip.Rows[0]["pincode"].ToString();
                    //strMobileNo = dtShip.Rows[0]["mobile_number"].ToString();

                    ShippingName = dtShip.Rows[0]["shipping_name"].ToString();
                    ShippingAddress = dtShip.Rows[0]["address"].ToString();
                    ShippingCity = dtShip.Rows[0]["city"].ToString();
                    ShippingState = dtShip.Rows[0]["state"].ToString();
                    ShippingPin = dtShip.Rows[0]["pin"].ToString();
                    strMobileNo = dtShip.Rows[0]["mobile_no"].ToString();

                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    email_id = dt.Rows[i]["user_id"].ToString();
                    cart_data = dt.Rows[i]["cart_data"].ToString();


                }

                DataTable dtCompany = GetCompanyInfo(strStoreName);
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
                }


                update_product_stock(warehouse, order_status, JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString()));


           /*     strBodyEmail += "<html>";
                strBodyEmail += "<head>";
                strBodyEmail += "</head>";
                strBodyEmail += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                strBodyEmail += "<table cellpadding='0' cellspacing='0' width='100%' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #f5f5f5'>";
                strBodyEmail += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                strBodyEmail += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                strBodyEmail += "<div style='padding: 5px;'>";
                strBodyEmail += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
             //   strBodyEmail += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                strBodyEmail += "			            <td style='width: 300px;;'></td>";
                //strBodyEmail += "			            <td style='width: 35px;' ><a href='https://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                //strBodyEmail += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                //strBodyEmail += "			            <td style='width: 35px;'><a href='http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                strBodyEmail += "			            <table cellpadding='0' cellspacing='0'  >";
                strBodyEmail += "		            <tr>";
                //strBodyEmail += "		            <td> <img src='img/emails/shiped.png' /></td>";
                //strBodyEmail += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'> Free Shipping for purchase above 500</td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "            </div>";
                strBodyEmail += "        </td>";
                strBodyEmail += "    </tr>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "        Hi " + ShippingName + ", <br /><br />";
                strBodyEmail += "";
                strBodyEmail += "        <div style='font-size: 12px; color: #333'>";
                strBodyEmail += "<br/>";
                strBodyEmail += "Your order number : " + OrderID + " has been canceled. <br />";
                strBodyEmail += "Reason : " + cancel_reason + "<br />";
                strBodyEmail += "<br/>";
                strBodyEmail += "Our customer service representative will call you within the next business day to update you on the order status.";
                strBodyEmail += "<br/>";
                strBodyEmail += "If you have any questions, please feel free to contact us at customerfirst@grassapprels.in or by phone at +91 9686202046 /9972334590 (10 am - 7 pm, Mon – Fri)";
                strBodyEmail += "<br/>";
                strBodyEmail += "<br/>";
                strBodyEmail += "Regards,";
                strBodyEmail += "<br/>";
                strBodyEmail += "annectoś Support Team";
                strBodyEmail += "</div>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "<tr>";
                strBodyEmail += "<td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "</table>";

                strBodyEmail += "</body>";
                strBodyEmail += "</html>"; */


                mailBody = @"
                        <html>
                        <body bgcolor='#8d8e90'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#8d8e90'>
                        <tr>
                        <td><table width='800' border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' align='center'>
                        <tr>
                        <td style='border-bottom:5px solid #f1613b; padding: 10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>              
                        <td width='144'><a href= '##GRASS_LINK##' target='_blank'><img src='##GRASS_LOGO##'  border='0' alt=''/></a></td>
                        <td width='393'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td height='46' align='right' valign='middle'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='67%' align='left'><font style='font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#68696a; font-size:18px; text-transform:uppercase'></td>                         
                        <td width='4%'>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr>                  
                        </table></td>
                        </tr>
                        </table></td>
                        </tr>
                        <tr>
                        <td align='center'>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='5%'>&nbsp;</td>
                        <td width='90%' align='left' valign='top'><font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:24px'><strong><em>Hi ##FIRST_NAME##,</em></strong></font><br /><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>               
                        Your order number <strong style='color: #ff0000'></strong> ##ORDER_ID## has been canceled. </font></br> 
                        
						 <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>               
                        Reason: <strong style='color: #ff0000'></strong>##CANCEL_REASON##</font></br></br>
                       
						 <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>               
                        Our customer service representative will call you within the next business day to update you on the order status.<strong style='color: #ff0000'></strong> </font></br></br> 
                        
						 <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>               
                        If you have any questions, please feel free to contact us at  customerfirst@grassapparels.in or by phone at +91 9686202046 /9972334590 (9:30 am - 6:30 pm, Mon – Fri)";


                mailBody += @"<br /><br />
                                                
                        </font></td>             
                        </tr>             
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>     
                        <td style='padding: 10px;' align='left' valign='top'>
                        <font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'>
                        <strong>Regards</strong> ,<br/>
                        <strong>Team GRASS</strong>            
                        </font>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>               
                        <tr>
                        <td align='center'><font style='font-family:Helvetica, Arial, sans-serif; color:#231f20; font-size:12px'>Copyright &copy; 2014 Annectos.All rights reserved.</font></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align='center'>
                        <font style=' font-family:Helvetica, Arial, sans-serif; color:#f1613b; font-size:12px; text-transform:uppercase'><a href='http://grass.shopinbulk.in' style='color:#f1613b; text-decoration:none'target='_blank' >http://grass.shopinbulk.in</a></font>
                        </td>
                        </tr>
                        <tr>
                        <td style='padding: 10px;padding-left:600px' align='center'><a href= '####' target='_blank'><img src='http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/annectostemp.png'  border='0' alt=''/></a></td>                        
                        </tr>
                        </table></td>
                        </tr> 
                        </table>
                        </body>
                        </html> ";

                strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";
                poweredbyLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/annectostemp.png";



                mailBody = mailBody.Replace("##GRASS_LINK##", "http://grass.shopinbulk.in");
                mailBody = mailBody.Replace("##GRASS_LOGO##", strLogo);
                mailBody = mailBody.Replace("##ORDER_ID##", OrderID);
                // mailBody = mailBody.Replace("##DELAY_DAY##", DelayDays);
                mailBody = mailBody.Replace("##POWERED_BY##", poweredbyLogo);
                // mailBody = mailBody.Replace("##TOTAL_AMT##", convertToString(finalamt));
                mailBody = mailBody.Replace("##FIRST_NAME##", ShippingName);
                mailBody = mailBody.Replace("##CANCEL_REASON##", cancel_reason);
                // mailBody = mailBody.Replace("##SHIP_VALUE##", convertToString(shipmentvalue));




                mailBody = mailBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");

                return mailBody;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }




        }

        public string GetOrderTrackBody(string strStoreName)
        {

            try
            {
                string strBodyEmail = string.Empty;


                string strLogo = "";
                string strCompanyUrl = "";

                DataTable dtCompany = GetCompanyInfo(strStoreName);
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
                }


                strBodyEmail += "<html>";
                strBodyEmail += "<head>";
                strBodyEmail += "</head>";
                strBodyEmail += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                strBodyEmail += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #f5f5f5'>";
                strBodyEmail += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                strBodyEmail += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                strBodyEmail += "<div style='padding: 5px;'>";
                strBodyEmail += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                //strBodyEmail += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                strBodyEmail += "			            <td style='width: 300px;;'></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;'><a href='http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                strBodyEmail += "			            <table cellpadding='0' cellspacing='0'  >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "		            <td> <img src='img/emails/shiped.png' /></td>";
                strBodyEmail += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'> Free Shipping for purchase above 500</td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "            </div>";
                strBodyEmail += "        </td>";
                strBodyEmail += "    </tr>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "        Hi There , <br /><br />";
                strBodyEmail += "";
                strBodyEmail += "        <div style='font-size: 12px; color: #333'>";
                strBodyEmail += "Please find the attached list of the orders which are still in Order Received Status after 24 Hours. <br />";
                strBodyEmail += "Required your attention immediately.<br />";
                strBodyEmail += "<br/>";
                strBodyEmail += "<br/>";
                strBodyEmail += "Regards,";
                strBodyEmail += "<br/>";
                strBodyEmail += "<br/>";
                strBodyEmail += "annectoś Support Team";
                strBodyEmail += "</div>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "<tr>";
                strBodyEmail += "<td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "</table>";

                strBodyEmail += "</body>";
                strBodyEmail += "</html>";

                strBodyEmail = strBodyEmail.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return strBodyEmail;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }

        }

        public string GetWillShipOrderTrackBody(string strStoreName)
        {

            try
            {
                string strBodyEmail = string.Empty;


                string strLogo = "";
                string strCompanyUrl = "";

                DataTable dtCompany = GetCompanyInfo(strStoreName);
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
                }


                strBodyEmail += "<html>";
                strBodyEmail += "<head>";
                strBodyEmail += "</head>";
                strBodyEmail += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                strBodyEmail += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #f5f5f5'>";
                strBodyEmail += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                strBodyEmail += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                strBodyEmail += "<div style='padding: 5px;'>";
                strBodyEmail += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                //strBodyEmail += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                strBodyEmail += "			            <td style='width: 300px;;'></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;'><a href='http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                strBodyEmail += "			            <table cellpadding='0' cellspacing='0'  >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "		            <td> <img src='img/emails/shiped.png' /></td>";
                strBodyEmail += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'> Free Shipping for purchase above 500</td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "            </div>";
                strBodyEmail += "        </td>";
                strBodyEmail += "    </tr>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "        Hi There , <br /><br />";
                strBodyEmail += "";
                strBodyEmail += "        <div style='font-size: 12px; color: #333'>";
                strBodyEmail += "Please find the attached list of the orders which are still in Will Ship Status after Minimum Shipping Days. <br />";
                strBodyEmail += "Required your attention immediately.<br />";
                strBodyEmail += "<br/>";
                strBodyEmail += "<br/>";
                strBodyEmail += "Regards,";
                strBodyEmail += "<br/>";
                strBodyEmail += "<br/>";
                strBodyEmail += "annectoś Support Team";
                strBodyEmail += "</div>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "<tr>";
                strBodyEmail += "<td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "</table>";

                strBodyEmail += "</body>";
                strBodyEmail += "</html>";

                strBodyEmail = strBodyEmail.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return strBodyEmail;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }

        }

        public string GetPaymentReceivedBody(string OrderID, string shipdate, string CourServProv, string CourTrackNo, string CourTrackLink, string warehouse, string order_status, double dCourierCost, int Status, ref string email_id)
        {

            try
            {
                string mailBody = string.Empty;
                string cart_data = "";
                double shipmentvalue = 0;
                double discount = 0;
                double finalamt = 0;
                double paidamt = 0;
                double special_amt = 0;
                double final_amt_paid = 0;
                string vouchers = "";
                Int64 points = 0;
                double pointamt = 0;
                double voucheramt = 0;
                double PromoOffer = 0;
                string discount_rule = "";
                string discount_code = "";
                string strShippingInfo = "";
                string strStoreName = "";
                string strLogo = "";
                string poweredbyLogo = "";
                int order_quantity = 0;
                string pr_qnty;
                string prod_name = "";
                string strMobileNo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";
                string shippingAmount = "";
                string ShippingThreshold = "0";// ConfigurationSettings.AppSettings["shipping_threshold"].ToString();
                string strCompanyUrl = "";
                List<int> lst_qnty = new List<int>();
                shippingAmount = "0";// ConfigurationSettings.AppSettings["shipping_amount"].ToString();

                DataTable dt = GetDataFromOrder(Convert.ToInt64(OrderID));
                DataTable dtShip = GetDataFromUserID(Convert.ToInt64(OrderID));
                checkoutrepository cr = new checkoutrepository();


                if (dt == null || dt.Rows.Count == 0)
                    return "No Such order";

                if ((dtShip == null) || (dtShip.Rows.Count == 0))
                {
                    return "No Shipping Information";
                }
                else
                {
                    //ShippingName = dtShip.Rows[0]["name"].ToString();
                    //ShippingAddress = dtShip.Rows[0]["address"].ToString();
                    //ShippingCity = dtShip.Rows[0]["city"].ToString();
                    //ShippingState = dtShip.Rows[0]["state"].ToString();
                    //ShippingPin = dtShip.Rows[0]["pincode"].ToString();
                    //strMobileNo = dtShip.Rows[0]["mobile_number"].ToString();

                    ShippingName = dtShip.Rows[0]["shipping_name"].ToString();
                    ShippingAddress = dtShip.Rows[0]["address"].ToString();
                    ShippingCity = dtShip.Rows[0]["city"].ToString();
                    ShippingState = dtShip.Rows[0]["state"].ToString();
                    ShippingPin = dtShip.Rows[0]["pin"].ToString();
                    strMobileNo = dtShip.Rows[0]["mobile_no"].ToString();
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    email_id = dt.Rows[i]["user_id"].ToString();
                    cart_data = dt.Rows[i]["cart_data"].ToString();
                    paidamt = convertToDouble(dt.Rows[i]["paid_amount"]);
                    special_amt = convertToDouble(dt.Rows[i]["special_discount"]);
                    final_amt_paid = convertToDouble(dt.Rows[i]["final_amt_paid"]);
                    points = Convert.ToInt64(dt.Rows[i]["points"].ToString()); ;
                    pointamt = convertToDouble(dt.Rows[i]["points_amount"]); ;
                    vouchers = dt.Rows[i]["egift_vou_no"].ToString();
                    voucheramt = convertToDouble(dt.Rows[i]["egift_vou_amt"]);
                    strShippingInfo = Convert.ToString(dt.Rows[i]["shipping_info"]);
                    strStoreName = Convert.ToString(dt.Rows[i]["store"].ToString().Trim());
                    if (dt.Rows[i]["discount_code"] != null)
                    {
                        discount_code = dt.Rows[i]["discount_code"].ToString();
                    }
                    break;
                }


                if (discount_code != null || discount_code != "")
                {

                    DataTable dtDiscInfo = GetDiscountCodeInfo(discount_code);

                    if (dtDiscInfo == null || dtDiscInfo.Rows.Count > 0)
                    {
                        for (int p = 0; p < dtDiscInfo.Rows.Count; p++)
                        {
                            discount_rule = dtDiscInfo.Rows[p]["rule"].ToString();
                        }

                    }

                    disc_rule odrulelist = JsonConvert.DeserializeObject<disc_rule>(discount_rule.ToString());

                    if (odrulelist != null)
                    {
                        PromoOffer = convertToDouble(odrulelist.discount_value);
                    }
                }


                DataTable dtCompany = GetCompanyInfo(strStoreName);
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
                    strMailSubCompany = dtCompany.Rows[0]["displayname"].ToString();

                    if (dtCompany.Rows[0]["shipping_charge"] == null)
                        shippingAmount = "0";
                    else
                        shippingAmount = dtCompany.Rows[0]["shipping_charge"].ToString();

                    if (dtCompany.Rows[0]["min_order_for_free_shipping"] == null)
                        ShippingThreshold = "0";
                    else
                        ShippingThreshold = dtCompany.Rows[0]["min_order_for_free_shipping"].ToString();

                    if (shippingAmount == "")
                        shippingAmount = "0";

                    if (ShippingThreshold == "")
                        ShippingThreshold = "0";


                }



                if (cart_data.Length == 0)
                    return "Invalid Order";


                // List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());
                List<cart_item> ordlist = cr.cart_items_to_item(JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString()));

                var d = ordlist;

                update_product_stock(warehouse, order_status, JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString()));


                mailBody = @"
                        <html>
                        <body bgcolor='#ffffff'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#ffffff'>
                        <tr>
                        <td><table width='800' border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' align='center'>
                        <tr>
                        <td style='border-bottom:5px solid #f1613b; padding: 10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>              
                        <td width='144'><a href= '##GRASS_LINK##' target='_blank'><img src='##GRASS_LOGO##'  border='0' alt=''/></a></td>
                        <td width='393'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td height='46' align='right' valign='middle'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='67%' align='left'><font style='font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#68696a; font-size:18px; text-transform:uppercase'><strong>Payment Confirmation</strong></td>                         
                        <td width='4%'>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr>                  
                        </table></td>
                        </tr>
                        </table></td>
                        </tr>
                        <tr>
                        <td align='center'>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='1%'>&nbsp;</td>
                        <td width='90%' align='left' valign='top'><font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#000; font-size:24px'><strong><em>Hi ##FIRST_NAME##,</em></strong></font><br /><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#68696a; font-size:13px; line-height:21px'>
                        
                        We would like to inform you that we have received the payment against the order no:  ##ORDER_ID## . You will receive your order over the next few days. You might receive a call from us to confirm your order and contact details within the next 72 hours.
                        <br /><br />
                        Here is the summary of your order: <strong style='color: #f1613b'>Order ID:</strong> ##ORDER_ID##</font></td>
                        <td width='1%'>&nbsp;</td>
                        </tr>      
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
        	            <td style='padding: 10px;'>
        		        <table style='border-collapse: collapse' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100.0%'>
                        <tbody>";



                int k = 0;
                //Loop for Product - Start




                foreach (cart_item ord in ordlist)
                {
                    k = k + 1;

                    mailBody += "  <tr>";
                    mailBody += "  <td colspan='6'  style= 'font-family:Helvetica, Arial, sans-serif; padding-bottom:10px; padding-top:10px;   font-size:12px; text-transform:uppercase'>";
                    mailBody += "  <strong>" + k.ToString() + " Product Code: " + ord.sku + "-" + ord.name + "";
                    mailBody += "  </td>   ";
                    mailBody += "  <td style='padding:0cm 0cm 0cm 0cm'></td>";
                    mailBody += "  </tr>";

                    mailBody += "  <tr>";
                    mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                    mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                    mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>IMAGE</span></p>";
                    mailBody += "  <div style='margin-top:7.5pt'>";
                    mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:red'><img src='" + ord.image.thumb_link + "' /></span></p>";
                    mailBody += "  </div>";
                    mailBody += "  </div>";
                    mailBody += "  </td>";

                    mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                    mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                    mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Size: </span></p>";
                    mailBody += "  <div style='margin-top:7.5pt'>";
                    mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + ord.sizes[0].size_name + "</span></p>";
                    mailBody += "  </div>";
                    mailBody += "  </div>";
                    mailBody += "  </td>";


                    mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                    mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                    mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Quantity: </span></p>";
                    mailBody += "  <div style='margin-top:7.5pt'>";
                    mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + ord.quantity + "</span></p>";
                    mailBody += "  </div>";
                    mailBody += "  </div>";
                    mailBody += "  </td>";

                    mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                    mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                    mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Unit Price: </span></p>";
                    mailBody += "  <div style='margin-top:7.5pt'>";
                    mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + ord.final_offer + "</span></p>";
                    mailBody += "  </div>";
                    mailBody += "  </div>";
                    mailBody += "  </td>";


                    mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                    mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                    mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Discount: </span></p>";
                    mailBody += "  <div style='margin-top:7.5pt'>";
                    mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + ord.discount + "</span></p>";
                    mailBody += "  </div>";
                    mailBody += "  </div>";
                    mailBody += "  </td>";

                    if (ord.quantity == 0)
                    {
                        ord.final_offer = 0;

                    }
                    else
                    {

                        mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif; padding: 5px;' >";
                        mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                        mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Sub Total:</span></p>";
                        mailBody += "  <div style='margin-top:7.5pt'>";
                        mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + (ord.final_offer * ord.quantity) + "</span></p>";
                        mailBody += "  </div> ";
                        mailBody += "  </div> ";
                        mailBody += "  </td>  ";
                        mailBody += "  </tr>  ";
                        mailBody += "  <br>  ";
                        mailBody += "  <br>  ";
                    }
                    shipmentvalue = shipmentvalue + Convert.ToDouble(ord.final_offer * ord.quantity);
                    discount = discount + Convert.ToDouble(ord.discount);
                    finalamt = shipmentvalue;
                }
                if (finalamt > Convert.ToDouble(ShippingThreshold))
                {
                    shippingAmount = "Free";
                }
                else
                    finalamt = finalamt + Convert.ToDouble(shippingAmount);

                if (special_amt > 0)
                {
                    mailBody += @" </td>   
                        <td  bgcolor='#fff'  >                          
                       
                       
                        </td>
                        <td  bgcolor='#fff'  style='padding:0cm 0cm 0cm 0cm'></td>
                        <td  bgcolor='#fff'  style='padding:0cm 0cm 0cm 0cm'></td>
                        <td  bgcolor='#fff'  style='padding:0cm 0cm 0cm 0cm'>                      
                        </td>
                       
                        <td  bgcolor='#fff'  style='padding:0cm 0cm 0cm 0cm'>
                         <br>
                        <div style='padding-top: 2px; padding-bottom: 2px;' >
                        <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#f1613b'>Special Discount </span></p>
                        <div >
                        <p class='MsoNormal' align='center' style='text-align:center; margin: 0px;'><span style='font-size:10.0pt;color:#f1613b'>##Special_Discount##</span></p>
                        </div>
                        </div>
                      
                         <div style='padding-top: 2px; padding-bottom: 2px;' >
                       

                        <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#f1613b'>Final Amount paid  </span></p>
                        <div >
                        <p class='MsoNormal' align='center' style='text-align:center; margin: 0px;'><span style='font-size:10.0pt;color:#f1613b'>##Final_Amt_paid##</span></p>
                        </div>
                        </div>
                       


                        </td>
                        </tr>
                        
                        </tbody>
                        </table>
                        </td>
                        </tr>
                         <tr>                            
                        <td>&nbsp;</td>
                        </tr>                      
                        <td>&nbsp;</td>
                        </tr>  
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>             
                         	

                        <font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#000; font-size:24px'>";
                    //                        mailBody += "   Shippment Value:  " + ShippingName + " <br />";
                    //                        mailBody += "    Shipping :" + ShippingAddress + ", <br />";
                    //                        mailBody += "    Points Redeemed :" + ShippingCity + " <br />";
                    //                        mailBody += "    Gift Voucher :" + ShippingState + "<br />";
                    //                        mailBody += "   Promo Code:" + strMobileNo + " ";
                    //                        mailBody += "   Check,NEFT/Card Paid:" + strMobileNo + " ";
                    //                        mailBody += "   Amount Paid:" + strMobileNo + " ";
                }

                else
                {
                    mailBody += @" </td>   
                        <td  bgcolor='#fff'  >                          
                       
                       
                        </td>
                        <td  bgcolor='#fff'  style='padding:0cm 0cm 0cm 0cm'></td>
                        <td  bgcolor='#fff'  style='padding:0cm 0cm 0cm 0cm'></td>
                        <td  bgcolor='#fff'  style='padding:0cm 0cm 0cm 0cm'>                      
                        </td>
                       
                        <td  bgcolor='#fff'  style='padding:0cm 0cm 0cm 0cm'>
                         <br>
                        <div style='padding-top: 2px; padding-bottom: 2px;' >
                        <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#f1613b'>Shipment Value </span></p>
                        <div >
                        <p class='MsoNormal' align='center' style='text-align:center; margin: 0px;'><span style='font-size:10.0pt;color:#f1613b'>##SHIP_VALUE##</span></p>
                        </div>
                        </div>
                      
                         <div style='padding-top: 2px; padding-bottom: 2px;' >
                       

                        <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#f1613b'>Total Amount paid  </span></p>
                        <div >
                        <p class='MsoNormal' align='center' style='text-align:center; margin: 0px;'><span style='font-size:10.0pt;color:#f1613b'>##TOTAL_AMT##</span></p>
                        </div>
                        </div>
                       


                        </td>
                        </tr>
                        
                        </tbody>
                        </table>
                        </td>
                        </tr>
                         <tr>                            
                        <td>&nbsp;</td>
                        </tr>                      
                        <td>&nbsp;</td>
                        </tr>  
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>             
                         	

                        <font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#000; font-size:24px'>";
                    //                        mailBody += "   Shippment Value:  " + ShippingName + " <br />";
                    //                        mailBody += "    Shipping :" + ShippingAddress + ", <br />";
                    //                        mailBody += "    Points Redeemed :" + ShippingCity + " <br />";
                    //                        mailBody += "    Gift Voucher :" + ShippingState + "<br />";
                    //                        mailBody += "   Promo Code:" + strMobileNo + " ";
                    //                        mailBody += "   Check,NEFT/Card Paid:" + strMobileNo + " ";
                    //                        mailBody += "   Amount Paid:" + strMobileNo + " ";
                }

                mailBody += @"<br /><br />
                         If you have any queries or clarifications please connect with our customer support team at 91 9686202046 / 9972334590 or email us at <a  style='color: #f1613b'  href='mailto:customerfirst@grassapparels.in ' target='_blank'>customerfirst@grassapparels.in .</a>
                       
                        You can continue shopping at <a style='color: #f1613b' href='http://grass.shopinbulk.in' target='_blank'>grass.shopinbulk.in .</a>
                        </font></td>             
  
                        </font></td>             
                        </tr>             
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>     
                        <td style='padding: 10px;' align='left' valign='top'>
                        <font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'>
                        <strong>Regards</strong> ,<br/>
                        <strong>Team GRASS</strong>            
                        </font>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>               
                        <tr>
                        <td align='center'><font style='font-family:Helvetica, Arial, sans-serif; color:#f1613b; font-size:12px'>Copyright &copy; 2014 Annectos.All rights reserved.</font></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align='center'>
                        <font style=' font-family:Helvetica, Arial, sans-serif; color:#f1613b; font-size:12px; text-transform:uppercase'><a href='http://grass.shopinbulk.in' style='color:#f1613b; text-decoration:none'target='_blank' >http://grass.shopinbulk.in</a></font>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr> 
                        </table>
                        </body>
                        </html> ";

                strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";

                mailBody = mailBody.Replace("##GRASS_LINK##", "http://grass.shopinbulk.in");
                mailBody = mailBody.Replace("##GRASS_LOGO##", strLogo);
                mailBody = mailBody.Replace("##ORDER_ID##", OrderID);
              //  mailBody = mailBody.Replace("##PAYMENT_MODE##", payment);
              //  mailBody = mailBody.Replace("##ORDER_DATE##", date);
             //   mailBody = mailBody.Replace("##TOTAL_QTY##", convertToString(totalqty));
                mailBody = mailBody.Replace("##Final_Amt_paid##", convertToString(final_amt_paid));
                mailBody = mailBody.Replace("##FIRST_NAME##", ShippingName);
                mailBody = mailBody.Replace("##Special_Discount##", convertToString(special_amt));
                mailBody = mailBody.Replace("##POWERED_LOGO##", poweredbyLogo);
                mailBody = mailBody.Replace("##TOTAL_AMT##", convertToString(finalamt));
                mailBody = mailBody.Replace("##SHIP_VALUE##", convertToString(shipmentvalue));



                mailBody = mailBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return mailBody;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public void update_product_stock(string warehouse,string order_status, List<cart_items> cart)
        {

            for (int i = 0; i < cart.Count; i++) {

                for (int j = 0; j < cart[i].child_prod_detail.Count; j++) {

                    if (cart[i].child_prod_detail[j].ord_qty != "" && cart[i].child_prod_detail[j].ord_qty != null)
                    {

                        balance_prod_stock(cart[i].child_prod_detail[j].id, convertToDouble(cart[i].child_prod_detail[j].ord_qty), cart[i].child_prod_detail[j].stock, warehouse, order_status, "Minus");

                    }
                }            
            }     
        }


        public void balance_prod_stock(string prod_id, double order_quantity, List<stock_list> prod_stock_list, string warehouse, string order_status, string Cond)
        {


            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + prod_id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product prod = new product();
                    prod.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prod.block_number = check_field(c.ToBsonDocument(), "block_number") ? convertToDouble(c.ToBsonDocument()["block_number"]) : 0;
                    prod.stock = JsonConvert.DeserializeObject<List<bulk_stock>>(c.ToBsonDocument()["stock"].ToString()); 
                    products.Add(prod);
                }

                var PrevBlockStock = convertToDouble(products[0].block_number.ToString());
                var PrevProdStock = products[0].stock;
                var NewProdStock = new List<bulk_stock>();
                
                var Curr_Block_Stock = 0.00;
                if (order_quantity > 0)
                {

                    //Decrease Block Stock
                    if (Cond == "Add")
                    {
                        Curr_Block_Stock = PrevBlockStock + order_quantity;
                    }
                    else
                    {
                        Curr_Block_Stock = PrevBlockStock - order_quantity;
                    }


                    //Warehouse wise stock minus
                    var cur_stock_val = 0.0;
                    for (int i = 0; i < PrevProdStock.Count; i++) {
                        var ord_qty = 0.0;                       
                        var cur_ord_val = order_quantity;
                        if (warehouse == PrevProdStock[i].warehouse)
                        {
                            if (convertToDouble(PrevProdStock[i].stock) > order_quantity)
                            {
                                PrevProdStock[i].stock = convertToDouble(PrevProdStock[i].stock) - order_quantity;
                                cur_ord_val = cur_ord_val - convertToDouble(PrevProdStock[i].stock);
                            }                               
                        }
                                        
                    }

                    //if (cur_stock_val != 0.0)
                    //{
                    //    for (int i = 0; i < PrevProdStock.Count; i++)
                    //    {
                    //        var ord_qty = 0.0;
                    //        var cur_ord_val = order_quantity;

                    //        if (convertToDouble(PrevProdStock[i].stock) > cur_stock_val)
                    //            {
                    //                PrevProdStock[i].stock = convertToDouble(PrevProdStock[i].stock) - order_quantity;
                    //                cur_ord_val = cur_ord_val - convertToDouble(PrevProdStock[i].stock);
                    //            }
                            
                    //    }
                    //}




                 }



                BsonDocument bd_prod = products[0].ToBsonDocument();
                var whereclause = "{id:'" + prod_id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();
                if (order_status.ToString() == "7")
                {
                    update = MongoDB.Driver.Builders.Update.Set("block_number", Curr_Block_Stock);
                }
                else
                {

                    update = MongoDB.Driver.Builders.Update.Set("block_number", Curr_Block_Stock).SetWrapped("stock", PrevProdStock);
                }
              
                string result = dal.mongo_update("product", whereclause, bd_prod, update);
                //return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");               
            }
        
        
        }


        public string GetOrderStatusBody(string OrderID, int Status, ref string email_id)
        {

            try
            {
                string mailBody = string.Empty;
                string cart_data = "";
                double shipmentvalue = 0;
                double discount = 0;
                double finalamt = 0;
                double paidamt = 0;
                double totalamount = 0;
                string vouchers = "";
                Int64 points = 0;
                double lineitemprice = 0;
                double pointamt = 0;
                double voucheramt = 0;
                double PromoOffer = 0;
                string strStoreName = "";
                string strLogo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";
                string shippingAmount = "";
                string strMobileNo = "";
                string strCompanyUrl = "";
                string strMyorderURL = "";
                string expshippingAmount = "0";
                string discount_code = "";
                string discount_rule = "";

                string ShippingThreshold = ConfigurationSettings.AppSettings["shipping_threshold"].ToString();
                shippingAmount = ConfigurationSettings.AppSettings["shipping_amount"].ToString();

                DataTable dt = GetDataFromOrder(Convert.ToInt64(OrderID));
                DataTable dtShip = GetDataFromUserID(Convert.ToInt64(OrderID));

                if (dt == null || dt.Rows.Count == 0)
                    return "No Such order";

                if ((dtShip == null) || (dtShip.Rows.Count == 0))
                {
                    return "No Shipping Information";
                }
                else
                {
                    ShippingName = dtShip.Rows[0]["name"].ToString();
                    ShippingAddress = dtShip.Rows[0]["address"].ToString();
                    ShippingCity = dtShip.Rows[0]["city"].ToString();
                    ShippingState = dtShip.Rows[0]["state"].ToString();
                    ShippingPin = dtShip.Rows[0]["pincode"].ToString();
                    strMobileNo = dtShip.Rows[0]["mobile_number"].ToString();
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    email_id = dt.Rows[i]["user_id"].ToString();
                    cart_data = dt.Rows[i]["cart_data"].ToString();
                    paidamt = Convert.ToDouble(dt.Rows[i]["paid_amount"].ToString());
                    totalamount = Convert.ToDouble(dt.Rows[i]["total_amount"].ToString());
                    points = convertToLong(dt.Rows[i]["points"]); ;
                    pointamt = convertToDouble(dt.Rows[i]["points_amount"]); ;
                    vouchers = dt.Rows[i]["egift_vou_no"].ToString();
                    voucheramt = convertToDouble(dt.Rows[i]["egift_vou_amt"]);
                    strStoreName = Convert.ToString(dt.Rows[i]["store"].ToString().Trim());

                    if (dt.Rows[i]["discount_code"] != null)
                    {
                        discount_code = dt.Rows[i]["discount_code"].ToString();
                    }
                    break;
                }

                if (discount_code != null || discount_code != "")
                {

                    DataTable dtDiscInfo = GetDiscountCodeInfo(discount_code);

                    if (dtDiscInfo == null || dtDiscInfo.Rows.Count > 0)
                    {
                        for (int p = 0; p < dtDiscInfo.Rows.Count; p++)
                        {
                            discount_rule = dtDiscInfo.Rows[p]["rule"].ToString();
                        }

                    }

                    disc_rule odrulelist = JsonConvert.DeserializeObject<disc_rule>(discount_rule.ToString());

                    if (odrulelist != null)
                    {
                        PromoOffer = convertToDouble(odrulelist.discount_value);                     
                    }
                }

                DataTable dtCompany = GetCompanyInfo(strStoreName);
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
                    strMailSubCompany = dtCompany.Rows[0]["displayname"].ToString();

                    if (dtCompany.Rows[0]["shipping_charge"] == null)
                        shippingAmount = "0";
                    else
                        shippingAmount = dtCompany.Rows[0]["shipping_charge"].ToString();

                    if (dtCompany.Rows[0]["min_order_for_free_shipping"] == null)
                        ShippingThreshold = "0";
                    else
                        ShippingThreshold = dtCompany.Rows[0]["min_order_for_free_shipping"].ToString();

                    if (shippingAmount == "")
                        shippingAmount = "0";

                    if (ShippingThreshold == "")
                        ShippingThreshold = "0";
                    strMyorderURL = strCompanyUrl + "index.html#/myorder";
                }

                if (cart_data.Length == 0)
                    return "Invalid Order";


                List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());

                //Update Stock here
                if (ordlist.Count > 0)
                {
                    //Update Discount Coupon
                    UpdateDiscountCode(email_id, discount_code);

                    ecomm.model.repository.category_repository cr = new ecomm.model.repository.category_repository();
                    foreach (cart_item ordupdt in ordlist)
                    {
                        cr.update_stock_qty(ordupdt.id, Convert.ToDouble(ordupdt.quantity), "Minus");
                    }
                }
                //End of Stock Update
                mailBody += "<html>";
                mailBody += "<head>";
                mailBody += "</head>";
                mailBody += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                mailBody += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #f5f5f5'>";
                mailBody += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                mailBody += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                mailBody += "<div style='padding: 5px;'>";
                mailBody += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                mailBody += "		            <tr>";
                mailBody += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                //mailBody += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                mailBody += "			            <td style='width: 300px;;'></td>";
                mailBody += "			            <td style='width: 35px;' ><a href='http://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                mailBody += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                mailBody += "			            <td style='width: 35px;'><a href=http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                mailBody += "		            </tr>";
                mailBody += "		            </table>";
                mailBody += "		            </div>";
                mailBody += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                mailBody += "			            <table cellpadding='0' cellspacing='0'  >";
                mailBody += "		            <tr>";
                mailBody += "		            <td> <img src='img/emails/shiped.png' /></td>";
                mailBody += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'> Free Shipping for purchase above 500</td>";
                mailBody += "		            </tr>";
                mailBody += "		            </table>";
                mailBody += "		            </div>";
                mailBody += "            </div>";
                mailBody += "        </td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        Hi " + ShippingName + ", <br /><br />";
                mailBody += "";
                mailBody += "Thank you for shopping at " + strCompanyUrl + "<br /><br />";
                mailBody += "This email contains your order summary. When the item(s) in your order are shipped, you will receive an email with the Courier Tracking ID and the link where you can track your order. You can also check the status of your order by <a style='color: #41a0ff; text-decoration:none' href='" + strMyorderURL + "'>clicking here</a>.<br /><br />";
                mailBody += "Please find below the summary of your order";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "        <tr>";
                mailBody += "        <td style='background: #fff; padding: 0px 10px;  font-size: 16px;color:#3b3a3a; font-weight:bold '>";
                mailBody += "        <div style='border-top:1px dotted #cccccc; border-bottom:1px dotted #cccccc; padding-top: 5px; padding-bottom: 5px;'>";
                mailBody += "        Order No. #" + OrderID;
                mailBody += "        </div>";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        <table cellpadding='0' cellspacing='0' width='100%' >";

                int k = 0;
                //Loop for Product - Start
                foreach (cart_item ord in ordlist)
                {
                    k = k + 1;
                    lineitemprice = ord.quantity * ord.final_offer;

                    mailBody += "            <tr>";
                    mailBody += "             <td colspan='6' style='color: #333333; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold' >" + k.ToString() + " Product Code: " + ord.sku + " - " + ord.name + "</td>	";
                    mailBody += "            </tr>";


                    mailBody += "            <tr>";
                    mailBody += "             <td style='text-align: center; padding-left: 10px; padding-right: 10px;'  ><img src='" + ord.image.thumb_link + "' /></td>	";
                    mailBody += "             <td style='color: #666666; font-size: 13px; padding-left: 10px; padding-right: 10px; text-align: center;'>Size <div style='margin-top: 10px; color: #333333'>#Size</div></td>";
                    mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Quantity <div style='margin-top: 10px; color: #333333; '>" + ord.quantity + "</div></div></td>";
                    mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Unit Price <div style='margin-top: 10px; color: #333333'>" + ord.mrp + "</div></div></td>";
                    mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Discount <div style='margin-top: 10px; color: #ff0000'>" + ord.discount + " %" + "</div></div></td> ";    //Diwakar 30/01/2014
                    mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'> <div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Sub Total <div style='margin-top: 10px; color: #333333'>" + lineitemprice + "</div></div></td>";
                    mailBody += "            </tr>";

                    shipmentvalue = shipmentvalue + lineitemprice;
                    discount = discount + Convert.ToDouble(ord.discount);
                    finalamt = shipmentvalue;
                }

                if (finalamt >= Convert.ToDouble(ShippingThreshold))
                {
                    shippingAmount = "Free";
                }
                else
                    finalamt = finalamt + Convert.ToDouble(shippingAmount);

                expshippingAmount = Convert.ToString(totalamount - finalamt);

                //Loop for Product - End 
                /*
                mailBody += "            <tr>";
                mailBody += "             <td colspan='6' style='color: #333333; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold' >2. Product Code: 00328 - MeeMee Baby Carrier (Black)</td>	";
                mailBody += "            </tr>";
            
                mailBody += "            <tr>";
                mailBody += "             <td style='text-align: center; padding-left: 10px; padding-right: 10px;'  ><img src='img/emails/product2.png' /></td>	";
                mailBody += "             <td style='color: #666666; font-size: 13px; padding-left: 10px; padding-right: 10px; text-align: center;'>Size <div style='margin-top: 10px; color: #333333'>-</div></td>";
                mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Quantity <div style='margin-top: 10px; color: #333333; '>1</div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Unit Price <div style='margin-top: 10px; color: #333333'>1200 </div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Discount <div style='margin-top: 10px; color: #ff0000'>201</div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'> <div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Sub Total <div style='margin-top: 10px; color: #333333'>999</div></div></td>";
                mailBody += "            </tr>";
                 */
                mailBody += "            <tr>";
                mailBody += "	            <td style='text-align: right; padding:10px 0px;' colspan='5' >";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; width:100%; float:left;  padding:5px 0px '>";
                mailBody += "	            <div style='float: right; text-align:left'>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Shipment Value </div>";
                //mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Discount   </div> ";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Shipping   </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Express Shipping   </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Points Redeemed   </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Gift Voucher    </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Promo Code    </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Cash/Card Paid  </div>";
                mailBody += "		            </div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "	            <td style='text-align: right'>";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; padding:5px 0px '>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>" + shipmentvalue.ToString() + "</div>";
                //mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#ff0000; '>" + discount.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + shippingAmount.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + expshippingAmount.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + points.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + voucheramt.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + PromoOffer.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + paidamt.ToString() + "</div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "            </tr>";
                mailBody += "            <tr>";
                mailBody += "	            <td valign='top' colspan='5' >";
                mailBody += "	            <div style='float: right; text-align:left; border-top: 1px dotted #ccc; width: 110px; padding-left: 40px; padding-top:5px;'>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Amount paid   </div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "	            <td  valign='top'  style='text-align: right'>";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; padding:5px 0px '>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>" + totalamount.ToString() + "</div>"; //finalamt.ToString()
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "            </tr>";
                mailBody += "            <tr>";
                mailBody += "	            <td valign='top' style='font-size: 12px; padding-top:20px; color: #333333' colspan='6' >";
                mailBody += "	            <b>Shipping Address:</b>    <br />";
                mailBody += ShippingName + " <br />    ";
                mailBody += ShippingAddress + ",  <br />    ";
                mailBody += ShippingCity + " <br />    ";
                mailBody += ShippingState + " - " + ShippingPin + " <br />";
                mailBody += "Mobile: " + strMobileNo + " <br /> <br />";
                //mailBody += "You can track or manage your order at any time by going to <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/myaccounts.php?view=myorders</a>. If you need any assistance or have any questions, feel free to contact us by using the web form at <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/contactus</a> or call us at +91 9686202046 | +91 9972334590";
                mailBody += "If you need any assistance or have any questions, feel free to contact us by using the web form at <a href='" + strCompanyUrl + "index.html#/contact_us' style='color: #41a0ff; text-decoration:none'>" + strCompanyUrl + "index.html#/contact_us" + "</a> or call us at +91 9686202046 | +91 9972334590";
                mailBody += "	            </td>";
                mailBody += "	            </tr>";
                mailBody += "        </table>";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "<tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";

                //Promotion/Marketting
                /*
                mailBody += "        <div style='font-size: 18px; font-weight:bold; color:#333333; text-transform:uppercase; line-height:30px; border-bottom: 1px solid #ccc'>You may also consider</div>";
                mailBody += "        <div style='padding-top: 10px'>";
                mailBody += "            <table cellspacing='0' cellpadding='5' style='width: 100%'>";
                mailBody += "	            <tr>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer1.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer2.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer3.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "	            <tr>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>Rooptex Kurthi</div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>Grass Black Crew Neck ";
                mailBody += "Sporty T Shirt</div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>US Polo Assn Blue Shirt</div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "	            <tr>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "            </table>";
                mailBody += "        </div>";
                 */
                mailBody += "        </td>";
                mailBody += "</tr>";
                /*mailBody += "<tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        <table style='width: 100%' cellpadding='0' cellspacing='0'>";
                mailBody += "            <tr>";
                mailBody += "	            <td align='left'><a href='#'><img src='img/emails/add1.png' /></a></td>";
                mailBody += "	            <td align='right'><a href='#'><img src='img/emails/add2.png' /></a></td>";
                mailBody += "            </tr>";
                mailBody += "        </table>";
                mailBody += "        </td>";
                mailBody += "        </tr>";*/
                mailBody += "</table>";
                /*mailBody += "<table cellpadding='0' cellspacing='0' width='500' align='center'  style=' font-size: 12px; color: #666666'>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px;' align='center'>customerfirst@annectos.in   call us at +91 9686202046 | +91 9972334590";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px; vertical-align: top;' align='center'>Connect With Us <a href='#'><img width='18' height='18' src='img/emails/facebook.png' /></a>  <a href='#'><img width='18' height='18' src='img/emails/twitter.png' /></a>  <a href='#'><img width='18' height='18' src='img/emails/linkedin.png' /></a>";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px; font-size: 10px;' align='center'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. All Rights Reserved. www.annectos.in";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "</table>";*/
                mailBody += "</body>";
                mailBody += "</html>";


                mailBody = mailBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return mailBody;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }


        public string GetOrderToWarehouseBody(string OrderID, string warehouseName, int Status, ref string email_id)
        {

            try
            {
                string mailBody = string.Empty;
                string cart_data = "";
                double shipmentvalue = 0;
                double discount = 0;
                double finalamt = 0;
                double paidamt = 0;
                double totalamount = 0;
                string vouchers = "";
                Int64 points = 0;
                double lineitemprice = 0;
                double pointamt = 0;
                double voucheramt = 0;
                double PromoOffer = 0;
                string strStoreName = "";
                string strLogo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";
                string shippingAmount = "";
                string strMobileNo = "";
                string strCompanyUrl = "";
                string strMyorderURL = "";
                string expshippingAmount = "0";
                string discount_code = "";
                string discount_rule = "";

                string ShippingThreshold = ConfigurationSettings.AppSettings["shipping_threshold"].ToString();
                shippingAmount = ConfigurationSettings.AppSettings["shipping_amount"].ToString();

                DataTable dt = GetDataFromOrder(Convert.ToInt64(OrderID));
                DataTable dtShip = GetDataFromUserID(Convert.ToInt64(OrderID));

                if (dt == null || dt.Rows.Count == 0)
                    return "No Such order";

                if ((dtShip == null) || (dtShip.Rows.Count == 0))
                {
                    return "No Shipping Information";
                }
                else
                {
                    ShippingName = dtShip.Rows[0]["name"].ToString();
                    ShippingAddress = dtShip.Rows[0]["address"].ToString();
                    ShippingCity = dtShip.Rows[0]["city"].ToString();
                    ShippingState = dtShip.Rows[0]["state"].ToString();
                    ShippingPin = dtShip.Rows[0]["pincode"].ToString();
                    strMobileNo = dtShip.Rows[0]["mobile_number"].ToString();
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    email_id = dt.Rows[i]["user_id"].ToString();
                    cart_data = dt.Rows[i]["cart_data"].ToString();
                    paidamt = Convert.ToDouble(dt.Rows[i]["paid_amount"].ToString());
                    totalamount = Convert.ToDouble(dt.Rows[i]["total_amount"].ToString());
                    points = convertToLong(dt.Rows[i]["points"]); ;
                    pointamt = convertToDouble(dt.Rows[i]["points_amount"]); ;
                    vouchers = dt.Rows[i]["egift_vou_no"].ToString();
                    voucheramt = convertToDouble(dt.Rows[i]["egift_vou_amt"]);
                    strStoreName = Convert.ToString(dt.Rows[i]["store"].ToString().Trim());

                    if (dt.Rows[i]["discount_code"] != null)
                    {
                        discount_code = dt.Rows[i]["discount_code"].ToString();
                    }
                    break;
                }

                if (discount_code != null || discount_code != "")
                {

                    DataTable dtDiscInfo = GetDiscountCodeInfo(discount_code);

                    if (dtDiscInfo == null || dtDiscInfo.Rows.Count > 0)
                    {
                        for (int p = 0; p < dtDiscInfo.Rows.Count; p++)
                        {
                            discount_rule = dtDiscInfo.Rows[p]["rule"].ToString();
                        }

                    }

                    disc_rule odrulelist = JsonConvert.DeserializeObject<disc_rule>(discount_rule.ToString());

                    if (odrulelist != null)
                    {
                        PromoOffer = convertToDouble(odrulelist.discount_value);
                    }
                }

                DataTable dtCompany = GetCompanyInfo(strStoreName);
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
                    strMailSubCompany = dtCompany.Rows[0]["displayname"].ToString();

                    if (dtCompany.Rows[0]["shipping_charge"] == null)
                        shippingAmount = "0";
                    else
                        shippingAmount = dtCompany.Rows[0]["shipping_charge"].ToString();

                    if (dtCompany.Rows[0]["min_order_for_free_shipping"] == null)
                        ShippingThreshold = "0";
                    else
                        ShippingThreshold = dtCompany.Rows[0]["min_order_for_free_shipping"].ToString();

                    if (shippingAmount == "")
                        shippingAmount = "0";

                    if (ShippingThreshold == "")
                        ShippingThreshold = "0";
                    strMyorderURL = strCompanyUrl + "index.html#/myorder";
                }

                if (cart_data.Length == 0)
                    return "Invalid Order";


                List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());

                //Update Stock here
                if (ordlist.Count > 0)
                {
                    //Update Discount Coupon
                    UpdateDiscountCode(email_id, discount_code);

                    ecomm.model.repository.category_repository cr = new ecomm.model.repository.category_repository();
                    foreach (cart_item ordupdt in ordlist)
                    {
                        cr.update_stock_qty(ordupdt.id, Convert.ToDouble(ordupdt.quantity), "Minus");
                    }
                }
                //End of Stock Update
                mailBody += "<html>";
                mailBody += "<head>";
                mailBody += "</head>";
                mailBody += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                mailBody += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #f5f5f5'>";
                mailBody += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                mailBody += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                mailBody += "<div style='padding: 5px;'>";
                mailBody += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                mailBody += "		            <tr>";
                mailBody += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                //mailBody += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                mailBody += "			            <td style='width: 300px;;'></td>";
                //mailBody += "			            <td style='width: 35px;' ><a href='http://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                //mailBody += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                //mailBody += "			            <td style='width: 35px;'><a href=http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                mailBody += "		            </tr>";
                mailBody += "		            </table>";
                mailBody += "		            </div>";
                mailBody += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                mailBody += "			            <table cellpadding='0' cellspacing='0'  >";
                mailBody += "		            <tr>";
                //mailBody += "		            <td> <img src='img/emails/shiped.png' /></td>";
                //mailBody += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'> Free Shipping for purchase above 500</td>";
                mailBody += "		            </tr>";
                mailBody += "		            </table>";
                mailBody += "		            </div>";
                mailBody += "            </div>";
                mailBody += "        </td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        Hi " + warehouseName + ", <br /><br />";
                mailBody += "";
                mailBody += "This is to inform you that this order is placed in your warehouse.";
                //mailBody += "Thank you for shopping at " + strCompanyUrl + "<br /><br />";
              //  mailBody += "This email contains the order summary. When the item(s) in your order are shipped, you will receive an email with the Courier Tracking ID and the link where you can track your order. You can also check the status of your order by <a style='color: #41a0ff; text-decoration:none' href='" + strMyorderURL + "'>clicking here</a>.<br /><br />";
                mailBody += "Please find below the summary of the order";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "        <tr>";
                mailBody += "        <td style='background: #fff; padding: 0px 10px;  font-size: 16px;color:#3b3a3a; font-weight:bold '>";
                mailBody += "        <div style='border-top:1px dotted #cccccc; border-bottom:1px dotted #cccccc; padding-top: 5px; padding-bottom: 5px;'>";
                mailBody += "        Order No. #" + OrderID;
                mailBody += "        </div>";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        <table cellpadding='0' cellspacing='0' width='100%' >";

                int k = 0;
                //Loop for Product - Start
                foreach (cart_item ord in ordlist)
                {
                    k = k + 1;
                    lineitemprice = ord.quantity * ord.final_offer;

                    mailBody += "            <tr>";
                    mailBody += "             <td colspan='6' style='color: #333333; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold' >" + k.ToString() + " Product Code: " + ord.sku + " - " + ord.name + "</td>	";
                    mailBody += "            </tr>";


                    mailBody += "            <tr>";
                    mailBody += "             <td style='text-align: center; padding-left: 10px; padding-right: 10px;'  ><img src='" + ord.image.thumb_link + "' /></td>	";
                    mailBody += "             <td style='color: #666666; font-size: 13px; padding-left: 10px; padding-right: 10px; text-align: center;'>Size <div style='margin-top: 10px; color: #333333'>#Size</div></td>";
                    mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Quantity <div style='margin-top: 10px; color: #333333; '>" + ord.quantity + "</div></div></td>";
                    mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Unit Price <div style='margin-top: 10px; color: #333333'>" + ord.mrp + "</div></div></td>";
                    mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Discount <div style='margin-top: 10px; color: #ff0000'>" + ord.discount + " %" + "</div></div></td> ";    //Diwakar 30/01/2014
                    mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'> <div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Sub Total <div style='margin-top: 10px; color: #333333'>" + lineitemprice + "</div></div></td>";
                    mailBody += "            </tr>";

                    shipmentvalue = shipmentvalue + lineitemprice;
                    discount = discount + Convert.ToDouble(ord.discount);
                    finalamt = shipmentvalue;
                }

                if (finalamt >= Convert.ToDouble(ShippingThreshold))
                {
                    shippingAmount = "Free";
                }
                else
                    finalamt = finalamt + Convert.ToDouble(shippingAmount);

                expshippingAmount = Convert.ToString(totalamount - finalamt);

                //Loop for Product - End 
                /*
                mailBody += "            <tr>";
                mailBody += "             <td colspan='6' style='color: #333333; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold' >2. Product Code: 00328 - MeeMee Baby Carrier (Black)</td>	";
                mailBody += "            </tr>";
            
                mailBody += "            <tr>";
                mailBody += "             <td style='text-align: center; padding-left: 10px; padding-right: 10px;'  ><img src='img/emails/product2.png' /></td>	";
                mailBody += "             <td style='color: #666666; font-size: 13px; padding-left: 10px; padding-right: 10px; text-align: center;'>Size <div style='margin-top: 10px; color: #333333'>-</div></td>";
                mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Quantity <div style='margin-top: 10px; color: #333333; '>1</div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Unit Price <div style='margin-top: 10px; color: #333333'>1200 </div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Discount <div style='margin-top: 10px; color: #ff0000'>201</div></div></td>";
                mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'> <div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Sub Total <div style='margin-top: 10px; color: #333333'>999</div></div></td>";
                mailBody += "            </tr>";
                 */
                mailBody += "            <tr>";
                mailBody += "	            <td style='text-align: right; padding:10px 0px;' colspan='5' >";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; width:100%; float:left;  padding:5px 0px '>";
                mailBody += "	            <div style='float: right; text-align:left'>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Shipment Value </div>";
                //mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Discount   </div> ";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Shipping   </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Express Shipping   </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Points Redeemed   </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Gift Voucher    </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Promo Code    </div>";
                mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Cash/Card Paid  </div>";
                mailBody += "		            </div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "	            <td style='text-align: right'>";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; padding:5px 0px '>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>" + shipmentvalue.ToString() + "</div>";
                //mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#ff0000; '>" + discount.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + shippingAmount.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + expshippingAmount.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + points.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + voucheramt.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + PromoOffer.ToString() + "</div>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + paidamt.ToString() + "</div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "            </tr>";
                mailBody += "            <tr>";
                mailBody += "	            <td valign='top' colspan='5' >";
                mailBody += "	            <div style='float: right; text-align:left; border-top: 1px dotted #ccc; width: 110px; padding-left: 40px; padding-top:5px;'>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Amount paid   </div>";
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "	            <td  valign='top'  style='text-align: right'>";
                mailBody += "	            <div style='border-top: 1px dotted #ccc; padding:5px 0px '>";
                mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>" + totalamount.ToString() + "</div>"; //finalamt.ToString()
                mailBody += "	            </div>";
                mailBody += "	            </td>";
                mailBody += "            </tr>";
                mailBody += "            <tr>";
                mailBody += "	            <td valign='top' style='font-size: 12px; padding-top:20px; color: #333333' colspan='6' >";
                mailBody += "	            <b>Shipping Address:</b>    <br />";
                mailBody += ShippingName + " <br />    ";
                mailBody += ShippingAddress + ",  <br />    ";
                mailBody += ShippingCity + " <br />    ";
                mailBody += ShippingState + " - " + ShippingPin + " <br />";
                mailBody += "Mobile: " + strMobileNo + " <br /> <br />";
                //mailBody += "You can track or manage your order at any time by going to <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/myaccounts.php?view=myorders</a>. If you need any assistance or have any questions, feel free to contact us by using the web form at <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/contactus</a> or call us at +91 9686202046 | +91 9972334590";
                mailBody += "If you need any assistance or have any questions, feel free to contact us by using the web form at <a href='" + strCompanyUrl + "index.html#/contact_us' style='color: #41a0ff; text-decoration:none'>" + strCompanyUrl + "index.html#/contact_us" + "</a> or call us at +91 9686202046 | +91 9972334590";
                mailBody += "	            </td>";
                mailBody += "	            </tr>";
                mailBody += "        </table>";
                mailBody += "        </td>";
                mailBody += "        </tr>";
                mailBody += "<tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";

                //Promotion/Marketting
                /*
                mailBody += "        <div style='font-size: 18px; font-weight:bold; color:#333333; text-transform:uppercase; line-height:30px; border-bottom: 1px solid #ccc'>You may also consider</div>";
                mailBody += "        <div style='padding-top: 10px'>";
                mailBody += "            <table cellspacing='0' cellpadding='5' style='width: 100%'>";
                mailBody += "	            <tr>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer1.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer2.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
                mailBody += "			            <div><img src='img/emails/offer3.png' /></div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "	            <tr>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>Rooptex Kurthi</div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>Grass Black Crew Neck ";
                mailBody += "Sporty T Shirt</div>";
                mailBody += "		            </td>";
                mailBody += "		            <td align='left' style='width: 33.3%'>";
                mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>US Polo Assn Blue Shirt</div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "	            <tr>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "		            <td  align='left' style='width: 33.3%; '>";
                mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
                mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
                mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
                mailBody += "			            </div>";
                mailBody += "		            </td>";
                mailBody += "	            </tr>";
                mailBody += "            </table>";
                mailBody += "        </div>";
                 */
                mailBody += "        </td>";
                mailBody += "</tr>";
                /*mailBody += "<tr>";
                mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
                mailBody += "        <table style='width: 100%' cellpadding='0' cellspacing='0'>";
                mailBody += "            <tr>";
                mailBody += "	            <td align='left'><a href='#'><img src='img/emails/add1.png' /></a></td>";
                mailBody += "	            <td align='right'><a href='#'><img src='img/emails/add2.png' /></a></td>";
                mailBody += "            </tr>";
                mailBody += "        </table>";
                mailBody += "        </td>";
                mailBody += "        </tr>";*/
                mailBody += "</table>";
                /*mailBody += "<table cellpadding='0' cellspacing='0' width='500' align='center'  style=' font-size: 12px; color: #666666'>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px;' align='center'>customerfirst@annectos.in   call us at +91 9686202046 | +91 9972334590";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px; vertical-align: top;' align='center'>Connect With Us <a href='#'><img width='18' height='18' src='img/emails/facebook.png' /></a>  <a href='#'><img width='18' height='18' src='img/emails/twitter.png' /></a>  <a href='#'><img width='18' height='18' src='img/emails/linkedin.png' /></a>";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "    <tr>";
                mailBody += "        <td style='padding-top: 10px; font-size: 10px;' align='center'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. All Rights Reserved. www.annectos.in";
                mailBody += "</td>";
                mailBody += "    </tr>";
                mailBody += "</table>";*/
                mailBody += "</body>";
                mailBody += "</html>";


                mailBody = mailBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return mailBody;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string GetDeliverStatusBody(string OrderID, int Status, ref string email_id)
        {

            try
            {
                string strBodyEmail = string.Empty;
                string cart_data = "";
                double shipmentvalue = 0;
                double discount = 0;
                double finalamt = 0;
                double paidamt = 0;
                string vouchers = "";
                Int64 points = 0;
                double pointamt = 0;
                double voucheramt = 0;
                double PromoOffer = 0;
                string discount_rule = "";
                string discount_code = "";
                string strShippingInfo = "";
                string strStoreName = "";
                string strLogo = "";

                string strMobileNo = "";
                string ShippingName = "";
                string ShippingAddress = "";
                string ShippingState = "";
                string ShippingCity = "";
                string ShippingPin = "";
                string shippingAmount = "";
                string ShippingThreshold = "0";// ConfigurationSettings.AppSettings["shipping_threshold"].ToString();
                string strCompanyUrl = "";
                shippingAmount = "0";// ConfigurationSettings.AppSettings["shipping_amount"].ToString();

                DataTable dt = GetDataFromOrder(Convert.ToInt64(OrderID));
                DataTable dtShip = GetDataFromUserID(Convert.ToInt64(OrderID));

                if (dt == null || dt.Rows.Count == 0)
                    return "No Such order";

                if ((dtShip == null) || (dtShip.Rows.Count == 0))
                {
                    return "No Shipping Information";
                }
                else
                {
                    ShippingName = dtShip.Rows[0]["name"].ToString();
                    ShippingAddress = dtShip.Rows[0]["address"].ToString();
                    ShippingCity = dtShip.Rows[0]["city"].ToString();
                    ShippingState = dtShip.Rows[0]["state"].ToString();
                    ShippingPin = dtShip.Rows[0]["pincode"].ToString();
                    strMobileNo = dtShip.Rows[0]["mobile_number"].ToString();
                }


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    email_id = dt.Rows[i]["user_id"].ToString();
                    cart_data = dt.Rows[i]["cart_data"].ToString();
                    paidamt = convertToDouble(dt.Rows[i]["paid_amount"]);
                    points = Convert.ToInt64(dt.Rows[i]["points"].ToString()); ;
                    pointamt = convertToDouble(dt.Rows[i]["points_amount"]); ;
                    vouchers = dt.Rows[i]["egift_vou_no"].ToString();
                    voucheramt = convertToDouble(dt.Rows[i]["egift_vou_amt"]);
                    strShippingInfo = Convert.ToString(dt.Rows[i]["shipping_info"]);
                    strStoreName = Convert.ToString(dt.Rows[i]["store"].ToString().Trim());
                    if (dt.Rows[i]["discount_code"] != null)
                    {
                        discount_code = dt.Rows[i]["discount_code"].ToString();
                    }
                    break;
                }



                DataTable dtCompany = GetCompanyInfo(strStoreName);
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
                    strMailSubCompany = dtCompany.Rows[0]["displayname"].ToString();

                    if (dtCompany.Rows[0]["shipping_charge"] == null)
                        shippingAmount = "0";
                    else
                        shippingAmount = dtCompany.Rows[0]["shipping_charge"].ToString();

                    if (dtCompany.Rows[0]["min_order_for_free_shipping"] == null)
                        ShippingThreshold = "0";
                    else
                        ShippingThreshold = dtCompany.Rows[0]["min_order_for_free_shipping"].ToString();

                    if (shippingAmount == "")
                        shippingAmount = "0";

                    if (ShippingThreshold == "")
                        ShippingThreshold = "0";

                }


                strBodyEmail += "<html>";
                strBodyEmail += "<head>";
                strBodyEmail += "</head>";
                strBodyEmail += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                strBodyEmail += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                strBodyEmail += "    <tr>";
                strBodyEmail += "        <td style='background: #f5f5f5'>";
                strBodyEmail += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                strBodyEmail += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                strBodyEmail += "<div style='padding: 5px;'>";
                strBodyEmail += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                //strBodyEmail += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
                strBodyEmail += "			            <td style='width: 300px;'></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
                strBodyEmail += "			            <td style='width: 35px;'><a href='http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                strBodyEmail += "			            <table cellpadding='0' cellspacing='0'  >";
                strBodyEmail += "		            <tr>";
                strBodyEmail += "		            <td> <img src='img/emails/shiped.png' /></td>";
                strBodyEmail += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'> Free Shipping for purchase above 500</td>";
                strBodyEmail += "		            </tr>";
                strBodyEmail += "		            </table>";
                strBodyEmail += "		            </div>";
                strBodyEmail += "            </div>";
                strBodyEmail += "        </td>";
                strBodyEmail += "    </tr>";
                strBodyEmail += "    <tr>";
                strBodyEmail += " <td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += " Hi " + ShippingName + ",";
                strBodyEmail += "<br/>";
                strBodyEmail += "We are pleased to inform that Your order number :" + OrderID + " has been delivered.This completes your order. Thank you for shopping! <br /><br />";
                strBodyEmail += "<div style='font-size: 12px; color: #333'>";
                strBodyEmail += "<br/>";
                strBodyEmail += "Our customer service representative will call you within the next business day to update you on the order status.";
                strBodyEmail += "<br/>";
                strBodyEmail += "If you have any questions, please feel free to contact us at customerfirst@annectos.in or by phone at +91 9686202046 /9972334590 (10 am - 7 pm, Mon – Fri)";
                strBodyEmail += "<br/>";
                strBodyEmail += "Regards,";
                strBodyEmail += "<br/>";
                strBodyEmail += "<br/>";
                strBodyEmail += "Annectos Support Team";
                strBodyEmail += "</div>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "<tr>";
                strBodyEmail += "<td style='background: #fff; padding: 10px; font-size: 13px'>";
                strBodyEmail += "</td>";
                strBodyEmail += "</tr>";

                strBodyEmail += "</table>";

                strBodyEmail += "</body>";
                strBodyEmail += "</html>";

                strBodyEmail = strBodyEmail.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                return strBodyEmail;

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        private Boolean UpdateDiscountCode(string email_id, string discount_code)
        {
            DataAccess da = new DataAccess();
            try
            {
                if (email_id.Trim().Length > 0 && discount_code.Trim().Length > 0)
                {
                    int i = da.ExecuteSP("user_discount_code_update",
                        da.Parameter("_user_id", email_id),
                        da.Parameter("_discount_code", discount_code));
                }

                return true;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return false;
            }
        }

        public DataTable GetDataFromUserID(Int64 OrderID)
        {
            try
            {
                DataAccess da = new DataAccess();
                DataTable dt = da.ExecuteDataTable("bulk_GetShippingDetail",
                    da.Parameter("_orderid", OrderID));

                return dt;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new DataTable();
            }
        }

        public DataTable GetCompanyInfo(string storename)
        {
            try
            {
                DataAccess da = new DataAccess();
                DataTable dt = da.ExecuteDataTable("get_company_info",
                    da.Parameter("cname", storename));
                return dt;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new DataTable();
            }
        }

        public DataTable GetDataFromOrder(Int64 OrderID)
        {
            try
            {
                DataAccess da = new DataAccess();
                //DataTable dt = da.ExecuteDataTable("get_cart_data",
                DataTable dt = da.ExecuteDataTable("get_order_info",
                         da.Parameter("_orderid", OrderID)
                        );
                return dt;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new DataTable();
            }
        }

        public DataTable GetDiscountCodeInfo(string disc_code)
        {
            try
            {
                DataAccess da = new DataAccess();
                //DataTable dt = da.ExecuteDataTable("get_cart_data",
                DataTable dt = da.ExecuteDataTable("get_disc_code_info",
                         da.Parameter("_discount_code", disc_code)
                        );
                return dt;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new DataTable();
            }
        }



        public DataTable GetUserPointBal(Int64 OrderID)
        {
            try
            {
                DataAccess da = new DataAccess();
                DataTable dt = da.ExecuteDataTable("get_user_point_bal",
                         da.Parameter("_order_id", OrderID)
                        );
                return dt;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new DataTable();
            }
        }

        public Double getOrderAmount(Int64 OrderID)
        {
            try
            {
                double ordamt = 0;
                DataTable dt = GetDataFromOrder(OrderID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ordamt = Convert.ToDouble(dt.Rows[0]["total_amount"].ToString());
                }
                return ordamt;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        public Double getUserPointBalance(Int64 OrderID)
        {
            double point_bal = 0;
            try
            {
                DataTable dt = GetUserPointBal(OrderID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    point_bal = Convert.ToDouble(dt.Rows[0]["point_balance"].ToString());
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return point_bal;

        }

        public int ProcessEmailAndStock(string OrderID, int Status)
        {

            //annectoś e-select : EXL , Order # 572, confirmed
            try
            {
                string email_id = "";
                string strEmailBody = GetOrderStatusBody(OrderID, Status, ref email_id);

                string strSubject = "annectoś e-select : " + strMailSubCompany + " , Order # " + OrderID + ", confirmed";
                helper_repository hr = new helper_repository();
                registration_repository rr = new registration_repository();
                string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();
                if (email_id.Length > 0 && strEmailBody.Length > 0)
                {
                    hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, strSubject, strEmailBody);
                }

                //Diwakar Clear cart 2/2/2014
                //start
                ecomm.model.repository.store_repository sr = new ecomm.model.repository.store_repository();

                if (sr.cart_exists(email_id))
                {
                    sr.del_cart(email_id);
                }
                //end
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return 1;

        }

        /// <summary>
        /// This is for buying items only with Points and GV
        /// </summary>
        /// <param name="EmailID"></param>
        /// <param name="OrderID"></param>
        /// <param name="PaidAmt"></param>
        /// <param name="Points"></param>
        /// <param name="Points_INR"></param>
        /// <param name="strActualAmt"></param>
        /// <param name="egiftVouNo"></param>
        /// <param name="egiftVouAmt"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public Boolean PayWithPointsGV(string EmailID, string OrderID, string PaidAmt, string Points, string Points_INR, string strActualAmt, string egiftVouNo, string egiftVouAmt, string discount_code, string discount_amount, string status)
        {
            try
            {
                DataAccess da = new DataAccess();
                if (string.IsNullOrEmpty(Points))
                    Points = "";
                if (OrderID.Trim().Length > 0)
                {
                    if (PaidAmt.Trim().Length == 0)
                        PaidAmt = "0";
                    if (Points.Trim().Length == 0)
                        Points = "0";
                    if (Points_INR.Trim().Length == 0)
                        Points_INR = "0";
                    if (egiftVouNo.Trim().Length == 0)
                        egiftVouNo = "";
                    if (egiftVouAmt.Trim().Length == 0)
                        egiftVouAmt = "0";
                    if (status.Trim().Length == 0)
                        status = "0";

                    int i = da.ExecuteSP("initiate_order",
                            da.Parameter("_usr_id", EmailID)
                            , da.Parameter("_order_id", Convert.ToInt64(OrderID))
                            , da.Parameter("_paidamt", Convert.ToDecimal(PaidAmt))
                            , da.Parameter("_points", Convert.ToInt64(Points))
                            , da.Parameter("_points_amt", Convert.ToDecimal(Points_INR))
                            , da.Parameter("_egift_vou_no", egiftVouNo)
                            , da.Parameter("_egift_vou_amt", Convert.ToDecimal(egiftVouAmt))
                            , da.Parameter("_discount_coupon", discount_code)
                            , da.Parameter("_discount_amount", Convert.ToDecimal(discount_amount))
                            , da.Parameter("_status", Convert.ToInt32(status))
                            );

                    ProcessOrderStatus(OrderID, PaidAmt, "Paid using Points/GV", status);

                    //GV Update
                    string strGV = egiftVouNo;
                    if (strGV.Trim().Length > 0)
                    {
                        string[] strSplitGV = strGV.Split('|');
                        gift_repository gr = new gift_repository();
                        for (int j = 0; j < strSplitGV.Length; j++)
                        {
                            if (strSplitGV[j] != null && strSplitGV[j] != "undefined")
                            {
                                gr.redeem_gv(strSplitGV[j]);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return false;
            }
            return true;
        }

        public Boolean InitiateOrder(string EmailID, string OrderID, string PaidAmt, string Points, string Points_INR, string strActualAmt, string egiftVouNo, string egiftVouAmt, string discount_coupon, string discount_amount, string status)
        {
            DataAccess da = new DataAccess();

            try
            {
                if (OrderID.Trim().Length > 0)
                {
                    if (String.IsNullOrEmpty(PaidAmt))
                        PaidAmt = "0";
                    if (Points.Trim().Length == 0)
                        Points = "0";
                    if (Points_INR.Trim().Length == 0)
                        Points_INR = "0";
                    if (egiftVouNo.Trim().Length == 0)
                        egiftVouNo = "";
                    if (egiftVouAmt.Trim().Length == 0)
                        egiftVouAmt = "0";
                    if (status.Trim().Length == 0)
                        status = "0";

                    if (string.IsNullOrEmpty(discount_coupon))
                        discount_coupon = "";
                    if (string.IsNullOrEmpty(discount_amount))
                        discount_amount = "0";


                    int i = da.ExecuteSP("initiate_order",
                            da.Parameter("_usr_id", EmailID)
                            , da.Parameter("_order_id", Convert.ToInt64(OrderID))
                            , da.Parameter("_paidamt", Convert.ToDecimal(PaidAmt))
                            , da.Parameter("_points", Convert.ToInt64(Points))
                            , da.Parameter("_points_amt", Convert.ToDecimal(Points_INR))
                            , da.Parameter("_egift_vou_no", egiftVouNo)
                            , da.Parameter("_egift_vou_amt", Convert.ToDecimal(egiftVouAmt))
                            , da.Parameter("_discount_coupon", discount_coupon)
                            , da.Parameter("_discount_amount", Convert.ToDecimal(discount_amount))
                            , da.Parameter("_status", Convert.ToInt32(status))
                            );
                }
                return true;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return false;
            }
        }

        public string GetCompanyURLFromOrderID(string OrderID)
        {
            string strCompanyUrl = "";
            try
            {
                DataAccess da = new DataAccess();
                DataTable dtCompany = da.ExecuteDataTable("get_company_url",
                    da.Parameter("_orderid", OrderID));

                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    //strLogo = dtCompany.Rows[0]["logo"].ToString();
                    strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
                    //strMailSubCompany = dtCompany.Rows[0]["displayname"].ToString();If you have any questions, please feel free to contact us at customerfirst@grassapprels.in or by phone at +91 9686202046 /9972334590 (10 am -
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return strCompanyUrl;
        }

        /// <summary>
        /// Post Action after Payment Gateway
        /// </summary>
        /// <param name="OrderID"></param>
        /// <param name="PaidAmt"></param>
        /// <param name="remarks"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public string ProcessOrderStatus(string OrderID, string PaidAmt, string remarks, string status)
        {
            DataAccess da = new DataAccess();
            string strPayInfo = "";
            string strRejectInfo = "";

            try
            {
                if (OrderID.Trim().Length > 0)
                {
                    if (String.IsNullOrEmpty(PaidAmt))
                        PaidAmt = "0";
                    if (status.Trim().Length == 0)
                        status = "0";

                    if (status == "2")
                    {
                        strPayInfo = remarks;
                    }
                    else if (status == "3")
                    {
                        strRejectInfo = remarks;
                    }
                    else if (status == "4")
                    {
                        strPayInfo = remarks;
                    }
                    else if (status == "9")
                    {
                        strRejectInfo = remarks;
                    }

                    int i = da.ExecuteSP("pay_gateway_order",
                             da.Parameter("_order_id", Convert.ToInt64(OrderID))
                            , da.Parameter("_paidamt", Convert.ToDecimal(PaidAmt))
                            , da.Parameter("_paymentinfo", strPayInfo)
                            , da.Parameter("_rejectioninfo", strRejectInfo)
                            , da.Parameter("_status", Convert.ToInt32(status))
                            );

                    ProcessEmailAndStock(OrderID, 2);
                    return "Success";
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Error: " + ex.Message + " -- Trace:" + ex.InnerException + " -- " + ex.StackTrace;
            }
            return "";
        }

        /*************End of Payment Gateway Pre & Post Actions***********/


    }
}
