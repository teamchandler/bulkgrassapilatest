﻿using ecomm.dal;
using ecomm.util.entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using System.Security.Cryptography;
using ecomm.util;


namespace ecomm.model.repository
{
    public class ad_repository
    {
        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private decimal convertToDecimal(object o)
        {
            try
            {
                if (o != null)
                {
                    return decimal.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private float convertToFloat(object o)
        {
            try
            {
                if (o != null)
                {
                    return float.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private double convertToDouble(object o)
        {
            try
            {
                if (o != null)
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString();
                }
                else { return ""; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        private DateTime convertToDate(object o)
        {
            try
            {
                if ((o != null) && (o != ""))
                {
                    //string f = "mm/dd/yyyy";
                    return DateTime.Parse(o.ToString());
                    //return DateTime.ParseExact(o.ToString(), "MM-dd-yy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else { return DateTime.Parse("1/1/1800"); }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return DateTime.Parse("1/1/1800");
            }
        }
        private QueryDocument createQueryDocument(string s)
        {
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(s);
            QueryDocument queryDoc = new QueryDocument(document);
            return queryDoc;
        }
        private UpdateDocument createUpdateDocument(string s)
        {
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(s);
            UpdateDocument updateDoc = new UpdateDocument(document);
            return updateDoc;
        }
        private string[] mongo_query(string qs, string collection, string[] include_fields, string[] exclude_fields)
        {
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {

                list.Add(c.ToBsonDocument().ToString());
            }
            return list.ToArray();
        }
        private string mongo_query_singleton(string qs, string collection, string[] include_fields, string[] exclude_fields)
        {
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields);
            List<String> list = new List<string>();
            if (cursor.Size() > 0)
            {
                foreach (var c in cursor)
                {
                    list.Add(c.ToBsonDocument().ToString());
                }
                return list[0];
            }
            else
            {
                return "-1";
            }
        }
        private MongoCursor mongo_query_singleton_cursor(string qs, string collection, string[] include_fields, string[] exclude_fields)
        {
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields);
            return cursor;
        }


        #region ad mongo write

        

        #endregion


        #region ad mongo read
        public gift validate_gv(string gv_code, string store)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{'gift_voucher_code':'" + gv_code + "', 'company':'" + store + "', 'active' : 1}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "amount", "giver_email", "rcv_email" };
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            MongoCursor cursor = mongo_query_singleton_cursor(q, "gift_cert", include_fields, exclude_fields);
            gift g = new gift();
            if (cursor.Size() > 0)
            {
                foreach (var c in cursor)
                {
                    product prod = new product();
                    //prod.Id = c.ToBsonDocument()["_id"].ToString();
                    //prod.prod_name = c.ToBsonDocument()["prod_name"].ToString();
                    //g.gift_voucher_code = gv_code;
                    g.amount = convertToInt(c.ToBsonDocument()["amount"].ToString());
                    g.giver_email = c.ToBsonDocument()["giver_email"].ToString();
                    g.rcv_email = c.ToBsonDocument()["rcv_email"].ToString();

                }
            }
            return g;
        }
        public List<adurl> get_landing_page_scroller()
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{'active':1}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "landing_page_main_scroller" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = mongo_query_singleton_cursor(q, "ad_config", include_fields, exclude_fields);
            List<adurl> ads = new List<adurl>();// g = new gift();

            if (cursor.Size() > 0)
            {
                foreach (var c in cursor)
                {
                    ads = JsonConvert.DeserializeObject<List<adurl>>(c.ToBsonDocument()["landing_page_main_scroller"].ToString());
                }
            }
            return ads;
        }
        public string get_landing_page_static_html()
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{'active':1}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "home_static_html" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = mongo_query_singleton_cursor(q, "ad_config", include_fields, exclude_fields);
            string home_static_html = "";// g = new gift();

            if (cursor.Size() > 0)
            {
                foreach (var c in cursor)
                {
                    home_static_html = c.ToBsonDocument()["home_static_html"].ToString();
                }
            }
            return home_static_html;
        }
        public List<level1_banner> get_level1_banner()
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{'active':1}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "l1_banner" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = mongo_query_singleton_cursor(q, "ad_config", include_fields, exclude_fields);
            List<level1_banner> banners = new List<level1_banner>();// g = new gift();

            if (cursor.Size() > 0)
            {
                foreach (var c in cursor)
                {
                    banners = JsonConvert.DeserializeObject<List<level1_banner>>(c.ToBsonDocument()["l1_banner"].ToString());
                }
            }
            return banners;
        }

        #endregion


    }
}
