﻿using ecomm.dal;
using ecomm.util.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using System.Configuration;
using System.Security.Cryptography;
using ecomm.util;
using System.Globalization;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

//using System.Web.UI.WebControls;

namespace ecomm.model.repository
{
    public class checkoutrepository
    {

        private bool check_field(BsonDocument doc, string field_name)
        {
            if (doc.Contains(field_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private DateTime convertToDate(object o)
        {
            try
            {
                if ((o != null) && (o != ""))
                {
                    //string f = "mm/dd/yyyy";
                    return DateTime.Parse(o.ToString());
                    //return DateTime.ParseExact(o.ToString(), "MM-dd-yy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else { return DateTime.Parse("1/1/1800"); }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return DateTime.Parse("1/1/1800");
            }
        }
        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private decimal convertToDecimal(object o)
        {
            try
            {
                if (o != null)
                {
                    return decimal.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        private double convertToDouble(object o)
        {
            try
            {
                if (o != null && o.ToString() != "")
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString().Trim();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        private string ConvToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString().Trim();
                }
                else if (o == null)
                {
                    return null;
                }
                else if (o.ToString().Trim() == "")
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }
        }

        public string AddCartInfo(List<ecomm.util.entities.cart> crtlist)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = Guid.NewGuid().ToString();
            int i = 0;
            try
            {
                foreach (ecomm.util.entities.cart crt in crtlist)
                {
                    i = da.ExecuteSP("cart_items_Insert", ref oc,
                        da.Parameter("_cart_id", guid)
                        , da.Parameter("_company_id", 111) // To be removed
                        , da.Parameter("_user_id", "annectos") // To be removed
                        , da.Parameter("_status", 1)
                        , da.Parameter("_token_id", "") // To be removed
                        , da.Parameter("_product_id", crt.id)
                        , da.Parameter("_quantity", crt.quantity)
                        , da.Parameter("_unit_price", crt.mrp)
                        , da.Parameter("_mrp", crt.mrp)
                        , da.Parameter("_list_price", crt.final_offer)
                        , da.Parameter("_create_ts", System.DateTime.Now.ToLocalTime())
                        , da.Parameter("_modify_ts", System.DateTime.Now.ToLocalTime())
                        );
                }
                if (i == 1)
                {
                    return guid.ToString();
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }


        /// <summary>
        /// Add Shipping Information
        /// </summary>
        /// <param name="ship"></param>
        /// <returns></returns>
        public string AddShippingInfo(ecomm.util.entities.shipping ship)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string cart_id = Guid.NewGuid().ToString();
            int i = 0;
            try
            {
                i = da.ExecuteSP("shipping_info_Insert", ref oc,
                    da.Parameter("_cart_id", cart_id)
                    , da.Parameter("_order_id", ship.order_id)
                    , da.Parameter("_user_id", "annectos")
                    , da.Parameter("_shipping_name", ship.name)
                    , da.Parameter("_pincode", ship.pincode)
                    , da.Parameter("_address", ship.address)
                    , da.Parameter("_state", ship.state)
                    , da.Parameter("_city", ship.city)
                    , da.Parameter("_country", ship.country)
                    , da.Parameter("_landmark", ship.landmark)
                    , da.Parameter("_mobile_number", ship.mobile_number)
                    );

                if (i == 1)
                {
                    return "Success";
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }
        }

        /// <summary>
        /// Add Order Booking Information
        /// </summary>
        /// <param name="ord"></param>
        /// <returns></returns>
        public string AddOrderBooking(ecomm.util.entities.order ord)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            int i = 0;
            try
            {
                i = da.ExecuteSP("order_insert", ref oc,
                    da.Parameter("_order_id", ord.order_id)
                    , da.Parameter("_cart_id", ord.cart_id)
                    , da.Parameter("_user_id", ord.user_id)
                    , da.Parameter("_discount_code", ord.discount_code)
                    , da.Parameter("_total_amount", ord.total_amount)
                    , da.Parameter("_points", ord.points)
                    , da.Parameter("_paid_amount", ord.paid_amount)
                    , da.Parameter("_status", ord.status)
                    , da.Parameter("_payment_info", ord.payment_info)
                    , da.Parameter("_rejection_reason", ord.rejection_reason)
                    , da.Parameter("_create_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_modify_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_pay_sent_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_pay_conf_ts", ord.pay_conf_ts)
                    );

                if (i == 1)
                {
                    return "Success";
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }
        }

        public List<ordadmin> getOrderHistory(string OrderStore, int OrderType, string OrderFrmDt, string OrderToDt)
        {

            DataAccess da = new DataAccess();
            login_msg return_msg = new login_msg();
            List<ordadmin> ol = new List<ordadmin>();
            DataTable dtResult = new DataTable();

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("populate_orders_details"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_order_frm_date", OrderFrmDt)
                                , da.Parameter("_order_to_date", OrderToDt)
                                );




                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        ordadmin ordr = new ordadmin();
                        ordr.order_id = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString());
                        ordr.order_display_id = dtResult.Rows[i]["order_display_id"].ToString();
                        ordr.total_amount = convertToDecimal(dtResult.Rows[i]["total_amount"].ToString());
                        ordr.store = dtResult.Rows[i]["store"].ToString();
                        ordr.Order_Status = dtResult.Rows[i]["Order_Status"].ToString();
                        ordr.BillToName = dtResult.Rows[i]["BillToName"].ToString();
                        ordr.ShipToName = dtResult.Rows[i]["ShipToName"].ToString();
                        ordr.order_date = dtResult.Rows[i]["order_date"].ToString();
                        ordr.invoice_no = dtResult.Rows[i]["invoice_no"].ToString();
                        ordr.invoice_amount = convertToDecimal(dtResult.Rows[i]["invoice_amount"].ToString());
                        ordr.invoice_date = convertToDate(dtResult.Rows[i]["invoice_date"]);

                        ol.Add(ordr);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return ol;

        }

        public string Order_Export_To_Excel(string OrderStore, int OrderType, string OrderFrmDt, string OrderToDt)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            string FileName = Guid.NewGuid().ToString();
            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("populate_orders_details"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_order_frm_date", OrderFrmDt)
                                , da.Parameter("_order_to_date", OrderToDt)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    string strFileName = "";
                    strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + FileName + ".csv";
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);
                    sw.WriteLine("Order #,Store Company,Date,Customer Name,Status,Total Purchase Amount");
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        string strLine = "";
                        strLine = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString()) + "," + dtResult.Rows[i]["store"].ToString() + "," + dtResult.Rows[i]["order_date"].ToString() + "," + dtResult.Rows[i]["BillToName"].ToString() + "," + dtResult.Rows[i]["Order_Status"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["total_amount"].ToString());
                        sw.WriteLine(strLine);
                    }
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return FileName;

        }


        public string Order_Export_To_Excel_invoice_raised(string OrderStore, int OrderType, string OrderFrmDt, string OrderToDt)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            string FileName = Guid.NewGuid().ToString();
            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("populate_orders_details"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_order_frm_date", OrderFrmDt)
                                , da.Parameter("_order_to_date", OrderToDt)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    string strFileName = "";
                    strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + FileName + ".csv";
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);
                    sw.WriteLine("Order #,Store Company,Date,Customer Name,Status,Total Purchase Amount, Invoice Number, Invoice Amount, Invoice Date");
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        string strLine = "";
                        strLine = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString()) + "," + dtResult.Rows[i]["store"].ToString() + "," + dtResult.Rows[i]["order_date"].ToString() + "," + dtResult.Rows[i]["BillToName"].ToString() + "," + dtResult.Rows[i]["Order_Status"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["total_amount"].ToString()) + "," + dtResult.Rows[i]["invoice_no"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["invoice_amount"].ToString()) + "," + convertToDate(dtResult.Rows[i]["invoice_date"]);
                        sw.WriteLine(strLine);
                    }
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return FileName;

        }



        public string Order_Shipped_Export_To_Excel(string OrderStore, int OrderType)
        {

            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();
            DataTable dtResult = new DataTable();
            registration_repository rr = new registration_repository();
            string FileName = Guid.NewGuid().ToString();
            string strLine = "";
            try
            {

                Boolean bln = false;
                string result = "";
                string strFileName = "";
                string toemail = "";
                strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + FileName + ".csv";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("populate_shipped_orders_details"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                               
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    bln = true;

                    MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");
                    var Category_List = new List<category>();

                    foreach (var c in cursor)
                    {
                        category cat = new category();
                        cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                        cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                        Category_List.Add(cat);
                    }


                    sw.WriteLine("Sl.No, Date, Email/Mobile No, Alternate Email, Phone Number, Order Number, Customer Name, Contact No, Shipping Name, Address, City, State, Pin,Brand,Level 3 Category , Article No, Article Name, Size, Qty, Rate/Uni, Points Reedemed, Cash, Total Amount ,Order Status,Service Provider	,Dispatch Date,	Courier Tracking Number,Courier Tracking Link");
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        string cart_data = "";
                        cart_data = dtResult.Rows[i]["cart_data"].ToString();
                        List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());
                        if (ordlist.Count() > 0)
                        {
                            foreach (cart_item ordupdt in ordlist)
                            {
                                int count = 0;
                                string Product_ID = "";

                                var Prod_Id = ordupdt.id.ToString().Split('.');

                                Product_ID = Prod_Id[0].ToString();

                                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_ID + "'}");
                                QueryDocument queryDoc = new QueryDocument(document);
                                MongoCursor cursor1 = dal.execute_mongo_db("product", queryDoc);

                                foreach (var c1 in cursor1)
                                {
                                    string Category_Id = "";
                                    string CategoryName = "";
                                    Category_Id = check_field(c1.ToBsonDocument(), "parent_cat_id") ? convertToString(c1.ToBsonDocument()["parent_cat_id"]) : "0";

                                    if (Category_Id != "" && Category_Id != "0")
                                    {

                                        for (var m = 0; m < Category_List.Count(); m++)
                                        {
                                            if (Category_List[m].id.ToString() == Category_Id)
                                            {
                                                CategoryName = Category_List[m].Name.ToString();
                                                break;
                                            }
                                        }
                                    }
                                    string Size = "";
                                    if (ordupdt.selected_size != null)
                                    {
                                        Size = convertToString(ordupdt.selected_size);
                                    }
                                    string SlNo = convertToString(i + 1 + count);
                                    strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + ",''" + (convertToString(dtResult.Rows[i]["Shipping_Address"]).Replace(",", " ")).Replace("\n", " ") + "''," + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + convertToString(ordupdt.brand) + "," + convertToString(CategoryName) + "," + convertToString(ordupdt.sku).Replace(",", " ") + "," + convertToString(ordupdt.name) + "," + Size + "," + convertToString(ordupdt.quantity) + "," + convertToString(ordupdt.mrp) + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Cash"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]);
                                    sw.WriteLine(strLine);
                                }


                                count++;
                            }
                        }
                    }
                    sw.Close();

                    if (bln == true)
                    {
                        helper_repository hr = new helper_repository();
                       
                      //  string bcclist = System.Configuration.ConfigurationSettings.AppSettings["MailGiftTo"].ToString();
                        string bcclist = System.Configuration.ConfigurationSettings.AppSettings["Management"].ToString();
                        string tomail = System.Configuration.ConfigurationSettings.AppSettings["toemail_report"].ToString();
                        string strBodyEmail = "Hi There";
                        strBodyEmail += "<br/>";
                        strBodyEmail += "<br/>";
                        strBodyEmail += "Please find the newly generated Report as attachment.";
                        strBodyEmail += "<br/>";
                        strBodyEmail += "<br/>";
                        strBodyEmail += "Regards,";
                        strBodyEmail += "<br/>";
                        strBodyEmail += "<br/>";
                        strBodyEmail += "Annectos Support Team";
                        hr.SendMailWithAttachment(tomail, "", "", "Report Generated", strBodyEmail, strFileName);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return FileName;

        }


        public List<ordadmin> getOrderTrackRecord(string OrderStore, int OrderType)
        {

            DataAccess da = new DataAccess();
            List<ordadmin> ol = new List<ordadmin>();
            DataTable dtResult = new DataTable();
            string FileName = Guid.NewGuid().ToString();
            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_order_track_record"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_current_time", System.DateTime.Now.ToLocalTime())
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        ordadmin ordr = new ordadmin();
                        ordr.order_id = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString());
                        ordr.order_display_id = dtResult.Rows[i]["order_display_id"].ToString();
                        ordr.total_amount = convertToDecimal(dtResult.Rows[i]["total_amount"].ToString());
                        ordr.store = dtResult.Rows[i]["store"].ToString();
                        ordr.Order_Status = dtResult.Rows[i]["Order_Status"].ToString();
                        ordr.BillToName = dtResult.Rows[i]["BillToName"].ToString();
                        ordr.ShipToName = dtResult.Rows[i]["ShipToName"].ToString();
                        ordr.order_date = dtResult.Rows[i]["order_date"].ToString();


                        ol.Add(ordr);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return ol;

        }

        public string getSendTrackReport(string OrderStore, int OrderType)
        {

            DataAccess da = new DataAccess();
            List<ordadmin> ol = new List<ordadmin>();
            DataTable dtResult = new DataTable();
            string FileName = Guid.NewGuid().ToString();

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_order_track_record"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_current_time", System.DateTime.Now.ToLocalTime())
                                );

                string toemail = "";
                string emailsubject = "";
                string strStoreName = "annectos";

                if (OrderStore == null || OrderStore == "null")
                {
                    emailsubject = "Order Track Report For All Store";
                }
                else
                {
                    emailsubject = "Order Track Report For : " + OrderStore;
                }

                if (dtResult != null && dtResult.Rows.Count > 0)
                {

                    string strFileName = "";
                    strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + FileName + ".csv";
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                    sw.WriteLine("Order #,Store Company,Date,Customer Name,Status,Total Purchase Amount");
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        string strLine = "";
                        strLine = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString()) + "," + dtResult.Rows[i]["store"].ToString() + "," + dtResult.Rows[i]["order_date"].ToString() + "," + dtResult.Rows[i]["BillToName"].ToString() + "," + dtResult.Rows[i]["Order_Status"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["total_amount"].ToString());
                        sw.WriteLine(strLine);
                    }
                    sw.Close();


                    helper_repository hr = new helper_repository();
                    toemail = "sharvani@annectos.in";
                    string cclist = System.Configuration.ConfigurationSettings.AppSettings["bcc_email"].ToString();
                    string bcclist = System.Configuration.ConfigurationSettings.AppSettings["Management"].ToString();


                    payment_repository pr = new payment_repository();
                    string strMsgBody = pr.GetOrderTrackBody(strStoreName);

                    hr.SendMailWithAttachment(toemail, cclist, bcclist, emailsubject, strMsgBody, strFileName);

                }



            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return "Report Send";

        }


        public List<ordadmin> getWillShipTrackReport(string OrderStore, int OrderType)
        {

            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();

            List<ordadmin> ol = new List<ordadmin>();
            DataTable dtResult = new DataTable();
            DataTable dtReport = new DataTable();
            payment_repository pr = new payment_repository();

            string GenerateOrderIds = "";

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_order_track_record"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_current_time", System.DateTime.Now.ToLocalTime())
                                );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");
                    var Category_List = new List<category>();

                    foreach (var c in cursor)
                    {
                        category cat = new category();
                        cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                        cat.min_ship = check_field(c.ToBsonDocument(), "min_ship") ? convertToInt(c.ToBsonDocument()["min_ship"]) : 0;
                        cat.max_ship = check_field(c.ToBsonDocument(), "max_ship") ? convertToInt(c.ToBsonDocument()["max_ship"]) : 0;
                        Category_List.Add(cat);
                    }

                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {

                        DataTable dtOrdDtls = new DataTable();
                        int min_ship_days = 0;
                        int max_ship_days = 0;

                        string OrdWiseStoreName = convertToString(dtResult.Rows[i]["store"]);
                        long OrderID = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString());

                        dtOrdDtls = pr.GetDataFromOrder(OrderID);

                        if (dtOrdDtls != null || dtOrdDtls.Rows.Count > 0)
                        {
                            for (int j = 0; j < dtOrdDtls.Rows.Count; j++)
                            {

                                string cart_data = "";
                                cart_data = dtOrdDtls.Rows[j]["cart_data"].ToString();
                                List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());



                                foreach (cart_item ord in ordlist)
                                {
                                    string Product_ID = "";

                                    var Prod_Id = ord.id.ToString().Split('.');

                                    Product_ID = Prod_Id[0].ToString();

                                    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_ID + "'}");
                                    QueryDocument queryDoc = new QueryDocument(document);
                                    MongoCursor cursor1 = dal.execute_mongo_db("product", queryDoc);

                                    foreach (var c1 in cursor1)
                                    {
                                        string Category_Id = "";
                                        Category_Id = check_field(c1.ToBsonDocument(), "parent_cat_id") ? convertToString(c1.ToBsonDocument()["parent_cat_id"]) : "0";

                                        if (Category_Id != "" && Category_Id != "0")
                                        {

                                            for (var m = 0; m < Category_List.Count(); m++)
                                            {

                                                if (Category_List[m].id.ToString() == Category_Id)
                                                {
                                                    int Temp_Min_Ship = 0;
                                                    int Temp_Max_Ship = 0;

                                                    Temp_Min_Ship = convertToInt(Category_List[m].min_ship);
                                                    Temp_Max_Ship = convertToInt(Category_List[m].max_ship);

                                                    if (min_ship_days == 0)
                                                    {
                                                        min_ship_days = Temp_Min_Ship;
                                                    }
                                                    else if (Temp_Min_Ship < min_ship_days)
                                                    {
                                                        min_ship_days = Temp_Min_Ship;
                                                    }
                                                    if (max_ship_days == 0)
                                                    {
                                                        max_ship_days = Temp_Max_Ship;
                                                    }
                                                    else if (Temp_Max_Ship < max_ship_days)
                                                    {
                                                        max_ship_days = Temp_Max_Ship;
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }

                        List<OutCollection> oc = new List<OutCollection>();
                        oc = GetOrderWillShipRep(OrderID, OrdWiseStoreName, OrderType, min_ship_days);
                        if (oc[0].strParamValue.ToString() == "Order Status Needs To Change")
                        {

                            if (GenerateOrderIds == "")
                            {
                                GenerateOrderIds = OrderID.ToString();
                            }
                            else
                            {
                                GenerateOrderIds = GenerateOrderIds + "," + OrderID.ToString();
                            }
                        }
                    }


                    GenerateOrderIds = "(" + GenerateOrderIds + ")";


                    string MysqlQuery = "";

                    if (OrderStore == null || OrderStore == "null")
                    {
                        MysqlQuery = "SELECT a.order_id,a.order_display_id,a.store,a.total_amount,a.create_ts as order_date,CONCAT(b.first_name,' ',b.last_name ) AS BillToName,c.name AS ShipToName,'Will Ship'  AS Order_Status FROM  `order` a left outer join `user_registration` b on a.user_id=b.email_id left outer join `user_shipping` c on a.shipping_info_id=c.id where  a.status=" + OrderType + " and order_id in " + GenerateOrderIds + "order by a.order_id desc";

                    }
                    else
                    {
                        MysqlQuery = "SELECT a.order_id,a.order_display_id,a.store,a.total_amount,a.create_ts as order_date,CONCAT(b.first_name,' ',b.last_name ) AS BillToName,c.name AS ShipToName,'Will Ship'  AS Order_Status FROM  `order` a left outer join `user_registration` b on a.user_id=b.email_id left outer join `user_shipping` c on a.shipping_info_id=c.id where a.store='" + OrderStore + "' and a.status=" + OrderType + " and order_id in " + GenerateOrderIds + "order by a.order_id desc";

                    }
                    dtReport = da.ExecuteSQL(MysqlQuery);

                    if (dtReport != null && dtReport.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtReport.Rows.Count; i++)
                        {
                            ordadmin ordr = new ordadmin();
                            ordr.order_id = Convert.ToInt64(dtReport.Rows[i]["order_id"].ToString());
                            ordr.order_display_id = dtReport.Rows[i]["order_display_id"].ToString();
                            ordr.total_amount = convertToDecimal(dtReport.Rows[i]["total_amount"].ToString());
                            ordr.store = dtReport.Rows[i]["store"].ToString();
                            ordr.Order_Status = dtReport.Rows[i]["Order_Status"].ToString();
                            ordr.BillToName = dtReport.Rows[i]["BillToName"].ToString();
                            ordr.ShipToName = dtReport.Rows[i]["ShipToName"].ToString();
                            ordr.order_date = dtReport.Rows[i]["order_date"].ToString();
                            ol.Add(ordr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return ol;

        }


        public string getWillShipReport(string OrderStore, int OrderType)
        {

            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();

            List<ordadmin> ol = new List<ordadmin>();
            DataTable dtResult = new DataTable();
            DataTable dtReport = new DataTable();
            payment_repository pr = new payment_repository();

            string GenerateOrderIds = "";

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_order_track_record"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_current_time", System.DateTime.Now.ToLocalTime())
                                );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");
                    var Category_List = new List<category>();

                    foreach (var c in cursor)
                    {
                        category cat = new category();
                        cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                        cat.min_ship = check_field(c.ToBsonDocument(), "min_ship") ? convertToInt(c.ToBsonDocument()["min_ship"]) : 0;
                        cat.max_ship = check_field(c.ToBsonDocument(), "max_ship") ? convertToInt(c.ToBsonDocument()["max_ship"]) : 0;
                        Category_List.Add(cat);
                    }

                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {

                        DataTable dtOrdDtls = new DataTable();
                        int min_ship_days = 0;
                        int max_ship_days = 0;

                        string OrdWiseStoreName = convertToString(dtResult.Rows[i]["store"]);
                        long OrderID = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString());

                        dtOrdDtls = pr.GetDataFromOrder(OrderID);

                        if (dtOrdDtls != null || dtOrdDtls.Rows.Count > 0)
                        {
                            for (int j = 0; j < dtOrdDtls.Rows.Count; j++)
                            {

                                string cart_data = "";
                                cart_data = dtOrdDtls.Rows[j]["cart_data"].ToString();
                                List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());



                                foreach (cart_item ord in ordlist)
                                {
                                    string Product_ID = "";

                                    var Prod_Id = ord.id.ToString().Split('.');

                                    Product_ID = Prod_Id[0].ToString();

                                    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_ID + "'}");
                                    QueryDocument queryDoc = new QueryDocument(document);
                                    MongoCursor cursor1 = dal.execute_mongo_db("product", queryDoc);

                                    foreach (var c1 in cursor1)
                                    {
                                        string Category_Id = "";
                                        Category_Id = check_field(c1.ToBsonDocument(), "parent_cat_id") ? convertToString(c1.ToBsonDocument()["parent_cat_id"]) : "0";

                                        if (Category_Id != "" && Category_Id != "0")
                                        {

                                            for (var m = 0; m < Category_List.Count(); m++)
                                            {

                                                if (Category_List[m].id.ToString() == Category_Id)
                                                {
                                                    int Temp_Min_Ship = 0;
                                                    int Temp_Max_Ship = 0;

                                                    Temp_Min_Ship = convertToInt(Category_List[m].min_ship);
                                                    Temp_Max_Ship = convertToInt(Category_List[m].max_ship);

                                                    if (min_ship_days == 0)
                                                    {
                                                        min_ship_days = Temp_Min_Ship;
                                                    }
                                                    else if (Temp_Min_Ship < min_ship_days)
                                                    {
                                                        min_ship_days = Temp_Min_Ship;
                                                    }
                                                    if (max_ship_days == 0)
                                                    {
                                                        max_ship_days = Temp_Max_Ship;
                                                    }
                                                    else if (Temp_Max_Ship < max_ship_days)
                                                    {
                                                        max_ship_days = Temp_Max_Ship;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }

                        List<OutCollection> oc = new List<OutCollection>();
                        oc = GetOrderWillShipRep(OrderID, OrdWiseStoreName, OrderType, min_ship_days);
                        if (oc[0].strParamValue.ToString() == "Order Status Needs To Change")
                        {

                            if (GenerateOrderIds == "")
                            {
                                GenerateOrderIds = OrderID.ToString();
                            }
                            else
                            {
                                GenerateOrderIds = GenerateOrderIds + "," + OrderID.ToString();
                            }
                        }
                    }


                    GenerateOrderIds = "(" + GenerateOrderIds + ")";


                    string MysqlQuery = "";
                    string toemail = "";
                    string emailsubject = "";
                    string strStoreName = "annectos";
                    string FileName = Guid.NewGuid().ToString();


                    if (OrderStore == null || OrderStore == "null")
                    {
                        MysqlQuery = "SELECT a.order_id,a.order_display_id,a.store,a.total_amount,a.create_ts as order_date,CONCAT(b.first_name,' ',b.last_name ) AS BillToName,c.name AS ShipToName,'Will Ship'  AS Order_Status FROM  `order` a left outer join `user_registration` b on a.user_id=b.email_id left outer join `user_shipping` c on a.shipping_info_id=c.id where  a.status=" + OrderType + " and order_id in " + GenerateOrderIds + "order by a.order_id desc";
                        emailsubject = "Will Ship Order Track Report After Min Ship Days For All Store";
                    }
                    else
                    {
                        MysqlQuery = "SELECT a.order_id,a.order_display_id,a.store,a.total_amount,a.create_ts as order_date,CONCAT(b.first_name,' ',b.last_name ) AS BillToName,c.name AS ShipToName,'Will Ship'  AS Order_Status FROM  `order` a left outer join `user_registration` b on a.user_id=b.email_id left outer join `user_shipping` c on a.shipping_info_id=c.id where a.store='" + OrderStore + "' and a.status=" + OrderType + " and order_id in " + GenerateOrderIds + "order by a.order_id desc";
                        emailsubject = "Will Ship Order Track Report After Min Ship Days For : " + OrderStore;
                    }
                    dtReport = da.ExecuteSQL(MysqlQuery);


                    if (dtReport != null && dtReport.Rows.Count > 0)
                    {
                        string strFileName = "";
                        strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + FileName + ".csv";
                        System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                        sw.WriteLine("Order #,Store Company,Date,Customer Name,Status,Total Purchase Amount");
                        for (int i = 0; i < dtResult.Rows.Count; i++)
                        {
                            string strLine = "";
                            strLine = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString()) + "," + dtResult.Rows[i]["store"].ToString() + "," + dtResult.Rows[i]["order_date"].ToString() + "," + dtResult.Rows[i]["BillToName"].ToString() + "," + dtResult.Rows[i]["Order_Status"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["total_amount"].ToString());
                            sw.WriteLine(strLine);
                        }
                        sw.Close();


                        helper_repository hr = new helper_repository();
                        toemail = "sharvani@annectos.in";
                        string cclist = System.Configuration.ConfigurationSettings.AppSettings["bcc_email"].ToString();
                        string bcclist = System.Configuration.ConfigurationSettings.AppSettings["Management"].ToString();


                        payment_repository prr = new payment_repository();
                        string strMsgBody = prr.GetWillShipOrderTrackBody(strStoreName);

                        hr.SendMailWithAttachment(toemail, cclist, bcclist, emailsubject, strMsgBody, strFileName);

                    }

                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return "Report Send";

        }

        public string getWillShipReportAfterMaxShip(string OrderStore, int OrderType)
        {

            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();

            List<ordadmin> ol = new List<ordadmin>();
            DataTable dtResult = new DataTable();
            DataTable dtReport = new DataTable();
            payment_repository pr = new payment_repository();

            string GenerateOrderIds = "";

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_order_track_record"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_current_time", System.DateTime.Now.ToLocalTime())
                                );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");
                    var Category_List = new List<category>();

                    foreach (var c in cursor)
                    {
                        category cat = new category();
                        cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                        cat.min_ship = check_field(c.ToBsonDocument(), "min_ship") ? convertToInt(c.ToBsonDocument()["min_ship"]) : 0;
                        cat.max_ship = check_field(c.ToBsonDocument(), "max_ship") ? convertToInt(c.ToBsonDocument()["max_ship"]) : 0;
                        Category_List.Add(cat);
                    }

                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {

                        DataTable dtOrdDtls = new DataTable();
                        int min_ship_days = 0;
                        int max_ship_days = 0;

                        string OrdWiseStoreName = convertToString(dtResult.Rows[i]["store"]);
                        long OrderID = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString());

                        dtOrdDtls = pr.GetDataFromOrder(OrderID);

                        if (dtOrdDtls != null || dtOrdDtls.Rows.Count > 0)
                        {
                            for (int j = 0; j < dtOrdDtls.Rows.Count; j++)
                            {

                                string cart_data = "";
                                cart_data = dtOrdDtls.Rows[j]["cart_data"].ToString();
                                List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());



                                foreach (cart_item ord in ordlist)
                                {
                                    string Product_ID = "";

                                    var Prod_Id = ord.id.ToString().Split('.');

                                    Product_ID = Prod_Id[0].ToString();

                                    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_ID + "'}");
                                    QueryDocument queryDoc = new QueryDocument(document);
                                    MongoCursor cursor1 = dal.execute_mongo_db("product", queryDoc);

                                    foreach (var c1 in cursor1)
                                    {
                                        string Category_Id = "";
                                        Category_Id = check_field(c1.ToBsonDocument(), "parent_cat_id") ? convertToString(c1.ToBsonDocument()["parent_cat_id"]) : "0";

                                        if (Category_Id != "" && Category_Id != "0")
                                        {

                                            for (var m = 0; m < Category_List.Count(); m++)
                                            {

                                                if (Category_List[m].id.ToString() == Category_Id)
                                                {
                                                    int Temp_Min_Ship = 0;
                                                    int Temp_Max_Ship = 0;

                                                    Temp_Min_Ship = convertToInt(Category_List[m].min_ship);
                                                    Temp_Max_Ship = convertToInt(Category_List[m].max_ship);

                                                    if (min_ship_days == 0)
                                                    {
                                                        min_ship_days = Temp_Min_Ship;
                                                    }
                                                    else if (Temp_Min_Ship < min_ship_days)
                                                    {
                                                        min_ship_days = Temp_Min_Ship;
                                                    }
                                                    if (max_ship_days == 0)
                                                    {
                                                        max_ship_days = Temp_Max_Ship;
                                                    }
                                                    else if (Temp_Max_Ship < max_ship_days)
                                                    {
                                                        max_ship_days = Temp_Max_Ship;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }

                        List<OutCollection> oc = new List<OutCollection>();
                        oc = GetOrderWillShipRep(OrderID, OrdWiseStoreName, OrderType, max_ship_days);
                        if (oc[0].strParamValue.ToString() == "Order Status Needs To Change")
                        {

                            if (GenerateOrderIds == "")
                            {
                                GenerateOrderIds = OrderID.ToString();
                            }
                            else
                            {
                                GenerateOrderIds = GenerateOrderIds + "," + OrderID.ToString();
                            }
                        }
                    }


                    GenerateOrderIds = "(" + GenerateOrderIds + ")";


                    string MysqlQuery = "";
                    string toemail = "";
                    string emailsubject = "";
                    string strStoreName = "annectos";
                    string FileName = Guid.NewGuid().ToString();


                    if (OrderStore == null || OrderStore == "null")
                    {
                        MysqlQuery = "SELECT a.order_id,a.order_display_id,a.store,a.total_amount,a.create_ts as order_date,CONCAT(b.first_name,' ',b.last_name ) AS BillToName,c.name AS ShipToName,'Will Ship'  AS Order_Status FROM  `order` a left outer join `user_registration` b on a.user_id=b.email_id left outer join `user_shipping` c on a.shipping_info_id=c.id where  a.status=" + OrderType + " and order_id in " + GenerateOrderIds + "order by a.order_id desc";
                        emailsubject = "Will Ship Order Track Report After Max Ship Days For All Store";
                    }
                    else
                    {
                        MysqlQuery = "SELECT a.order_id,a.order_display_id,a.store,a.total_amount,a.create_ts as order_date,CONCAT(b.first_name,' ',b.last_name ) AS BillToName,c.name AS ShipToName,'Will Ship'  AS Order_Status FROM  `order` a left outer join `user_registration` b on a.user_id=b.email_id left outer join `user_shipping` c on a.shipping_info_id=c.id where a.store='" + OrderStore + "' and a.status=" + OrderType + " and order_id in " + GenerateOrderIds + "order by a.order_id desc";
                        emailsubject = "Will Ship Order Track Report After Max Ship Days For : " + OrderStore;
                    }
                    dtReport = da.ExecuteSQL(MysqlQuery);


                    if (dtReport != null && dtReport.Rows.Count > 0)
                    {
                        string strFileName = "";
                        strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + FileName + ".csv";
                        System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                        sw.WriteLine("Order #,Store Company,Date,Customer Name,Status,Total Purchase Amount");
                        for (int i = 0; i < dtResult.Rows.Count; i++)
                        {
                            string strLine = "";
                            strLine = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString()) + "," + dtResult.Rows[i]["store"].ToString() + "," + dtResult.Rows[i]["order_date"].ToString() + "," + dtResult.Rows[i]["BillToName"].ToString() + "," + dtResult.Rows[i]["Order_Status"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["total_amount"].ToString());
                            sw.WriteLine(strLine);
                        }
                        sw.Close();


                        helper_repository hr = new helper_repository();
                        toemail = "sharvani@annectos.in";
                        string cclist = System.Configuration.ConfigurationSettings.AppSettings["bcc_email"].ToString();
                        string bcclist = System.Configuration.ConfigurationSettings.AppSettings["Management"].ToString();


                        payment_repository prr = new payment_repository();
                        string strMsgBody = prr.GetWillShipOrderTrackBody(strStoreName);

                        hr.SendMailWithAttachment(toemail, cclist, bcclist, emailsubject, strMsgBody, strFileName);

                    }

                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return "Report Send";

        }



        public List<OutCollection> GetOrderWillShipRep(Int64 OrderID, string store_name, int orderstatus, int min_shp)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                int i = da.ExecuteSP("get_order_status_report", ref oc
                      , da.Parameter("_order_id", OrderID)
                      , da.Parameter("_order_store", store_name)
                      , da.Parameter("_order_type", orderstatus)
                      , da.Parameter("_current_time", System.DateTime.Now.ToLocalTime())
                      , da.Parameter("_min_ship", min_shp)
                      , da.Parameter("_outmsg", String.Empty, true)
                      );

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");

            }

            return oc;

        }


        public string UserPin(string UserID)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            string User_Pin = "";
            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_user_pin"
                                , da.Parameter("_user_id", UserID)
                                , da.Parameter("_pin", null)
                                , da.Parameter("_action_flag", 1)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        User_Pin = convertToString(dtResult.Rows[i]["PIN"]);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return User_Pin;

        }

        public string ChangeUserPin(string UserID)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            string User_Pin = "";
            try
            {
                string[] strout = new string[0];
                User_Pin = GetUniquePin();
                dtResult = da.ExecuteDataTable("get_user_pin"
                                , da.Parameter("_user_id", UserID)
                                , da.Parameter("_pin", User_Pin)
                                , da.Parameter("_action_flag", 2)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        User_Pin = convertToString(dtResult.Rows[i]["PIN"]);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return User_Pin;

        }

        public List<OutCollection> Upload_Cust_points(string email_id, string points)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string FileName = Guid.NewGuid().ToString();
            try
            {


                int i = da.ExecuteSP("cust_points_insert", ref oc
                   , da.Parameter("_user_id", email_id)
                    , da.Parameter("_txn_amount", convertToDecimal(points))
                    , da.Parameter("_txn_type", "c")
                    , da.Parameter("_txn_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_txn_comment", "Uploaded Points")
                    , da.Parameter("_txn_status", 1)
                    , da.Parameter("_outmsg", String.Empty, true));

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }

        public List<OutCollection> Upload_Cust_Data(string strore, string FirstName, string LastName, string EmailId, string PointsLoaded, string PointsRedeemed, string ShippingAddress)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string guid = "";
            string Pwd = "";
            try
            {


                guid = Guid.NewGuid().ToString();
                Pwd = GetUniquePassword();
                int i = da.ExecuteSP("upld_user_data", ref oc
                   , da.Parameter("_first_name", FirstName)
                   , da.Parameter("_last_name", LastName)
                   , da.Parameter("_gender", 0)
                   , da.Parameter("_email_id", EmailId)
                   , da.Parameter("_mobile_no", null)
                   , da.Parameter("_office_no", null)
                   , da.Parameter("_street", ShippingAddress)
                   , da.Parameter("_city", null)
                   , da.Parameter("_state", null)
                   , da.Parameter("_country", null)
                   , da.Parameter("_zipcode", null)
                   , da.Parameter("_password", Pwd)
                   , da.Parameter("_status", 9)
                   , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
                   , da.Parameter("_company", strore)
                   , da.Parameter("_points_loaded", convertToDecimal(PointsLoaded))
                   , da.Parameter("_points_redeemed", convertToDecimal(PointsRedeemed))
                   , da.Parameter("_guid", guid)
                   , da.Parameter("_expiry_date", System.DateTime.Today.AddHours(24).ToLocalTime())
                   , da.Parameter("_action_flag", 1)
                   , da.Parameter("_outmsg", String.Empty, true));


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }

        public List<OutCollection> Upload_Cust_Points(string EmailId, string PointsLoaded)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {

                int i = da.ExecuteSP("upload_customer_incentive", ref oc
                   , da.Parameter("_email_id", EmailId)
                   , da.Parameter("_points_loaded", convertToDecimal(PointsLoaded))
                   , da.Parameter("_outmsg", String.Empty, true));
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }

        public List<OutCollection> Upload_Sales_Target(sales_target st)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string guid = "";
            string Pwd = "";
            string Pin = "";



            try
            {

                guid = Guid.NewGuid().ToString();
                Pwd = GetUniquePassword();
                Pin = GetUniquePin();
                int i = da.ExecuteSP("import_sales_target", ref oc
               , da.Parameter("_first_name", convertToString(st.first_name))
               , da.Parameter("_last_name", convertToString(st.last_name))
               , da.Parameter("_mobile_no", convertToString(st.mobile_no))
               , da.Parameter("_code", convertToString(st.code))
               , da.Parameter("_email_id", convertToString(st.email_id))
               , da.Parameter("_address", convertToString(st.street))
               , da.Parameter("_city", convertToString(st.city))
               , da.Parameter("_state", convertToString(st.state))
               , da.Parameter("_zipcode", convertToString(st.zipcode))
               , da.Parameter("_password", Pwd)
               , da.Parameter("_status", 9)
               , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
               , da.Parameter("_company", convertToString(st.company))
               , da.Parameter("_guid", guid)
               , da.Parameter("_expiry_date", System.DateTime.Today.AddHours(24).ToLocalTime())
               , da.Parameter("_mother_maiden_name", ConvToString(st.Mother_Maiden_Name))
               , da.Parameter("_first_school", ConvToString(st.First_School))
               , da.Parameter("_native_place", ConvToString(st.Native_Place))
               , da.Parameter("_retailer", ConvToString(st.Retailer))
               , da.Parameter("_date_of_birth", ConvToString(st.Date_Of_Birth))
               , da.Parameter("_target", convertToString(st.Target))
               , da.Parameter("_achievement", convertToString(st.Achievement))
               , da.Parameter("_points", convertToString(st.Points))
               , da.Parameter("_start_date", convertToString(st.Start_Date))
               , da.Parameter("_end_date", convertToString(st.End_Date))
               , da.Parameter("_pin", Pin)
               , da.Parameter("_outmsg", String.Empty, true));
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }

        public List<OutCollection> ImportLavaData(Lava_Data ld)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string guid = "";
            string Pwd = "";
            string Pin = "";

            try
            {

                guid = Guid.NewGuid().ToString();
                Pwd = GetUniquePassword();
                Pin = GetUniquePin();
                int i = da.ExecuteSP("import_lava_data", ref oc
               , da.Parameter("_first_name", convertToString(ld.FirstName))
               , da.Parameter("_last_name", convertToString(ld.LastName))
               , da.Parameter("_dob", convertToDate(ld.DateOfBirth))
               , da.Parameter("_doj", convertToDate(ld.DateOfJoiningLAVA))
               , da.Parameter("_qualification", convertToString(ld.Qualifications))
               , da.Parameter("_email_id", convertToString(ld.DSEContactNo))
               , da.Parameter("_email_address", convertToString(ld.DSEEMailId))
               , da.Parameter("_address", convertToString(ld.DSEPostalAddress))
               , da.Parameter("_achieve_vol", convertToLong(ld.DecValueAchivement))
               , da.Parameter("_points", convertToLong(ld.DSEPoints))
               , da.Parameter("_source", "excel_upload")
               , da.Parameter("_branch_name", convertToString(ld.BranchName))
               , da.Parameter("_lava_status", convertToString(ld.Status))
               , da.Parameter("_password", Pwd)
               , da.Parameter("_status", 9)
               , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
               , da.Parameter("_company_id", convertToLong(ld.CompanyId))
               , da.Parameter("_company", "")
               , da.Parameter("_guid", guid)
               , da.Parameter("_sales_cycle_id", convertToLong(ld.SelesCycleId))
               , da.Parameter("_outmsg", String.Empty, true));





            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }

        public List<OutCollection> ImportXoloData(Xolo_Data ld)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string guid = "";
            string Pwd = "";
            string Pin = "";

            try
            {

                guid = Guid.NewGuid().ToString();
                Pwd = GetUniquePassword();
                Pin = GetUniquePin();
                int i = da.ExecuteSP("import_xolo_data", ref oc
                     , da.Parameter("_first_name", convertToString(ld.First_Name))
                     , da.Parameter("_last_name", convertToString(ld.Last_Name))
                     , da.Parameter("_dob", convertToDate(ld.DOB))
                     , da.Parameter("_doj", convertToDate(ld.DOJ))
                     , da.Parameter("_qualification", convertToString(ld.Qualifiations))
                     , da.Parameter("_email_id", convertToString(ld.DSE_Contact_Number))
                     , da.Parameter("_email_address", convertToString(ld.FOS_DSE_Mail_Id))
                     , da.Parameter("_address", convertToString(ld.Address))
                     , da.Parameter("_achieve_vol", 500000)
                     , da.Parameter("_points", 0)
                     , da.Parameter("_source", "excel_upload")
                     , da.Parameter("_branch_name", convertToString(ld.Branch))
                     , da.Parameter("_lava_status", "")
                     , da.Parameter("_password", Pwd)
                     , da.Parameter("_status", 9)
                     , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
                     , da.Parameter("_company_id", convertToLong(ld.CompanyId))
                     , da.Parameter("_company", "")
                     , da.Parameter("_guid", guid)
                     , da.Parameter("_sales_cycle_id", convertToLong(ld.SelesCycleId))
                     , da.Parameter("_Emp_Id", convertToString(ld.Emp_Id))
                     , da.Parameter("_BU", convertToString(ld.BU))
                     , da.Parameter("_Outlet_Code", convertToString(ld.Outlet_Code))
                     , da.Parameter("_Outlet_Name", convertToString(ld.Outlet_Name))
                     , da.Parameter("_Fos_Dse_Code", convertToString(ld.FOS_DSE_CODE))
                     , da.Parameter("_TM_Name", convertToString(ld.TM_Name))
                     , da.Parameter("_TM_Code", convertToString(ld.TM_Code))
                     , da.Parameter("_city", convertToString(ld.City))
                     , da.Parameter("_District", convertToString(ld.District))
                     , da.Parameter("_outmsg", String.Empty, true));
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }


        public List<OutCollection> ImportPumaData(Puma_data pd)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string guid = "";
            string Pwd = "";
            string Pin = "";

            try
            {

                guid = Guid.NewGuid().ToString();
                Pwd = GetUniquePassword();
                Pin = GetUniquePin();
                int i = da.ExecuteSP("import_puma_data", ref oc
                     , da.Parameter("_first_name", convertToString(pd.first_name))
                     , da.Parameter("_last_name", convertToString(pd.last_name))
                     , da.Parameter("_dob", convertToDate(null))
                     , da.Parameter("_doj", convertToDate(null))
                     , da.Parameter("_qualification", null)
                     , da.Parameter("_email_id", convertToString(pd.email_id))
                     , da.Parameter("_email_address", convertToString(pd.email_address))
                      , da.Parameter("_mobile_no", convertToString(pd.mobile_no))
                     , da.Parameter("_address", convertToString(pd.address))
                     , da.Parameter("_designation", convertToString(pd.designation))
                     , da.Parameter("_achieve_vol", 0)
                     , da.Parameter("_points", 0)
                     , da.Parameter("_source", "excel_upload")
                     , da.Parameter("_branch_name", null)
                     , da.Parameter("_lava_status", "")
                     , da.Parameter("_password", Pwd)
                     , da.Parameter("_status", 9)
                     , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
                     , da.Parameter("_company_id", convertToLong(pd.CompanyId))
                     , da.Parameter("_company", "")
                     , da.Parameter("_guid", guid)
                     , da.Parameter("_sales_cycle_id", convertToLong(0))
                     , da.Parameter("_Emp_Id", null)
                     , da.Parameter("_BU", null)
                     , da.Parameter("_Outlet_Code", convertToString(pd.outlet_phone))
                     , da.Parameter("_Outlet_Name", convertToString(pd.outlet_name))
                     , da.Parameter("_Fos_Dse_Code", null)
                     , da.Parameter("_TM_Name", null)
                     , da.Parameter("_TM_Code", null)
                     , da.Parameter("_city", convertToString(pd.city))
                     , da.Parameter("_District", convertToString(pd.district))
                     , da.Parameter("_region", convertToString(pd.zone))
                     , da.Parameter("_outmsg", String.Empty, true));
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }


        public List<OutCollection> Upload_Sales_Target_MonthWise(sales_target st)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {
                int i = da.ExecuteSP("import_sales_target_monthly", ref oc
               , da.Parameter("_mobile_no", convertToString(st.mobile_no))
               , da.Parameter("_email_id", convertToString(st.email_id))
               , da.Parameter("_target", convertToString(st.Target))
               , da.Parameter("_achievement", convertToString(st.Achievement))
               , da.Parameter("_points", convertToString(st.Points))
               , da.Parameter("_start_date", convertToString(st.Start_Date))
               , da.Parameter("_end_date", convertToString(st.End_Date))
               , da.Parameter("_outmsg", String.Empty, true));
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }
        public List<OutCollection> Upload_Sales_Target_Monthly_Lava(sales_target_monthly_lava stm)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {
                int i = da.ExecuteSP("import_sales_target_monthly_lava", ref oc
                   , da.Parameter("_company_id", null)
                   , da.Parameter("_user_id", convertToString(stm.Email))
                   , da.Parameter("_sales_cycle_id", 0)
                   , da.Parameter("_sku", convertToString(stm.Sku))
                   , da.Parameter("_target_val", convertToInt(stm.TargetValue))
                   , da.Parameter("_achieve_val", convertToInt(stm.AchieveValue))
                   , da.Parameter("_target_vol", convertToInt(stm.TargetVol))
                   , da.Parameter("_achieve_vol", convertToInt(stm.AchieveVol))
                   , da.Parameter("_point", convertToInt(stm.Points))
                   , da.Parameter("_start_date", convertToString(stm.StartDate))
                   , da.Parameter("_end_date", convertToString(stm.EndDate))
                   , da.Parameter("_source", "excel_upload")
                   , da.Parameter("_create_ts", System.DateTime.Now.ToLocalTime())
                   , da.Parameter("_outmsg", String.Empty, true));

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }
        private string GetUniquePassword()
        {
            try
            {
                int maxSize = 7;
                char[] chars = new char[62];
                string a;
                a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                chars = a.ToCharArray();
                RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
                int size = maxSize;
                byte[] data = new byte[size];
                crypto.GetNonZeroBytes(data);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sb.Append(data[i].ToString("X2"));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        private string GetUniquePin()
        {
            try
            {
                int maxSize = 5;
                char[] chars = new char[62];
                string a;
                a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                chars = a.ToCharArray();
                RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
                int size = maxSize;
                byte[] data = new byte[size];
                crypto.GetNonZeroBytes(data);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sb.Append(data[i].ToString("X2"));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }


        public string update_order(ordadmin o)
        {

            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
           // checkoutrepository cr = new checkoutrepository();
            
            string guid = "";
            string MobNo = "";
            string strMobileNo = "";
            int stock_quantity = 0;
            string product_id = "";
            int order_qnty = 0;
            try
            {


                if (o.partial_order_products != null)
                {

                    if (o.partial_order_products.Count() > 0)
                    {

                        for (int m = 0; m < o.partial_order_products.Count(); m++)
                        {

                            int total_ordered_qty = convertToInt(o.partial_order_products[m].quantity);
                            int shipping_qty = convertToInt(o.partial_order_products[m].shipping_qty);
                            int already_delivered_qty = convertToInt(o.partial_order_products[m].delivered_qty);
                            int total_delivered_qty = shipping_qty + already_delivered_qty;
                            int item_status = 0;
                            if (total_ordered_qty == total_delivered_qty)
                            {
                                item_status = 1;
                            }



                            int j = da.ExecuteSP("partial_order_update", ref oc
                                  , da.Parameter("_order_id", o.order_id)
                                  , da.Parameter("_status", o.status)
                                  , da.Parameter("_shipping_info", o.shipping_info)
                                  , da.Parameter("_courier_track_no", o.courier_track_no)
                                  , da.Parameter("_courier_track_link", o.courier_track_link)
                                  , da.Parameter("_dispute_notes", o.dispute_notes)
                                  , da.Parameter("_delayed_note", o.delayed_note)
                                  , da.Parameter("_cancellation_note", o.cancellation_note)
                                  , da.Parameter("_modify_ts", System.DateTime.Now.ToLocalTime())
                                  , da.Parameter("_shipping_date", convertToDate(o.shipping_date))
                                  , da.Parameter("_courier_cost", convertToDouble(o.courier_cost))
                                  , da.Parameter("_item_id", convertToString(o.partial_order_products[m].id))
                                  , da.Parameter("_item_sku", convertToString(o.partial_order_products[m].sku))
                                  , da.Parameter("_item_status", item_status)/**Item Status 0 Not Delivered, 1 Shipped**/
                                  , da.Parameter("_del_qty", total_delivered_qty)
                                  , da.Parameter("_outmsg", String.Empty, true));


                            int s = da.ExecuteSP("partial_shipping_Insert", ref oc,
                                 da.Parameter("_order_id", o.order_id)
                               , da.Parameter("_status", 1)
                               , da.Parameter("_create_ts", System.DateTime.Now.ToLocalTime())
                               , da.Parameter("_modify_ts", System.DateTime.Now.ToLocalTime())
                               , da.Parameter("_courier_service", o.shipping_info)
                               , da.Parameter("_courier_track_no", o.courier_track_no)
                               , da.Parameter("_courier_track_link", o.courier_track_link)
                               , da.Parameter("_dispatch_date", convertToDate(o.shipping_date))
                               , da.Parameter("_courier_cost", o.courier_cost)
                               , da.Parameter("_brand", convertToString(o.partial_order_products[m].brand))
                               , da.Parameter("_final_offer", convertToDouble(o.partial_order_products[m].final_offer))
                               , da.Parameter("_id", convertToString(o.partial_order_products[m].id))
                               , da.Parameter("_mrp", convertToDouble(o.partial_order_products[m].mrp))
                               , da.Parameter("_name", convertToString(o.partial_order_products[m].name))
                               , da.Parameter("_quantity", convertToInt(o.partial_order_products[m].quantity))
                               , da.Parameter("_selected_size", convertToString(o.partial_order_products[m].selected_size))
                               , da.Parameter("_sku", convertToString(o.partial_order_products[m].sku))
                               , da.Parameter("_shipping_qty", convertToInt(o.partial_order_products[m].shipping_qty))
                               , da.Parameter("_outmsg", String.Empty, true));


                        }
                    }
                }


                int i = da.ExecuteSP("bulk_update_order_info_new", ref oc
                    , da.Parameter("_order_id", o.order_id)
                    , da.Parameter("_status", o.status)
                    , da.Parameter("_shipping_info", o.shipping_info)
                    , da.Parameter("_courier_track_no", o.courier_track_no)
                    , da.Parameter("_courier_track_link", o.courier_track_link)
                    , da.Parameter("_dispute_notes", o.dispute_notes)
                    , da.Parameter("_delayed_note", o.delayed_note)
                    , da.Parameter("_cancellation_note", o.cancellation_note)
                    , da.Parameter("_modify_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_shipping_date", convertToDate(o.shipping_date))
                    , da.Parameter("_courier_cost", convertToDouble(o.courier_cost))
                    , da.Parameter("_expiry_time", o.expiry_date)
                    , da.Parameter("_extended_reason", o.extended_reason)
                    //, da.Parameter("_wirehouse", o.warehouse)
                    //, da.Parameter("_payment_notes", o.payment_notes)                    
                    , da.Parameter("_outmsg", String.Empty, true));

              

                    registration_repository rr = new registration_repository();

                    //For SMS
                    helper_repository hp = new helper_repository();
                    string sms_user_id = ConfigurationSettings.AppSettings["SMSNONPROMOLOGIN"].ToString();
                    string sms_pwd = ConfigurationSettings.AppSettings["SMSNONPROMOPWD"].ToString();
                    string sms_message = ConfigurationSettings.AppSettings["SMS_USER_TRNS_MSG2"].ToString();
                    string sms_ssid = ConfigurationSettings.AppSettings["SMSTRNID"].ToString();
                    // End

                    if (o.status.ToString() == "2")
                    {
                        ecomm.model.repository.store_repository sr = new ecomm.model.repository.store_repository();
                       
                                                 
                            string email_id = "";
                          
                           
                            payment_repository pr = new payment_repository();
                       //     string strMsgBody = pr.GetPaymentReceivedBody(o.order_id.ToString(), convertToString(o.shipping_date), convertToString(o.shipping_info), convertToString(o.courier_track_no), convertToString(o.courier_track_link),convertToString(o.warehouse) ,convertToDouble(o.courier_cost), 5, ref email_id);
                            string strMsgBody = pr.GetPaymentReceivedBody(o.order_id.ToString(), convertToString(o.shipping_date), convertToString(o.shipping_info), convertToString(o.courier_track_no), convertToString(o.courier_track_link), convertToString(o.warehouse), convertToString(o.status), convertToDouble(o.courier_cost), 5, ref email_id);
                            helper_repository hr = new helper_repository();

                            string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();
                            if (email_id.Length > 0 && strMsgBody.Length > 0)
                            {
                                hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Payment Receipt: Grass", strMsgBody);

                                //sms_message = sms_message.Replace("##OrderNo##", o.order_id.ToString());
                                //sms_message = sms_message.Replace("##Courier##", o.shipping_info.ToString());
                                //sms_message = sms_message.Replace("##TrackNo##", o.courier_track_no.ToString());

                                //strMobileNo = rr.getUserMobileNo(email_id);
                                //if (rr.CheckMobileNo(strMobileNo, ref MobNo) != "Failure")
                                //{
                                //    string resp = hp.SendSMS(sms_user_id, sms_pwd, MobNo, sms_ssid, sms_message, "TRNS");
                                //}
                            }

                            //if (sr.cart_exists(rr.getUserEMailAddress(email_id)))
                            //{
                            //    //Response.Write("cart Exists?");
                            //    sr.del_cart(o.user_id);
                            //}

                        return "send emnail";

                    }
                    else if (o.status.ToString() == "17")
                    {
                        if (convertToInt(o.send_email) == 1)
                        {

                            string email_id = "";
                            payment_repository pr = new payment_repository();

                            string partsub = "Part of your ##COMP_URL## order has been dispatched from the warehouse";
                            string strMsgBody = pr.GetPartialShippingStatusBody(o.order_id.ToString(), convertToString(o.shipping_date), convertToString(o.shipping_info), convertToString(o.courier_track_no), convertToString(o.courier_track_link), convertToDouble(o.courier_cost), 5, o.partial_order_products, ref email_id, ref partsub);
                            helper_repository hr = new helper_repository();

                            string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();
                            if (email_id.Length > 0 && strMsgBody.Length > 0)
                            {
                                hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, partsub, strMsgBody);

                                sms_message = sms_message.Replace("##OrderNo##", o.order_id.ToString());
                                sms_message = sms_message.Replace("##Courier##", o.shipping_info.ToString());
                                sms_message = sms_message.Replace("##TrackNo##", o.courier_track_no.ToString());

                                strMobileNo = rr.getUserMobileNo(email_id);
                                if (rr.CheckMobileNo(strMobileNo, ref MobNo) != "Failure")
                                {
                                    string resp = hp.SendSMS(sms_user_id, sms_pwd, MobNo, sms_ssid, sms_message, "TRNS");
                                }
                            }

                            return "send emnail";
                        }

                    }
                    else if (o.status.ToString() == "5")
                    {
                        if (convertToInt(o.send_email) == 1)
                        {

                            string email_id = "";
                            payment_repository pr = new payment_repository();
                            string strMsgBody = pr.GetShippingStatusBody(o.order_id.ToString(), convertToString(o.shipping_date), convertToString(o.shipping_info), convertToString(o.courier_track_no), convertToString(o.courier_track_link), convertToDouble(o.courier_cost), 5, ref email_id);
                            helper_repository hr = new helper_repository();

                            string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();
                            if (email_id.Length > 0 && strMsgBody.Length > 0)
                            {
                                hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Order Shipment: Grass", strMsgBody);

                                sms_message = sms_message.Replace("##OrderNo##", o.order_id.ToString());
                                sms_message = sms_message.Replace("##Courier##", o.shipping_info.ToString());
                                sms_message = sms_message.Replace("##TrackNo##", o.courier_track_no.ToString());

                                strMobileNo = rr.getUserMobileNo(email_id);
                                if (rr.CheckMobileNo(strMobileNo, ref MobNo) != "Failure")
                                {
                                    string resp = hp.SendSMS(sms_user_id, sms_pwd, MobNo, sms_ssid, sms_message, "TRNS");
                                }
                            }

                            return "send emnail";
                        }

                    }
                    else if (o.status.ToString() == "6")
                    {
                        string email_id = "";
                        payment_repository pr = new payment_repository();
                        string strMsgBody = pr.GetDelayStatusBody(o.order_id.ToString(), convertToString(o.shipping_info), convertToString(o.Delay_Days), 6, ref email_id);
                        helper_repository hr = new helper_repository();
                        string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();

                        if (email_id.Length > 0 && strMsgBody.Length > 0)
                        {
                            hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Order Delayed Information: Grass", strMsgBody);
                        }

                        return "send emnail";

                    }
                    else if (o.status.ToString() == "7")
                    {
                        string email_id = "";
                        payment_repository pr = new payment_repository();
                    //    string strMsgBody = pr.GetCancelStatusBody(o.order_id.ToString(), convertToString(o.shipping_info), 7, ref email_id);
                        string strMsgBody = pr.GetCancelStatusBody(o.order_id.ToString(), convertToString(o.shipping_info), convertToString(o.status), convertToString(o.warehouse), 7, ref email_id);
                        helper_repository hr = new helper_repository();
                        string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();


                        long OrderID = convertToLong(o.order_id);
                        DataTable dtOrdDtls = new DataTable();
                        dtOrdDtls = pr.GetDataFromOrder(OrderID);

                        if (dtOrdDtls != null || dtOrdDtls.Rows.Count > 0)
                        {
                            for (int j = 0; j < dtOrdDtls.Rows.Count; j++)
                            {

                                string cart_data = "";
                                cart_data = dtOrdDtls.Rows[j]["cart_data"].ToString();
                                List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());

                                ecomm.model.repository.category_repository cr = new ecomm.model.repository.category_repository();
                                foreach (cart_item ordupdt in ordlist)
                                {
                                    cr.update_stock_qty(ordupdt.id, Convert.ToDouble(ordupdt.quantity), "Add");
                                }

                            }
                        }

                        if (email_id.Length > 0 && strMsgBody.Length > 0)
                        {
                            hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Order Cancelled Information: Grass", strMsgBody);
                        }

                        return "send emnail";

                    }

                    else if (o.status.ToString() == "10")
                    {
                        string warehouse_email_id = "";
                        string warehouse_name = "";
                        string cust_email_id = "";

                        var olw = get_warehouse_details(o.warehouse.ToString());
                        warehouse_email_id = olw[0].owner_email.ToString();
                        warehouse_name = olw[0].warehouse_name.ToString();

                        payment_repository pr = new payment_repository();
                        string strMsgBody = pr.GetOrderToWarehouseBody(o.order_id.ToString(),warehouse_name, 10, ref cust_email_id);

                        helper_repository hr = new helper_repository();
                        string cclist = ConfigurationSettings.AppSettings["Management"].ToString();

                        warehouse_email_id = warehouse_email_id + "," + ConfigurationSettings.AppSettings["WarehouseTo"].ToString();

                        if (warehouse_email_id.Length > 0 && strMsgBody.Length > 0)
                        {
                            hr.SendMail(warehouse_email_id, cclist, "Warehouse Information: Annectos", strMsgBody);
                        }

                        return "send emnail";

                    }

                    else if (o.status.ToString() == "15")
                    {
                        string email_id = "";
                        payment_repository pr = new payment_repository();
                        string strMsgBody = pr.GetDeliverStatusBody(o.order_id.ToString(), 15, ref email_id);
                        helper_repository hr = new helper_repository();
                        string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();

                        if (email_id.Length > 0 && strMsgBody.Length > 0)
                        {
                            hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Order Delivered Information: Annectos", strMsgBody);
                        }

                        return "send emnail";

                    }

                    return "Success";
              
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }








            public string update_order_invoice(ordadmin o)
        {

            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            string MobNo = "";
            string strMobileNo = "";
            try
            {


                int m = da.ExecuteSP("update_invoice_info", ref oc
                    , da.Parameter("_order_id", o.order_id)
                    , da.Parameter("_status", o.status)
                    , da.Parameter("_invoice_no", o.invoice_no)
                    , da.Parameter("_invoice_amount", o.invoice_amount)
                    , da.Parameter("_invoice_date", o.invoice_date)
                    , da.Parameter("_outmsg", String.Empty, true));

                return "Success";

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; 
            }

        }

        public string give_spl_discount(spl_discount spd)
        {
            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();
            try
            {

                int i = da.ExecuteSP("give_spl_discount",
                     da.Parameter("_order_id", spd.order_id),
                     da.Parameter("_spl_discount", spd.special_discount)
                    );



                return "Discount Given";

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }


        }
      
       



        public List<warehouse> get_warehouse_details(string warehouse_name)
        {

            var olw = new List<warehouse>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'warehouse_name':'" + warehouse_name + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("warehouse", queryDoc);

                foreach (var c in cursor)
                {
                    warehouse ow = new warehouse();
                    ow.warehouse_name = check_field(c.ToBsonDocument(), "warehouse_name") ? convertToString(c.ToBsonDocument()["warehouse_name"]) : null;
                    ow.owner_name = check_field(c.ToBsonDocument(), "owner_name") ? convertToString(c.ToBsonDocument()["owner_name"]) : null;
                    ow.owner_email = check_field(c.ToBsonDocument(), "owner_email") ? convertToString(c.ToBsonDocument()["owner_email"]) : null;
                    ow.warehouse_address = check_field(c.ToBsonDocument(), "warehouse_address") ? convertToString(c.ToBsonDocument()["warehouse_address"]) : null;
                    olw.Add(ow);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }

            return olw;


        }


        public List<user_security> getUser_For_Rights(string company)
        {

            DataAccess da = new DataAccess();
            List<user_security> lus = new List<user_security>();
            DataTable dtResult = new DataTable();

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_user_for_rights"
                                , da.Parameter("_company", company));

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        user_security ous = new user_security();
                        ous.Id = convertToLong(dtResult.Rows[i]["Id"].ToString());
                        ous.user_name = dtResult.Rows[i]["user_name"].ToString();
                        ous.password = dtResult.Rows[i]["password"].ToString();
                        ous.company_id = convertToLong(dtResult.Rows[i]["company_id"].ToString());
                        ous.user_type = dtResult.Rows[i]["user_type"].ToString();
                        lus.Add(ous);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return lus;

        }

        public List<promo> get_promo_list(string promo_id)
        {

            DataAccess da = new DataAccess();
            List<promo> lp = new List<promo>();
            DataTable dtResult = new DataTable();

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_promotion_details"
                                , da.Parameter("_promo_id", promo_id));

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        promo op = new promo();
                        op.promo_id = convertToString(dtResult.Rows[i]["promo_id"]);
                        op.promo_name = convertToString(dtResult.Rows[i]["promo_name"]);
                        op.company = convertToString(dtResult.Rows[i]["company"].ToString());
                        op.min_purchase = convertToDecimal(dtResult.Rows[i]["min_purchase"]);
                        op.disc_type = convertToString(dtResult.Rows[i]["disc_type"]);
                        op.max_discount = convertToDecimal(dtResult.Rows[i]["max_discount"].ToString());
                        op.start_date = convertToString(dtResult.Rows[i]["Start_Dt"]);
                        op.end_date = convertToString(dtResult.Rows[i]["End_Dt"]);
                        lp.Add(op);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return lp;

        }

        public string save_promo(promo op)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            if (op.promo_id == null || op.promo_id == "0")
            {
                string promo_id = GetUniquePromoID();
                op.promo_id = promo_id;
            }


            int i = 0;
            try
            {
                i = da.ExecuteSP("ins_upd_promo_info", ref oc
                   , da.Parameter("_promo_id", op.promo_id)
                   , da.Parameter("_promo_name", op.promo_name)
                   , da.Parameter("_company", op.company)
                   , da.Parameter("_min_purchase", op.min_purchase)
                   , da.Parameter("_disc_type", op.disc_type)
                   , da.Parameter("_max_discount", op.max_discount)
                   , da.Parameter("_start_date", op.start_date)
                   , da.Parameter("_end_date", op.end_date)
                   , da.Parameter("_outmsg", String.Empty, true));


                if (i == 1)
                {
                    return oc[0].strParamValue.ToString();
                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }
        }

        public string transfer_promo(promo op)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            int i = 0;
            try
            {
                i = da.ExecuteSP("transfer_points_to_store", ref oc
                   , da.Parameter("_promo_id", op.promo_id)
                   , da.Parameter("_company", op.company)
                   , da.Parameter("_outmsg", String.Empty, true));


                if (i == 1)
                {
                    return oc[0].strParamValue.ToString();
                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }
        }

        private string GetUniquePromoID()
        {
            try
            {
                int maxSize = 8;
                char[] chars = new char[62];
                string a;
                a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                chars = a.ToCharArray();
                RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
                int size = maxSize;
                byte[] data = new byte[size];
                crypto.GetNonZeroBytes(data);
                //int value = BitConverter.ToInt32(data, 0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sb.Append(data[i].ToString("X2"));
                }
                return sb.ToString();

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string ProcessOrderStatusAfterPG(paymentinfostatus pis)
        {

            try
            {
                DataAccess da = new DataAccess();
                string strPayInfo = "";
                string strRejectInfo = "";
                Int64 OrderID = pis.order_id;
                string PaidAmt = "";
                string remarks = "";
                int status = pis.status;
                ordadmin ordr = new ordadmin();
                store_repository sr = new store_repository();

                if (OrderID > 0)
                {

                    if (status == 2)
                    {
                        strPayInfo = remarks;
                    }
                    else if (status == 3)
                    {
                        strRejectInfo = remarks;
                    }
                    else if (status == 4)
                    {
                        strPayInfo = remarks;
                    }
                    else if (status == 9)
                    {
                        strRejectInfo = remarks;
                    }

                    int i = da.ExecuteSP("pay_gateway_order",
                             da.Parameter("_order_id", Convert.ToInt64(OrderID))
                            , da.Parameter("_paidamt", Convert.ToDecimal(PaidAmt))
                            , da.Parameter("_paymentinfo", strPayInfo)
                            , da.Parameter("_rejectioninfo", strRejectInfo)
                            , da.Parameter("_status", Convert.ToInt32(status))
                            );

                    //diwakar 13/2/2014 clear cart if order is through ; using existing method so that code is not
                    //changed at multiple tiers. May be an overhead because the entire order data is retrived can change sp
                    //at later stage to return only value thats needed.
                    /************Start*****************************************/

                    ordr = get_order_data(Convert.ToInt64(OrderID));

                    if (ordr.email_id != "")
                    {
                        //  delete
                        sr.del_cart(ordr.email_id);

                    }
                    /************End*****************************************/

                }
                return "Success";
            }
            catch (Exception ex)
            {

                ExternalLogger.LogError(ex, this, "#");
                return "Failure";
            }
        }


        public ordadmin get_order_data(long Order_Id)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            DataTable dtPartialOrderResult = new DataTable();
            ordadmin ordr = new ordadmin();

            try
            {
                string[] strout = new string[0];

                dtPartialOrderResult = da.ExecuteDataTable("get_partial_order_info"
                              , da.Parameter("_order_id", Order_Id)
                              );

                dtResult = da.ExecuteDataTable("get_bulk_order_information"
                                , da.Parameter("_order_id", Order_Id)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        ordr.order_id = convertToLong(dtResult.Rows[i]["order_id"]);
                        ordr.order_display_id = convertToString(dtResult.Rows[i]["order_display_id"]);
                        ordr.total_amount = convertToDecimal(dtResult.Rows[i]["total_amount"]);
                        ordr.store = convertToString(dtResult.Rows[i]["store"]);
                        ordr.status = convertToInt(dtResult.Rows[i]["status"].ToString());
                        ordr.Order_Status = convertToString(dtResult.Rows[i]["Order_Status"]);
                        ordr.order_date = convertToString(dtResult.Rows[i]["order_date"]);
                        ordr.Cust_Name = convertToString(dtResult.Rows[i]["Cust_Name"]);
                        ordr.email_id = convertToString(dtResult.Rows[i]["email_id"]);
                        //ordr.contact_number = convertToString(dtResult.Rows[i]["mobile_number"]);
                        //ordr.billing_address = convertToString(dtResult.Rows[i]["BillingAddress"]);
                        ordr.contact_number = convertToString(dtResult.Rows[i]["mobile_no"]);
                        ordr.shipping_address = convertToString(dtResult.Rows[i]["ShippingAddress"]);
                        ordr.billing_address = convertToString(dtResult.Rows[i]["BillingAddress"]);

                        ordr.bal_points = convertToString(dtResult.Rows[i]["bal_points"]);
                        ordr.paid_amount = convertToDouble(dtResult.Rows[i]["paid_amount"]);
                        ordr.points = convertToLong(dtResult.Rows[i]["points"]);
                        ordr.points_amount = convertToDouble(dtResult.Rows[i]["points_amount"]);

                        if (dtResult.Rows[i]["egift_vou_no"].ToString() != null && dtResult.Rows[i]["egift_vou_no"].ToString() != "")
                        {
                            var EgiftVouNos = dtResult.Rows[i]["egift_vou_no"].ToString().Split('|');
                            string VouNo = "";
                            for (int m = 0; m < EgiftVouNos.Count(); m++)
                            {
                                if (EgiftVouNos[m].ToString() != "undefined" && EgiftVouNos[m].ToString() != "undef")
                                {
                                    if (VouNo == "")
                                    {
                                        VouNo = EgiftVouNos[m].ToString();
                                    }
                                    else
                                    {
                                        VouNo = VouNo + "," + EgiftVouNos[m].ToString();
                                    }
                                }
                            }
                            ordr.egift_vou_no = VouNo;
                        }
                        ordr.egift_vou_amt = convertToDouble(dtResult.Rows[i]["egift_vou_amt"].ToString());
                        //ordr.cart_data = JsonConvert.DeserializeObject<List<cart_items>>(dtResult.Rows[i]["cart_data"].ToString());
                        List<cart_item> lst_cart = cart_items_to_item(JsonConvert.DeserializeObject<List<cart_items>>(dtResult.Rows[i]["cart_data"].ToString()));


                        ordr.cart_data = lst_cart;

                        if (dtPartialOrderResult != null && dtPartialOrderResult.Rows.Count > 0)
                        {
                            for (int l = 0; l < dtPartialOrderResult.Rows.Count; l++)
                            {
                                for (int m = 0; m < ordr.cart_data.Count; m++)
                                {
                                    if ((ordr.cart_data[m].id.ToString() == convertToString(dtPartialOrderResult.Rows[l]["id"])) && (ordr.cart_data[m].sku.ToString() == convertToString(dtPartialOrderResult.Rows[l]["sku"])))
                                    {

                                        ordr.cart_data[m].delivered_qty = convertToInt(dtPartialOrderResult.Rows[l]["del_qty"]);
                                        ordr.cart_data[m].shipping_qty = 0;
                                        ordr.cart_data[m].selected_size = convertToString(dtPartialOrderResult.Rows[l]["selected_size"]);

                                        if (convertToInt(dtPartialOrderResult.Rows[l]["item_status"]) == 1)
                                        {
                                            ordr.cart_data[m].item_status = "Shipped";
                                        }
                                        else
                                        {
                                            ordr.cart_data[m].item_status = "Pending";
                                        }
                                    }
                                }
                            }
                        }

                        ordr.shipping_info = convertToString(dtResult.Rows[i]["shipping_info"]);
                        ordr.courier_track_no = convertToString(dtResult.Rows[i]["courier_track_no"]);
                        ordr.courier_track_link = convertToString(dtResult.Rows[i]["courier_track_link"]);
                        ordr.courier_cost = convertToDouble(dtResult.Rows[i]["courier_cost"]);
                        ordr.shipping_date = convertToString(dtResult.Rows[i]["Shipping_Date"]);
                        ordr.dispute_notes = convertToString(dtResult.Rows[i]["dispute_notes"]);
                        ordr.special_discount = convertToDecimal(dtResult.Rows[i]["special_discount"]);
                        ordr.final_amt_paid = convertToDecimal(dtResult.Rows[i]["final_amt_paid"]);



                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return ordr;

        }

        public List<cart_item> cart_items_to_item(List<cart_items> cart)
        {
            List<cart_items> lst = cart;
            List<cart_item> lst_cart = new List<cart_item>();

            for (int j = 0; j < lst.Count; j++)
            {
                
                foreach (child_cart_items ch in lst[j].child_prod_detail)
                {

                    if (convertToInt(ch.ord_qty) > 0)
                    {

                        cart_item ci = new cart_item();
                        bool express;
                        if (lst[j].parent_prod_detail.Express == "1")
                        {
                            express = true;
                        }
                        else
                        {
                            express = false;
                        }


                        ci.id = ch.id;
                        ci.sku = ch.sku;
                        ci.quantity = convertToInt(ch.ord_qty);
                        ci.name = lst[j].parent_prod_detail.Name;
                        ci.brand = lst[j].parent_prod_detail.Brand;
                        ci.mrp = convertToDouble(lst[j].parent_prod_detail.price[j].mrp);

                        var total_ord_qty = lst[j].order_quantity;
                        var prod_price = lst[j].parent_prod_detail.price;
                        var prod_final_offer = 0.0;
                        var prod_final_discount = 0.0;
                        foreach (price item in prod_price)
                        {

                        if (total_ord_qty == item.min_qty || total_ord_qty == item.max_qty || (total_ord_qty > item.min_qty && total_ord_qty < item.max_qty))
                         {                           
                             prod_final_offer =Math.Round(item.list);
                             prod_final_discount = item.discount;
                         }
                        
                        }

                        //ci.final_offer = convertToDouble(lst[j].parent_prod_detail.price[j].final_offer);
                        ci.final_offer = prod_final_offer;
                        ci.express = express;
                        List<size> lst_size = new List<size>();
                        var x = new size();
                        lst_size.Add(x);
                        x.size_name = ch.size;
                        ci.sizes = lst_size;
                        ci.selected_size = "";
                        ci.cart_item_id = "";
                        //ci.discount = convertToDouble(lst[j].parent_prod_detail.price[j].final_discount);
                        ci.discount = prod_final_discount;
                        ci.image = lst[j].parent_prod_detail.image_urls[j];
                        lst_cart.Add(ci);
                    }

                }
               

            }
            return lst_cart;
        
        }

        public List<user_rigths> getUserRights(long USER_ID, long FLAG)
        {
            DataAccess da = new DataAccess();
            List<user_rigths> lur = new List<user_rigths>();
            DataTable dtResult = new DataTable();

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_user_rights"
                                , da.Parameter("_user_id", USER_ID)
                                , da.Parameter("_flag", FLAG));

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        user_rigths our = new user_rigths();
                        our.user_right_id = convertToLong(dtResult.Rows[i]["user_right_id"].ToString());
                        our.user_right_desc = dtResult.Rows[i]["user_right_desc"].ToString();
                        lur.Add(our);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return lur;

        }

        public List<registration> getUserIntimateData(string company, long pagenum, string useremail)
        {
            DataAccess da = new DataAccess();
            List<registration> lr = new List<registration>();
            DataTable dtResult = new DataTable();

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_user_data_pagewise"
                                , da.Parameter("_company", company)
                                , da.Parameter("_user_email", useremail)
                                , da.Parameter("_flag", 1)
                                , da.Parameter("_pagenum", pagenum)
                                , da.Parameter("_record_per_page", 500)
                                , da.Parameter("_limit1", 0)
                                , da.Parameter("_limit2", 0));

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        registration or = new registration();
                        or.first_name = dtResult.Rows[i]["first_name"].ToString();
                        or.last_name = dtResult.Rows[i]["last_name"].ToString();
                        or.email_id = dtResult.Rows[i]["email_id"].ToString();
                        or.mobile_no = dtResult.Rows[i]["mobile_no"].ToString();
                        or.street = dtResult.Rows[i]["street"].ToString();
                        or.status = convertToInt(dtResult.Rows[i]["status"].ToString());
                        or.points = convertToDecimal(dtResult.Rows[i]["points"].ToString());
                        or.user_status = dtResult.Rows[i]["user_status"].ToString();
                        or.password = dtResult.Rows[i]["password"].ToString();
                        or.location = dtResult.Rows[i]["location"].ToString();
                        or.Total_Page = convertToLong(dtResult.Rows[i]["Total_Page"].ToString());
                        or.Total_Records = convertToLong(dtResult.Rows[i]["Total_Records"].ToString());
                        lr.Add(or);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return lr;

        }

        public List<OutCollection> postAssignRights(user_assign_rigths uar)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {

                List<long> RIGHT_ID;
                RIGHT_ID = uar.RIGHTS_IDS;
                string strItemTagNo = "";
                for (int i = 0; i < RIGHT_ID.Count; i++)
                {
                    int j = da.ExecuteSP("user_rights_ins_del", ref oc
                    , da.Parameter("_user_id", uar.USER_ID)
                    , da.Parameter("_right_id", convertToLong(RIGHT_ID[i].ToString()))
                    , da.Parameter("_created_by", "admin")
                    , da.Parameter("_created_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_flag", 1)
                    , da.Parameter("_outmsg", String.Empty, true));

                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }

        public string postExportCustList(expo_cust_grid ecg)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {
                Boolean bln = false;
                //evoke mongodb connection method in dal//
                string result = "";
                string toemail = "";
                string strFileName = "";
                string strLine = "";
                string strCustFileId = "";
                strCustFileId = Guid.NewGuid().ToString();
                strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + strCustFileId + ".csv";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                sw.WriteLine("First Name,Last Name,User ID,Password,Status,Location");
                for (int i = 0; i < ecg.GridData.Count(); i++)
                {
                    bln = true;
                    strLine = convertToString(ecg.GridData[i].first_name) + "," + convertToString(ecg.GridData[i].last_name) + "," + convertToString(ecg.GridData[i].email_id) + "," + convertToString(ecg.GridData[i].password) + "," + convertToString(ecg.GridData[i].user_status) + "," + convertToString(ecg.GridData[i].location);
                    sw.WriteLine(strLine);
                }
                sw.Close();
                return strCustFileId;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }



        }

        //public void ExportToExcel(DataTable dt)
        //{
        //    if ((dt.Rows.Count > 0) && (dt != null))
        //    {
        //        string filename = "CMS_Report" + System.DateTime.Now.Date.ToString("yy-MM-dd") + ".xls";
        //        System.IO.StringWriter tw = new System.IO.StringWriter();
        //        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        //        DataGrid dgGrid = new DataGrid();
        //        dgGrid.DataSource = dt;
        //        dgGrid.DataBind();

        //        //Get the HTML for the control.
        //        dgGrid.RenderControl(hw);
        //        //Write the HTML back to the browser.
        //        //Response.ContentType = application/vnd.ms-excel;
        //        Response.ContentType = "application/vnd.ms-excel";
        //        Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
        //        this.EnableViewState = false;
        //        Response.Write(tw.ToString());
        //        Response.End();

        //    }
        //    else
        //    {
        //        Response.Write("no record");
        //        Response.End();
        //    }
        //}


        //public string getExportDailySalesReport(string company, string ReportFromDate, string ReportToDate)
        //{

        //    DataAccess da = new DataAccess();
        //    dal.DataAccess dal = new DataAccess();
        //    List<OutCollection> oc = new List<OutCollection>();
        //    DataTable dtResult = new DataTable();
        //    registration_repository rr = new registration_repository();
        //    try
        //    {
        //        Boolean bln = false;
        //        //evoke mongodb connection method in dal//
        //        string result = "";
        //        string toemail = "";
        //        string strFileName = "";
        //        string strLine = "";
        //        string strReportId = "";
        //        strReportId = "DSWReport_" + company.Replace(" ", "") + System.DateTime.Now.ToLocalTime().ToString("MM-dd-yyyy hh-mm-ss").Replace("-", "").Replace(" ", "");


        //        strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + strReportId + ".csv";
        //        System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);


        //        dtResult = da.ExecuteDataTable("get_daily_sales_wise_report"
        //                       , da.Parameter("_comapny", company)
        //                       , da.Parameter("_report_frm_date", ReportFromDate)
        //                       , da.Parameter("_report_to_date", ReportToDate)
        //                       );


        //        if (dtResult != null && dtResult.Rows.Count > 0)
        //        {

        //            MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");
        //            var Category_List = new List<category>();

        //            foreach (var c in cursor)
        //            {
        //                category cat = new category();
        //                cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
        //                cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
        //                Category_List.Add(cat);
        //            }


        //            sw.WriteLine("Sl.No, Date, Email/Mobile No, Alternate Email, Phone Number, Order Number, Customer Name, Contact No, Shipping Name, Address, City, State, Pin,Brand,Level 3 Category , Article No, Article Name, Size, Qty, Rate/Uni, Points Reedemed, Cash, Total Amount ,Order Status,Service Provider	,Dispatch Date,	Courier Tracking Number,Courier Tracking Link");
        //            for (int i = 0; i < dtResult.Rows.Count; i++)
        //            {
        //                string cart_data = "";
        //                cart_data = dtResult.Rows[i]["cart_data"].ToString();
        //                List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());
        //                if (ordlist.Count() > 0)
        //                {
        //                    foreach (cart_item ordupdt in ordlist)
        //                    {
        //                        int count = 0;
        //                        string Product_ID = "";

        //                        var Prod_Id = ordupdt.id.ToString().Split('.');

        //                        Product_ID = Prod_Id[0].ToString();

        //                        BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_ID + "'}");
        //                        QueryDocument queryDoc = new QueryDocument(document);
        //                        MongoCursor cursor1 = dal.execute_mongo_db("product", queryDoc);

        //                        foreach (var c1 in cursor1)
        //                        {
        //                            string Category_Id = "";
        //                            string CategoryName = "";
        //                            Category_Id = check_field(c1.ToBsonDocument(), "parent_cat_id") ? convertToString(c1.ToBsonDocument()["parent_cat_id"]) : "0";

        //                            if (Category_Id != "" && Category_Id != "0")
        //                            {

        //                                for (var m = 0; m < Category_List.Count(); m++)
        //                                {
        //                                    if (Category_List[m].id.ToString() == Category_Id)
        //                                    {
        //                                        CategoryName = Category_List[m].Name.ToString();
        //                                        break;
        //                                    }
        //                                }
        //                            }
        //                            string Size = "";
        //                            if (ordupdt.selected_size != null)
        //                            {
        //                                Size = convertToString(ordupdt.selected_size);
        //                            }
        //                            string SlNo = convertToString(i + 1 + count);
        //                            strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + ",''" + (convertToString(dtResult.Rows[i]["Shipping_Address"]).Replace(",", " ")).Replace("\n", " ") + "''," + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + convertToString(ordupdt.brand) + "," + convertToString(CategoryName) + "," + convertToString(ordupdt.sku).Replace(",", " ") + "," + convertToString(ordupdt.name) + "," + Size + "," + convertToString(ordupdt.quantity) + "," + convertToString(ordupdt.mrp) + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Cash"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]);
        //                            sw.WriteLine(strLine);
        //                        }


        //                        count++;
        //                    }
        //                }
        //            }
        //            sw.Close();

        //        }
        //        return strReportId;
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        throw ex;
        //    }



        //}

        //public string getExportDailySalesReport(string company, string ReportFromDate, string ReportToDate)
        //{

        //    DataAccess da = new DataAccess();
        //    dal.DataAccess dal = new DataAccess();
        //    List<OutCollection> oc = new List<OutCollection>();
        //    DataTable dtResult = new DataTable();
        //    registration_repository rr = new registration_repository();
        //    try
        //    {
        //        Boolean bln = false;
        //        //evoke mongodb connection method in dal//
        //        double final_price = 0;
        //        double mrp;
        //        int quantity;
        //        string size = "";
        //        string selected_size = "";
        //        string sku_and_name = "";
        //        string sku_no = "";
        //        string prod_name = "";
        //        double order_amount = 0;
        //        double qty_ordered = 0;
        //        double rate_per_unit = 0;
        //        string result = "";
        //        string toemail = "";
        //        string strFileName = "";
        //        string strLine = "";
        //        string strReportId = "";
        //        string SlNo = "";
        //        int counter = 1;
        //        strReportId = "DSWReport_" + company.Replace(" ", "") + System.DateTime.Now.ToLocalTime().ToString("MM-dd-yyyy hh-mm-ss").Replace("-", "").Replace(" ", "");


        //        strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + strReportId + ".csv";
        //        System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);


        //        dtResult = da.ExecuteDataTable("get_daily_sales_wise_report_bulk"
        //                       , da.Parameter("_comapny", company)
        //                       , da.Parameter("_report_frm_date", ReportFromDate)
        //                       , da.Parameter("_report_to_date", ReportToDate)
        //                       );


        //        if (dtResult != null && dtResult.Rows.Count > 0)
        //        {

        //            MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");
        //            var Category_List = new List<category>();

        //            foreach (var c in cursor)
        //            {
        //                category cat = new category();
        //                cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
        //                cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
        //                Category_List.Add(cat);
        //            }


        //            //sw.WriteLine("Sl.No, Date, Email/Mobile No, Alternate Email, Phone Number, Order Number, Customer Name, Contact No, Shipping Name, Address, City, State, Pin,Brand,Level 3 Category , Article No, Article Name, Size, Qty, Rate/Uni, Points Reedemed, Cash, Total Amount ,Order Status,Service Provider	,Dispatch Date,	Courier Tracking Number,Courier Tracking Link");
        //            sw.WriteLine("Sl.No, Date, Email/Mobile No, Alternate Email, Phone Number, Order Number, Customer Name, Contact No, Shipping Name, Address, City, State, Pin,Brand,Level 3 Category , Article No, Article Name, Size, Qty, Rate/Uni, Points Reedemed, Total Order Amount- for Order Id, Order Value ,Order Status,Service Provider	,Dispatch Date,	Courier Tracking Number,Courier Tracking Link");
        //            for (int i = 0; i < dtResult.Rows.Count; i++)
        //            {
        //                string cart_data = "";
        //                cart_data = dtResult.Rows[i]["cart_data"].ToString();
        //                List<cart_items> ordlist = JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString());
        //                if (ordlist[0].parent_prod_detail != null)
        //                {
        //                    if (ordlist.Count() > 0)
        //                    {
        //                        int count = 0;
        //                        foreach (cart_items ordupdt in ordlist)
        //                        {

        //                            string Product_ID = "";
        //                            string prod_brand = "";


        //                            if (ordupdt.child_prod_detail != null)
        //                            {
        //                                for (int p = 0; p < ordupdt.child_prod_detail.Count; p++)
        //                                {
        //                                    if (ordupdt.child_prod_detail[p].ord_qty != "")
        //                                    {
        //                                        selected_size = ordupdt.child_prod_detail[p].size;
        //                                        Product_ID = ordupdt.child_prod_detail[p].sku;
        //                                        qty_ordered = convertToDouble(ordupdt.child_prod_detail[p].ord_qty);

        //                                        BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_ID + "'}");
        //                                        QueryDocument queryDoc = new QueryDocument(document);
        //                                        MongoCursor cursor1 = dal.execute_mongo_db("product", queryDoc);

        //                                        rate_per_unit = (ordupdt.order_amount / ordupdt.order_quantity);
        //                                        //   qty_ordered = ordupdt.order_quantity.ToString();
        //                                        order_amount = qty_ordered * rate_per_unit;
        //                                        prod_name = ordupdt.parent_prod_detail.Name;
        //                                        prod_brand = ordupdt.parent_prod_detail.Brand;

        //                                        SlNo = convertToString(counter + count);
        //                                        // strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + (convertToString(dtResult.Rows[i]["Shipping_Address"]) + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + convertToString(ordupdt.brand) + "," + " " + "," + Product_ID + "," + prod_name + "," + selected_size + "," + qty_ordered + "," + rate_per_unit + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + order_amount + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]) );
        //                                        strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + ",''" + (convertToString(dtResult.Rows[i]["Shipping_Address"]).Replace(",", " ")).Replace("\n", " ") + "''," + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + prod_brand + "," + " " + "," + Product_ID + "," + prod_name + "," + selected_size + "," + qty_ordered + "," + rate_per_unit + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + order_amount + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]);

        //                                        sw.WriteLine(strLine);
        //                                        count++;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        counter = convertToInt(SlNo);
        //                        counter++;
        //                    }
        //                }
        //                else
        //                {
        //                    List<cart_item> ordlists = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());
        //                    if (ordlists.Count() > 0)
        //                    {
        //                        int count = 0;
        //                        foreach (cart_item ordupdt in ordlists)
        //                        {
        //                            if ((ordupdt.cart_item_id != null) || ((convertToInt(dtResult.Rows[i]["Totalamount"]) > 0)))
        //                            {
        //                                string Prod_ID = "";
        //                                selected_size = ordupdt.selected_size;
        //                                Prod_ID = ordupdt.sku;
        //                                qty_ordered = convertToDouble(ordupdt.quantity);
        //                                rate_per_unit = (convertToInt(dtResult.Rows[i]["Totalamount"]) / ordupdt.quantity);
        //                                order_amount = convertToDouble(dtResult.Rows[i]["Totalamount"]);
        //                                prod_name = ordupdt.name;

        //                                SlNo = convertToString(counter + count);
        //                                //  strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + (convertToString(dtResult.Rows[i]["Shipping_Address"]) + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + convertToString(ordupdt.brand) + "," + " " + "," + Prod_ID + "," + prod_name + "," + selected_size + "," + qty_ordered + "," + rate_per_unit + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + order_amount + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]));
        //                                strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + ",''" + (convertToString(dtResult.Rows[i]["Shipping_Address"]).Replace(",", " ")).Replace("\n", " ") + "''," + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + convertToString(ordupdt.brand) + "," + " " + "," + Prod_ID + "," + prod_name + "," + selected_size + "," + qty_ordered + "," + rate_per_unit + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + order_amount + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]);
        //                                sw.WriteLine(strLine);
        //                                count++;
        //                            }
        //                        }
        //                        counter = convertToInt(SlNo);
        //                        counter++;

        //                    }


        //                }

        //            }


        //        }
        //        sw.Close();
        //        return strReportId;
        //    }


        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        throw ex;
        //    }

        //}

        //public string getExportDailySalesReport(string company, string ReportFromDate, string ReportToDate)
        //{

        //    DataAccess da = new DataAccess();
        //    dal.DataAccess dal = new DataAccess();
        //    List<OutCollection> oc = new List<OutCollection>();
        //    DataTable dtResult = new DataTable();
        //    registration_repository rr = new registration_repository();
        //    try
        //    {
        //        Boolean bln = false;
        //        //evoke mongodb connection method in dal//
        //        double final_price = 0;
        //        double mrp;
        //        int quantity;
        //        string size = "";
        //        string selected_size = "";
        //        string sku_and_name = "";
        //        string sku_no = "";
        //        string prod_name = "";
        //        double order_amount = 0;
        //        double qty_ordered = 0;
        //        double rate_per_unit = 0;
        //        string result = "";
        //        string toemail = "";
        //        string strFileName = "";
        //        string strLine = "";
        //        string strReportId = "";
        //        string SlNo = "";
        //        int counter = 1;
        //        strReportId = "DSWReport_" + company.Replace(" ", "") + System.DateTime.Now.ToLocalTime().ToString("MM-dd-yyyy hh-mm-ss").Replace("-", "").Replace(" ", "");


        //        strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + strReportId + ".csv";
        //        System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);


        //        dtResult = da.ExecuteDataTable("get_daily_sales_wise_report_bulk"
        //                       , da.Parameter("_comapny", company)
        //                       , da.Parameter("_report_frm_date", ReportFromDate)
        //                       , da.Parameter("_report_to_date", ReportToDate)
        //                       );


        //        if (dtResult != null && dtResult.Rows.Count > 0)
        //        {

        //            MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");
        //            var Category_List = new List<category>();

        //            foreach (var c in cursor)
        //            {
        //                category cat = new category();
        //                cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
        //                cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
        //                Category_List.Add(cat);
        //            }


        //            //sw.WriteLine("Sl.No, Date, Email/Mobile No, Alternate Email, Phone Number, Order Number, Customer Name, Contact No, Shipping Name, Address, City, State, Pin,Brand,Level 3 Category , Article No, Article Name, Size, Qty, Rate/Uni, Points Reedemed, Cash, Total Amount ,Order Status,Service Provider	,Dispatch Date,	Courier Tracking Number,Courier Tracking Link");
        //            sw.WriteLine("Sl.No, Date, Email/Mobile No, Alternate Email, Phone Number, Order Number, Customer Name, Contact No, Shipping Name, Address, City, State, Pin,Brand,Level 3 Category , Article No, Article Name, Size, Qty, Rate/Uni, Points Reedemed, Total Order Amount- for Order Id, Order Value ,Order Status,Service Provider	,Dispatch Date,	Courier Tracking Number,Courier Tracking Link,Invoice Number,Invoice Amount,Invoice Date");
        //            for (int i = 0; i < dtResult.Rows.Count; i++)
        //            {
        //                string cart_data = "";
        //                cart_data = dtResult.Rows[i]["cart_data"].ToString();
        //                List<cart_items> ordlist = JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString());
        //                if (ordlist[0].parent_prod_detail != null)
        //                {
        //                    if (ordlist.Count() > 0)
        //                    {
        //                        int count = 0;
        //                        foreach (cart_items ordupdt in ordlist)
        //                        {

        //                            string Product_ID = "";
        //                            string prod_brand = "";


        //                            if (ordupdt.child_prod_detail != null)
        //                            {
        //                                for (int p = 0; p < ordupdt.child_prod_detail.Count; p++)
        //                                {
        //                                    if (ordupdt.child_prod_detail[p].ord_qty != "")
        //                                    {
        //                                        selected_size = ordupdt.child_prod_detail[p].size;
        //                                        Product_ID = ordupdt.child_prod_detail[p].sku;
        //                                        qty_ordered = convertToDouble(ordupdt.child_prod_detail[p].ord_qty);

        //                                        BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_ID + "'}");
        //                                        QueryDocument queryDoc = new QueryDocument(document);
        //                                        MongoCursor cursor1 = dal.execute_mongo_db("product", queryDoc);

        //                                        rate_per_unit = (ordupdt.order_amount / ordupdt.order_quantity);
        //                                        //   qty_ordered = ordupdt.order_quantity.ToString();
        //                                        order_amount = qty_ordered * rate_per_unit;
        //                                        prod_name = ordupdt.parent_prod_detail.Name;
        //                                        prod_brand = ordupdt.parent_prod_detail.Brand;

        //                                        SlNo = convertToString(counter + count);
        //                                        // strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + (convertToString(dtResult.Rows[i]["Shipping_Address"]) + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + convertToString(ordupdt.brand) + "," + " " + "," + Product_ID + "," + prod_name + "," + selected_size + "," + qty_ordered + "," + rate_per_unit + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + order_amount + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]) );
        //                                        strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + ",''" + (convertToString(dtResult.Rows[i]["Shipping_Address"]).Replace(",", " ")).Replace("\n", " ") + "''," + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + prod_brand + "," + " " + "," + Product_ID + "," + prod_name + "," + selected_size + "," + qty_ordered + "," + rate_per_unit + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + order_amount + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]) + "," + dtResult.Rows[i]["Invoice_Number"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["Invoice_Amount"].ToString()) + "," + convertToDate(dtResult.Rows[i]["Invoice_Date"]);

        //                                        sw.WriteLine(strLine);
        //                                        count++;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        counter = convertToInt(SlNo);
        //                        counter++;
        //                    }
        //                }
        //                else
        //                {
        //                    List<cart_item> ordlists = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());
        //                    if (ordlists.Count() > 0)
        //                    {
        //                        int count = 0;
        //                        foreach (cart_item ordupdt in ordlists)
        //                        {
        //                            if ((ordupdt.cart_item_id != null) || ((convertToInt(dtResult.Rows[i]["Totalamount"]) > 0)))

                                    
        //                            {
        //                                string Prod_ID = "";
        //                                selected_size = ordupdt.selected_size;
        //                                Prod_ID = ordupdt.sku;
        //                                qty_ordered = convertToDouble(ordupdt.quantity);
        //                                rate_per_unit = (convertToInt(dtResult.Rows[i]["Totalamount"]) / ordupdt.quantity);
        //                                order_amount = convertToDouble(dtResult.Rows[i]["Totalamount"]);
        //                                prod_name = ordupdt.name;

        //                                SlNo = convertToString(counter + count);
        //                                //  strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + (convertToString(dtResult.Rows[i]["Shipping_Address"]) + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + convertToString(ordupdt.brand) + "," + " " + "," + Prod_ID + "," + prod_name + "," + selected_size + "," + qty_ordered + "," + rate_per_unit + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + order_amount + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]));
        //                                strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + ",''" + (convertToString(dtResult.Rows[i]["Shipping_Address"]).Replace(",", " ")).Replace("\n", " ") + "''," + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + convertToString(ordupdt.brand) + "," + " " + "," + Prod_ID + "," + prod_name + "," + selected_size + "," + qty_ordered + "," + rate_per_unit + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + order_amount + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]) + "," + dtResult.Rows[i]["Invoice_Number"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["Invoice_Amount"].ToString()) + "," + convertToDate(dtResult.Rows[i]["Invoice_Date"]);
        //                                sw.WriteLine(strLine);
        //                                count++;
        //                            }
        //                        }
        //                        counter = convertToInt(SlNo);
        //                        counter++;

        //                    }


        //                }

        //            }


        //        }
        //        sw.Close();
        //        return strReportId;
        //    }


        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        throw ex;
        //    }

        //}



        public string getExportDailySalesReport(string company, string ReportFromDate, string ReportToDate)
        {

            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            DataTable dtResult = new DataTable();
            registration_repository rr = new registration_repository();
            try
            {
                Boolean bln = false;
                //evoke mongodb connection method in dal//
                double final_price = 0;
                double mrp;
                int quantity;
                string size = "";
                string selected_size = "";
                string sku_and_name = "";
                string sku_no = "";
                string prod_name = "";
                double order_amount = 0;
                double qty_ordered = 0;
                double rate_per_unit = 0;
                string result = "";
                string toemail = "";
                string strFileName = "";
                string strLine = "";
                string strReportId = "";
                string SlNo = "";
                int counter = 1;
                strReportId = "DSWReport_" + company.Replace(" ", "") + System.DateTime.Now.ToLocalTime().ToString("MM-dd-yyyy hh-mm-ss").Replace("-", "").Replace(" ", "");


                strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + strReportId + ".csv";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);


                dtResult = da.ExecuteDataTable("get_daily_sales_wise_report_bulk"
                               , da.Parameter("_comapny", company)
                               , da.Parameter("_report_frm_date", ReportFromDate)
                               , da.Parameter("_report_to_date", ReportToDate)
                               );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {

                    MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");
                    var Category_List = new List<category>();

                    foreach (var c in cursor)
                    {
                        category cat = new category();
                        cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                        cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                        Category_List.Add(cat);
                    }


                    //sw.WriteLine("Sl.No, Date, Email/Mobile No, Alternate Email, Phone Number, Order Number, Customer Name, Contact No, Shipping Name, Address, City, State, Pin,Brand,Level 3 Category , Article No, Article Name, Size, Qty, Rate/Uni, Points Reedemed, Cash, Total Amount ,Order Status,Service Provider	,Dispatch Date,	Courier Tracking Number,Courier Tracking Link");
                    sw.WriteLine("Sl.No, Date, Email/Mobile No, Alternate Email, Phone Number, Order Number, Customer Name, Contact No, Shipping Name, Address, City, State, Pin,Brand,Level 3 Category , Article No, Article Name, Size, Qty, Rate/Uni, Points Reedemed, Total Order Amount- for Order Id, Order Value ,Order Status,Service Provider	,Dispatch Date,	Courier Tracking Number,Courier Tracking Link,Invoice Number,Invoice Amount,Invoice Date");
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        string cart_data = "";
                        cart_data = dtResult.Rows[i]["cart_data"].ToString();
                        List<cart_items> ordlist = JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString());
                        if (ordlist[0].parent_prod_detail != null)
                        {
                            if (ordlist.Count() > 0)
                            {
                                int count = 0;
                                foreach (cart_items ordupdt in ordlist)
                                {

                                    string Product_ID = "";
                                    string prod_brand = "";


                                    if (ordupdt.child_prod_detail != null)
                                    {
                                        for (int p = 0; p < ordupdt.child_prod_detail.Count; p++)
                                        {
                                            if (ordupdt.child_prod_detail[p].ord_qty != "")
                                            {
                                                selected_size = ordupdt.child_prod_detail[p].size;
                                                Product_ID = ordupdt.child_prod_detail[p].sku;
                                                qty_ordered = convertToDouble(ordupdt.child_prod_detail[p].ord_qty);

                                                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_ID + "'}");
                                                QueryDocument queryDoc = new QueryDocument(document);
                                                MongoCursor cursor1 = dal.execute_mongo_db("product", queryDoc);

                                                rate_per_unit = (ordupdt.order_amount / ordupdt.order_quantity);
                                                //   qty_ordered = ordupdt.order_quantity.ToString();
                                                order_amount = qty_ordered * rate_per_unit;
                                                prod_name = ordupdt.parent_prod_detail.Name;
                                                prod_brand = ordupdt.parent_prod_detail.Brand;

                                                SlNo = convertToString(counter + count);
                                                // strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + (convertToString(dtResult.Rows[i]["Shipping_Address"]) + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + convertToString(ordupdt.brand) + "," + " " + "," + Product_ID + "," + prod_name + "," + selected_size + "," + qty_ordered + "," + rate_per_unit + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + order_amount + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]) );
                                                strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + ",''" + (convertToString(dtResult.Rows[i]["Shipping_Address"]).Replace(",", " ")).Replace("\n", " ") + "''," + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + prod_brand + "," + " " + "," + Product_ID + "," + prod_name + "," + selected_size + "," + qty_ordered + "," + rate_per_unit + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + order_amount + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]) + "," + dtResult.Rows[i]["Invoice_Number"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["Invoice_Amount"].ToString()) + "," + convertToDate(dtResult.Rows[i]["Invoice_Date"]);

                                                sw.WriteLine(strLine);
                                                count++;
                                            }
                                        }
                                    }
                                }
                                counter = convertToInt(SlNo);
                                counter++;
                            }
                        }
                        else
                        {
                            List<cart_item> ordlists = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());
                            if (ordlists.Count() > 0)
                            {
                                int count = 0;
                                foreach (cart_item ordupdt in ordlists)
                                {
                                    if ((ordupdt.cart_item_id != null) || ((convertToInt(dtResult.Rows[i]["Totalamount"]) > 0)))
                                    {
                                        string Prod_ID = "";
                                        selected_size = ordupdt.selected_size;
                                        Prod_ID = ordupdt.sku;
                                        qty_ordered = convertToDouble(ordupdt.quantity);
                                        rate_per_unit = (convertToInt(dtResult.Rows[i]["Totalamount"]) / ordupdt.quantity);
                                        order_amount = convertToDouble(dtResult.Rows[i]["Totalamount"]);
                                        prod_name = ordupdt.name;

                                        SlNo = convertToString(counter + count);
                                        //  strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + (convertToString(dtResult.Rows[i]["Shipping_Address"]) + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + convertToString(ordupdt.brand) + "," + " " + "," + Prod_ID + "," + prod_name + "," + selected_size + "," + qty_ordered + "," + rate_per_unit + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + order_amount + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]));
                                        strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + ",''" + (convertToString(dtResult.Rows[i]["Shipping_Address"]).Replace(",", " ")).Replace("\n", " ") + "''," + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + convertToString(ordupdt.brand) + "," + " " + "," + Prod_ID + "," + prod_name + "," + selected_size + "," + qty_ordered + "," + rate_per_unit + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + order_amount + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]) + "," + dtResult.Rows[i]["Invoice_Number"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["Invoice_Amount"].ToString()) + "," + convertToDate(dtResult.Rows[i]["Invoice_Date"]);
                                        sw.WriteLine(strLine);
                                        count++;
                                    }
                                }
                                counter = convertToInt(SlNo);
                                counter++;

                            }


                        }

                    }


                }
                sw.Close();
                return strReportId;
            }


            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

        }




        public string getExportProdEnqReport(string company, string name, string status)
        {
            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            DataTable dtResult = new DataTable();
            registration_repository rr = new registration_repository();
            try
            {
                Boolean bln = false;
                //evoke mongodb connection method in dal//
                string strFileName = "";
                string strLine = "";
                string strReportId = "";
                string SlNo = "";
                int counter = 1;

                strReportId = "DSWReport_" + company.Replace(" ", "") + System.DateTime.Now.ToLocalTime().ToString("MM-dd-yyyy hh-mm-ss").Replace("-", "").Replace(" ", "");
                strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + strReportId + ".csv";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                if (name == null)
                {
                    name = "";
                }

                if (company == "All")
                {
                    try
                    {

                        dtResult = da.ExecuteDataTable("get_all_prodenquire",
                                          da.Parameter("v_username", name),
                                          da.Parameter("_status", status)
                                          );
                  sw.WriteLine("Sl.No,EnqId,Logo Needed,Date,Prod Id,Size,Sku,Enq Qty,Prod Name,Company,Name,Email,City,Enquiry From,Mobile,Status,Comments,Total EnqQty");

                        if (dtResult != null && dtResult.Rows.Count > 0)
                        {



                            for (int i = 0; i < dtResult.Rows.Count; i++)
                            {
                                int count = 0;
                                string enquire_for_product = "";
                                enquire_for_product = convertToString(dtResult.Rows[i]["size_quantity"]);
                                List<prod_enq> prodenqlist = JsonConvert.DeserializeObject<List<prod_enq>>(enquire_for_product);
                                if (prodenqlist != null)
                                {
                                    for (int x = 0; x < prodenqlist.Count; x++)
                                    {
                                        int ord_qt = 0;
                                        prod_enq prdenq = new prod_enq();
                                        ord_qt = convertToInt(prodenqlist[x].ord_qty);
                                        if (ord_qt > 0)
                                        {
                                            prdenq.id = prodenqlist[x].id;
                                            prdenq.size = prodenqlist[x].size;
                                            prdenq.sku = prodenqlist[x].sku;
                                            prdenq.name = prodenqlist[x].name;
                                            prdenq.ord_qty = prodenqlist[x].ord_qty;

                                            SlNo = convertToString(counter + count);
                                            strLine = SlNo + "," + convertToString(dtResult.Rows[i]["id"]) + "," + convertToString(dtResult.Rows[i]["logo_needed"]) + "," + (convertToString(dtResult.Rows[i]["create_ts"])) + "," + convertToString(prdenq.id) + "," + convertToString(prdenq.size) + "," + convertToString(prdenq.sku) + "," + convertToString(prdenq.ord_qty) + "," + convertToString(prdenq.name) + "," + convertToString(dtResult.Rows[i]["company_name"]) + "," + convertToString(dtResult.Rows[i]["name"]) + "," + convertToString(dtResult.Rows[i]["email"]) + "," + convertToString(dtResult.Rows[i]["city"]) + "," + convertToString(dtResult.Rows[i]["enquire_from"]) + "," + (convertToString(dtResult.Rows[i]["mobile"]) + "," + convertToString(dtResult.Rows[i]["enquire_status"]) + "," + convertToString(dtResult.Rows[i]["comments"]).Replace(",", " ")).Replace("\n", " ") + "," + convertToString(dtResult.Rows[i]["total_enq_qty"]);

                                            sw.WriteLine(strLine);

                                        }


                                    }
                                }
                                counter++;
                            }

                        }
                        sw.Close();
                        return strReportId;

                    }
                    catch (Exception ex)
                    {
                        ExternalLogger.LogError(ex, this, "#");
                        throw ex;
                    }

                }

                else
                {
                    try
                    {

                        dtResult = da.ExecuteDataTable("bulk_prodenquire_search"
                                             , da.Parameter("_company_name", company),
                                               da.Parameter("v_username", name),
                                               da.Parameter("_status", status)
                                             );

                        sw.WriteLine("Sl.No,EnqId,Logo Needed,Date,Prod Id,Size,Sku,Enq Qty,Prod Name,Company,Name,Email,City,Enquiry From,Mobile,Status,Comments,Total EnqQty");

                        if (dtResult != null && dtResult.Rows.Count > 0)
                        {


                            for (int i = 0; i < dtResult.Rows.Count; i++)
                            {
                                int count = 0;
                                string enquire_for_product = "";
                                enquire_for_product = convertToString(dtResult.Rows[i]["size_quantity"]);
                                List<prod_enq> prodenqlist = JsonConvert.DeserializeObject<List<prod_enq>>(enquire_for_product);
                                if (prodenqlist != null)
                                {
                                    for (int x = 0; x < prodenqlist.Count; x++)
                                    {
                                        int ord_qt = 0;
                                        prod_enq prdenq = new prod_enq();
                                        ord_qt = convertToInt(prodenqlist[x].ord_qty);
                                        if (ord_qt > 0)
                                        {
                                            prdenq.id = prodenqlist[x].id;
                                            prdenq.size = prodenqlist[x].size;
                                            prdenq.sku = prodenqlist[x].sku;
                                            prdenq.name = prodenqlist[x].name;
                                            prdenq.ord_qty = prodenqlist[x].ord_qty;

                                            SlNo = convertToString(counter + count);
                                            strLine = SlNo + "," + convertToString(dtResult.Rows[i]["id"]) + "," + convertToString(dtResult.Rows[i]["logo_needed"]) + "," + (convertToString(dtResult.Rows[i]["create_ts"])) + "," + convertToString(prdenq.id) + "," + convertToString(prdenq.size) + "," + convertToString(prdenq.sku) + "," + convertToString(prdenq.ord_qty) + "," + convertToString(prdenq.name) + "," + convertToString(dtResult.Rows[i]["company_name"]) + "," + convertToString(dtResult.Rows[i]["name"]) + "," + convertToString(dtResult.Rows[i]["email"]) + "," + convertToString(dtResult.Rows[i]["city"]) + "," + convertToString(dtResult.Rows[i]["enquire_from"]) + "," + (convertToString(dtResult.Rows[i]["mobile"]) + "," + convertToString(dtResult.Rows[i]["enquire_status"]) + "," + convertToString(dtResult.Rows[i]["comments"]).Replace(",", " ")).Replace("\n", " ") + "," + convertToString(dtResult.Rows[i]["total_enq_qty"]);

                                            sw.WriteLine(strLine);

                                        }


                                    }
                                }
                                counter++;
                            }

                        }
                        sw.Close();
                        return strReportId;

                    }
                    catch (Exception ex)
                    {
                        ExternalLogger.LogError(ex, this, "#");
                        throw ex;
                    }


                }
            }
                
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

        }

        public string getExportGrassCamp(string CampaignType)
        {
            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            DataTable dtResult = new DataTable();
            registration_repository rr = new registration_repository();
            try
            {
                Boolean bln = false;
                //evoke mongodb connection method in dal//
                string strFileName = "";
                string strLine = "";
                string strReportId = "";
                string SlNo = "";
                int counter = 1;

                strReportId = "DSWReport_" + "GrassCampaign" + System.DateTime.Now.ToLocalTime().ToString("MM-dd-yyyy hh-mm-ss").Replace("-", "").Replace(" ", "");
                strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + strReportId + ".csv";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                //if (name == null)
                //{
                //    name = "";
                //}

                //if (company == "All")
                //{
                //try
                //{

                dtResult = da.ExecuteDataTable("get_grass_camp_data",
                                  da.Parameter("_comments", CampaignType)

                                  );
                sw.WriteLine("Sl.No,Id.No,Name,Mobile,Landline,City,CompanyName,CompanyEmail,Designation,Address,Pincode,ProductName,ProductQty,CampaignType,Date");

                if (dtResult != null && dtResult.Rows.Count > 0)
                {



                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        int count = 0;
                        //string enquire_for_product = "";
                        //enquire_for_product = convertToString(dtResult.Rows[i]["size_quantity"]);
                        //List<prod_enq> prodenqlist = JsonConvert.DeserializeObject<List<prod_enq>>(enquire_for_product);
                        //if (prodenqlist != null)
                        //{
                        //    for (int x = 0; x < prodenqlist.Count; x++)
                        //    {
                        //        int ord_qt = 0;
                        //        prod_enq prdenq = new prod_enq();
                        //        ord_qt = convertToInt(prodenqlist[x].ord_qty);
                        //        if (ord_qt > 0)
                        //        {
                        //            prdenq.id = prodenqlist[x].id;
                        //            prdenq.size = prodenqlist[x].size;
                        //            prdenq.sku = prodenqlist[x].sku;
                        //            prdenq.name = prodenqlist[x].name;
                        //            prdenq.ord_qty = prodenqlist[x].ord_qty;

                        SlNo = convertToString(counter + count);
                        strLine = SlNo + "," + convertToString(dtResult.Rows[i]["id"]) + "," + convertToString(dtResult.Rows[i]["name"]) + "," + (convertToString(dtResult.Rows[i]["mobile"])) + "," + (convertToString(dtResult.Rows[i]["landline"])) + "," + (convertToString(dtResult.Rows[i]["city"])) + "," + (convertToString(dtResult.Rows[i]["company_name"])) + "," + (convertToString(dtResult.Rows[i]["company_email"])) + "," + (convertToString(dtResult.Rows[i]["designation"])) + "," + (convertToString(dtResult.Rows[i]["address"]).Replace(",", " ")).Replace("\n", " ") + "," + (convertToString(dtResult.Rows[i]["pincode"])) + "," + (convertToString(dtResult.Rows[i]["product_name"])) + "," + (convertToString(dtResult.Rows[i]["product_qty"])) + "," + convertToString(dtResult.Rows[i]["comments"]) + "," + (convertToString(dtResult.Rows[i]["created_ts"]));

                        sw.WriteLine(strLine);

                        //  }


                        //  }
                        //  }
                        counter++;
                    }

                }
                sw.Close();
                return strReportId;

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

        }


        public string getNewsletterData(string email)
        {
            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            DataTable dtResult = new DataTable();
            registration_repository rr = new registration_repository();
            try
            {
                Boolean bln = false;
                //evoke mongodb connection method in dal//
                string strFileName = "";
                string strLine = "";
                string strReportId = "";
                string SlNo = "";
                int counter = 1;

                strReportId = "DSWReport_" + "GrassCampaign" + System.DateTime.Now.ToLocalTime().ToString("MM-dd-yyyy hh-mm-ss").Replace("-", "").Replace(" ", "");
                strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + strReportId + ".csv";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                if (email == null)
                {
                    email = "";
                }

                //if (company == "All")
                //{
                //try
                //{

                dtResult = da.ExecuteDataTable("get_newsletter_data",
                                  da.Parameter("_email", email)

                                  );
                sw.WriteLine("Sl.No,Id.No,Name,Email,Telephone,Company,Designation,Date");

                if (dtResult != null && dtResult.Rows.Count > 0)
                {



                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        int count = 0;
                        //string enquire_for_product = "";
                        //enquire_for_product = convertToString(dtResult.Rows[i]["size_quantity"]);
                        //List<prod_enq> prodenqlist = JsonConvert.DeserializeObject<List<prod_enq>>(enquire_for_product);
                        //if (prodenqlist != null)
                        //{
                        //    for (int x = 0; x < prodenqlist.Count; x++)
                        //    {
                        //        int ord_qt = 0;
                        //        prod_enq prdenq = new prod_enq();
                        //        ord_qt = convertToInt(prodenqlist[x].ord_qty);
                        //        if (ord_qt > 0)
                        //        {
                        //            prdenq.id = prodenqlist[x].id;
                        //            prdenq.size = prodenqlist[x].size;
                        //            prdenq.sku = prodenqlist[x].sku;
                        //            prdenq.name = prodenqlist[x].name;
                        //            prdenq.ord_qty = prodenqlist[x].ord_qty;

                        SlNo = convertToString(counter + count);
                        strLine = SlNo + "," + convertToString(dtResult.Rows[i]["_id"]) + "," + convertToString(dtResult.Rows[i]["name"]) + "," + (convertToString(dtResult.Rows[i]["email_id"])) + "," + (convertToString(dtResult.Rows[i]["telephone"])) + "," + (convertToString(dtResult.Rows[i]["company"])) + "," + (convertToString(dtResult.Rows[i]["designation"])) + "," + (convertToString(dtResult.Rows[i]["created_ts"]));

                        sw.WriteLine(strLine);

                        //  }


                        //  }
                        //  }
                        counter++;
                    }

                }
                sw.Close();
                return strReportId;

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

        }
        

       
        public string getExportCustPointBalReport(string company, string email_id)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            registration_repository rr = new registration_repository();
            try
            {
                Boolean bln = false;
                //evoke mongodb connection method in dal//
                string result = "";
                string toemail = "";
                string strFileName = "";
                string strLine = "";
                string strReportId = "";
                strReportId = "CPBReport_" + company.Replace(" ", "") + System.DateTime.Now.ToLocalTime().ToString("MM-dd-yyyy hh-mm-ss").Replace("-", "").Replace(" ", "");


                strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + strReportId + ".csv";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);


                dtResult = da.ExecuteDataTable("get_user_points"
                               , da.Parameter("_company", company)
                               , da.Parameter("_email_id", email_id)
                               );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {

                    sw.WriteLine("Sl.No, First Name,Last Name, Email/Mobile No, Alternate Email, Mobile No., Location, Points Balance, Points Alloted");
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        string SlNo = convertToString(i + 1);
                        strLine = SlNo + "," + convertToString(dtResult.Rows[i]["first_name"]) + "," + convertToString(dtResult.Rows[i]["last_name"]) + "," + convertToString(dtResult.Rows[i]["email_id"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["email_id"])) + "," + convertToString(dtResult.Rows[i]["mobile_no"]) + ",''" + (convertToString(dtResult.Rows[i]["location"]).Replace(",", " ")).Replace("\n", " ") + "''," + convertToString(dtResult.Rows[i]["PointsBalance"]) + "," + convertToString(dtResult.Rows[i]["PointsAlloted"]);
                        sw.WriteLine(strLine);
                    }
                    sw.Close();

                }
                return strReportId;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }




        }

        public List<OutCollection> postDeleteAssignRights(user_assign_rigths uar)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();


            try
            {

                List<long> RIGHT_ID;
                RIGHT_ID = uar.RIGHTS_IDS;

                for (int i = 0; i < RIGHT_ID.Count; i++)
                {

                    int j = da.ExecuteSP("user_rights_ins_del", ref oc
                    , da.Parameter("_user_id", uar.USER_ID)
                    , da.Parameter("_right_id", convertToLong(RIGHT_ID[i].ToString()))
                    , da.Parameter("_created_by", "admin")
                    , da.Parameter("_created_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_flag", 2)
                    , da.Parameter("_outmsg", String.Empty, true));

                }



            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }

        public List<user_rigths> getuser_assign_rights(string USER_NAME, long Comp_ID)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            List<user_rigths> lur = new List<user_rigths>();
            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_user_assign_rights"
                                , da.Parameter("_user_name", USER_NAME)
                                , da.Parameter("_company", Comp_ID)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        user_rigths our = new user_rigths();
                        our.USER_ID = convertToLong(dtResult.Rows[i]["user_id"].ToString());
                        our.user_right_id = convertToLong(dtResult.Rows[i]["user_right_id"].ToString());
                        lur.Add(our);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return lur;
        }

        public string insert_check_details(List<ecomm.util.entities.check_payment> op)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            payment_repository pr = new payment_repository();
            registration_repository rr = new registration_repository();
            store_repository sr = new store_repository();
            // op.date = new DateTime();
            // List<order_payment> lst = new List<order_payment>();


            string guid = "";
            int order_quantity = 0;
            string product_id = "";
            string user_id = "";
            int i = 0;
            string Order_id = "";



            try
            {
                foreach (ecomm.util.entities.check_payment odr in op)
                {
                    Order_id = odr.order_id.ToString();

                    i = da.ExecuteSP("insert_order_payment_details", ref oc
                                   , da.Parameter("_order_id", odr.order_id)
                                    , da.Parameter("_sl_no", odr.sln_no)
                                   , da.Parameter("_transaction_no", odr.transaction_no)
                                   , da.Parameter("_cheque_no", odr.check_number)
                                   , da.Parameter("_bank_name", odr.banks_name)
                                   , da.Parameter("_payment_mode", odr.payment_mode)
                                   , da.Parameter("_transaction_date", odr.trn_date)
                                   , da.Parameter("_outmsg", String.Empty, true)
                                   );
                }

                if (i == 1)
                {

                    DataTable dtorderdtl = new DataTable();
                    dtorderdtl = pr.GetDataFromOrder(Convert.ToInt64(Order_id));

                    if (dtorderdtl != null || dtorderdtl.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtorderdtl.Rows.Count; j++)
                        {

                            string cart_data = "";
                            cart_data = dtorderdtl.Rows[j]["cart_data"].ToString();

                            List<cart_items> orderlist = JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString());

                            if (orderlist != null)
                            {
                                for (int x = 0; x < orderlist.Count; x++)
                                {

                                    List<child_cart_items> lst = new List<child_cart_items>(orderlist[x].child_prod_detail);
                                    for (int z = 0; z < lst.Count; z++)
                                    {
                                        order_quantity = convertToInt(lst[z].ord_qty);

                                        product_id = convertToString(lst[z].id);

                                        List<product> block_quantity = get_block_details(product_id);

                                        for (int y = 0; y < block_quantity.Count; y++)
                                        {
                                            int quantity = convertToInt(block_quantity[y].block_number) * 1 + order_quantity;


                                            string q = "{ id : '" + product_id + "'}";
                                            QueryDocument queryDoc = createQueryDocument(q);

                                            UpdateBuilder u = new UpdateBuilder();
                                            u.Set("block_number", quantity);

                                            // exec
                                            dal.DataAccess dal = new DataAccess();
                                            BsonDocument result = dal.mongo_find_and_modify("product", queryDoc, u);
                                        }


                                    }

                                }
                            }


                        }


                    }


                    DataTable dt = pr.GetDataFromOrder(Convert.ToInt64(Order_id));
                    user_id = dt.Rows[0]["user_id"].ToString();

                    string order_id = Order_id.ToString();
                    string email_id = "";
                    email_id = user_id.ToString();

                    string email_body = "";
                    email_body = GetCheckOfflinePaymentMailBody(op, ref email_id, "Grass");



                    helper_repository hr = new helper_repository();

                    // email_id = ConfigurationSettings.AppSettings["MailReplyTo"].ToString();

                    // string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();

                    string bcclist = ConfigurationSettings.AppSettings["Management"].ToString() + "," + ConfigurationSettings.AppSettings["bcc_email"].ToString();


                    if (email_id.Length > 0 && email_body.Length > 0)
                    {


                        if (sr.cart_exists(rr.getUserEMailAddress(email_id)))
                        {
                            //Response.Write("cart Exists?");
                            sr.del_cart(email_id);
                        }

                        hr.SendMail(email_id, bcclist, "GRASS : Order No " + order_id + " : Received and is being verified", email_body);


                    }

                    return "send email";

                    //  return "Success";
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }


        private QueryDocument createQueryDocument(string s)
        {
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(s);
            QueryDocument queryDoc = new QueryDocument(document);
            return queryDoc;
        }





        public List<product> get_block_details(string id)
        {
            // evoke mongodb connection method in dal
            var products = new List<product>();
            try
            {
                dal.DataAccess dal = new DataAccess();

                //MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);


                string q = "{id:'" + id + "'}";// qs;//"{cat_id:{id:'" + cat_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] {"block_number"};
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                foreach (var c in cursor)
                {

                    product prod = new product();
                   // prod.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prod.block_number = check_field(c.ToBsonDocument(), "block_number") ? convertToDouble(c.ToBsonDocument()["block_number"]) : 0.0;                  
                    products.Add(prod);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products;
        }


       

        public string insert_transaction_details(ecomm.util.entities.order_payment op)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            payment_repository pr = new payment_repository();
            registration_repository rr = new registration_repository();
            store_repository sr = new store_repository();
           // op.date = new DateTime();
            

            string guid = "";
            int order_quantity = 0;
            string product_id = "";
            string user_id ="";
         
            try
            {

                int i = da.ExecuteSP("insert_order_payment_details", ref oc
                                , da.Parameter("_order_id", op.order_id)
                                , da.Parameter("_transaction_no", op.transaction_no)
                                , da.Parameter("_cheque_no", op.check_no)
                                , da.Parameter("_bank_name", op.bank_name)
                                , da.Parameter("_payment_mode", op.payment_mode)
                                , da.Parameter("_transaction_date", op.date)
                                , da.Parameter("_outmsg", String.Empty, true)
                                , da.Parameter("_sl_no", op.sln_no)
                                );

                if (i == 1)
                {

                    DataTable dtorderdtl = new DataTable();
                    dtorderdtl = pr.GetDataFromOrder(op.order_id);

                    if (dtorderdtl != null || dtorderdtl.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtorderdtl.Rows.Count; j++)
                        {

                           string cart_data = "";
                           cart_data = dtorderdtl.Rows[j]["cart_data"].ToString();

                           List<cart_items> orderlist = JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString());

                           if (orderlist != null)
                           {
                               for ( int x = 0; x < orderlist.Count; x++)
                               {

                                   List<child_cart_items> lst = new List<child_cart_items>(orderlist[x].child_prod_detail);
                                   for (int z = 0; z < lst.Count; z++)
                                   { 
                                      order_quantity  = convertToInt(lst[z].ord_qty);

                                      product_id = convertToString(lst[z].id);

                                      List<product> block_quantity = get_block_details(product_id);

                                      for (int y = 0; y < block_quantity.Count; y++)
                                      {
                                          int quantity = convertToInt(block_quantity[y].block_number) * 1 + order_quantity;


                                          string q = "{ id : '" + product_id + "'}";
                                          QueryDocument queryDoc = createQueryDocument(q);

                                          UpdateBuilder u = new UpdateBuilder();
                                          u.Set("block_number", quantity);

                                          // exec
                                          dal.DataAccess dal = new DataAccess();
                                          BsonDocument result = dal.mongo_find_and_modify("product", queryDoc, u);
                                      }


                                   }
                                      
                         
                                  
                               }
                           }

                           

                        }

                                              
                    }


                    DataTable dt = pr.GetDataFromOrder(Convert.ToInt64(op.order_id));                  
                    user_id = dt.Rows[0]["user_id"].ToString();
                    
                    string order_id = op.order_id.ToString();
                    string email_id = "";
                    email_id = user_id.ToString();
                       
                    string email_body = "";
                    email_body = GetOfflinePaymentMailBody(op, ref email_id, "grass");

                   
                    helper_repository hr = new helper_repository();
                   
                   // email_id = ConfigurationSettings.AppSettings["MailReplyTo"].ToString();

                   string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();


                   if (email_id.Length > 0 && email_body.Length > 0)
                    {


                        if (sr.cart_exists(rr.getUserEMailAddress(email_id)))
                        {
                            //Response.Write("cart Exists?");
                            sr.del_cart(email_id);
                        }

                        hr.SendMail(email_id, bcclist, "Payment Details: Grass", email_body);

                        
                    }

                    return "send email";

                 //  return "Success";
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }


        private string ConvertStringArrayToString(List<string> lst)
        {
            //
            // Concatenate all the elements into a StringBuilder.
            //
            StringBuilder builder = new StringBuilder();
            foreach (string value in lst)
            {
                builder.Append(value);
                builder.Append(',');
            }
            return builder.ToString();
        }


        private string ConvertIntArrayToString(List<int> lst)
        {
            //
            // Concatenate all the elements into a StringBuilder.
            //
            StringBuilder builder = new StringBuilder();
            foreach (int value in lst)
            {
                builder.Append(value);
                builder.Append(',');
            }
            return builder.ToString();
        }

        

        //public string GetOfflinePaymentMailBody(string OrderID,ref string email_id)
        //{

        //    try
        //    {
        //        string mailBody = string.Empty;
        //        string cart_data = "";
        //        double shipmentvalue = 0;
        //        double discount = 0;
        //        double finalamt = 0;
        //        double paidamt = 0;
        //        double totalamount = 0;
        //        string vouchers = "";
        //        Int64 points = 0;
        //        double lineitemprice = 0;
        //        double pointamt = 0;
        //        double voucheramt = 0;
        //        double PromoOffer = 0;
        //        string strStoreName = "";
        //        string strLogo = "";
        //        string ShippingName = "";
        //        string ShippingAddress = "";
        //        string ShippingState = "";
        //        string ShippingCity = "";
        //        string ShippingPin = "";
        //        string shippingAmount = "";
        //        string strMobileNo = "";
        //        string strCompanyUrl = "";
        //        string strMyorderURL = "";
        //        string expshippingAmount = "0";
        //        string discount_code = "";
        //        string discount_rule = "";
        //        string strMailSubCompany = "";
        //        int order_quantity = 0;
        //        string pr_qnty = "";
        //        double ttlamount = 0;                
        //        string prod_name = "";
        //        List<string> lst_name = new List<string>();
        //        List<int> lst_qnty = new List<int>();
        //        string ShippingThreshold = ConfigurationSettings.AppSettings["shipping_threshold"].ToString();
        //        shippingAmount = ConfigurationSettings.AppSettings["shipping_amount"].ToString();
        //        payment_repository pr = new payment_repository();
        //        DataTable dt = pr.GetDataFromOrder(Convert.ToInt64(OrderID));
        //        DataTable dtShip = pr.GetDataFromUserID(Convert.ToInt64(OrderID));

        //        if (dt == null || dt.Rows.Count == 0)
        //            return "No Such order";

        //        if ((dtShip == null) || (dtShip.Rows.Count == 0))
        //        {
        //            return "No Shipping Information";
        //        }
        //        else
        //        {
        //            ShippingName = dtShip.Rows[0]["name"].ToString();
        //            ShippingAddress = dtShip.Rows[0]["address"].ToString();
        //            ShippingCity = dtShip.Rows[0]["city"].ToString();
        //            ShippingState = dtShip.Rows[0]["state"].ToString();
        //            ShippingPin = dtShip.Rows[0]["pincode"].ToString();
        //            strMobileNo = dtShip.Rows[0]["mobile_number"].ToString();
        //        }


        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            email_id = dt.Rows[i]["user_id"].ToString();
        //            cart_data = dt.Rows[i]["cart_data"].ToString();
        //            paidamt = Convert.ToDouble(dt.Rows[i]["paid_amount"].ToString());
        //            totalamount = Convert.ToDouble(dt.Rows[i]["total_amount"].ToString());
        //            points = convertToLong(dt.Rows[i]["points"]); ;
        //            pointamt = convertToDouble(dt.Rows[i]["points_amount"]); ;
        //            vouchers = dt.Rows[i]["egift_vou_no"].ToString();
        //            voucheramt = convertToDouble(dt.Rows[i]["egift_vou_amt"]);
        //            strStoreName = Convert.ToString(dt.Rows[i]["store"].ToString().Trim());

        //            if (dt.Rows[i]["discount_code"] != null)
        //            {
        //                discount_code = dt.Rows[i]["discount_code"].ToString();
        //            }
        //            break;
        //        }

        //        if (discount_code != null || discount_code != "")
        //        {

        //            DataTable dtDiscInfo = pr.GetDiscountCodeInfo(discount_code);

        //            if (dtDiscInfo == null || dtDiscInfo.Rows.Count > 0)
        //            {
        //                for (int p = 0; p < dtDiscInfo.Rows.Count; p++)
        //                {
        //                    discount_rule = dtDiscInfo.Rows[p]["rule"].ToString();
        //                }

        //            }

        //            disc_rule odrulelist = JsonConvert.DeserializeObject<disc_rule>(discount_rule.ToString());

        //            if (odrulelist != null)
        //            {
        //                PromoOffer = convertToDouble(odrulelist.discount_value);
        //            }
        //        }

        //        DataTable dtCompany = pr.GetCompanyInfo(strStoreName);
        //        if (dtCompany != null && dtCompany.Rows.Count > 0)
        //        {
        //            strLogo = dtCompany.Rows[0]["logo"].ToString();
        //            strCompanyUrl = dtCompany.Rows[0]["root_url_1"].ToString();
        //            strMailSubCompany = dtCompany.Rows[0]["displayname"].ToString();

        //            if (dtCompany.Rows[0]["shipping_charge"] == null)
        //                shippingAmount = "0";
        //            else
        //                shippingAmount = dtCompany.Rows[0]["shipping_charge"].ToString();

        //            if (dtCompany.Rows[0]["min_order_for_free_shipping"] == null)
        //                ShippingThreshold = "0";
        //            else
        //                ShippingThreshold = dtCompany.Rows[0]["min_order_for_free_shipping"].ToString();

        //            if (shippingAmount == "")
        //                shippingAmount = "0";

        //            if (ShippingThreshold == "")
        //                ShippingThreshold = "0";
        //            strMyorderURL = strCompanyUrl + "index.html#/myorder";
        //        }

        //        if (cart_data.Length == 0)
        //            return "Invalid Order";


        //       // List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());
        //        List<cart_items> ordlist = JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString());
                
        //        //            totalamount = orderlist[j].order_amount;

        //        for (int j = 0; j < ordlist.Count; j++)
        //        {
        //            order_quantity = convertToInt(ordlist[j].order_quantity);

        //            lst_qnty.Add(order_quantity);
        //            string o = ordlist[j].parent_prod_detail.ToString();
        //            parent_cart_items position = JsonConvert.DeserializeObject<parent_cart_items>(o);
        //            string p_name = position.Name.ToString();
        //            lst_name.Add(p_name);
        //            ttlamount = ordlist[j].order_amount;

        //            //lineitemprice = ord.quantity * ord.final_offer;
        //        }
        //        //Update Stock here
        //        if (ordlist.Count > 0)
        //        {
        //            //Update Discount Coupon
        //            UpdateDiscountCode(email_id, discount_code);

        //            ecomm.model.repository.category_repository cr = new ecomm.model.repository.category_repository();
        //            //foreach (cart_item ordupdt in ordlist)
        //            //{
        //            //    cr.update_stock_qty(ordupdt.id, Convert.ToDouble(ordupdt.quantity), "Minus");
        //            //}
        //        }
        //        //End of Stock Update
        //        mailBody += "<html>";
        //        mailBody += "<head>";
        //        mailBody += "</head>";
        //        mailBody += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
        //        mailBody += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
        //        mailBody += "    <tr>";
        //        mailBody += "        <td style='background: #f5f5f5'>";
        //        mailBody += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
        //        mailBody += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
        //        mailBody += "<div style='padding: 5px;'>";
        //        mailBody += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
        //        mailBody += "		            <tr>";
        //        mailBody += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
        //        mailBody += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg' /></td>";
        //        mailBody += "			            <td style='width: 300px;;'></td>";
        //        mailBody += "			            <td style='width: 35px;' ><a href='http://www.facebook.com/annectosindia'><img src='img/emails/facebook.png' /></a></td>";
        //        mailBody += "			            <td style='width: 35px;' ><a href='https://twitter.com/annectos'><img src='img/emails/twitter.png' /></a></td>";
        //        mailBody += "			            <td style='width: 35px;'><a href=http://www.linkedin.com/company/annecto-rewards-&-retail-pvt-ltd-/'><img src='img/emails/linkedin.png' /></a></td>";
        //        mailBody += "		            </tr>";
        //        mailBody += "		            </table>";
        //        mailBody += "		            </div>";
        //        mailBody += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
        //        mailBody += "			            <table cellpadding='0' cellspacing='0'  >";
        //        mailBody += "		            <tr>";
        //        mailBody += "		            <td> <img src='img/emails/shiped.png' /></td>";
        //        mailBody += "		            <td style='font-size: 12px; font-style:italic; padding-left: 10px;'> Free Shipping for purchase above 500</td>";
        //        mailBody += "		            </tr>";
        //        mailBody += "		            </table>";
        //        mailBody += "		            </div>";
        //        mailBody += "            </div>";
        //        mailBody += "        </td>";
        //        mailBody += "    </tr>";
        //        mailBody += "    <tr>";
        //        mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
        //        mailBody += "        Hi " + ShippingName + ", <br /><br />";
        //        mailBody += "";
        //        mailBody += "Thank you for shopping at " + strCompanyUrl + "<br /><br />";
        //        mailBody += "This email contains your order summary. When the item(s) in your order are shipped, you will receive an email with the Courier Tracking ID and the link where you can track your order. You can also check the status of your order by <a style='color: #41a0ff; text-decoration:none' href='" + strMyorderURL + "'>clicking here</a>.<br /><br />";
        //        mailBody += "Please find below the summary of your order";
        //        mailBody += "        </td>";
        //        mailBody += "        </tr>";
        //        mailBody += "        <tr>";
        //        mailBody += "        <td style='background: #fff; padding: 0px 10px;  font-size: 16px;color:#3b3a3a; font-weight:bold '>";
        //        mailBody += "        <div style='border-top:1px dotted #cccccc; border-bottom:1px dotted #cccccc; padding-top: 5px; padding-bottom: 5px;'>";
        //        mailBody += "        Order No. #" + OrderID;
        //        mailBody += "        </div>";
        //        mailBody += "        </td>";
        //        mailBody += "        </tr>";
        //        mailBody += "    <tr>";
        //        mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
        //        mailBody += "        <table cellpadding='0' cellspacing='0' width='100%' >";

        //        pr_qnty = ConvertIntArrayToString(lst_qnty);
        //        prod_name = ConvertStringArrayToString(lst_name);
        //        int k = 0;
        //        //Loop for Product - Start
        //     //  foreach (cart_item ord in ordlist)
        //       // {
        //            k = k + 1;
        //          //  lineitemprice = ord.quantity * ord.final_offer;

        //            mailBody += "            <tr>";
        //            //mailBody += "             <td colspan='6' style='color: #333333; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold' >  Product Name: " + prod_name + "</td>	";
        //            mailBody += "            </tr>";


        //            mailBody += "            <tr>";
        //            //mailBody += "             <td style='text-align: center; padding-left: 10px; padding-right: 10px;'  ><img src='" + ord.image.thumb_link + "' /></td>	";
        //            mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Product Name <div style='margin-top: 10px; color: #333333; '>" + prod_name + "</div></div></td>";
        //            mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Quantity <div style='margin-top: 10px; color: #333333; '>" + pr_qnty + "</div></div></td>";
        //            mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Estimated Dispatch Date: <div style='margin-top: 10px; color: #333333; '>"+"In Process"+"</div></div></td>";
        //           // mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Unit Price <div style='margin-top: 10px; color: #333333'>" + ord.mrp + "</div></div></td>";
        //           // mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Discount <div style='margin-top: 10px; color: #ff0000'>" + ord.discount + " %" + "</div></div></td> ";    //Diwakar 30/01/2014
        //            mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'> <div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Sub Total <div style='margin-top: 10px; color: #333333'>" + ttlamount + "</div></div></td>";
        //            mailBody += "            </tr>";

        //            shipmentvalue = shipmentvalue + lineitemprice;
        //          //  discount = discount + Convert.ToDouble(ord.discount);
        //            finalamt = shipmentvalue;
        //       // }

        //        if (finalamt >= Convert.ToDouble(ShippingThreshold))
        //        {
        //            shippingAmount = "Free";
        //        }
        //        else
        //            finalamt = finalamt + Convert.ToDouble(shippingAmount);

        //        expshippingAmount = Convert.ToString(totalamount - finalamt);

        //        //Loop for Product - End 
        //        /*
        //        mailBody += "            <tr>";
        //        mailBody += "             <td colspan='6' style='color: #333333; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold' >2. Product Code: 00328 - MeeMee Baby Carrier (Black)</td>	";
        //        mailBody += "            </tr>";
            
        //        mailBody += "            <tr>";
        //        mailBody += "             <td style='text-align: center; padding-left: 10px; padding-right: 10px;'  ><img src='img/emails/product2.png' /></td>	";
        //        mailBody += "             <td style='color: #666666; font-size: 13px; padding-left: 10px; padding-right: 10px; text-align: center;'>Size <div style='margin-top: 10px; color: #333333'>-</div></td>";
        //        mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Quantity <div style='margin-top: 10px; color: #333333; '>1</div></div></td>";
        //        mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Unit Price <div style='margin-top: 10px; color: #333333'>1200 </div></div></td>";
        //        mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Discount <div style='margin-top: 10px; color: #ff0000'>201</div></div></td>";
        //        mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'> <div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Sub Total <div style='margin-top: 10px; color: #333333'>999</div></div></td>";
        //        mailBody += "            </tr>";
        //         */
        //        mailBody += "            <tr>";
        //        mailBody += "	            <td style='text-align: right; padding:10px 0px;' colspan='5' >";
        //        mailBody += "	            <div style='border-top: 1px dotted #ccc; width:100%; float:left;  padding:5px 0px '>";
        //        mailBody += "	            <div style='float: right; text-align:left'>";
        //      //  mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Shipment Value </div>";
        //        //mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Discount   </div> ";
        //      //  mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Shipping   </div>";
        //       // mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Express Shipping   </div>";
        //        //mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Points Redeemed   </div>";
        //       // mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Gift Voucher    </div>";
        //      //  mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Promo Code    </div>";
        //      //  mailBody += "		            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Cash/Card Paid  </div>";
        //        mailBody += "		            </div>";
        //        mailBody += "	            </div>";
        //        mailBody += "	            </td>";
        //        mailBody += "	            <td style='text-align: right'>";
        //        mailBody += "	            <div style='border-top: 1px dotted #ccc; padding:5px 0px '>";
        //        mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>" + shipmentvalue.ToString() + "</div>";
        //        //mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#ff0000; '>" + discount.ToString() + "</div>";
        //        mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + shippingAmount.ToString() + "</div>";
        //        mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + expshippingAmount.ToString() + "</div>";
        //        mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + points.ToString() + "</div>";
        //        mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + voucheramt.ToString() + "</div>";
        //        mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + PromoOffer.ToString() + "</div>";
        //        mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#038203; '>" + paidamt.ToString() + "</div>";
        //        mailBody += "	            </div>";
        //        mailBody += "	            </td>";
        //        mailBody += "            </tr>";
        //        mailBody += "            <tr>";
        //        mailBody += "	            <td valign='top' colspan='5' >";
        //        mailBody += "	            <div style='float: right; text-align:left; border-top: 1px dotted #ccc; width: 110px; padding-left: 40px; padding-top:5px;'>";
        //        mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>Amount paid   </div>";
        //        mailBody += "	            </div>";
        //        mailBody += "	            </td>";
        //        mailBody += "	            <td  valign='top'  style='text-align: right'>";
        //        mailBody += "	            <div style='border-top: 1px dotted #ccc; padding:5px 0px '>";
        //        mailBody += "	            <div style='font-weight: bold; padding-right:20px; font-size:12px; color:#333333; '>" + totalamount.ToString() + "</div>"; //finalamt.ToString()
        //        mailBody += "	            </div>";
        //        mailBody += "	            </td>";
        //        mailBody += "            </tr>";
        //        mailBody += "            <tr>";
        //        mailBody += "	            <td valign='top' style='font-size: 12px; padding-top:20px; color: #333333' colspan='6' >";
        //        mailBody += "	            <b>Shipping Address:</b>    <br />";
        //        mailBody += ShippingName + " <br />    ";
        //        mailBody += ShippingAddress + ",  <br />    ";
        //        mailBody += ShippingCity + " <br />    ";
        //        mailBody += ShippingState + " - " + ShippingPin + " <br />";
        //        mailBody += "Mobile: " + strMobileNo + " <br /> <br />";
        //        //mailBody += "You can track or manage your order at any time by going to <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/myaccounts.php?view=myorders</a>. If you need any assistance or have any questions, feel free to contact us by using the web form at <a href='#' style='color: #41a0ff; text-decoration:none'>http://www.annectos.in/contactus</a> or call us at +91 9686202046 | +91 9972334590";
        //        mailBody += "If you need any assistance or have any questions, feel free to contact us by using the web form at <a href='" + strCompanyUrl + "index.html#/contact_us' style='color: #41a0ff; text-decoration:none'>" + strCompanyUrl + "index.html#/contact_us" + "</a> or call us at +91 9686202046 | +91 9972334590";
        //        mailBody += "	            </td>";
        //        mailBody += "	            </tr>";
        //        mailBody += "        </table>";
        //        mailBody += "        </td>";
        //        mailBody += "        </tr>";
        //        mailBody += "<tr>";
        //        mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";

        //        //Promotion/Marketting
        //        /*
        //        mailBody += "        <div style='font-size: 18px; font-weight:bold; color:#333333; text-transform:uppercase; line-height:30px; border-bottom: 1px solid #ccc'>You may also consider</div>";
        //        mailBody += "        <div style='padding-top: 10px'>";
        //        mailBody += "            <table cellspacing='0' cellpadding='5' style='width: 100%'>";
        //        mailBody += "	            <tr>";
        //        mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
        //        mailBody += "			            <div><img src='img/emails/offer1.png' /></div>";
        //        mailBody += "		            </td>";
        //        mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
        //        mailBody += "			            <div><img src='img/emails/offer2.png' /></div>";
        //        mailBody += "		            </td>";
        //        mailBody += "		            <td align='left' style='width: 33.3%; padding:0px 10px'>";
        //        mailBody += "			            <div><img src='img/emails/offer3.png' /></div>";
        //        mailBody += "		            </td>";
        //        mailBody += "	            </tr>";
        //        mailBody += "	            <tr>";
        //        mailBody += "		            <td align='left' style='width: 33.3%'>";
        //        mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>Rooptex Kurthi</div>";
        //        mailBody += "		            </td>";
        //        mailBody += "		            <td align='left' style='width: 33.3%'>";
        //        mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>Grass Black Crew Neck ";
        //        mailBody += "Sporty T Shirt</div>";
        //        mailBody += "		            </td>";
        //        mailBody += "		            <td align='left' style='width: 33.3%'>";
        //        mailBody += "			            <div style='font-size: 13px; color:#333; font-weight: bold; padding-left: 10px;'>US Polo Assn Blue Shirt</div>";
        //        mailBody += "		            </td>";
        //        mailBody += "	            </tr>";
        //        mailBody += "	            <tr>";
        //        mailBody += "		            <td  align='left' style='width: 33.3%; '>";
        //        mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
        //        mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
        //        mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
        //        mailBody += "			            </div>";
        //        mailBody += "		            </td>";
        //        mailBody += "		            <td  align='left' style='width: 33.3%; '>";
        //        mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
        //        mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
        //        mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
        //        mailBody += "			            </div>";
        //        mailBody += "		            </td>";
        //        mailBody += "		            <td  align='left' style='width: 33.3%; '>";
        //        mailBody += "			            <div style='font-size: 12px; color:#333; border-top:1px dotted #ccc;  padding:5px 10px;'>";
        //        mailBody += "			            <div><span style='color:#666666'>MRP: <span style='text-decoration: line-through'>1790</span></span> | 60% off </div>";
        //        mailBody += "			            <div style='color: #ff6600'>Offer: 758 <span style='color: #333'>|</span> Points: 758 </div>";
        //        mailBody += "			            </div>";
        //        mailBody += "		            </td>";
        //        mailBody += "	            </tr>";
        //        mailBody += "            </table>";
        //        mailBody += "        </div>";
        //         */
        //        mailBody += "        </td>";
        //        mailBody += "</tr>";
        //        /*mailBody += "<tr>";
        //        mailBody += "        <td style='background: #fff; padding: 10px; font-size: 13px'>";
        //        mailBody += "        <table style='width: 100%' cellpadding='0' cellspacing='0'>";
        //        mailBody += "            <tr>";
        //        mailBody += "	            <td align='left'><a href='#'><img src='img/emails/add1.png' /></a></td>";
        //        mailBody += "	            <td align='right'><a href='#'><img src='img/emails/add2.png' /></a></td>";
        //        mailBody += "            </tr>";
        //        mailBody += "        </table>";
        //        mailBody += "        </td>";
        //        mailBody += "        </tr>";*/
        //        mailBody += "</table>";
        //        /*mailBody += "<table cellpadding='0' cellspacing='0' width='500' align='center'  style=' font-size: 12px; color: #666666'>";
        //        mailBody += "    <tr>";
        //        mailBody += "        <td style='padding-top: 10px;' align='center'>customerfirst@annectos.in   call us at +91 9686202046 | +91 9972334590";
        //        mailBody += "</td>";
        //        mailBody += "    </tr>";
        //        mailBody += "    <tr>";
        //        mailBody += "        <td style='padding-top: 10px; vertical-align: top;' align='center'>Connect With Us <a href='#'><img width='18' height='18' src='img/emails/facebook.png' /></a>  <a href='#'><img width='18' height='18' src='img/emails/twitter.png' /></a>  <a href='#'><img width='18' height='18' src='img/emails/linkedin.png' /></a>";
        //        mailBody += "</td>";
        //        mailBody += "    </tr>";
        //        mailBody += "    <tr>";
        //        mailBody += "        <td style='padding-top: 10px; font-size: 10px;' align='center'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. All Rights Reserved. www.annectos.in";
        //        mailBody += "</td>";
        //        mailBody += "    </tr>";
        //        mailBody += "</table>";*/
        //        mailBody += "</body>";
        //        mailBody += "</html>";


        //        mailBody = mailBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
        //        return mailBody;
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        return "";
        //    }
        //}



        public DataTable GetShippingByUserID(String user_id)
        {
            try
            {
                DataAccess da = new DataAccess();
                DataTable dt = da.ExecuteDataTable("get_bulk_shipping_by_user",
                    da.Parameter("_user_id", user_id.ToString()));

                return dt;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new DataTable();
            }
        }


        //public string GetOfflinePaymentMailBody(order_payment op, ref string email_id, string strCompany)
        //{
        //    string mailBody = string.Empty;
        //    string strCompanyURL = "";
        //    string strdisplayname = "";
        //    string strLogo = "";
        //    string order_id = "";
        //    string date;
        //    string odr_date;
        //    string pr_qnty;
        //    double final_price = 0;
        //    double totalamount = 0;
        //    double mrp = 0;
        //    int order_quantity = 0;
        //    string prod_name = "";
        //    string ShippingName = "";
        //    string ShippingAddress = "";
        //    string ShippingState = "";
        //    string ShippingCity = "";
        //    string ShippingPin = "";
        //    string shippingAmount = "";
        //    string strMobileNo = "";
        //    string user_id = "";
        //    string payment = "";
        //    string cart_data = "";
        //    double quantity = 0;
        //    double totalqty = 0;
        //    string size = "";
        //    List<int> lst_qnty = new List<int>();

        //    List<string> lst_name = new List<string>();
        //    List<child_cart_items> chld;

        //    if (op.payment_mode == 1)
        //    {

        //        payment = "Cheque Payment";


        //    }
        //    else if (op.payment_mode == 2)
        //    {
        //        payment = "NEFT Payment";

        //    }
        //    else
        //    {
        //        payment = "Payment gateway";

        //    }

        //    order_id = op.order_id.ToString();
        //    date = DateTime.Now.ToString();
        //    odr_date = op.date.ToString();
        //    user_id = email_id.ToString();

        //    payment_repository pr = new payment_repository();

        //    DataTable dtorderdtl = new DataTable();
        //    dtorderdtl = pr.GetDataFromOrder(op.order_id);
        //    DataTable dtShip = pr.GetDataFromUserID(Convert.ToInt64(op.order_id));
        //    //DataTable dtShip = GetShippingByUserID(user_id);

        //    if ((dtShip == null) || (dtShip.Rows.Count == 0))
        //    {
        //        return "No Shipping Information";
        //    }
        //    else
        //    {
        //        //ShippingName = dtShip.Rows[0]["name"].ToString();
        //        //ShippingAddress = dtShip.Rows[0]["address"].ToString();
        //        //ShippingCity = dtShip.Rows[0]["city"].ToString();
        //        //ShippingState = dtShip.Rows[0]["state"].ToString();
        //        //ShippingPin = dtShip.Rows[0]["pincode"].ToString();
        //        //strMobileNo = dtShip.Rows[0]["mobile_number"].ToString();

        //        ShippingName = dtShip.Rows[0]["billing_name"].ToString();
        //        ShippingAddress = dtShip.Rows[0]["address"].ToString();
        //        ShippingCity = dtShip.Rows[0]["city"].ToString();
        //        ShippingState = dtShip.Rows[0]["state"].ToString();
        //        ShippingPin = dtShip.Rows[0]["pin"].ToString();
        //        strMobileNo = dtShip.Rows[0]["mobile_no"].ToString();
        //    }



          

        //    if (dtorderdtl != null || dtorderdtl.Rows.Count > 0)
        //    {



        //        //cart_data = dtorderdtl.Rows[j]["cart_data"].ToString();

        //        for (int j = 0; j < dtorderdtl.Rows.Count; j++)
        //        {

        //            //string cart_data = "";
        //            // totalamount = Convert.ToDouble(dtorderdtl.Rows[j]["total_amount"].ToString());


        //            cart_data = dtorderdtl.Rows[j]["cart_data"].ToString();

        //            List<cart_items> orderlist = JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString());
        //           // totalamount = orderlist[j].order_amount;

        //            //  string o = orderlist[j].parent_prod_detail.ToString();
        //            //   parent_cart_items position = JsonConvert.DeserializeObject<parent_cart_items>(o);
        //            // string p_name = position.Name.ToString();
        //           // string p_name = orderlist[j].parent_prod_detail.Name.ToString();
        //            //lst_name.Add(p_name);



        //            if (orderlist != null)
        //            {
        //                for (int x = 0; x < orderlist.Count; x++)
        //                {
        //                    order_quantity = convertToInt(orderlist[x].order_quantity);

        //                    lst_qnty.Add(order_quantity);


        //                }

        //            }

        //        }


        //    }




        //    pr_qnty = ConvertIntArrayToString(lst_qnty);
        //    prod_name = ConvertStringArrayToString(lst_name);

        //    //  List<cart_item> ordlist = cart_items_to_item(JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString()));

        //    List<cart_items> ordlist = JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString());





        //    try
        //    {
        //        mailBody += "<html xmlns='http://www.w3.org/1999/xhtml'>";
        //        mailBody += "<head>";
        //        mailBody += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
        //        mailBody += "<style type='text/css'>";
        //        mailBody += "* {";
        //        mailBody += "padding:0px;";
        //        mailBody += "margin:0px;";
        //        mailBody += "}";
        //        mailBody += "</style>";
        //        mailBody += "</head>";
        //        mailBody += "<body>";
        //        mailBody += "<div style='width: 100%; position: relative; float: left'>";
        //        mailBody += "<div style='width:700px; line-height:20px; margin:0 auto; color:#494949; font-family:Arial, Helvetica, sans-serif; font-size: 14px;'>";
        //        mailBody += "<div style='float:left; height:70px; width:100%; background-color:#fdfdfd;'> ";
        //        mailBody += "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
        //        mailBody += "<tr>";
        //        // mailBody += "<td background='[email_bg]' height='70'><a style='outline:none' target='_blank'  href='http://www.annectos.in'> <img style='border:none;' src='[email_logo]' alt='annectos' width='222'  margin-top='20px' height='54' /></a></td>";
        //        mailBody += "</tr>";
        //        mailBody += "</table>";
        //        mailBody += "</div>";
        //        mailBody += "<div style='float:left;  width:100%; background-color:#ffffff;'>";
        //        mailBody += "<div style='width:100%; font-size:24px; color:#1c66ca; height:75px; float:left; text-align:center;'>";
        //        mailBody += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
        //        mailBody += "<tr>";
        //        mailBody += "<td><img style='border:none; margin-right:-150px;' src='[email_logo]' alt='annectos' width='156'  margin-top='20px' height='54' /></a></td><td height='75' align='center' valign='middle'><u>Your Order is in verification queue</u></td>";
        //        mailBody += "</tr>";
        //        mailBody += "</table>";
        //        mailBody += "</div>";
        //        mailBody += "<div style='width:100%; float:left;'>";
        //        mailBody += "<div style='width:620px; margin:0 auto;'>";
        //        mailBody += "<div style='width:100%; float:left;'>";
        //        mailBody += "<br />";
        //        mailBody += "<br />";
        //        mailBody += "  Hi,<br />";
        //        mailBody += "<br />";
        //        mailBody += "<br />";
        //        mailBody += "We received your <b> Order no.  </b>" + order_id + " and the payment mode you have selected is <b> " + payment + "</b>  on  " + date + "<br />";
        //        mailBody += "<br />";
        //        mailBody += "Please note that your order has been queued for verification. You might receive a call from us to confirm your order and contact details within the next 72 hours.";
        //        mailBody += "<br />";
        //        mailBody += "<br />";
        //        mailBody += "<br />";
        //        mailBody += "Here is the summary of your order:" + "<b>Order ID: </b> " + order_id + "<b>,order placed on:  </b> " + date;
        //        mailBody += "<br />";
        //        mailBody += "<br />";
        //        mailBody += "<div>";
        //        mailBody += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
        //        int i = 0;
        //        int item = 0;
        //        for (int j = 0; j < ordlist.Count; j++)
        //        {
        //            i = i + 1;

        //            //var total_ord_qty = lst[j].order_quantity;
        //            //var prod_price = lst[j].parent_prod_detail.price;
        //            //var prod_final_offer = 0.0;
        //            //var prod_final_discount = 0.0;
        //            //foreach (price item in prod_price)
        //            //{

        //            //    if (total_ord_qty == item.min_qty || total_ord_qty == item.max_qty || (total_ord_qty > item.min_qty && total_ord_qty < item.max_qty))
        //            //    {
        //            //        prod_final_offer = Math.Round(item.list);
        //            //        prod_final_discount = item.discount;
        //            //    }

        //            //}

        //         //   mrp = convertToDouble(ordlist[j].parent_prod_detail.price[j].mrp);

        //            for (int z = 0; z < ordlist[j].child_prod_detail.Count; z++)
        //            {
        //                if ((ordlist[j].order_quantity >= ordlist[j].parent_prod_detail.price[z].min_qty) && (ordlist[j].order_quantity <= ordlist[j].parent_prod_detail.price[z].max_qty))
        //                {
        //                    final_price = 0;
        //                    final_price = ordlist[j].parent_prod_detail.price[z].final_offer;

        //                }


        //            }

        //           mrp = convertToDouble(ordlist[j].parent_prod_detail.price[j].list);
        //       //    totalamount = convertToDouble(ordlist[j].order_amount);
                    
        //            for (int k = 0; k < ordlist[j].child_prod_detail.Count; k++)
        //            {

        //                quantity = convertToInt(ordlist[j].child_prod_detail[k].ord_qty);  
        //                size = ordlist[j].child_prod_detail[k].size;

                       
        //                if (quantity > 0)
        //                {


                           
        //                    item = item + 1;

        //                    mailBody += " <tr>";
        //                    mailBody += " <td colspan='4' style='color: #333333; padding-top:10px; padding-bottom:10px; font-size:13px; font-weight:bold' >" + item.ToString() + " Product Code: " + ordlist[j].parent_prod_detail.sku + " - " + ordlist[j].parent_prod_detail.Name + "</td>	";
        //                    mailBody += "</tr>";
        //                    mailBody += "            <tr>";
        //                    mailBody += "              <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Estimated Dispatch Date: <div style='margin-top: 10px; color: #ff0000'>In Process</div></div></td> ";
        //                    mailBody += "             <td style='color: #666666; font-size: 13px;  text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Quantity: <div style='margin-top: 10px; color: #333333; '>" + quantity + "</div></div></td>";
        //                    mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Unit Price: <div style='margin-top: 10px; color: #333333'>" + final_price + "</div></div></td>";
        //                    mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Size: <div style='margin-top: 10px; color: #333333'>" + size + "</div></div></td>";
        //                    mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'> Amount: <div style='margin-top: 10px; color: #333333'>" + quantity + "*" + final_price + "=" + (quantity * final_price) + " </div></div></td>"; //Atul 13-08-2014
        //           //         mailBody += "              <td style='color: #666666; font-size: 13px; text-align: center;'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px;'>Total Amount: <div style='margin-top: 10px; color: #333333'>" + totalamount + "</div></div></td>";
        //                    mailBody += "            </tr>";
        //                }

        //                quantity = quantity + convertToDouble(ordlist[j].child_prod_detail[k].ord_qty);
        //            }


        //            totalqty = quantity;
        //            totalamount = totalamount + convertToDouble(ordlist[j].order_amount);
                    
        //        }
        //        mailBody += "<br />";
        //        mailBody += "<br />";
        //        mailBody += "<br />";
        //        mailBody += "<td></td> <td></td> <td></td> <td></td> <td style='color: #666666; font-size: 13px; text-align: center'><div style=' border-left: 1px solid #ccc; padding-left: 10px; padding-right: 10px; margin-top:30px '>Total Amount: <div style='margin-top: 10px; color: #333333' >" + totalamount + "</div></div></td>";
        //        mailBody += "</table>";

        //        mailBody += "<br />";
        //        mailBody += "<br />";
        //        mailBody += "<b><u>Dispatch Information</u></b> <br />";
        //        mailBody += "Name: " + ShippingName + " <br />    ";
        //        mailBody += "Address:" + ShippingAddress + ",  <br />    ";
        //        mailBody += "City:" + ShippingCity + " <br />    ";
        //        mailBody += "State:" + ShippingState + "<br />";
        //        mailBody += "Mobile:" + strMobileNo + " <br /> <br />";
        //        mailBody += "<br />";
        //        mailBody += "<br />";
        //        mailBody += "Once verified and payment is confirmed ,You will receive the order confirmation mail with further details.In the meanwhile if you have any queries or clarifications please connect with our customer support team at  91 9686202046 / 9972334590  or email us at customerfirst@grassapprels.in";
        //        mailBody += "<br />";
        //        mailBody += "<br />";
        //        mailBody += "You can continue shopping at www.grass.shopinbulk.in,<br />";
        //        mailBody += "<br />";
        //        mailBody += "<br />";
        //        mailBody += "Regards,<br />";
        //        mailBody += "<span style='color:#0f0f0f;'>Team Annectos</span>";
        //        mailBody += "</div>";
        //        mailBody += "<div style='width:100%; float:left; text-align:center; font-size:12px; border-top:2px  solid #b41919; margin-top:50px; padding-top:25px; padding-bottom:25px; '>";
        //        mailBody += "Copyright &copy; 2014 Annectos.All rights reserved.<br />";
        //        mailBody += "<span style='color:#373737;'> http://grass.shopinbulk.in </span>";
        //        mailBody += "</div>";
        //        mailBody += "</div>";
        //        mailBody += "</div>";
        //        mailBody += "</div>";
        //        mailBody += "</div>";
        //        mailBody += "</div>";
        //        mailBody += "</body>";
        //        mailBody += "</html>";


        //        string siteurl = ConfigurationSettings.AppSettings["SiteUrl"].ToString();


        //        DataAccess da = new DataAccess();
        //        DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", strCompany));
        //        if (dtCompany != null && dtCompany.Rows.Count > 0)
        //        {
        //            strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
        //            strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
        //            strLogo = dtCompany.Rows[0]["logo"].ToString();
        //            siteurl = strCompanyURL;
        //        }




        //        string email_bg = ConfigurationSettings.AppSettings["email_bg"].ToString();
        //        string email_logo = ConfigurationSettings.AppSettings["email_logo"].ToString();

        //        mailBody = mailBody.Replace("[email_bg]", email_bg);
        //        strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";
        //        mailBody = mailBody.Replace("[email_logo]", strLogo);


              
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //    }
        //    return mailBody;
        //}



        public string GetOfflinePaymentMailBody(order_payment op, ref string email_id, string strCompany)
        {
            string mailBody = string.Empty;
            string strCompanyURL = "";
            string strdisplayname = "";
            string strLogo = "";
            string order_id = "";
            string date;
            string odr_date;
            string pr_qnty;
            double totalamount = 0;
            double mrp = 0;
            double final_price = 0;
            double totalqty = 0;
            int order_quantity = 0;
            string prod_name = "";
            string ShippingName = "";
            string ShippingAddress = "";
            string ShippingState = "";
            string ShippingCity = "";
            string ShippingPin = "";
            string shippingAmount = "";
            string strMobileNo = "";
            string user_id = "";
            string payment = "";
            string cart_data = "";
            int quantity = 0;
            string size = "";
            string bank_name = "";
            string trn_date = "";
            string trn_no = "";
            string trn_text = "";
            string poweredbyLogo = "";
            string transaction_date = "";
            string trn_dt = "";
            List<int> lst_qnty = new List<int>();

            List<string> lst_name = new List<string>();
            List<child_cart_items> chld;

            if (op.payment_mode == 1)
            {

                bank_name = op.bank_name.ToString();
                trn_date = op.date.ToString();
                transaction_date = trn_date.Substring(0, 10);
                trn_no = op.check_no.ToString();
                trn_text = "Cheque No:";
                payment = "Cheque Payment";


            }
            else if (op.payment_mode == 2)
            {
                bank_name = op.bank_name.ToString();
                trn_date = op.date.ToString("dd/MM/yyyy");
               
                trn_no = op.transaction_no.ToString();
                trn_text = "Transaction ID:";
                payment = "NEFT Payment";

            }
            else
            {
                payment = "Payment gateway";

            }
            
            order_id = op.order_id.ToString();
            date = DateTime.Now.ToString();
            odr_date = op.date.ToString();
            user_id = email_id.ToString();

            payment_repository pr = new payment_repository();

            DataTable dtorderdtl = new DataTable();
            dtorderdtl = pr.GetDataFromOrder(op.order_id);
            DataTable dtShip = pr.GetDataFromUserID(Convert.ToInt64(op.order_id));

            if ((dtShip == null) || (dtShip.Rows.Count == 0))
            {
                return "No Shipping Information";
            }
            else
            {

                ShippingName = dtShip.Rows[0]["billing_name"].ToString();
                ShippingAddress = dtShip.Rows[0]["address"].ToString();
                ShippingCity = dtShip.Rows[0]["city"].ToString();
                ShippingState = dtShip.Rows[0]["state"].ToString();
                ShippingPin = dtShip.Rows[0]["pin"].ToString();
                strMobileNo = dtShip.Rows[0]["mobile_no"].ToString();
            }

            if (dtorderdtl != null || dtorderdtl.Rows.Count > 0)
            {

                for (int j = 0; j < dtorderdtl.Rows.Count; j++)
                {
                    cart_data = dtorderdtl.Rows[j]["cart_data"].ToString();
                    List<cart_items> orderlist = JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString());

                    if (orderlist != null)
                    {
                        for (int x = 0; x < orderlist.Count; x++)
                        {
                            order_quantity = convertToInt(orderlist[x].order_quantity);
                            lst_qnty.Add(order_quantity);
                        }
                    }
                }
            }

            pr_qnty = ConvertIntArrayToString(lst_qnty);
            prod_name = ConvertStringArrayToString(lst_name);

            List<cart_items> ordlist = JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString());

            try
            {


                mailBody = @"
                        <html>
                        <body bgcolor='#8d8e90'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#8d8e90'>
                        <tr>
                        <td><table width='800' border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' align='center'>
                        <tr>
                        <td style='border-bottom:5px solid #f1613b; padding: 10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>              
                        <td width='144'><a href= '##GRASS_LINK##' target='_blank'><img src='##GRASS_LOGO##'  border='0' alt=''/></a></td>
                        <td width='393'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td height='46' align='right' valign='middle'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='67%' align='left'><font style='font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#68696a; font-size:18px; text-transform:uppercase'>Your Order is in verification queue</td>                         
                        <td width='4%'>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr>                  
                        </table></td>
                        </tr>
                        </table></td>
                        </tr>
                        <tr>
                        <td align='center'>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='5%'>&nbsp;</td>
                        <td width='90%' align='left' valign='top'><font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:24px'><strong><em>Hi ##FIRST_NAME##,</em></strong></font><br /><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>We received your Order no. ##ORDER_ID## and the payment mode you have selected is ##PAYMENT_MODE## on ##ORDER_DATE##
                        <br /><br />
                         
                        <table style='border-collapse: collapse' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100.0%'>
                        <tbody>

              

                              <tr>
                           
                             <td style='padding:0cm 0cm 0cm 0cm'></td>
                             </tr>
                           <tr>
                              <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >
                           <div style='padding:0cm 0cm 0cm 8.0pt'>
                          
              
                             <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>##TRN_TEXT##</span></p>
                             <div style='margin-top:7.5pt'>
                              <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:red'>##TRN_NO## </span></p>
                             </div>
                           </div>
                           </td>
                             <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >
                           <div style='padding:0cm 0cm 0cm 8.0pt'>
                            <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Bank Name: </span></p>
                            <div style='margin-top:7.5pt'>
                            <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>##BANK_NAME##</span></p>
                              </div>
                              </div>
                              </td>
                            <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >
                            <div style='padding:0cm 0cm 0cm 8.0pt'>
                            <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Date: </span></p>
                              <div style='margin-top:7.5pt'>
                             <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>##TRN_DATE##</span></p>
                              </div>
                           </div>
                            </td>
                            </tbody>
                           </table><br/><br/>
                           
                        Please note that your order has been queued for verification. You might receive a call from us to confirm your order and contact details within the next 72 hours.
                        <br /><br />
                        Here is the summary of your order: <strong style='color: #f1613b'>Order ID:</strong> ##ORDER_ID##, <strong style='color: #f1613b'>order placed on:</strong> ##ORDER_DATE##</font></td>
                        <td width='5%'>&nbsp;</td>
                        </tr>      
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
        	            <td style='padding: 10px;'>
        		        <table style='border-collapse: collapse' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100.0%'>
                        <tbody>";

                int i = 0;
                int item = 0;
                for (int j = 0; j < ordlist.Count; j++)
                {
                    i = i + 1;
                    if (ordlist[j].child_prod_detail.Count <= 1)
                    {
                        final_price = (ordlist[j].order_amount / ordlist[j].order_quantity);
                    }
                    for (int z = 0; z < ordlist[j].child_prod_detail.Count; z++)
                    {
                        if ((ordlist[j].order_quantity >= ordlist[j].parent_prod_detail.price[z].min_qty) && (ordlist[j].order_quantity <= ordlist[j].parent_prod_detail.price[z].max_qty))
                        {
                            final_price = 0;
                            final_price = ordlist[j].parent_prod_detail.price[z].final_offer;
                        }
                    }

                    mrp = convertToDouble(ordlist[j].parent_prod_detail.price[j].list);

                    for (int k = 0; k < ordlist[j].child_prod_detail.Count; k++)
                    {

                        quantity = convertToInt(ordlist[j].child_prod_detail[k].ord_qty);
                        size = ordlist[j].child_prod_detail[k].size;

                        if (quantity > 0)
                        {

                            item = item + 1;

                            mailBody += "  <tr>";
                            mailBody += "  <td colspan='4'  style= 'font-family:Helvetica, Arial, sans-serif; padding-bottom:10px; padding-top:10px;   font-size:12px; text-transform:uppercase'>";
                            mailBody += "  <strong>" + item.ToString() + " Product Code: " + ordlist[j].parent_prod_detail.sku + "-" + ordlist[j].parent_prod_detail.Name + "";
                            mailBody += "  </td>   ";
                            mailBody += "  <td style='padding:0cm 0cm 0cm 0cm'></td>";
                            mailBody += "  </tr>";
                            mailBody += "  <tr>";
                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Estimated Dispatch Date:</span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:red'>In Process</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";
                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Quantity: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + quantity + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";
                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Unit Price: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + final_price + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";
                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Size: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + size + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";
                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif; padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Amount:</span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + quantity + "*" + final_price + "=" + (quantity * final_price) + "</span></p>";
                            mailBody += "  </div> ";
                            mailBody += "  </div> ";
                            mailBody += "  </td>  ";
                            mailBody += "  </tr>  ";

                        }
                    }


                    totalqty = totalqty + (convertToDouble(ordlist[j].order_quantity) * 1);
                    totalamount = totalamount + convertToDouble(ordlist[j].order_amount) * 1;
                }


                mailBody += @"<tr>      
                        <td bgcolor='#f1613b' ></td>
                        <td  bgcolor='#f1613b'  >
                        <div style='padding-top: 2px; padding-bottom: 2px;' >
                        <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#ffffff'>Total Quantity: </span></p>
                        <div >
                        <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#ffffff'>##TOTAL_QTY##</span></p>
                        </div>
                        </div>
                        </td>
                        <td  bgcolor='#f1613b'  style='padding:0cm 0cm 0cm 0cm'></td>
                        <td  bgcolor='#f1613b'  style='padding:0cm 0cm 0cm 0cm'></td>
                        <td  bgcolor='#f1613b'  style='padding:0cm 0cm 0cm 0cm'>
                        <div style='padding-top: 2px; padding-bottom: 2px;' >
                        <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#ffffff'>Total Amount: </span></p>
                        <div >
                        <p class='MsoNormal' align='center' style='text-align:center; margin: 0px;'><span style='font-size:10.0pt;color:#ffffff'>##TOTAL_AMT##</span></p>
                        </div>
                        </div>
                        </td>
                        <td  bgcolor='#f1613b'  style='padding:0cm 0cm 0cm 0cm'></td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>  
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>             
                        <td style='padding: 10px;' align='left' valign='top'><font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'><strong><em>Dispatch Information </em></strong></font><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>";
                mailBody += "   Name:  " + ShippingName + " <br />";
                mailBody += "   Address:" + ShippingAddress + ", <br />";
                mailBody += "   City:" + ShippingCity + " <br />";
                mailBody += "   State:" + ShippingState + "<br />";
                mailBody += "   Mobile:" + strMobileNo + " ";

                mailBody += @"<br /><br />
                        Once verified and payment is confirmed ,You will receive the order confirmation mail with further details.In the meanwhile if you have any queries or clarifications please connect with our customer support team at 91 9686202046 / 9972334590 or email us at <a  style='color: #f1613b'  href='mailto:customerfirst@grassapparels.in ' target='_blank'>customerfirst@grassapparels.in </a>
                        <br />
                        <br />
                        You can continue shopping at <a style='color: #f1613b' href='http://grass.shopinbulk.in' target='_blank'>grass.shopinbulk.in</a>
                        </font></td>             
                        </tr>             
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>     
                        <td style='padding: 10px;' align='left' valign='top'>
                        <font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'>
                        <strong>Regards</strong> ,<br/>
                        <strong>Team GRASS</strong>            
                        </font>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>               
                        <tr>
                        <td align='center'><font style='font-family:Helvetica, Arial, sans-serif; color:#231f20; font-size:12px'>Copyright &copy; 2014 Annectos.All rights reserved.</font></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align='center'>
                        <font style=' font-family:Helvetica, Arial, sans-serif; color:#f1613b; font-size:12px; text-transform:uppercase'><a href='http://grass.shopinbulk.in' style='color:#f1613b; text-decoration:none'target='_blank' >http://grass.shopinbulk.in</a></font>
                        </td>
                        </tr>
                        <tr>
                        <td style='padding: 10px;padding-left:600px;' align='center'><a href= '####' target='_blank'><img src='http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/annectostemp.png'  border='0' alt=''/></a></td>  
                        </tr>
                        </table></td>
                        </tr> 
                        </table>
                        </body>
                        </html> ";


                string siteurl = ConfigurationSettings.AppSettings["SiteUrl"].ToString();


                DataAccess da = new DataAccess();
                DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", strCompany));
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                    strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    siteurl = strCompanyURL;
                }


                string email_bg = ConfigurationSettings.AppSettings["email_bg"].ToString();
                string email_logo = ConfigurationSettings.AppSettings["email_logo"].ToString();

                mailBody = mailBody.Replace("[email_bg]", email_bg);
                strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";
                poweredbyLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/annectostemp.png"; 
                mailBody = mailBody.Replace("[email_logo]", strLogo);

                mailBody = mailBody.Replace("##GRASS_LINK##", "http://grass.shopinbulk.in");
                mailBody = mailBody.Replace("##GRASS_LOGO##", strLogo);
                mailBody = mailBody.Replace("##ORDER_ID##", order_id);
                mailBody = mailBody.Replace("##PAYMENT_MODE##", payment);
                mailBody = mailBody.Replace("##ORDER_DATE##", date);
                mailBody = mailBody.Replace("##TOTAL_QTY##", convertToString(totalqty));
                mailBody = mailBody.Replace("##TOTAL_AMT##", convertToString(totalamount));
                mailBody = mailBody.Replace("##FIRST_NAME##", ShippingName);
                mailBody = mailBody.Replace("##TRN_NO##",trn_no);
                mailBody = mailBody.Replace("##BANK_NAME##",bank_name);
                mailBody = mailBody.Replace("##TRN_DATE##", trn_date);
                mailBody = mailBody.Replace("##TRN_TEXT##", trn_text);
                mailBody = mailBody.Replace("##POWERED_LOGO##", poweredbyLogo);

               
               
                


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return mailBody;
        }



        public List<order_payment> get_transaction_details(int order_id)
        
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            payment_repository pr = new payment_repository();
            registration_repository rr = new registration_repository();
            store_repository sr = new store_repository();
            List<order_payment> lst = new List<order_payment>();
            DataTable dt = da.ExecuteDataTable("get_transaction_details", da.Parameter("order_id", order_id));

            if (dt != null && dt.Rows.Count > 0)
            {
               

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    order_payment oei = new order_payment();
                    oei.order_id = convertToLong(dt.Rows[i]["order_id"]);
                    oei.check_no = convertToString(dt.Rows[i]["cheque_no"]);
                    oei.transaction_no = convertToString(dt.Rows[i]["transaction_no"]);
                    oei.bank_name = convertToString(dt.Rows[i]["bank_name"]);
       
                    oei.date = convertToDate((dt.Rows[i]["transaction_date"]));
                    lst.Add(oei);
                }
               
            }
            return lst;


        }

        public List<enquiry_info> get_product_enquire_info(string email_id)
        {

            DataAccess da = new DataAccess();
            // string enquire_data = "";
            // string status;
            // string id;
            List<enquiry_info> lst = new List<enquiry_info>();
            DataTable dt = da.ExecuteDataTable("get_bulk_enquire_info", da.Parameter("_user_id", email_id));
            if (dt != null && dt.Rows.Count > 0)
            {
                //enquiry_info ei = new enquiry_info();

                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    ei.id = dt.Rows[i]["id"].ToString();
                //    ei.status = dt.Rows[i]["status"].ToString();
                //    ei.size_quantity = dt.Rows[i]["size_quantity"].ToString();
                //    lst.Add(ei);

                //}
                //    if (oei.size_quantity

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    enquiry_info oei = new enquiry_info();
                    oei.id = convertToLong(dt.Rows[i]["id"]);
                    oei.quantity = convertToLong(dt.Rows[i]["quantity"]);
                    oei.logo_needed = convertToString(dt.Rows[i]["logo_needed"]);
                    oei.name = convertToString(dt.Rows[i]["name"]);
                    oei.mobile = convertToString(dt.Rows[i]["mobile"]);
                    oei.email = convertToString(dt.Rows[i]["email"]);
                    oei.city = convertToString(dt.Rows[i]["city"]); ;
                    oei.company_name = convertToString(dt.Rows[i]["company_name"]); ;
                    oei.website = convertToString(dt.Rows[i]["website"]); ;
                    //oei.create_ts   =convertToString(dt.Rows[i]["create_ts"]);;
                    oei.enquire_from = convertToString(dt.Rows[i]["enquire_from"]); ;
                    oei.size_quantity = convertToString(dt.Rows[i]["size_quantity"]); ;
                    oei.created_by = convertToString(dt.Rows[i]["created_by"]); ;
                    oei.status = convertToLong(dt.Rows[i]["status"]); ;
                    oei.enquire_status = convertToString(dt.Rows[i]["enquire_status"]);
                    lst.Add(oei);
                }
                //  List<enquiry_detail> ed = JsonConvert.DeserializeObject<List<enquiry_detail>>(enquire_data);
                //   JsonConvert.DeserializeObject<List<cart_items>>(dtResult.Rows[i]["cart_data"].ToString();

                //  return ed;
            }
            return lst;


        }

        public string GetCheckOfflinePaymentMailBody(List<check_payment> op, ref string email_id, string strCompany)
        {
            string mailBody = string.Empty;
            string strCompanyURL = "";
            string strdisplayname = "";
            string strLogo = "";
            string order_id = "";
            string date;
            string odr_date;
            string pr_qnty;
            double totalamount = 0;
            double mrp = 0;
            double final_price = 0;
            double totalqty = 0;
            int order_quantity = 0;
            string prod_name = "";
            string ShippingName = "";
            string ShippingAddress = "";
            string ShippingState = "";
            string ShippingCity = "";
            string ShippingPin = "";
            string shippingAmount = "";
            string strMobileNo = "";
            string user_id = "";
            string payment = "";
            string cart_data = "";
            int quantity = 0;
            string size = "";
            string bank_name = "";
            string trn_date = "";
            string trn_no = "";
            string trn_text = "";
            string poweredbyLogo = "";
            string Order_id = "";
            string transaction_date = "";
            string trn = "";
            string trn_dt = "";
            List<int> lst_qnty = new List<int>();

            List<string> lst_name = new List<string>();
            List<child_cart_items> chld;

            //foreach (ecomm.util.entities.check_payment odr in op)
            //{
            //    Order_id = odr.order_id.ToString();


            //    if (odr.payment_mode == 1)
            //    {

            //        bank_name = odr.banks_name.ToString();
            //      //  trn_date = odr.trn_date.ToString();
            //      //  trn_no = odr.transaction_no.ToString();
            //        trn_text = "Cheque No:";
            //        payment = "Cheque Payment";


            //    }
            //    //else if (odr.payment_mode == 2)
            //    //{
            //    //    bank_name = odr.bank_name.ToString();
            //    //    trn_date = odr.date.ToString();
            //    //    trn_no = odr.transaction_no.ToString();
            //    //    trn_text = "Transaction ID:";
            //    //    payment = "NEFT Payment";

            //    //}
            //    else
            //    {
            //        payment = "Payment gateway";

            //    }

            //}

            foreach (ecomm.util.entities.check_payment odr in op)
            {
                order_id = odr.order_id.ToString();


            }

            //  order_id = Order_id;

            payment = "Cheque Payment";
            date = DateTime.Now.ToString();
            odr_date = date;
            user_id = email_id.ToString();

            payment_repository pr = new payment_repository();

            DataTable dtorderdtl = new DataTable();
            dtorderdtl = pr.GetDataFromOrder(Convert.ToInt64(order_id));
            DataTable dtShip = pr.GetDataFromUserID(Convert.ToInt64(order_id));

            if ((dtShip == null) || (dtShip.Rows.Count == 0))
            {
                return "No Shipping Information";
            }
            else
            {

                ShippingName = dtShip.Rows[0]["billing_name"].ToString();
                ShippingAddress = dtShip.Rows[0]["address"].ToString();
                ShippingCity = dtShip.Rows[0]["city"].ToString();
                ShippingState = dtShip.Rows[0]["state"].ToString();
                ShippingPin = dtShip.Rows[0]["pin"].ToString();
                strMobileNo = dtShip.Rows[0]["mobile_no"].ToString();
            }

            if (dtorderdtl != null || dtorderdtl.Rows.Count > 0)
            {

                for (int j = 0; j < dtorderdtl.Rows.Count; j++)
                {
                    cart_data = dtorderdtl.Rows[j]["cart_data"].ToString();
                    List<cart_items> orderlist = JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString());

                    if (orderlist != null)
                    {
                        for (int x = 0; x < orderlist.Count; x++)
                        {
                            order_quantity = convertToInt(orderlist[x].order_quantity);
                            lst_qnty.Add(order_quantity);
                        }
                    }
                }
            }

            pr_qnty = ConvertIntArrayToString(lst_qnty);
            prod_name = ConvertStringArrayToString(lst_name);

            List<cart_items> ordlist = JsonConvert.DeserializeObject<List<cart_items>>(cart_data.ToString());

            try
            {


                mailBody = @"
                        <html>
                        <body bgcolor='#8d8e90'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#8d8e90'>
                        <tr>
                        <td><table width='800' border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' align='center'>
                        <tr>
                        <td style='border-bottom:5px solid #f1613b; padding: 10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>              
                        <td width='144'><a href= '##GRASS_LINK##' target='_blank'><img src='##GRASS_LOGO##'  border='0' alt=''/></a></td>
                        <td width='393'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td height='46' align='right' valign='middle'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='67%' align='left'><font style='font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#68696a; font-size:18px; text-transform:uppercase'>Your Order is in verification queue</td>                         
                        <td width='4%'>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr>                  
                        </table></td>
                        </tr>
                        </table></td>
                        </tr>
                        <tr>
                        <td align='center'>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='5%'>&nbsp;</td>
                        <td width='90%' align='left' valign='top'><font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:24px'><strong><em>Hi ##FIRST_NAME##,</em></strong></font><br /><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>We received your Order no. ##ORDER_ID## and the payment mode you have selected is ##PAYMENT_MODE## on ##ORDER_DATE##
                        <br /><br />


                 <font style='font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#68696a; font-size:18px; text-transform:uppercase'>Please Find Your Cheque Details Below:  <br /><br />                                                  
                 <table style='border-collapse: collapse' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100.0%'>
                            <tbody>
                              <tr>
                           
                             <td style='padding:0cm 0cm 0cm 0cm'></td>
                             </tr>";
                foreach (ecomm.util.entities.check_payment odr in op)
                {

                    trn_text = "Cheque No:";
                    bank_name = odr.banks_name.ToString();
                    trn_no = odr.check_number.ToString();
                    transaction_date = odr.trn_date.ToString("dd/MM/yyyy");

                    //DateTime dt = DateTime.ParseExact(trn_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    //trn_dt = dt.ToString();
                    // transaction_date = trn_dt.Substring(0, 10);





                    mailBody += "  <tr>";
                    mailBody += "<td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                    mailBody += "<div style='padding:0cm 0cm 0cm 8.0pt'>";
                    mailBody += "<p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>" + trn_text + "</span></p>";
                    mailBody += "<div style='margin-top:7.5pt'>";
                    mailBody += "<p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + trn_no + " </span></p>";
                    mailBody += "</div>";
                    mailBody += "</div>";
                    mailBody += "</td>";
                    mailBody += "<td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                    mailBody += "<div style='padding:0cm 0cm 0cm 8.0pt'>";
                    mailBody += "<p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Bank Name: </span></p>";
                    mailBody += "<div style='margin-top:7.5pt'>";
                    mailBody += "<p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + bank_name + "</span></p>";
                    mailBody += "</div>";
                    mailBody += "</div>";
                    mailBody += "</td>";
                    mailBody += "<td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                    mailBody += "<div style='padding:0cm 0cm 0cm 8.0pt'>";
                    mailBody += "<p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Date: </span></p>";
                    mailBody += "<div style='margin-top:7.5pt'>";
                    mailBody += "<p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + transaction_date + "</span></p>";
                    mailBody += " </div>";
                    mailBody += "</div>";

                }


                mailBody += @"</td>
                            </tbody>
                           </table><br/><br/>
                                             
                        Please note that your order has been queued for verification. You might receive a call from us to confirm your order and contact details within the next 72 hours.
                        <br /><br />
                        Here is the summary of your order: <strong style='color: #f1613b'>Order ID:</strong> ##ORDER_ID##, <strong style='color: #f1613b'>Order placed on:</strong> ##ORDER_DATE##.</font></td>
                                                                 
                        <td width='5%'>&nbsp;</td>
                        </tr>      
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
        	            <td style='padding: 10px;'>
        		        <table style='border-collapse: collapse' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100.0%'>
                        <tbody>";

                int i = 0;
                int item = 0;
                for (int j = 0; j < ordlist.Count; j++)
                {
                    i = i + 1;
                    if (ordlist[j].child_prod_detail.Count <= 1)
                    {
                        final_price = (ordlist[j].order_amount / ordlist[j].order_quantity);
                    }
                    for (int z = 0; z < ordlist[j].child_prod_detail.Count; z++)
                    {
                        if ((ordlist[j].order_quantity >= ordlist[j].parent_prod_detail.price[z].min_qty) && (ordlist[j].order_quantity <= ordlist[j].parent_prod_detail.price[z].max_qty))
                        {
                            final_price = 0;
                            final_price = ordlist[j].parent_prod_detail.price[z].final_offer;
                        }
                    }

                    mrp = convertToDouble(ordlist[j].parent_prod_detail.price[j].list);

                    for (int k = 0; k < ordlist[j].child_prod_detail.Count; k++)
                    {

                        quantity = convertToInt(ordlist[j].child_prod_detail[k].ord_qty);
                        size = ordlist[j].child_prod_detail[k].size;

                        if (quantity > 0)
                        {

                            item = item + 1;

                            mailBody += "  <tr>";
                            mailBody += "  <td colspan='4'  style= 'font-family:Helvetica, Arial, sans-serif; padding-bottom:10px; padding-top:10px;   font-size:12px; text-transform:uppercase'>";
                            mailBody += "  <strong>" + item.ToString() + " Product Code: " + ordlist[j].parent_prod_detail.sku + "-" + ordlist[j].parent_prod_detail.Name + "";
                            mailBody += "  </td>   ";
                            mailBody += "  <td style='padding:0cm 0cm 0cm 0cm'></td>";
                            mailBody += "  </tr>";
                            mailBody += "  <tr>";
                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Estimated Dispatch Date:</span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:red'>In Process</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";
                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Quantity: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + quantity + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";
                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Unit Price: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + final_price + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";
                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Size: </span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + size + "</span></p>";
                            mailBody += "  </div>";
                            mailBody += "  </div>";
                            mailBody += "  </td>";
                            mailBody += "  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif; padding: 5px;' >";
                            mailBody += "  <div style='padding:0cm 0cm 0cm 8.0pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Amount:</span></p>";
                            mailBody += "  <div style='margin-top:7.5pt'>";
                            mailBody += "  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>" + quantity + "*" + final_price + "=" + (quantity * final_price) + "</span></p>";
                            mailBody += "  </div> ";
                            mailBody += "  </div> ";
                            mailBody += "  </td>  ";
                            mailBody += "  </tr>  ";

                        }
                    }


                    totalqty = totalqty + (convertToDouble(ordlist[j].order_quantity) * 1);
                    totalamount = totalamount + convertToDouble(ordlist[j].order_amount) * 1;
                }


                mailBody += @"<tr>      
                        <td bgcolor='#f1613b' ></td>
                        <td  bgcolor='#f1613b'  >
                        <div style='padding-top: 2px; padding-bottom: 2px;' >
                        <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#ffffff'>Total Quantity: </span></p>
                        <div >
                        <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#ffffff'>##TOTAL_QTY##</span></p>
                        </div>
                        </div>
                        </td>
                        <td  bgcolor='#f1613b'  style='padding:0cm 0cm 0cm 0cm'></td>
                        <td  bgcolor='#f1613b'  style='padding:0cm 0cm 0cm 0cm'></td>
                        <td  bgcolor='#f1613b'  style='padding:0cm 0cm 0cm 0cm'>
                        <div style='padding-top: 2px; padding-bottom: 2px;' >
                        <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#ffffff'>Total Amount: </span></p>
                        <div >
                        <p class='MsoNormal' align='center' style='text-align:center; margin: 0px;'><span style='font-size:10.0pt;color:#ffffff'>##TOTAL_AMT##</span></p>
                        </div>
                        </div>
                        </td>
                        <td  bgcolor='#ff0000'  style='padding:0cm 0cm 0cm 0cm'></td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>  
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>             
                        <td style='padding: 10px;' align='left' valign='top'><font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'><strong><em>Dispatch Information </em></strong></font><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>";
                mailBody += "   Name:  " + ShippingName + " <br />";
                mailBody += "   Address:" + ShippingAddress + ", <br />";
                mailBody += "   City:" + ShippingCity + " <br />";
                mailBody += "   State:" + ShippingState + "<br />";
                mailBody += "   Mobile:" + strMobileNo + " ";

                mailBody += @"<br /><br />
                        Once verified and payment is confirmed ,You will receive the order confirmation mail with further details.In the meanwhile if you have any queries or clarifications please connect with our customer support team at 91 9686202046 / 9972334590 or email us at <a  style='color: #f1613b'  href='mailto:customerfirst@grassapparels.in ' target='_blank'>customerfirst@grassapparels.in</a>
                        <br />
                        <br />
                        You can continue shopping at <a style='color: #f1613b' href='http://grass.shopinbulk.in' target='_blank'>grass.shopinbulk.in</a>
                        </font></td>             
                        </tr>             
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>     
                        <td style='padding: 10px;' align='left' valign='top'>
                        <font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'>
                        <strong>Regards</strong> ,<br/>
                        <strong>Team GRASS</strong>            
                        </font>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>               
                        <tr>
                        <td align='center'><font style='font-family:Helvetica, Arial, sans-serif; color:#231f20; font-size:12px'>Copyright &copy; 2014 Annectos.All rights reserved.</font></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align='center'>
                        <font style=' font-family:Helvetica, Arial, sans-serif; color:#f1613b; font-size:12px; text-transform:uppercase'><a href='http://grass.shopinbulk.in' style='color:#f1613b; text-decoration:none'target='_blank' >http://grass.shopinbulk.in</a></font>
                        </td>
                        </tr>
                        <tr>
                       <td style='padding: 10px;padding-left:600px;' align='center'><a href= '####' target='_blank'><img src='http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/annectostemp.png'  border='0' alt=''/></a></td>  
                        </tr>
                        </table></td>
                        </tr> 
                        </table>
                        </body>
                        </html> ";


                string siteurl = ConfigurationSettings.AppSettings["SiteUrl"].ToString();


                DataAccess da = new DataAccess();
                DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", strCompany));
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                    strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    siteurl = strCompanyURL;
                }


                string email_bg = ConfigurationSettings.AppSettings["email_bg"].ToString();
                string email_logo = ConfigurationSettings.AppSettings["email_logo"].ToString();

                mailBody = mailBody.Replace("[email_bg]", email_bg);
                strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";
                poweredbyLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/annectostemp.png";
                mailBody = mailBody.Replace("[email_logo]", strLogo);
                mailBody = mailBody.Replace("##FIRST_NAME##", ShippingName);
                mailBody = mailBody.Replace("##GRASS_LINK##", "http://grass.shopinbulk.in");
                mailBody = mailBody.Replace("##GRASS_LOGO##", strLogo);
                mailBody = mailBody.Replace("##ORDER_ID##", order_id);
                mailBody = mailBody.Replace("##PAYMENT_MODE##", payment);
                mailBody = mailBody.Replace("##ORDER_DATE##", date);
                mailBody = mailBody.Replace("##TOTAL_QTY##", convertToString(totalqty));
                mailBody = mailBody.Replace("##TOTAL_AMT##", convertToString(totalamount));
                mailBody = mailBody.Replace("##TRN_NO##", trn_no);
                mailBody = mailBody.Replace("##BANK_NAME##", bank_name);
                mailBody = mailBody.Replace("##TRN_DATE##", trn_date);
                mailBody = mailBody.Replace("##TRN_TEXT##", trn_text);
                mailBody = mailBody.Replace("##POWERED_LOGO##", poweredbyLogo);


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return mailBody;
        }

        }

    }


