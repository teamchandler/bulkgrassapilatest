﻿using ecomm.dal;
using ecomm.util;
using ecomm.util.entities;
// to be removed
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace ecomm.model.repository
{
    public class category_repository
    {

        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private decimal convertToDecimal(object o)
        {
            try
            {
                if (o != null)
                {
                    return decimal.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private float convertToFloat(object o)
        {
            try
            {
                if (o != null)
                {
                    return float.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private double convertToDouble(object o)
        {
            try
            {
                if (o != null)
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString();
                }
                else { return ""; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        private DateTime convertToDate(object o)
        {
            try
            {
                if ((o != null) && (o != ""))
                {
                    //string f = "mm/dd/yyyy";
                    return DateTime.Parse(o.ToString());
                    //return DateTime.ParseExact(o.ToString(), "MM-dd-yy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else { return DateTime.Parse("1/1/1800"); }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return DateTime.Parse("1/1/1800");
            }
        }
        private bool check_field(BsonDocument doc, string field_name)
        {
            if (doc.Contains(field_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #region get methods
        public List<category> get_parent_cat_list(string level)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db("category");
                var catagories = new List<category>();

                category cate = new category();
                cate.id = "0";
                cate.Name = "";
                catagories.Add(cate);
                List<category> Category_List = new List<category>();
                List<category> Mod_Cat_List = new List<category>();
                //if (level == "1")
                //{
                //    category cate = new category();
                //    cate.id = "0";
                //    cate.Name = "";
                //    catagories.Add(cate);

                //}
                if (level == "2")
                {
                    foreach (var c in cursor)
                    {
                        if (c.ToBsonDocument()["ParentId"].ToString() == "0")
                        {
                            category cat = new category();
                            cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                            cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                            catagories.Add(cat);
                        }
                    }

                }
                else if (level == "3")
                {
                    foreach (var c in cursor)
                    {
                        category cat = new category();
                        cat.id = c.ToBsonDocument()["id"].ToString();
                        cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                        cat.ParentId = check_field(c.ToBsonDocument(), "ParentId") ? convertToString(c.ToBsonDocument()["ParentId"]) : null;
                        catagories.Add(cat);
                    }
                    Category_List = GenerateCategoryList(catagories, Category_List);

                    for (var i = 0; i < Category_List.Count(); i++)
                    {
                        string[] Category_ID = Category_List[i].id.ToString().Split('.');
                        {
                            if (Category_ID.Count() == 3 && Category_ID[2].ToString() == "0")
                            {
                                category cat1 = new category();
                                cat1.id = Category_List[i].id.ToString();
                                cat1.Name = Category_List[i].Name.ToString();
                                Mod_Cat_List.Add(cat1);
                            }
                        }
                    }
                }

                if (level == "3")
                {
                    return Mod_Cat_List;
                }

                else
                {
                    return catagories;
                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }

        }


        public List<category> get_cat_list_to_copy(string cate_id)
        {

            try
            {
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db("category");
                var catagories = new List<category>();
                category cate = new category();
                cate.id = "0";
                cate.Name = "";
                catagories.Add(cate);
                foreach (var c in cursor)
                {
                    if (c.ToBsonDocument()["id"].ToString().Contains("."))
                    {
                        string[] Category_ID = c.ToBsonDocument()["id"].ToString().Split('.');
                        if (Category_ID.Count() == 3)
                        {
                            if (c.ToBsonDocument()["id"].ToString() != cate_id)
                            {
                                if (JsonConvert.DeserializeObject<List<filter>>(c.ToBsonDocument()["filters"].ToString()).Count() != 0)
                                {
                                    category cat = new category();
                                    cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                                    cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                                    cat.filters = JsonConvert.DeserializeObject<List<filter>>(c.ToBsonDocument()["filters"].ToString());
                                    catagories.Add(cat);
                                }
                            }
                        }
                    }
                }
                return catagories;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }
        }

        public List<category> get_cat_filter_list()
        {
            // evoke mongodb connection method in dal

            try
            {
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");
                var catagories = new List<category>();
                var Category_List = new List<category>();
                category cate = new category();
                cate.id = "0";
                cate.Name = "";
                catagories.Add(cate);

                foreach (var c in cursor)
                {
                    category cat = new category();
                    cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    cat.ParentId = check_field(c.ToBsonDocument(), "ParentId") ? convertToString(c.ToBsonDocument()["ParentId"]) : null;
                    catagories.Add(cat);
                }


                if (catagories.Count() > 1)
                {
                    category Categ = new category();
                    Categ.id = "0";
                    Categ.Name = "";
                    Category_List.Add(Categ);

                    for (int i = 1; i < catagories.Count(); i++)
                    {
                        if (catagories[i].ParentId.ToString() == "0")
                        {
                            category objcat = new category();
                            objcat.id = catagories[i].id.ToString();
                            objcat.Name = catagories[i].Name.ToString();
                            objcat.ParentId = catagories[i].ParentId.ToString();
                            Category_List.Add(objcat);
                            Category_List = SecondLevelCategory(catagories[i].id.ToString(), catagories[i].Name.ToString(), Category_List, catagories);
                        }
                    }
                    return Category_List;
                }
                else
                {
                    return catagories;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }
        }

        public List<category> get_cat_list()
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");

                var catagories = new List<category>();

                var Category_List = new List<category>();
                category cate = new category();
                cate.id = "0";
                cate.Name = "New Category";
                catagories.Add(cate);

                foreach (var c in cursor)
                {
                    category cat = new category();
                    cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    cat.ParentId = check_field(c.ToBsonDocument(), "ParentId") ? convertToString(c.ToBsonDocument()["ParentId"]) : null;
                    catagories.Add(cat);
                }

                if (catagories.Count() > 1)
                {
                    category categ = new category();
                    categ.id = "0";
                    categ.Name = "New Category";
                    Category_List.Add(categ);
                    return Category_List = GenerateCategoryList(catagories, Category_List);
                }
                else
                {
                    return catagories;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }
        }
        public List<category> get_store_cat_list(string StoreCompId)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");

                var catagories = new List<category>();

                var Category_List = new List<category>();
                category cate = new category();
                cate.id = "0";
                cate.Name = "";
                catagories.Add(cate);

                foreach (var c in cursor)
                {
                    category cat = new category();
                    cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    cat.ParentId = check_field(c.ToBsonDocument(), "ParentId") ? convertToString(c.ToBsonDocument()["ParentId"]) : null;
                    catagories.Add(cat);
                }

                if (catagories.Count() > 1)
                {
                    return Category_List = GenerateCategoryList(catagories, Category_List);
                }
                else
                {
                    return catagories;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }
        }

        public List<category> GenerateCategoryList(List<category> catagories, List<category> Category_List)
        {
            try
            {
                for (int i = 1; i < catagories.Count(); i++)
                {
                    if (catagories[i].ParentId.ToString() == "0")
                    {
                        category objcat = new category();
                        objcat.id = catagories[i].id.ToString();
                        objcat.Name = catagories[i].Name.ToString();
                        objcat.ParentId = catagories[i].ParentId.ToString();
                        Category_List.Add(objcat);
                        Category_List = SecondLevelCategory(catagories[i].id.ToString(), catagories[i].Name.ToString(), Category_List, catagories);
                    }
                }
                return Category_List;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return Category_List;
            }
        }


        public List<category> SecondLevelCategory(string Parent_ID, string Parent_Name, List<category> Category_Tree_List, List<category> Category_List)
        {
            try
            {
                for (int i = 1; i < Category_List.Count(); i++)
                {

                    if (Category_List[i].ParentId.ToString() == Parent_ID)
                    {
                        category objcat = new category();
                        objcat.id = Category_List[i].id.ToString();
                        objcat.Name = Parent_Name + "-" + Category_List[i].Name.ToString();
                        objcat.ParentId = Category_List[i].ParentId.ToString();
                        Category_Tree_List.Add(objcat);
                        Category_Tree_List = ThirdLevelCategory(Category_List[i].id.ToString(), Parent_Name + "-" + Category_List[i].Name.ToString(), Category_Tree_List, Category_List);
                    }

                }
                return Category_Tree_List;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }

        }
        public List<category> ThirdLevelCategory(string Parent_ID, string Parent_Nmae, List<category> Category_Tree_List, List<category> Category_List)
        {
            try
            {
                for (int i = 1; i < Category_List.Count(); i++)
                {
                    if (Category_List[i].ParentId.ToString() == Parent_ID)
                    {
                        category objcat = new category();
                        objcat.id = Category_List[i].id.ToString();
                        objcat.Name = Parent_Nmae + "-" + Category_List[i].Name.ToString();
                        Category_Tree_List.Add(objcat);
                    }
                }
                return Category_Tree_List;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return Category_Tree_List;
            }
        }


        public List<brand> get_brand_list(string Flag)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db("brand");

                var brands = new List<brand>();
                brand brnd = new brand();
                if (Flag == "L")
                {
                    brnd.name = "";
                    brands.Add(brnd);
                }
                else
                {
                    brnd.name = "New Brand";
                    brands.Add(brnd);
                }

                foreach (var c in cursor)
                {
                    brand obrnd = new brand();
                    obrnd.name = check_field(c.ToBsonDocument(), "name") ? convertToString(c.ToBsonDocument()["name"]) : null;
                    brands.Add(obrnd);
                }
                return brands;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }
        }
        public List<brand> get_store_Brand_list(string StoreCompId)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db("brand");

                var brands = new List<brand>();


                foreach (var c in cursor)
                {
                    brand obrnd = new brand();
                    obrnd.name = check_field(c.ToBsonDocument(), "name") ? convertToString(c.ToBsonDocument()["name"]) : null;
                    brands.Add(obrnd);
                }
                return brands;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }
        }
        public List<store_config> get_store_config(string StoreCompId)
        {
            // evoke mongodb connection method in dal

            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{store:'" + StoreCompId + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("store_config", queryDoc);

                var store_configs = new List<store_config>();
                foreach (var c in cursor)
                {
                    //		c.ToBsonDocument().Contains("id")

                    store_config osc = new store_config();
                    osc.store = check_field(c.ToBsonDocument(), "store") ? convertToString(c.ToBsonDocument()["store"]) : null;
                    osc.exclusion = check_field(c.ToBsonDocument(), "exclusion") ? JsonConvert.DeserializeObject<List<Exclu>>(c.ToBsonDocument()["exclusion"].ToString()) : null;
                    osc.discount = check_field(c.ToBsonDocument(), "discount") ? JsonConvert.DeserializeObject<List<Disc>>(c.ToBsonDocument()["discount"].ToString()) : null;


                    store_configs.Add(osc);
                }
                return store_configs;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }

        }

        public List<category> get_cat_details(string cat_id)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + cat_id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc);

                var catagories = new List<category>();
                foreach (var c in cursor)
                {
                    //		c.ToBsonDocument().Contains("id")

                    category cat = new category();
                    cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    cat.active = check_field(c.ToBsonDocument(), "active") ? convertToInt(c.ToBsonDocument()["active"]) : 1;
                    cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    cat.displayname = check_field(c.ToBsonDocument(), "displayname") ? convertToString(c.ToBsonDocument()["displayname"]) : null;
                    cat.description = check_field(c.ToBsonDocument(), "description") ? convertToString(c.ToBsonDocument()["description"]) : null;
                    if (check_field(c.ToBsonDocument(), "filters") == true)
                    {
                        cat.filters = JsonConvert.DeserializeObject<List<filter>>(c.ToBsonDocument()["filters"].ToString());
                    }
                    cat.special = check_field(c.ToBsonDocument(), "special") ? convertToString(c.ToBsonDocument()["special"]) : null;
                    cat.store_defn = check_field(c.ToBsonDocument(), "store_defn") ? JsonConvert.DeserializeObject<List<storedate>>(c.ToBsonDocument()["store_defn"].ToString()) : null;
                    cat.ParentId = check_field(c.ToBsonDocument(), "ParentId") ? convertToString(c.ToBsonDocument()["ParentId"]) : null;
                    cat.Shipping = check_field(c.ToBsonDocument(), "Shipping") ? convertToString(c.ToBsonDocument()["Shipping"]) : null;

                    cat.min_ship = check_field(c.ToBsonDocument(), "min_ship") ? convertToInt(c.ToBsonDocument()["min_ship"]) : 0;
                    cat.max_ship = check_field(c.ToBsonDocument(), "max_ship") ? convertToInt(c.ToBsonDocument()["max_ship"]) : 0;

                    catagories.Add(cat);
                }
                return catagories;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }
        }

        public List<brand> get_brand_details(string brand_name)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{name:'" + brand_name + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("brand", queryDoc);

                var brands = new List<brand>();
                foreach (var c in cursor)
                {
                    brand brd = new brand();
                    brd.name = check_field(c.ToBsonDocument(), "name") ? convertToString(c.ToBsonDocument()["name"]) : null;
                    brands.Add(brd);
                }
                return brands;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }
        }

        public List<filter> CreateNullFilter()
        {
            List<filter> filter = new List<filter>();
            try
            {
                filter ofil = new filter();
                ofil.name = null;
                ofil.values = null;
                filter.Add(ofil);
                return filter;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return filter;
            }
        }

        public List<category> get_cat_features(string cat_id)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + cat_id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc);

                var catagories = new List<category>();
                foreach (var c in cursor)
                {
                    category cat = new category();
                    cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    if (check_field(c.ToBsonDocument(), "filters") == true)
                    {
                        cat.filters = JsonConvert.DeserializeObject<List<filter>>(c.ToBsonDocument()["filters"].ToString());
                    }
                    catagories.Add(cat);
                }
                return catagories;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }
        }


        public List<category> get_cat_filters_details(string cat_id)
        {
            var catagories = new List<category>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + cat_id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc);


                foreach (var c in cursor)
                {
                    category cat = new category();
                    cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;


                    cat.filters = JsonConvert.DeserializeObject<List<filter>>(c.ToBsonDocument()["filters"].ToString());
                    catagories.Add(cat);
                }
                return catagories;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }
        }

        public List<category> get_cat_imgvidurls(string cat_id)
        {
            // evoke mongodb connection method in dal
            var catagories = new List<category>();
            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + cat_id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc);


                foreach (var c in cursor)
                {
                    category cat = new category();
                    cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;

                    if (check_field(c.ToBsonDocument(), "image_urls"))
                    {
                        cat.image_urls = JsonConvert.DeserializeObject<List<imgurl>>(c.ToBsonDocument()["image_urls"].ToString());
                    }
                    if (check_field(c.ToBsonDocument(), "video_urls"))
                    {
                        cat.video_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["video_urls"].ToString());
                    }
                    if (check_field(c.ToBsonDocument(), "ad_urls"))
                    {
                        cat.ad_urls = JsonConvert.DeserializeObject<List<adurl>>(c.ToBsonDocument()["ad_urls"].ToString());
                    }
                    catagories.Add(cat);
                }
                return catagories;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return catagories;
            }
        }


        public List<product> get_prod_imgvidurls(string prod_id)
        {
            // evoke mongodb connection method in dal
            var products = new List<product>();
            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + prod_id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);


                foreach (var c in cursor)
                {
                    product prd = new product();
                    prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;

                    if (check_field(c.ToBsonDocument(), "image_urls"))
                    {
                        prd.image_urls = JsonConvert.DeserializeObject<List<imgurl>>(c.ToBsonDocument()["image_urls"].ToString());
                    }
                    if (check_field(c.ToBsonDocument(), "video_urls"))
                    {
                        prd.video_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["video_urls"].ToString());
                    }
                    if (check_field(c.ToBsonDocument(), "ad_urls"))
                    {
                        prd.ad_urls = JsonConvert.DeserializeObject<List<adurl>>(c.ToBsonDocument()["ad_urls"].ToString());
                    }
                    products.Add(prd);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products;
        }

        public List<product> get_product_list()
        {
            var products = new List<product>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{parent_prod:'0'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                product prod = new product();
                prod.id = "0";
                prod.Name = "New Product";
                products.Add(prod);

                foreach (var c in cursor)
                {
                    product prd = new product();
                    prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prd.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    products.Add(prd);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }

            return products;
        }


        //Added By Hassan To Reduce Product List Search Time
        public List<product> get_product_list_less_time()
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{parent_prod:'0'}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "id", "Name" };
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }

        public List<warehouse> get_warehouse()
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            MongoCursor cursor = dal.execute_mongo_db_sort("warehouse", "warehouse_name");
            List<warehouse> olw = new List<warehouse>();

            foreach (var c in cursor)
            {
                warehouse ow = new warehouse();
                ow.warehouse_name = check_field(c.ToBsonDocument(), "warehouse_name") ? convertToString(c.ToBsonDocument()["warehouse_name"]) : null;
                olw.Add(ow);
            }


            return olw;
        }

        public string insert_warehouse(warehouse wire)
        {
            // evoke mongodb connection method in dal           
            try
            {

                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db("warehouse");
                var ware = new List<warehouse>();


                foreach (var c in cursor)
                {
                    warehouse w = new warehouse();
                    w.warehouse_name = check_field(c.ToBsonDocument(), "warehouse_name") ? convertToString(c.ToBsonDocument()["warehouse_name"]) : null;
                    ware.Add(w);

                }
                int Match = 0;
                string result = "";
                for (var i = 0; i < ware.Count(); i++)
                {
                    if (ware[i].warehouse_name.ToString().ToUpper() == wire.warehouse_name.ToString().ToUpper())
                    {
                        Match++;
                    }
                }

                if (Match == 0)
                {

                    wire.created_ts = System.DateTime.Now;
                    BsonDocument ware_house = wire.ToBsonDocument();
                    result = dal.mongo_write("warehouse", ware_house);

                }
                else
                {
                    result = "Warehouse Name Already Exists";

                }
                return result;


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }



        public string update_warehouse(warehouse ware)
        {
            try
            {
                // evoke mongodb connection method in dal

                ware.created_ts = System.DateTime.Now;
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_ware = ware.ToBsonDocument();

                var whereclause = "{warehouse_name:'" + ware.warehouse_name + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();
                update = MongoDB.Driver.Builders.Update.Set("warehouse_address", ware.warehouse_address).Set("owner_name", ware.owner_name).Set("owner_email", ware.owner_email).Set("created_ts", ware.created_ts);
                string result = dal.mongo_update("warehouse", whereclause, bd_ware, update);
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }








        public List<warehouse> getwarehouse_details(string warehouse_name)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{warehouse_name:'" + warehouse_name + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("warehouse", queryDoc);

                var warehouse = new List<warehouse>();
                foreach (var c in cursor)
                {
                    //		c.ToBsonDocument().Contains("id")

                    warehouse ware = new warehouse();
                    ware.warehouse_address = check_field(c.ToBsonDocument(), "warehouse_address") ? convertToString(c.ToBsonDocument()["warehouse_address"]) : null;
                    ware.owner_name = check_field(c.ToBsonDocument(), "owner_name") ? convertToString(c.ToBsonDocument()["owner_name"]) : null;
                    ware.owner_email = check_field(c.ToBsonDocument(), "owner_email") ? convertToString(c.ToBsonDocument()["owner_email"]) : null;
                    warehouse.Add(ware);
                }
                return warehouse;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }

        }


        public List<product_enquire> search_prod_enquire_data(product_enquire prod)
        {


            DataAccess da = new DataAccess();

            List<product_enquire> enq = new List<product_enquire>();
            //   List<product_enquire_with_product_details> pd = new List<product_enquire_with_product_details>();
            DataTable dtResult = new DataTable();
            if (prod.name == null)
            {
                prod.name = "";
            }

            if (prod.company_name == "All")
            {
                try
                {

                    dtResult = da.ExecuteDataTable("get_all_prodenquire",
                                      da.Parameter("v_username", prod.name),
                                      da.Parameter("_status", prod.status)
                                      );

                    if (dtResult != null && dtResult.Rows.Count > 0)
                    {


                        for (int i = 0; i < dtResult.Rows.Count; i++)
                        {
                            product_enquire prd_dtl = new product_enquire();

                            prd_dtl.company_name = convertToString(dtResult.Rows[i]["company_name"]);
                            prd_dtl.name = convertToString(dtResult.Rows[i]["name"]);
                            prd_dtl.email_id = convertToString(dtResult.Rows[i]["email"]);
                            prd_dtl.status = convertToString(dtResult.Rows[i]["enquire_status"]);
                            prd_dtl.city = convertToString(dtResult.Rows[i]["city"]);
                            prd_dtl.mobile_no = convertToString(dtResult.Rows[i]["mobile"]);
                            prd_dtl.enq_id = Convert.ToInt32(dtResult.Rows[i]["id"].ToString());
                            prd_dtl.comments = convertToString(dtResult.Rows[i]["comments"]);
                            prd_dtl.enquire_from = convertToString(dtResult.Rows[i]["enquire_from"]);
                            prd_dtl.size_quantity = convertToString(dtResult.Rows[i]["size_quantity"]);
                            prd_dtl.create_ts = convertToDate( dtResult.Rows[i]["create_ts"]);


                            enq.Add(prd_dtl);
                        }

                    }
                }

                catch (Exception ex)
                {
                    ExternalLogger.LogError(ex, this, "#");
                    throw ex;
                }
            }

            else
            {
                try
                {
                    dtResult = da.ExecuteDataTable("bulk_prodenquire_search"
                                    , da.Parameter("_company_name", prod.company_name),
                                      da.Parameter("v_username", prod.name),
                                      da.Parameter("_status", prod.status)
                                    );

                    if (dtResult != null && dtResult.Rows.Count > 0)
                    {

                        //  dtResult.Rows[0]
                        for (int i = 0; i < dtResult.Rows.Count; i++)
                        {
                            product_enquire prd_dtl = new product_enquire();
                            // product_enquire comp = new product_enquire();
                            //comp.company_name = Convert.ToInt32(dtResult.Rows[i]["company_name"].ToString());
                            prd_dtl.company_name = convertToString(dtResult.Rows[i]["company_name"]);
                            prd_dtl.name = convertToString(dtResult.Rows[i]["name"]);
                            prd_dtl.email_id = convertToString(dtResult.Rows[i]["email"]);
                            prd_dtl.status = convertToString(dtResult.Rows[i]["enquire_status"]);
                            prd_dtl.city = convertToString(dtResult.Rows[i]["city"]);
                            prd_dtl.mobile_no = convertToString(dtResult.Rows[i]["mobile"]);
                            prd_dtl.enq_id = Convert.ToInt32(dtResult.Rows[i]["id"].ToString());
                            prd_dtl.comments = convertToString(dtResult.Rows[i]["comments"]);
                            prd_dtl.enquire_from = convertToString(dtResult.Rows[i]["enquire_from"]);
                            prd_dtl.size_quantity = convertToString(dtResult.Rows[i]["size_quantity"]);
                            prd_dtl.create_ts = convertToDate(dtResult.Rows[i]["create_ts"]);

                            //List<product_enquire_with_product_details> pe = JsonConvert.DeserializeObject<List<product_enquire_with_product_details>>(prd_dtl.size_quantity.ToString());
                            //foreach (product_enquire_with_product_details cnt in pe)
                            //{
                            //   // product_details prd_dtl = new product_details();

                            //    prd_dtl.prod_id = cnt.id.ToString();
                            //    prd_dtl.name = cnt.name.ToString();
                            //   prd_dtl.size = cnt.size.ToString();
                            //   prd_dtl.sku = cnt.sku.ToString();
                            //   prd_dtl.ord_qty = cnt.ord_qty.ToString();
                            //}

                            enq.Add(prd_dtl);
                        }
                    }



                }
                catch (Exception ex)
                {
                    ExternalLogger.LogError(ex, this, "#");
                    throw ex;
                }
            }

            return enq;
        }


        public List<enquire> search_gen_enquire_data(enquire enqury)
        {


            DataAccess da = new DataAccess();

            List<enquire> enq = new List<enquire>();
            //   List<product_enquire_with_product_details> pd = new List<product_enquire_with_product_details>();
            DataTable dtResult = new DataTable();
            if (enqury.name == null)
            {
                enqury.name = "";
            }
            if (enqury.company_name == "All")
            {

                try
                {
                    dtResult = da.ExecuteDataTable("get_all_genenquire"
                                    , da.Parameter("_company_name", enqury.company_name),
                                      da.Parameter("v_username", enqury.name),
                                      da.Parameter("_status", enqury.status)
                                    );

                    if (dtResult != null && dtResult.Rows.Count > 0)
                    {

                        //  dtResult.Rows[0]
                        for (int i = 0; i < dtResult.Rows.Count; i++)
                        {
                            enquire eq = new enquire();
                            // product_enquire comp = new product_enquire();
                            //comp.company_name = Convert.ToInt32(dtResult.Rows[i]["company_name"].ToString());
                            eq.id = Convert.ToInt32(dtResult.Rows[i]["id"].ToString());
                            eq.name = convertToString(dtResult.Rows[i]["name"]);
                            eq.company_name = convertToString(dtResult.Rows[i]["company_name"]);
                            eq.email = convertToString(dtResult.Rows[i]["email"]);
                            eq.status = convertToString(dtResult.Rows[i]["enquire_status"]);
                            eq.enquire_from = convertToString(dtResult.Rows[i]["enquire_from"]);
                            eq.operating_city = convertToString(dtResult.Rows[i]["operating_city"]);
                            eq.mobile_no = convertToString(dtResult.Rows[i]["mobile"]);
                            eq.general_information = convertToString(dtResult.Rows[i]["general_information"]);
                            eq.product_code = convertToString(dtResult.Rows[i]["product_code"]);
                            eq.additional_remarks = convertToString(dtResult.Rows[i]["additional_remarks"]);
                            eq.comment = convertToString(dtResult.Rows[i]["comments"]);
                            eq.create_ts = convertToDate(dtResult.Rows[i]["create_ts"]);
                            //  eq.= convertToString(dtResult.Rows[i]["size_quantity"]);



                            enq.Add(eq);
                        }
                    }



                }
                catch (Exception ex)
                {
                    ExternalLogger.LogError(ex, this, "#");
                    throw ex;
                }

               
            }

            else
            {
                
                    try
                    {
                        dtResult = da.ExecuteDataTable("bulk_genenquire_search"
                                        , da.Parameter("_company_name", enqury.company_name),
                                          da.Parameter("v_username", enqury.name),
                                          da.Parameter("_status", enqury.status)
                                        );

                        if (dtResult != null && dtResult.Rows.Count > 0)
                        {

                            //  dtResult.Rows[0]
                            for (int i = 0; i < dtResult.Rows.Count; i++)
                            {
                                enquire eq = new enquire();
                                // product_enquire comp = new product_enquire();
                                //comp.company_name = Convert.ToInt32(dtResult.Rows[i]["company_name"].ToString());
                                eq.id = Convert.ToInt32(dtResult.Rows[i]["id"].ToString());
                                eq.name = convertToString(dtResult.Rows[i]["name"]);
                                eq.company_name = convertToString(dtResult.Rows[i]["company_name"]);
                                eq.email = convertToString(dtResult.Rows[i]["email"]);
                                eq.status = convertToString(dtResult.Rows[i]["enquire_status"]);
                                eq.enquire_from = convertToString(dtResult.Rows[i]["enquire_from"]);
                                eq.operating_city = convertToString(dtResult.Rows[i]["operating_city"]);
                                eq.mobile_no = convertToString(dtResult.Rows[i]["mobile"]);
                                eq.general_information = convertToString(dtResult.Rows[i]["general_information"]);
                                eq.product_code = convertToString(dtResult.Rows[i]["product_code"]);
                                eq.additional_remarks = convertToString(dtResult.Rows[i]["additional_remarks"]);
                                eq.comment = convertToString(dtResult.Rows[i]["comments"]);
                                eq.create_ts = convertToDate(dtResult.Rows[i]["create_ts"]);
                                //  eq.= convertToString(dtResult.Rows[i]["size_quantity"]);



                                enq.Add(eq);
                            }
                        }



                    }
                    catch (Exception ex)
                    {
                        ExternalLogger.LogError(ex, this, "#");
                        throw ex;
                    }

                
            }

            return enq;
        }




        private List<product> mongo_query(string qs, string collection, string[] include_fields, string[] exclude_fields)
        {
            List<product> products = new List<product>();
            try
            {
                dal.DataAccess dal = new DataAccess();
                string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] ascendin_sort_fields = new string[] { "Name" };

                MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields, ascendin_sort_fields, null);

                product prod = new product();
                prod.id = "0";
                prod.Name = "New Product";
                products.Add(prod);

                foreach (var c in cursor)
                {
                    product prd = new product();
                    prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prd.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    products.Add(prd);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products;
        }

        public List<product> get_store_Sku_list(string StoreCompId)
        {
            var products = new List<product>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{parent_prod:'0'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);


                foreach (var c in cursor)
                {
                    product prd = new product();
                    prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prd.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    products.Add(prd);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }

            return products;
        }

        public List<product> ProdListApp(product prod)
        {
            var products = new List<product>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{active: 2}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);


                var SearchProducts = new List<product>();

                if (prod.Name == null && prod.brand != null)
                {
                    foreach (var c in cursor)
                    {
                        if (convertToString(c.ToBsonDocument()["brand"]).Contains(prod.brand))
                        {
                            product prd = new product();
                            prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                            prd.sku = check_field(c.ToBsonDocument(), "sku") ? convertToString(c.ToBsonDocument()["sku"]) : null;
                            prd.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                            prd.brand = check_field(c.ToBsonDocument(), "brand") ? convertToString(c.ToBsonDocument()["brand"]) : null;
                            products.Add(prd);
                        }
                    }
                }
                else if (prod.Name != null && prod.brand == null)
                {
                    foreach (var c in cursor)
                    {
                        if (convertToString(c.ToBsonDocument()["Name"]).Contains(prod.Name))
                        {
                            product prd = new product();
                            prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                            prd.sku = check_field(c.ToBsonDocument(), "sku") ? convertToString(c.ToBsonDocument()["sku"]) : null;
                            prd.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                            prd.brand = check_field(c.ToBsonDocument(), "brand") ? convertToString(c.ToBsonDocument()["brand"]) : null;
                            products.Add(prd);
                        }
                    }
                }
                else if (prod.Name != null && prod.brand != null)
                {
                    foreach (var c in cursor)
                    {
                        if (convertToString(c.ToBsonDocument()["Name"]).Contains(prod.Name) && convertToString(c.ToBsonDocument()["brand"]).Contains(prod.brand))
                        {
                            product prd = new product();
                            prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                            prd.sku = check_field(c.ToBsonDocument(), "sku") ? convertToString(c.ToBsonDocument()["sku"]) : null;
                            prd.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                            prd.brand = check_field(c.ToBsonDocument(), "brand") ? convertToString(c.ToBsonDocument()["brand"]) : null;
                            products.Add(prd);
                        }
                    }
                }
                else
                {
                    foreach (var c in cursor)
                    {

                        product prd = new product();
                        prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                        prd.sku = check_field(c.ToBsonDocument(), "sku") ? convertToString(c.ToBsonDocument()["sku"]) : null;
                        prd.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                        prd.brand = check_field(c.ToBsonDocument(), "brand") ? convertToString(c.ToBsonDocument()["brand"]) : null;
                        products.Add(prd);

                    }

                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products;

        }

        public List<product> getProdSearchList(string prodsku, string prodid, string prodName, string prodbrand)
        {
            var products = new List<product>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();

                string q;
                q = "{'sku':/^" + prodsku + "/, 'id':/^" + prodid + "/, 'Name':/^" + prodName + "/, 'brand': /" + prodbrand + "/i}";

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                foreach (var c in cursor)
                {
                    product prd = new product();
                    prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prd.sku = check_field(c.ToBsonDocument(), "sku") ? convertToString(c.ToBsonDocument()["sku"]) : null;
                    prd.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    prd.brand = check_field(c.ToBsonDocument(), "brand") ? convertToString(c.ToBsonDocument()["brand"]) : null;
                    products.Add(prd);
                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products;

        }


        public string ProdForApproval(prodapp prodapp)
        {
            string st = "";
            try
            {

                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{active: 2}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product prd = new product();
                    prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prd.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    products.Add(prd);
                }

                for (int i = 0; i < prodapp.prod_ids.Count(); i++)
                {

                    for (int j = 0; j < products.Count(); j++)
                    {
                        if (products[j].id == prodapp.prod_ids[i].id)
                        {
                            product prod = new product();
                            prod = products[j];
                            prod.active = 1;
                            var whereclause = "";
                            whereclause = "{id:'" + prod.id + "'}";
                            prod._id = ObjectId.GenerateNewId().ToString();
                            BsonDocument bd_prd = prod.ToBsonDocument();
                            MongoDB.Driver.Builders.UpdateBuilder update = MongoDB.Driver.Builders.Update.Set("active", prod.active);
                            st = dal.mongo_update("product", whereclause, bd_prd, update);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return st;

        }

        public List<product> AddSearchProduct(List<product> products, BsonDocument c)
        {
            try
            {
                product prd = new product();
                prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                prd.sku = check_field(c.ToBsonDocument(), "sku") ? convertToString(c.ToBsonDocument()["sku"]) : null;
                prd.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                prd.brand = check_field(c.ToBsonDocument(), "brand") ? convertToString(c.ToBsonDocument()["brand"]) : null;
                products.Add(prd);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products;
        }

        public List<product> get_parent_product_list()
        {
            // evoke mongodb connection method in dal
            var products = new List<product>();
            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{parent_prod:'0'}");
                QueryDocument queryDoc = new QueryDocument(document);             
                //MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);
                string[] include_fields = new string[] { "id", "Name" };
                string[] exclude_fields = new string[] { "_id" };
                string[] ascendin_sort_fields = new string[] { "Name" };

                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields, ascendin_sort_fields, null);


                product prod = new product();
                prod.id = "0";
                prod.Name = "";
                products.Add(prod);

                foreach (var c in cursor)
                {
                    product prd = new product();
                    prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prd.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    products.Add(prd);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }

            return products;
        }

        public List<product> get_parent_prod_list()
        {
            // evoke mongodb connection method in dal
            var products = new List<product>();
            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{have_child: 1 }");
                QueryDocument queryDoc = new QueryDocument(document);
               // MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                string[] include_fields = new string[] { "id", "Name" };
                string[] exclude_fields = new string[] { "_id" };
                string[] ascendin_sort_fields = new string[] { "Name" };

                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields, ascendin_sort_fields, null);



                //product prod = new product();
                //prod.id = "0";
                //prod.Name = "";
                //products.Add(prod);

                foreach (var c in cursor)
                {
                    product prd = new product();
                    prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prd.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    products.Add(prd);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }

            return products;
        }


        public List<product> get_product_details(string prod_id)
        {
            // evoke mongodb connection method in dal
            var products = new List<product>();
            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + prod_id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);


                foreach (var c in cursor)
                {


                    product prod = new product();
                    prod._id = check_field(c.ToBsonDocument(), "_id") ? convertToString(c.ToBsonDocument()["_id"]) : null;
                    prod.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prod.sku = check_field(c.ToBsonDocument(), "sku") ? convertToString(c.ToBsonDocument()["sku"]) : null;
                    prod.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    prod.brand = check_field(c.ToBsonDocument(), "brand") ? convertToString(c.ToBsonDocument()["brand"]) : null;
                    prod.active = check_field(c.ToBsonDocument(), "active") ? convertToInt(c.ToBsonDocument()["active"]) : 0;
                    prod.min_order_qty = check_field(c.ToBsonDocument(), "min_order_qty") ? convertToDouble(c.ToBsonDocument()["min_order_qty"]) : 10;
                    prod.description = check_field(c.ToBsonDocument(), "description") ? convertToString(c.ToBsonDocument()["description"]) : null;

                    prod.shortdesc = check_field(c.ToBsonDocument(), "shortdesc") ? convertToString(c.ToBsonDocument()["shortdesc"]) : null;
                    //  prod.stock = check_field(c.ToBsonDocument(), "stock") ? convertToInt(c.ToBsonDocument()["stock"]) : 0;

                    //var vv=c.ToBsonDocument()["stock"].BsonType;

                    if (c.ToBsonDocument()["stock"].BsonType == MongoDB.Bson.BsonType.Int64)
                    {
                        prod.stock = new List<bulk_stock>();
                    }
                    else
                    {
                        prod.stock = JsonConvert.DeserializeObject<List<bulk_stock>>(c.ToBsonDocument()["stock"].ToString()); //Atul 31-07-2014
                    }

                    prod.parent_cat_id = check_field(c.ToBsonDocument(), "parent_cat_id") ? convertToString(c.ToBsonDocument()["parent_cat_id"]) : "0";

                    if (c.ToBsonDocument()["price"].BsonType == MongoDB.Bson.BsonType.Document)
                    {
                        prod.price = new List<price>();
                    }
                    else
                    {
                        prod.price = JsonConvert.DeserializeObject<List<price>>(c.ToBsonDocument()["price"].ToString()); // Atul 31-07-2014
                    }

                    prod.cat_id = JsonConvert.DeserializeObject<List<category_id>>(c.ToBsonDocument()["cat_id"].ToString());
                    prod.Express = check_field(c.ToBsonDocument(), "Express") ? convertToString(c.ToBsonDocument()["Express"]) : null;
                    prod.have_child = check_field(c.ToBsonDocument(), "have_child") ? convertToInt(c.ToBsonDocument()["have_child"]) : 0;
                    prod.Reorder_Level = check_field(c.ToBsonDocument(), "Reorder_Level") ? convertToInt(c.ToBsonDocument()["Reorder_Level"]) : 0;
                    products.Add(prod);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products;
        }



        public bool get_ChildProdExistResult(string prod_id)
        {
            // evoke mongodb connection method in dal
            bool Result = false;
            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{parent_prod:'" + prod_id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);


                foreach (var c in cursor)
                {
                    Result = true;

                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return Result;
        }

        public List<product> get_child_product_details(string prod_id)
        {
            // evoke mongodb connection method in dal
            var products = new List<product>();
            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + prod_id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                foreach (var c in cursor)
                {
                    product prod = new product();
                    prod.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prod.stock = JsonConvert.DeserializeObject<List<bulk_stock>>(c.ToBsonDocument()["stock"].ToString());  
                    products.Add(prod);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products;
        }
        
        public List<product> get_product_features(string prod_id)
        {
            // evoke mongodb connection method in dal
            var products = new List<product>();
            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + prod_id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);


                foreach (var c in cursor)
                {
                    product prod = new product();
                    prod.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prod.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    prod.feature = JsonConvert.DeserializeObject<List<features>>(c.ToBsonDocument()["feature"].ToString());
                    prod.parent_cat_id = check_field(c.ToBsonDocument(), "parent_cat_id") ? convertToString(c.ToBsonDocument()["parent_cat_id"]) : null;
                    products.Add(prod);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products;
        }
        public List<deals> get_deal_history(deals dl)
        {
            var deal_List = new List<deals>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db_sort("deals", "deal_details");

                foreach (var c in cursor)
                {
                    deals odl = new deals();
                    odl.deal_details = check_field(c.ToBsonDocument(), "deal_details") ? convertToString(c.ToBsonDocument()["deal_details"]) : null;
                    deal_List.Add(odl);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return deal_List;
        }


        public List<product> get_child_product(string parent_prod)
        {
            var products = new List<product>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument doc = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + parent_prod + "'}");
                QueryDocument qryDoc = new QueryDocument(doc);
                MongoCursor cur = dal.execute_mongo_db("product", qryDoc);

                var Parent_Prod_Name = "";
                foreach (var c in cur)
                {
                    Parent_Prod_Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                }

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{parent_prod:'" + parent_prod + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                foreach (var c in cursor)
                {
                    product prod = new product();
                    prod.id = c.ToBsonDocument()["id"].ToString();
                    prod.Name = Parent_Prod_Name;
                    prod.sku = check_field(c.ToBsonDocument(), "sku") ? convertToString(c.ToBsonDocument()["sku"]) : null;
                    prod.size = check_field(c.ToBsonDocument(), "size") ? convertToString(c.ToBsonDocument()["size"]) : null;

                    if (c.ToBsonDocument()["stock"].BsonType == MongoDB.Bson.BsonType.Double)
                    {
                        prod.stock = new List<bulk_stock>();
                    }
                    else
                    {
                     //   prod.stock = JsonConvert.DeserializeObject<List<bulk_stock>>(c.ToBsonDocument()["stock"].ToString()); //Atul 31-07-2014
                        prod.stock = JsonConvert.DeserializeObject<List<bulk_stock>>(c.ToBsonDocument()["stock"].ToString());
                    }
                   // prod.stock = JsonConvert.DeserializeObject<List<bulk_stock>>(c.ToBsonDocument()["stock"].ToString());
                    prod.Express = check_field(c.ToBsonDocument(), "Express") ? convertToString(c.ToBsonDocument()["Express"]) : null;
                    products.Add(prod);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products;
        }
        public List<category> get_cat_list_for_product()
        {
            //// evoke mongodb connection method in dal
            //dal.DataAccess dal = new DataAccess();
            //MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");

            //var catagories = new List<category>();

            //foreach (var c in cursor)
            //{
            //    category cat = new category();
            //    cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
            //    cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
            //    catagories.Add(cat);
            //}

            //return catagories;

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");

                var catagories = new List<category>();

                var Category_List = new List<category>();
                var Modify_Cat_List = new List<category>();
                category cate = new category();
                cate.id = "0";
                cate.Name = "";
                catagories.Add(cate);
                Modify_Cat_List.Add(cate);

                foreach (var c in cursor)
                {
                    category cat = new category();
                    cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    cat.ParentId = check_field(c.ToBsonDocument(), "ParentId") ? convertToString(c.ToBsonDocument()["ParentId"]) : null;
                    catagories.Add(cat);
                }

                if (catagories.Count() > 1)
                {
                    category categ = new category();
                    categ.id = "0";
                    categ.Name = "";
                    Category_List.Add(categ);
                    Category_List = GenerateCategoryList(catagories, Category_List);

                    for (var i = 0; i < Category_List.Count(); i++)
                    {
                        string[] Category_ID = Category_List[i].id.ToString().Split('.');
                        {
                            if (Category_ID.Count() == 3 && Category_ID[2].ToString() != "0")
                            {
                                category cat1 = new category();
                                cat1.id = Category_List[i].id.ToString();
                                cat1.Name = Category_List[i].Name.ToString();
                                Modify_Cat_List.Add(cat1);
                            }
                        }
                    }


                    return Modify_Cat_List;
                }
                else
                {
                    return catagories;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }

        }

        public List<category> get_cat_list_tree()
        {
            var Modify_Cat_List = new List<category>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");

                var cat_tree = new List<category>();
                var catagories = new List<category>();

                category cattest = new category();
                cattest.id = "0";
                cattest.Name = "";
                cattest.ParentId = "1111";//Demo Data It will not populate
                catagories.Add(cattest);


                foreach (var c in cursor)
                {
                    category cat = new category();
                    cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                    cat.ParentId = check_field(c.ToBsonDocument(), "ParentId") ? convertToString(c.ToBsonDocument()["ParentId"]) : null;
                    catagories.Add(cat);
                }

                cat_tree = GenerateCategoryList(catagories, cat_tree);

                for (var i = 0; i < cat_tree.Count(); i++)
                {
                    string[] Category_ID = cat_tree[i].id.ToString().Split('.');
                    {
                        if (Category_ID.Count() == 3)
                        {
                            category cat1 = new category();
                            cat1.id = cat_tree[i].id.ToString();
                            cat1.Name = cat_tree[i].Name.ToString();
                            Modify_Cat_List.Add(cat1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }

            return Modify_Cat_List;
            //if (catagories.Count() > 0)
            //{
            //    for (int i = 0; i < catagories.Count(); i++)
            //    {
            //        if (catagories[i].ParentId.ToString() == "0")
            //        {
            //            category objcat = new category();
            //            objcat.id = catagories[i].id.ToString();
            //            objcat.Name = catagories[i].Name.ToString();
            //            objcat.ParentId = catagories[i].ParentId.ToString();
            //            cat_tree.Add(objcat);
            //            cat_tree = AddSecondLevelCat(catagories[i].id.ToString(), cat_tree, catagories);
            //        }
            //    }
            //    return cat_tree;
            //}
            //else
            //{
            //    return catagories;
            //}


        }

        public List<category> AddSecondLevelCat(string Parent_ID, List<category> Category_Tree_List, List<category> Category_List)
        {
            try
            {
                for (int i = 0; i < Category_List.Count(); i++)
                {

                    if (Category_List[i].ParentId.ToString() == Parent_ID)
                    {
                        category objcat = new category();
                        objcat.id = Category_List[i].id.ToString();
                        objcat.Name = "|" + "|" + Category_List[i].Name.ToString();
                        objcat.ParentId = Category_List[i].ParentId.ToString();
                        Category_Tree_List.Add(objcat);
                        Category_Tree_List = AddThirdLevelCat(Category_List[i].id.ToString(), Category_Tree_List, Category_List);
                    }

                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return Category_Tree_List;

        }
        public List<category> AddThirdLevelCat(string Parent_ID, List<category> Category_Tree_List, List<category> Category_List)
        {
            try
            {
                for (int i = 0; i < Category_List.Count(); i++)
                {
                    if (Category_List[i].ParentId.ToString() == Parent_ID)
                    {
                        category objcat = new category();
                        objcat.id = Category_List[i].id.ToString();
                        objcat.Name = "|" + "|" + "|" + "|" + Category_List[i].Name.ToString();
                        Category_Tree_List.Add(objcat);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }

            return Category_Tree_List;
        }

        public string update_prod_cat_info(product prod)
        {
            // evoke mongodb connection method in dal
            prod._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_prod = prod.ToBsonDocument();

            string result = "";

            try
            {
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + prod.id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product product = new product();
                    product.cat_id = JsonConvert.DeserializeObject<List<category_id>>(c.ToBsonDocument()["cat_id"].ToString());
                    products.Add(product);
                }

                var Cat_ID_List = new List<category_id>();
                Cat_ID_List = products[0].cat_id;

                var whereclause = "";
                whereclause = "{id:'" + prod.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = MongoDB.Driver.Builders.Update.Set("parent_cat_id", prod.parent_cat_id);

                var bsonArray = new BsonArray();

                for (int i = 0; i < Cat_ID_List.Count; i++)
                {
                    bsonArray.Add(Cat_ID_List[i].ToBsonDocument());
                }

                update.PullAll("cat_id", bsonArray);

                result = dal.mongo_update("product", whereclause, bd_prod, update);

                whereclause = "{id:'" + prod.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder upd = MongoDB.Driver.Builders.Update.Set("parent_cat_id", prod.parent_cat_id);

                var bsonArrayNew = new BsonArray();

                for (int i = 0; i < prod.cat_id.Count; i++)
                {
                    bsonArrayNew.Add(prod.cat_id[i].ToBsonDocument());
                }

                upd.PushAll("cat_id", bsonArrayNew);

                result = dal.mongo_update("product", whereclause, bd_prod, upd);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return result;
        }

        public string update_cat_filter(category cate)
        {
            // evoke mongodb connection method in dal
            cate._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_prod = cate.ToBsonDocument();

            string result = "";

            try
            {
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + cate.id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc);

                var categories = new List<category>();
                foreach (var c in cursor)
                {
                    category categ = new category();
                    categ.filters = JsonConvert.DeserializeObject<List<filter>>(c.ToBsonDocument()["filters"].ToString());
                    categories.Add(categ);
                }

                var Cate_filters = new List<filter>();
                Cate_filters = categories[0].filters;

                var Prev_Cate_filters = new List<filter>();
                Prev_Cate_filters = categories[0].filters;

                var whereclause = "";
                whereclause = "{id:'" + cate.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonArray = new BsonArray();

                for (int i = 0; i < Cate_filters.Count; i++)
                {
                    bsonArray.Add(Cate_filters[i].ToBsonDocument());
                }

                update.PullAll("filters", bsonArray);

                result = dal.mongo_update("category", whereclause, bd_prod, update);



                int m = 0;

                for (int i = 0; i < Prev_Cate_filters.Count; i++)
                {
                    if (Prev_Cate_filters[i].name == cate.filters[0].name)
                    {
                        Prev_Cate_filters[i] = cate.filters[0];
                        m = 1;
                    }
                }

                if (m == 0)
                {
                    filter NewFilter = new filter();
                    NewFilter.name = cate.filters[0].name;
                    NewFilter.values = cate.filters[0].values;
                    Prev_Cate_filters.Add(NewFilter);
                }
                cate.filters = Prev_Cate_filters;

                whereclause = "{id:'" + cate.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder upd = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonArrayNew = new BsonArray();

                for (int i = 0; i < cate.filters.Count; i++)
                {
                    bsonArrayNew.Add(cate.filters[i].ToBsonDocument());
                }

                upd.PushAll("filters", bsonArrayNew);

                result = dal.mongo_update("category", whereclause, bd_prod, upd);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return result;
        }
        public string upload_cat_imgvid(category cate)
        {
            // evoke mongodb connection method in dal
            cate._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_prod = cate.ToBsonDocument();

            string result = "";

            try
            {
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + cate.id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc);

                var categories = new List<category>();
                foreach (var c in cursor)
                {
                    category categ = new category();
                    categ.image_urls = JsonConvert.DeserializeObject<List<imgurl>>(c.ToBsonDocument()["image_urls"].ToString());
                    categ.video_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["video_urls"].ToString());
                    categ.ad_urls = JsonConvert.DeserializeObject<List<adurl>>(c.ToBsonDocument()["ad_urls"].ToString());
                    categories.Add(categ);
                }

                var Prev_Cate_Img_Urls = new List<imgurl>();
                Prev_Cate_Img_Urls = categories[0].image_urls;

                var Prev_Cate_Video_Urls = new List<url>();
                Prev_Cate_Video_Urls = categories[0].video_urls;

                var Prev_Cate_Ad_Urls = new List<adurl>();
                Prev_Cate_Ad_Urls = categories[0].ad_urls;

                var whereclause = "";
                whereclause = "{id:'" + cate.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonImgArray = new BsonArray();

                for (int i = 0; i < categories[0].image_urls.Count(); i++)
                {
                    bsonImgArray.Add(categories[0].image_urls[i].ToBsonDocument());
                }

                var bsonVidArray = new BsonArray();

                for (int i = 0; i < categories[0].video_urls.Count(); i++)
                {
                    bsonVidArray.Add(categories[0].video_urls[i].ToBsonDocument());
                }

                var bsonAdArray = new BsonArray();

                for (int i = 0; i < categories[0].ad_urls.Count(); i++)
                {
                    bsonAdArray.Add(categories[0].ad_urls[i].ToBsonDocument());
                }

                update.PullAll("image_urls", bsonImgArray);
                update.PullAll("video_urls", bsonVidArray);
                update.PullAll("ad_urls", bsonAdArray);

                result = dal.mongo_update("category", whereclause, bd_prod, update);

                if ((cate.image_urls[0].display_name == null) && (cate.image_urls[0].link == null) && (cate.image_urls[0].thumb_link == null) && (cate.image_urls[0].zoom_link == null))
                {
                    cate.image_urls = Prev_Cate_Img_Urls;

                }
                else
                {
                    imgurl objImgUrl = new imgurl();
                    objImgUrl.display_name = cate.image_urls[0].display_name;
                    objImgUrl.link = cate.image_urls[0].link;
                    objImgUrl.thumb_link = cate.image_urls[0].thumb_link;
                    objImgUrl.zoom_link = cate.image_urls[0].zoom_link;
                    Prev_Cate_Img_Urls.Add(objImgUrl);
                    cate.image_urls = Prev_Cate_Img_Urls;
                }
                if ((cate.video_urls[0].display_name == null) && (cate.video_urls[0].link == null))
                {

                    cate.video_urls = Prev_Cate_Video_Urls;
                }
                else
                {
                    url objVidUrl = new url();
                    objVidUrl.display_name = cate.video_urls[0].display_name;
                    objVidUrl.link = cate.video_urls[0].link;
                    Prev_Cate_Video_Urls.Add(objVidUrl);
                    cate.video_urls = Prev_Cate_Video_Urls;

                }
                if ((cate.ad_urls[0].href == null) && (cate.ad_urls[0].link == null))
                {

                    cate.ad_urls = Prev_Cate_Ad_Urls;
                }
                else
                {
                    adurl objAdUrl = new adurl();
                    objAdUrl.href = cate.ad_urls[0].href;
                    objAdUrl.link = cate.ad_urls[0].link;
                    Prev_Cate_Ad_Urls.Add(objAdUrl);
                    cate.ad_urls = Prev_Cate_Ad_Urls;
                }

                whereclause = "{id:'" + cate.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder upd = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonNewImgArray = new BsonArray();

                for (int i = 0; i < cate.image_urls.Count; i++)
                {
                    bsonNewImgArray.Add(cate.image_urls[i].ToBsonDocument());
                }

                var bsonNewVidArray = new BsonArray();

                for (int i = 0; i < cate.video_urls.Count; i++)
                {
                    bsonNewVidArray.Add(cate.video_urls[i].ToBsonDocument());
                }

                var bsonNewAdArray = new BsonArray();

                for (int i = 0; i < cate.ad_urls.Count; i++)
                {
                    bsonNewAdArray.Add(cate.ad_urls[i].ToBsonDocument());
                }

                upd.PushAll("image_urls", bsonNewImgArray);
                upd.PushAll("video_urls", bsonNewVidArray);
                upd.PushAll("ad_urls", bsonNewAdArray);

                result = dal.mongo_update("category", whereclause, bd_prod, upd);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return result;
        }

        public string update_cat_imgvid(category cate)
        {
            // evoke mongodb connection method in dal
            cate._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_prod = cate.ToBsonDocument();

            string result = "";

            try
            {
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + cate.id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc);

                var categories = new List<category>();
                foreach (var c in cursor)
                {
                    category categ = new category();
                    categ.image_urls = JsonConvert.DeserializeObject<List<imgurl>>(c.ToBsonDocument()["image_urls"].ToString());
                    categ.video_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["video_urls"].ToString());
                    categ.ad_urls = JsonConvert.DeserializeObject<List<adurl>>(c.ToBsonDocument()["ad_urls"].ToString());
                    categories.Add(categ);
                }

                var Prev_Cate_Img_Urls = new List<imgurl>();
                Prev_Cate_Img_Urls = categories[0].image_urls;

                var Prev_Cate_Video_Urls = new List<url>();
                Prev_Cate_Video_Urls = categories[0].video_urls;

                var Prev_Cate_Ad_Urls = new List<adurl>();
                Prev_Cate_Ad_Urls = categories[0].ad_urls;

                var whereclause = "";
                whereclause = "{id:'" + cate.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonImgArray = new BsonArray();

                for (int i = 0; i < categories[0].image_urls.Count(); i++)
                {
                    bsonImgArray.Add(categories[0].image_urls[i].ToBsonDocument());
                }

                var bsonVidArray = new BsonArray();

                for (int i = 0; i < categories[0].video_urls.Count(); i++)
                {
                    bsonVidArray.Add(categories[0].video_urls[i].ToBsonDocument());
                }

                var bsonAdArray = new BsonArray();

                for (int i = 0; i < categories[0].ad_urls.Count(); i++)
                {
                    bsonAdArray.Add(categories[0].ad_urls[i].ToBsonDocument());
                }

                update.PullAll("image_urls", bsonImgArray);
                update.PullAll("video_urls", bsonVidArray);
                update.PullAll("ad_urls", bsonAdArray);

                result = dal.mongo_update("category", whereclause, bd_prod, update);

                var New_Cate_Img_Urls = new List<imgurl>();

                if ((cate.image_urls[0].display_name == null) && (cate.image_urls[0].link == null) && (cate.image_urls[0].thumb_link == null) && (cate.image_urls[0].zoom_link == null))
                {
                    cate.image_urls = Prev_Cate_Img_Urls;
                }
                else
                {
                    if (Prev_Cate_Img_Urls.Count() == 0)
                    {
                        imgurl oImgUrl = new imgurl();
                        oImgUrl.display_name = cate.image_urls[0].display_name;
                        oImgUrl.link = cate.image_urls[0].link;
                        oImgUrl.thumb_link = cate.image_urls[0].thumb_link;
                        oImgUrl.zoom_link = cate.image_urls[0].zoom_link;
                        New_Cate_Img_Urls.Add(oImgUrl);
                    }
                    else
                    {
                        for (int i = 0; i < Prev_Cate_Img_Urls.Count(); i++)
                        {
                            if (cate.image_urls[0].display_name != Prev_Cate_Img_Urls[i].display_name)
                            {
                                imgurl objImgUrl = new imgurl();
                                objImgUrl.display_name = Prev_Cate_Img_Urls[i].display_name;
                                objImgUrl.link = Prev_Cate_Img_Urls[i].link;
                                objImgUrl.thumb_link = Prev_Cate_Img_Urls[i].thumb_link;
                                objImgUrl.zoom_link = Prev_Cate_Img_Urls[i].zoom_link;
                                New_Cate_Img_Urls.Add(objImgUrl);
                            }
                            if (cate.image_urls[0].display_name == Prev_Cate_Img_Urls[i].display_name)
                            {
                                imgurl obImgUrl = new imgurl();
                                obImgUrl.display_name = cate.image_urls[0].display_name;
                                obImgUrl.link = cate.image_urls[0].link;
                                obImgUrl.thumb_link = cate.image_urls[0].thumb_link;
                                obImgUrl.zoom_link = cate.image_urls[0].zoom_link;
                                New_Cate_Img_Urls.Add(obImgUrl);
                            }
                        }
                    }
                    cate.image_urls = New_Cate_Img_Urls;
                }

                var New_Cate_Video_Urls = new List<url>();

                if ((cate.video_urls[0].display_name == null) && (cate.video_urls[0].link == null))
                {

                    cate.video_urls = Prev_Cate_Video_Urls;
                }
                else
                {
                    if (Prev_Cate_Video_Urls.Count() == 0)
                    {
                        url ourl = new url();
                        ourl.display_name = cate.image_urls[0].display_name;
                        ourl.link = cate.image_urls[0].link;
                        New_Cate_Video_Urls.Add(ourl);
                    }
                    else
                    {
                        for (int j = 0; j < Prev_Cate_Video_Urls.Count(); j++)
                        {
                            if (cate.video_urls[0].display_name != Prev_Cate_Video_Urls[j].display_name)
                            {
                                url objurl = new url();
                                objurl.display_name = Prev_Cate_Video_Urls[j].display_name;
                                objurl.link = Prev_Cate_Video_Urls[j].link;
                                New_Cate_Video_Urls.Add(objurl);
                            }
                            if (cate.video_urls[0].display_name == Prev_Cate_Video_Urls[j].display_name)
                            {
                                url oburl = new url();
                                oburl.display_name = cate.video_urls[0].display_name;
                                oburl.link = cate.video_urls[0].link;
                                New_Cate_Video_Urls.Add(oburl);
                            }
                        }
                    }

                    cate.video_urls = New_Cate_Video_Urls;
                }

                var New_Cate_Ad_Urls = new List<adurl>();

                if ((cate.ad_urls[0].href == null) && (cate.ad_urls[0].link == null))
                {
                    cate.ad_urls = Prev_Cate_Ad_Urls;
                }
                else
                {
                    if (Prev_Cate_Ad_Urls.Count() == 0)
                    {
                        adurl oadurl = new adurl();
                        oadurl.href = cate.ad_urls[0].href;
                        oadurl.link = cate.ad_urls[0].link;
                        New_Cate_Ad_Urls.Add(oadurl);
                    }
                    else
                    {
                        for (int k = 0; k < Prev_Cate_Ad_Urls.Count(); k++)
                        {
                            if (cate.ad_urls[0].href != Prev_Cate_Ad_Urls[k].href)
                            {
                                adurl objadurl = new adurl();
                                objadurl.href = Prev_Cate_Ad_Urls[k].href;
                                objadurl.link = Prev_Cate_Ad_Urls[k].link;
                                New_Cate_Ad_Urls.Add(objadurl);
                            }
                            if (cate.ad_urls[0].href == Prev_Cate_Ad_Urls[k].href)
                            {
                                adurl obadurl = new adurl();
                                obadurl.href = cate.ad_urls[0].href;
                                obadurl.link = cate.ad_urls[0].link;
                                New_Cate_Ad_Urls.Add(obadurl);
                            }
                        }
                    }
                    cate.ad_urls = New_Cate_Ad_Urls;
                }

                whereclause = "{id:'" + cate.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder upd = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonNewImgArray = new BsonArray();

                for (int i = 0; i < cate.image_urls.Count; i++)
                {
                    bsonNewImgArray.Add(cate.image_urls[i].ToBsonDocument());
                }

                var bsonNewVidArray = new BsonArray();

                for (int i = 0; i < cate.video_urls.Count; i++)
                {
                    bsonNewVidArray.Add(cate.video_urls[i].ToBsonDocument());
                }

                var bsonNewAdArray = new BsonArray();

                for (int i = 0; i < cate.ad_urls.Count; i++)
                {
                    bsonNewAdArray.Add(cate.ad_urls[i].ToBsonDocument());
                }

                upd.PushAll("image_urls", bsonNewImgArray);
                upd.PushAll("video_urls", bsonNewVidArray);
                upd.PushAll("ad_urls", bsonNewAdArray);

                result = dal.mongo_update("category", whereclause, bd_prod, upd);

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return result;
        }

        public string update_prod_imgvid(product prd)
        {
            // evoke mongodb connection method in dal
            prd._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_prod = prd.ToBsonDocument();

            string result = "";

            try
            {
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + prd.id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product prod = new product();
                    prod.image_urls = JsonConvert.DeserializeObject<List<imgurl>>(c.ToBsonDocument()["image_urls"].ToString());
                    prod.video_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["video_urls"].ToString());
                    prod.ad_urls = JsonConvert.DeserializeObject<List<adurl>>(c.ToBsonDocument()["ad_urls"].ToString());
                    products.Add(prod);
                }

                var Prev_Prod_Img_Urls = new List<imgurl>();
                Prev_Prod_Img_Urls = products[0].image_urls;

                var Prev_Prod_Video_Urls = new List<url>();
                Prev_Prod_Video_Urls = products[0].video_urls;

                var Prev_Prod_Ad_Urls = new List<adurl>();
                Prev_Prod_Ad_Urls = products[0].ad_urls;

                var whereclause = "";
                whereclause = "{id:'" + prd.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonImgArray = new BsonArray();

                for (int i = 0; i < products[0].image_urls.Count(); i++)
                {
                    bsonImgArray.Add(products[0].image_urls[i].ToBsonDocument());
                }

                var bsonVidArray = new BsonArray();

                for (int i = 0; i < products[0].video_urls.Count(); i++)
                {
                    bsonVidArray.Add(products[0].video_urls[i].ToBsonDocument());
                }

                var bsonAdArray = new BsonArray();

                for (int i = 0; i < products[0].ad_urls.Count(); i++)
                {
                    bsonAdArray.Add(products[0].ad_urls[i].ToBsonDocument());
                }

                update.PullAll("image_urls", bsonImgArray);
                update.PullAll("video_urls", bsonVidArray);
                update.PullAll("ad_urls", bsonAdArray);

                result = dal.mongo_update("product", whereclause, bd_prod, update);

                var New_Prod_Img_Urls = new List<imgurl>();

                if ((prd.image_urls[0].display_name == null) && (prd.image_urls[0].link == null) && (prd.image_urls[0].thumb_link == null) && (prd.image_urls[0].zoom_link == null))
                {
                    prd.image_urls = Prev_Prod_Img_Urls;
                }
                else
                {
                    if (Prev_Prod_Img_Urls.Count() == 0)
                    {
                        imgurl oImgUrl = new imgurl();
                        oImgUrl.display_name = prd.image_urls[0].display_name;
                        oImgUrl.link = prd.image_urls[0].link;
                        oImgUrl.thumb_link = prd.image_urls[0].thumb_link;
                        oImgUrl.zoom_link = prd.image_urls[0].zoom_link;
                        New_Prod_Img_Urls.Add(oImgUrl);
                    }
                    else
                    {
                        for (int i = 0; i < Prev_Prod_Img_Urls.Count(); i++)
                        {
                            if (prd.image_urls[0].display_name != Prev_Prod_Img_Urls[i].display_name)
                            {
                                imgurl objImgUrl = new imgurl();
                                objImgUrl.display_name = Prev_Prod_Img_Urls[i].display_name;
                                objImgUrl.link = Prev_Prod_Img_Urls[i].link;
                                objImgUrl.thumb_link = Prev_Prod_Img_Urls[i].thumb_link;
                                objImgUrl.zoom_link = Prev_Prod_Img_Urls[i].zoom_link;
                                New_Prod_Img_Urls.Add(objImgUrl);
                            }
                            if (prd.image_urls[0].display_name == Prev_Prod_Img_Urls[i].display_name)
                            {
                                imgurl obImgUrl = new imgurl();
                                obImgUrl.display_name = prd.image_urls[0].display_name;
                                obImgUrl.link = prd.image_urls[0].link;
                                obImgUrl.thumb_link = prd.image_urls[0].thumb_link;
                                obImgUrl.zoom_link = prd.image_urls[0].zoom_link;
                                New_Prod_Img_Urls.Add(obImgUrl);
                            }
                        }
                    }
                    prd.image_urls = New_Prod_Img_Urls;
                }

                var New_Prod_Video_Urls = new List<url>();

                if ((prd.video_urls[0].display_name == null) && (prd.video_urls[0].link == null))
                {

                    prd.video_urls = Prev_Prod_Video_Urls;
                }
                else
                {
                    if (Prev_Prod_Video_Urls.Count() == 0)
                    {
                        url ourl = new url();
                        ourl.display_name = prd.image_urls[0].display_name;
                        ourl.link = prd.image_urls[0].link;
                        New_Prod_Video_Urls.Add(ourl);
                    }
                    else
                    {
                        for (int j = 0; j < Prev_Prod_Video_Urls.Count(); j++)
                        {
                            if (prd.video_urls[0].display_name != Prev_Prod_Video_Urls[j].display_name)
                            {
                                url objurl = new url();
                                objurl.display_name = Prev_Prod_Video_Urls[j].display_name;
                                objurl.link = Prev_Prod_Video_Urls[j].link;
                                New_Prod_Video_Urls.Add(objurl);
                            }
                            if (prd.video_urls[0].display_name == Prev_Prod_Video_Urls[j].display_name)
                            {
                                url oburl = new url();
                                oburl.display_name = prd.video_urls[0].display_name;
                                oburl.link = prd.video_urls[0].link;
                                New_Prod_Video_Urls.Add(oburl);
                            }
                        }
                    }

                    prd.video_urls = New_Prod_Video_Urls;
                }

                var New_Prod_Ad_Urls = new List<adurl>();

                if ((prd.ad_urls[0].href == null) && (prd.ad_urls[0].link == null))
                {
                    prd.ad_urls = Prev_Prod_Ad_Urls;
                }
                else
                {
                    if (Prev_Prod_Ad_Urls.Count() == 0)
                    {
                        adurl oadurl = new adurl();
                        oadurl.href = prd.ad_urls[0].href;
                        oadurl.link = prd.ad_urls[0].link;
                        New_Prod_Ad_Urls.Add(oadurl);
                    }
                    else
                    {
                        for (int k = 0; k < Prev_Prod_Ad_Urls.Count(); k++)
                        {
                            if (prd.ad_urls[0].href != Prev_Prod_Ad_Urls[k].href)
                            {
                                adurl objadurl = new adurl();
                                objadurl.href = Prev_Prod_Ad_Urls[k].href;
                                objadurl.link = Prev_Prod_Ad_Urls[k].link;
                                New_Prod_Ad_Urls.Add(objadurl);
                            }
                            if (prd.ad_urls[0].href == Prev_Prod_Ad_Urls[k].href)
                            {
                                adurl obadurl = new adurl();
                                obadurl.href = prd.ad_urls[0].href;
                                obadurl.link = prd.ad_urls[0].link;
                                New_Prod_Ad_Urls.Add(obadurl);
                            }
                        }
                    }
                    prd.ad_urls = New_Prod_Ad_Urls;
                }

                whereclause = "{id:'" + prd.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder upd = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonNewImgArray = new BsonArray();

                for (int i = 0; i < prd.image_urls.Count; i++)
                {
                    bsonNewImgArray.Add(prd.image_urls[i].ToBsonDocument());
                }

                var bsonNewVidArray = new BsonArray();

                for (int i = 0; i < prd.video_urls.Count; i++)
                {
                    bsonNewVidArray.Add(prd.video_urls[i].ToBsonDocument());
                }

                var bsonNewAdArray = new BsonArray();

                for (int i = 0; i < prd.ad_urls.Count; i++)
                {
                    bsonNewAdArray.Add(prd.ad_urls[i].ToBsonDocument());
                }

                upd.PushAll("image_urls", bsonNewImgArray);
                upd.PushAll("video_urls", bsonNewVidArray);
                upd.PushAll("ad_urls", bsonNewAdArray);

                result = dal.mongo_update("product", whereclause, bd_prod, upd);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return result;
        }

        public string upload_prod_imgvid(product prod)
        {
            // evoke mongodb connection method in dal
            prod._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_prod = prod.ToBsonDocument();

            string result = "";

            try
            {
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + prod.id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product prd = new product();
                    prd.image_urls = JsonConvert.DeserializeObject<List<imgurl>>(c.ToBsonDocument()["image_urls"].ToString());
                    prd.video_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["video_urls"].ToString());
                    prd.ad_urls = JsonConvert.DeserializeObject<List<adurl>>(c.ToBsonDocument()["ad_urls"].ToString());
                    products.Add(prd);
                }

                var Prev_Prod_Img_Urls = new List<imgurl>();
                Prev_Prod_Img_Urls = products[0].image_urls;

                var Prev_Prod_Video_Urls = new List<url>();
                Prev_Prod_Video_Urls = products[0].video_urls;

                var Prev_Prod_Ad_Urls = new List<adurl>();
                Prev_Prod_Ad_Urls = products[0].ad_urls;

                var whereclause = "";
                whereclause = "{id:'" + prod.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonImgArray = new BsonArray();

                for (int i = 0; i < products[0].image_urls.Count(); i++)
                {
                    bsonImgArray.Add(products[0].image_urls[i].ToBsonDocument());
                }

                var bsonVidArray = new BsonArray();

                for (int i = 0; i < products[0].video_urls.Count(); i++)
                {
                    bsonVidArray.Add(products[0].video_urls[i].ToBsonDocument());
                }

                var bsonAdArray = new BsonArray();

                for (int i = 0; i < products[0].ad_urls.Count(); i++)
                {
                    bsonAdArray.Add(products[0].ad_urls[i].ToBsonDocument());
                }

                update.PullAll("image_urls", bsonImgArray);
                update.PullAll("video_urls", bsonVidArray);
                update.PullAll("ad_urls", bsonAdArray);

                result = dal.mongo_update("product", whereclause, bd_prod, update);

                if ((prod.image_urls[0].display_name == null) && (prod.image_urls[0].link == null) && (prod.image_urls[0].thumb_link == null) && (prod.image_urls[0].zoom_link == null))
                {
                    prod.image_urls = Prev_Prod_Img_Urls;

                }
                else
                {
                    imgurl objImgUrl = new imgurl();
                    objImgUrl.display_name = prod.image_urls[0].display_name;
                    objImgUrl.link = prod.image_urls[0].link;
                    objImgUrl.thumb_link = prod.image_urls[0].thumb_link;
                    objImgUrl.zoom_link = prod.image_urls[0].zoom_link;
                    Prev_Prod_Img_Urls.Add(objImgUrl);
                    prod.image_urls = Prev_Prod_Img_Urls;
                }
                if ((prod.video_urls[0].display_name == null) && (prod.video_urls[0].link == null))
                {

                    prod.video_urls = Prev_Prod_Video_Urls;
                }
                else
                {
                    url objVidUrl = new url();
                    objVidUrl.display_name = prod.video_urls[0].display_name;
                    objVidUrl.link = prod.video_urls[0].link;
                    Prev_Prod_Video_Urls.Add(objVidUrl);
                    prod.video_urls = Prev_Prod_Video_Urls;

                }
                if ((prod.ad_urls[0].href == null) && (prod.ad_urls[0].link == null))
                {

                    prod.ad_urls = Prev_Prod_Ad_Urls;
                }
                else
                {
                    adurl objAdUrl = new adurl();
                    objAdUrl.href = prod.ad_urls[0].href;
                    objAdUrl.link = prod.ad_urls[0].link;
                    Prev_Prod_Ad_Urls.Add(objAdUrl);
                    prod.ad_urls = Prev_Prod_Ad_Urls;
                }

                whereclause = "{id:'" + prod.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder upd = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonNewImgArray = new BsonArray();

                for (int i = 0; i < prod.image_urls.Count; i++)
                {
                    bsonNewImgArray.Add(prod.image_urls[i].ToBsonDocument());
                }

                var bsonNewVidArray = new BsonArray();

                for (int i = 0; i < prod.video_urls.Count; i++)
                {
                    bsonNewVidArray.Add(prod.video_urls[i].ToBsonDocument());
                }

                var bsonNewAdArray = new BsonArray();

                for (int i = 0; i < prod.ad_urls.Count; i++)
                {
                    bsonNewAdArray.Add(prod.ad_urls[i].ToBsonDocument());
                }

                upd.PushAll("image_urls", bsonNewImgArray);
                upd.PushAll("video_urls", bsonNewVidArray);
                upd.PushAll("ad_urls", bsonNewAdArray);

                result = dal.mongo_update("product", whereclause, bd_prod, upd);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return result;
        }

        public string update_prod_features(product prod)
        {
            // evoke mongodb connection method in dal
            prod._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_prod = prod.ToBsonDocument();

            string result = "";

            try
            {
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + prod.id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product prd = new product();
                    prd.feature = JsonConvert.DeserializeObject<List<features>>(c.ToBsonDocument()["feature"].ToString());
                    products.Add(prd);
                }

                var Prev_Prod_Feature = new List<features>();
                var New_Prod_Feature = new List<features>();
                Prev_Prod_Feature = products[0].feature;
                int Count = Prev_Prod_Feature.Count;
                //New_Prod_Feature = Prev_Prod_Feature;

                var whereclause = "";
                whereclause = "{id:'" + prod.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();
                var bsonFeatureArray = new BsonArray();

                for (int i = 0; i < products[0].feature.Count(); i++)
                {
                    bsonFeatureArray.Add(products[0].feature[i].ToBsonDocument());
                }
                update.PullAll("feature", bsonFeatureArray);
                result = dal.mongo_update("product", whereclause, bd_prod, update);


                for (int j = 0; j < prod.feature.Count(); j++)
                {
                    //int match = 0;
                    //for (var k = 0; k < Count; k++)
                    //{
                    //    if (prod.feature[j].name == Prev_Prod_Feature[k].name)
                    //    {
                    //        match = 1;
                    //    }
                    //}
                    //if (match == 0)
                    //{
                    features objFeature = new features();
                    objFeature.name = prod.feature[j].name;
                    objFeature.values = prod.feature[j].values;
                    New_Prod_Feature.Add(objFeature);
                    //}
                }

                MongoDB.Driver.Builders.UpdateBuilder upd = new MongoDB.Driver.Builders.UpdateBuilder();
                var bsonNewFeatureArray = new BsonArray();
                for (int i = 0; i < New_Prod_Feature.Count; i++)
                {
                    bsonNewFeatureArray.Add(New_Prod_Feature[i].ToBsonDocument());
                }

                upd.PushAll("feature", bsonNewFeatureArray);
                result = dal.mongo_update("product", whereclause, bd_prod, upd);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return result;
        }
        public string del_cat_filter(category cate)
        {

            // evoke mongodb connection method in dal
            cate._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_prod = cate.ToBsonDocument();

            string result = "";

            try
            {
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + cate.id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc);

                var categories = new List<category>();
                foreach (var c in cursor)
                {
                    category categ = new category();
                    categ.filters = JsonConvert.DeserializeObject<List<filter>>(c.ToBsonDocument()["filters"].ToString());
                    categories.Add(categ);
                }

                var Cate_filters = new List<filter>();
                Cate_filters = categories[0].filters;

                var Prev_Cate_filters = new List<filter>();
                Prev_Cate_filters = categories[0].filters;

                var whereclause = "";
                whereclause = "{id:'" + cate.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonArray = new BsonArray();

                for (int i = 0; i < Cate_filters.Count; i++)
                {
                    bsonArray.Add(Cate_filters[i].ToBsonDocument());
                }

                update.PullAll("filters", bsonArray);

                result = dal.mongo_update("category", whereclause, bd_prod, update);

                var Del_Cate_filters = new List<filter>();

                Del_Cate_filters = cate.filters;

                cate.filters = Prev_Cate_filters;

                whereclause = "{id:'" + cate.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder upd = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonArrayNew = new BsonArray();

                for (int i = 0; i < cate.filters.Count; i++)
                {
                    if (cate.filters[i].name != Del_Cate_filters[0].name)
                    {
                        bsonArrayNew.Add(cate.filters[i].ToBsonDocument());
                    }
                }

                upd.PushAll("filters", bsonArrayNew);

                result = dal.mongo_update("category", whereclause, bd_prod, upd);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return result;
        }

        public string del_child_product(product prod)
        {
            // evoke mongodb connection method in dal
            try
            {
                prod._id = ObjectId.GenerateNewId().ToString();
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_prod = prod.ToBsonDocument();
                var whereclause = "";
                whereclause = "{id:'" + prod.id + "'}";
                string result = dal.mongo_remove("product", whereclause, bd_prod);
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        public string upd_child_product(product prod)
        {
            // evoke mongodb connection method in dal
            //prod._id = ObjectId.GenerateNewId().ToString();
            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_prod = prod.ToBsonDocument();
                var whereclause = "{id:'" + prod.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();
                //  update = MongoDB.Driver.Builders.Update.Set("stock", prod.stock).Set("Express", prod.Express);
                update = MongoDB.Driver.Builders.Update.Set("Express", prod.Express);
                string result = dal.mongo_update("product", whereclause, bd_prod, update);
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string del_deal(deals dl)
        {
            // evoke mongodb connection method in dal
            try
            {
                dl._id = ObjectId.GenerateNewId().ToString();
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_deals = dl.ToBsonDocument();
                var whereclause = "";
                whereclause = "{deal_details:'" + dl.deal_details + "'}";
                string result = dal.mongo_remove("deals", whereclause, bd_deals);
                return result;

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string dele_product_feature(product prod)
        {
            try
            {
                // evoke mongodb connection method in dal
                prod._id = ObjectId.GenerateNewId().ToString();
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_prod = prod.ToBsonDocument();

                string result = "";

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + prod.id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product prd = new product();
                    prd.feature = JsonConvert.DeserializeObject<List<features>>(c.ToBsonDocument()["feature"].ToString());
                    products.Add(prd);
                }

                var New_Prod_Features = new List<features>();

                var Prev_Prod_Features = new List<features>();
                Prev_Prod_Features = products[0].feature;

                var whereclause = "";
                whereclause = "{id:'" + prod.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonArray = new BsonArray();

                for (int i = 0; i < Prev_Prod_Features.Count; i++)
                {
                    bsonArray.Add(Prev_Prod_Features[i].ToBsonDocument());
                }

                update.PullAll("feature", bsonArray);

                result = dal.mongo_update("product", whereclause, bd_prod, update);

                for (int i = 0; i < Prev_Prod_Features.Count; i++)
                {
                    if (Prev_Prod_Features[i].name != prod.feature[0].name)
                    {
                        features oFeatures = new features();
                        oFeatures.name = Prev_Prod_Features[i].name;
                        oFeatures.values = Prev_Prod_Features[i].values;
                        New_Prod_Features.Add(oFeatures);
                    }
                }

                MongoDB.Driver.Builders.UpdateBuilder upd = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonArrayNew = new BsonArray();

                for (int i = 0; i < New_Prod_Features.Count; i++)
                {
                    bsonArrayNew.Add(New_Prod_Features[i].ToBsonDocument());
                }

                upd.PushAll("feature", bsonArrayNew);

                result = dal.mongo_update("product", whereclause, bd_prod, upd);

                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }


        public string dele_product_stock(product prod)
        {
            try
            {
                // evoke mongodb connection method in dal
                prod._id = ObjectId.GenerateNewId().ToString();
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_prod = prod.ToBsonDocument();

                string result = "";

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + prod.id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product prd = new product();
                    prd.stock = JsonConvert.DeserializeObject<List<bulk_stock>>(c.ToBsonDocument()["stock"].ToString());
                    products.Add(prd);
                }

                var New_Prod_Stock = new List<bulk_stock>();
                var Prev_Prod_Stock = new List<bulk_stock>();
                Prev_Prod_Stock = products[0].stock;

                var whereclause = "";
                whereclause = "{id:'" + prod.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonArray = new BsonArray();

                for (int i = 0; i < Prev_Prod_Stock.Count; i++)
                {
                    bsonArray.Add(Prev_Prod_Stock[i].ToBsonDocument());
                }

                update.PullAll("stock", bsonArray);

                result = dal.mongo_update("product", whereclause, bd_prod, update);

                for (int i = 0; i < Prev_Prod_Stock.Count; i++)
                {
                    if (Prev_Prod_Stock[i].warehouse != prod.stock[0].warehouse)
                    {
                        bulk_stock obs = new bulk_stock();
                        obs.warehouse = Prev_Prod_Stock[i].warehouse;
                        obs.stock = Prev_Prod_Stock[i].stock;
                        New_Prod_Stock.Add(obs);
                    }
                }

                MongoDB.Driver.Builders.UpdateBuilder upd = new MongoDB.Driver.Builders.UpdateBuilder();

                var bsonArrayNew = new BsonArray();

                for (int i = 0; i < New_Prod_Stock.Count; i++)
                {
                    bsonArrayNew.Add(New_Prod_Stock[i].ToBsonDocument());
                }

                upd.PushAll("stock", bsonArrayNew);

                result = dal.mongo_update("product", whereclause, bd_prod, upd);

                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public login_msg admin_login(user_security us)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string Msg = "";

            login_msg return_msg = new login_msg();

            try
            {

                int i = da.ExecuteSP("company_login", ref oc
                                , da.Parameter("_user_name", us.user_name)
                                , da.Parameter("_password", us.password)
                                , da.Parameter("_outmsg", String.Empty, true)
                                );


                if (i == 1)
                {
                    Msg = oc[0].strParamValue;
                }
                else
                {
                    Msg = oc[0].strParamValue;
                }
                return_msg.type = "success";
                return_msg.value = Msg;

            }
            catch (Exception ex)
            {
                return_msg.type = "system_error";
                return_msg.value = ex.Message;
                ExternalLogger.LogError(ex, this, "#");
            }

            //return Msg;
            return return_msg;
        }

        #endregion

        #region post methods
        public string insert_catagory(category cat)
        {

            try
            {
                // evoke mongodb connection method in dal
                cat._id = ObjectId.GenerateNewId().ToString();
                cat.filters = new List<filter>();
                cat.image_urls = new List<imgurl>();
                cat.video_urls = new List<url>();
                cat.ad_urls = new List<adurl>();
                dal.DataAccess dal = new DataAccess();

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{ParentId:'" + cat.ParentId + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc);

                var categories = new List<category>();

                foreach (var c in cursor)
                {
                    category categ = new category();
                    categ.id = c.ToBsonDocument()["id"].ToString();
                    categories.Add(categ);
                }

                if (cat.ParentId == "0")
                {
                    if (categories.Count() == 0)
                    {
                        cat.id = "1.0";
                    }
                    else
                    {
                        int[] Max_catid_lvl1 = new int[categories.Count()];
                        for (int i = 0; i < categories.Count(); i++)
                        {
                            string[] ConsId = categories[i].id.Split('.');
                            if (ConsId.Count() == 2)
                            {
                                if (ConsId[1] == "0")
                                {
                                    Max_catid_lvl1[i] = convertToInt(ConsId[0].ToString());
                                }
                            }
                        }
                        var Max_Lvl1_ID = 0;
                        Max_Lvl1_ID = Max_catid_lvl1.Max() * 1 + 1;
                        cat.id = Max_Lvl1_ID.ToString() + ".0";
                    }
                }
                else if (cat.ParentId.Contains("."))
                {
                    string[] Parent_ID = cat.ParentId.Split('.');
                    if (Parent_ID.Count() == 2)
                    {
                        if (categories.Count() == 0)
                        {
                            cat.id = Parent_ID[0].ToString() + ".1.0";
                        }
                        else
                        {
                            int[] Max_catid_lvl2 = new int[categories.Count()];
                            for (int i = 0; i < categories.Count(); i++)
                            {
                                string[] ConsId = categories[i].id.Split('.');
                                if (ConsId.Count() == 3)
                                {
                                    if (ConsId[2] == "0")
                                    {
                                        Max_catid_lvl2[i] = convertToInt(ConsId[1].ToString());
                                    }
                                }
                            }
                            var Max_Lvl2_ID = 0;
                            Max_Lvl2_ID = Max_catid_lvl2.Max() * 1 + 1;
                            cat.id = Parent_ID[0].ToString() + "." + Max_Lvl2_ID.ToString() + ".0";
                        }

                    }
                    else if (Parent_ID.Count() == 3)
                    {
                        if (categories.Count() == 0)
                        {
                            cat.id = Parent_ID[0].ToString() + "." + Parent_ID[1].ToString() + ".1";
                        }
                        else
                        {
                            int[] Max_catid_lvl3 = new int[categories.Count()];
                            for (int i = 0; i < categories.Count(); i++)
                            {
                                string[] ConsId = categories[i].id.Split('.');
                                if (ConsId.Count() == 3)
                                {

                                    Max_catid_lvl3[i] = convertToInt(ConsId[2].ToString());

                                }
                            }
                            var Max_Lvl3_ID = 0;
                            Max_Lvl3_ID = Max_catid_lvl3.Max() * 1 + 1;
                            cat.id = Parent_ID[0].ToString() + "." + Parent_ID[1].ToString() + "." + Max_Lvl3_ID.ToString();
                        }
                    }
                }

                BsonDocument bd_cat = cat.ToBsonDocument();
                string result = dal.mongo_write("category", bd_cat);
                return result;

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string insert_brand(brand brnd)
        {
            // evoke mongodb connection method in dal           
            try
            {
                brnd._id = ObjectId.GenerateNewId().ToString();
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db("brand");
                var brands = new List<brand>();


                foreach (var c in cursor)
                {
                    brand brd = new brand();
                    brd.name = check_field(c.ToBsonDocument(), "name") ? convertToString(c.ToBsonDocument()["name"]) : null;
                    brands.Add(brd);

                }
                int Match = 0;
                string result = "";
                for (var i = 0; i < brands.Count(); i++)
                {
                    if (brands[i].name.ToString().ToUpper() == brnd.name.ToString().ToUpper())
                    {
                        Match++;
                    }
                }

                if (Match == 0)
                {
                    BsonDocument bd_brnd = brnd.ToBsonDocument();
                    result = dal.mongo_write("brand", bd_brnd);

                }
                else
                {
                    result = "Brand Name Already Exists";

                }
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string update_prod_enquire_status(enq_upd oeu)
        {

            DataAccess da = new DataAccess();

            List<OutCollection> oc = new List<OutCollection>();


            try
            {
                for (int j = 0; j < oeu.enqids.Count; j++)
                {
                    int i = da.ExecuteSP("update_product_enquiry_status", ref oc
                                    , da.Parameter("_id", oeu.enqids[j])
                                    , da.Parameter("_status", oeu.status)
                                    );

                }
                return " Updated Sucessfully";

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed";
            }
        }



        public string update_gen_enquire_status(gen_enq_upd ge)
        {

            DataAccess da = new DataAccess();

            List<OutCollection> oc = new List<OutCollection>();


            try
            {
                for (int j = 0; j < ge.enqids.Count; j++)
                {
                    int i = da.ExecuteSP("update_gen_enquiry_status", ref oc
                                    , da.Parameter("_id", ge.enqids[j])
                                    , da.Parameter("_status", ge.status)
                                    );

                }
                return " Status Updated Sucessfully";

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed";
            }
        }



        public string update_catagory(category cat)
        {
            // evoke mongodb connection method in dal

            try
            {
                cat._id = ObjectId.GenerateNewId().ToString();
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_cat = cat.ToBsonDocument();
                var whereclause = "{id:'" + cat.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();
                if (cat.special != null)
                {
                    update = MongoDB.Driver.Builders.Update.Set("Name", cat.Name).Set("description", cat.description).Set("special", cat.special).Set("Shipping", cat.Shipping).Set("active", cat.active);
                }
                else
                {
                    if (cat.Shipping != null)
                    {
                        update = MongoDB.Driver.Builders.Update.Set("Name", cat.Name).Set("description", cat.description).Set("Shipping", cat.Shipping).Set("active", cat.active).Set("min_ship", cat.min_ship).Set("max_ship", cat.max_ship);
                    }
                    else
                    {
                        update = MongoDB.Driver.Builders.Update.Set("Name", cat.Name).Set("description", cat.description).Set("active", cat.active);
                    }
                }

                string result = dal.mongo_update("category", whereclause, bd_cat, update);

                if (cat.store_defn[0].start_date != null && cat.store_defn[0].end_date != null)
                {
                    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + cat.id + "'}");
                    QueryDocument queryDoc = new QueryDocument(document);
                    MongoCursor cursor = dal.execute_mongo_db("category", queryDoc);
                    var categories = new List<category>();
                    foreach (var c in cursor)
                    {
                        category categ = new category();
                        categ.store_defn = JsonConvert.DeserializeObject<List<storedate>>(c.ToBsonDocument()["store_defn"].ToString());
                        categories.Add(categ);
                    }
                    var Cate_Store_Date = new List<storedate>();
                    Cate_Store_Date = categories[0].store_defn;
                    MongoDB.Driver.Builders.UpdateBuilder upd = new MongoDB.Driver.Builders.UpdateBuilder();
                    var bsonArray = new BsonArray();
                    for (int i = 0; i < Cate_Store_Date.Count; i++)
                    {
                        bsonArray.Add(Cate_Store_Date[i].ToBsonDocument());
                    }
                    upd.PullAll("store_defn", bsonArray);
                    result = dal.mongo_update("category", whereclause, bd_cat, upd);
                    MongoDB.Driver.Builders.UpdateBuilder upda = new MongoDB.Driver.Builders.UpdateBuilder();
                    var bsonArrayNew = new BsonArray();
                    for (int i = 0; i < cat.store_defn.Count; i++)
                    {
                        bsonArrayNew.Add(cat.store_defn[i].ToBsonDocument());
                    }
                    upda.PushAll("store_defn", bsonArrayNew);
                    result = dal.mongo_update("category", whereclause, bd_cat, upda);
                }
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public MongoDB.Bson.BsonNull MongoUpdateNull(string updVal)
        {

            return BsonNull.Value;

        }


        public string deal_setup(product prod)
        {
            // evoke mongodb connection method in dal

            try
            {
                string result = "";
                var odeal = new deals();

                odeal._id = ObjectId.GenerateNewId().ToString();
                //odeal.deal_details = "Brand : " + prod.brand + " ; " + " Discount : " + prod.price.discount + " % ;" + " Dated On : " + System.DateTime.Now;
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_deal = odeal.ToBsonDocument();

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{brand:'" + prod.brand + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product prd = new product();
                    prd.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    //prd.price = check_field(c.ToBsonDocument(), "price") ? JsonConvert.DeserializeObject<price>(c.ToBsonDocument()["price"].ToString()) : null;
                    products.Add(prd);
                }

                for (var i = 0; i < products.Count(); i++)
                {
                    string UpdateResult = update_deal_price(products[i], prod);
                }
                result = dal.mongo_write("deals", bd_deal);
                return result;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string update_deal_price(product PrevProd, product NewProd)
        {

            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_prod = PrevProd.ToBsonDocument();
                price price_list = new price();
                //price_list = PrevProd.price;

                //var NewDisc = convertToFloat(NewProd.price.discount.ToString());
                ////var PrevMRP = convertToDouble(PrevProd.price.mrp.ToString());
                //var Disc_Amt = ((PrevMRP * NewDisc) / 100);
                //var NewList = PrevMRP - Disc_Amt;
                //var ListPrice = (Math.Round(NewList * 100)) / 100;


                //price_list.mrp = convertToDecimal(PrevMRP);
                //price_list.mrp = convertToDouble(PrevMRP);
                //price_list.list = convertToDouble(ListPrice.ToString());
                //price_list.discount = convertToDouble(NewDisc.ToString());
                //price_list.min = convertToDouble(PrevMRP.ToString());
                //price_list.final_offer = convertToDouble(ListPrice.ToString());
                //price_list.final_discount = convertToDouble(NewDisc.ToString());

                var whereclause = "{id:'" + PrevProd.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = MongoDB.Driver.Builders.Update.SetWrapped("price", (price)price_list);
                //MongoDB.Driver.Builders.UpdateBuilder update = MongoDB.Driver.Builders.Update.Set("price.mrp", price_list.mrp.);                                                    
                //.SetWrapped("price", price_list.mrp);
                string result = dal.mongo_update("product", whereclause, bd_prod, update);
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }


        public string insert_product(product prd)
        {
            //evoke mongodb connection method in dal
            try
            {
                prd._id = ObjectId.GenerateNewId().ToString();
                prd.image_urls = new List<imgurl>();
                prd.video_urls = new List<url>();
                prd.ad_urls = new List<adurl>();
                prd.cat_id = new List<category_id>();
                prd.feature = new List<features>();
                prd.price = new List<price>();
                prd.stock = new List<bulk_stock>();
                prd.create_date = System.DateTime.Now.ToLocalTime();
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_cat = prd.ToBsonDocument();
                string result = dal.mongo_write("product", bd_cat);
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string insert_store_config(store_config sc)
        {
            try
            {
                //evoke mongodb connection method in dal
                sc._id = ObjectId.GenerateNewId().ToString();
                dal.DataAccess dal = new DataAccess();


                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{store:'" + sc.store + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("store_config", queryDoc);

                var store_configs = new List<store_config>();
                foreach (var c in cursor)
                {
                    //		c.ToBsonDocument().Contains("id")

                    store_config osc = new store_config();
                    osc.store = check_field(c.ToBsonDocument(), "store") ? convertToString(c.ToBsonDocument()["store"]) : null;
                    osc.exclusion = check_field(c.ToBsonDocument(), "exclusion") ? JsonConvert.DeserializeObject<List<Exclu>>(c.ToBsonDocument()["exclusion"].ToString()) : null;
                    osc.discount = check_field(c.ToBsonDocument(), "discount") ? JsonConvert.DeserializeObject<List<Disc>>(c.ToBsonDocument()["discount"].ToString()) : null;


                    store_configs.Add(osc);
                }

                if (store_configs.Count() == 0)
                {

                    BsonDocument bd_sc = sc.ToBsonDocument();
                    string result = dal.mongo_write("store_config", bd_sc);
                    return result;
                }
                else
                {
                    string whereclause = "{store:'" + sc.store + "'}";
                    BsonDocument bd_sc = sc.ToBsonDocument();

                    var bsonExcluArray = new BsonArray();
                    for (int i = 0; i < store_configs[0].exclusion.Count; i++)
                    {
                        bsonExcluArray.Add(store_configs[0].exclusion[i].ToBsonDocument());
                    }
                    MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();
                    update.PullAll("exclusion", bsonExcluArray);
                    string result = dal.mongo_update("store_config", whereclause, bd_sc, update);

                    var bsonNewExcluArray = new BsonArray();
                    for (int i = 0; i < sc.exclusion.Count; i++)
                    {
                        bsonNewExcluArray.Add(sc.exclusion[i].ToBsonDocument());
                    }
                    MongoDB.Driver.Builders.UpdateBuilder upd = new MongoDB.Driver.Builders.UpdateBuilder();
                    upd.PushAll("exclusion", bsonNewExcluArray);
                    result = dal.mongo_update("store_config", whereclause, bd_sc, upd);



                    var bsonDiscArray = new BsonArray();
                    for (int i = 0; i < store_configs[0].discount.Count; i++)
                    {
                        bsonDiscArray.Add(store_configs[0].discount[i].ToBsonDocument());
                    }
                    MongoDB.Driver.Builders.UpdateBuilder updateDisc = new MongoDB.Driver.Builders.UpdateBuilder();
                    updateDisc.PullAll("discount", bsonDiscArray);
                    result = dal.mongo_update("store_config", whereclause, bd_sc, updateDisc);

                    var bsonNewDiscArray = new BsonArray();
                    for (int i = 0; i < sc.discount.Count; i++)
                    {
                        bsonNewDiscArray.Add(sc.discount[i].ToBsonDocument());
                    }
                    MongoDB.Driver.Builders.UpdateBuilder updDisc = new MongoDB.Driver.Builders.UpdateBuilder();
                    updDisc.PushAll("discount", bsonNewDiscArray);
                    result = dal.mongo_update("store_config", whereclause, bd_sc, updDisc);

                    return result;

                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }


        }


        public string update_product(product prod)
        {
            try
            {
                // evoke mongodb connection method in dal
                prod._id = ObjectId.GenerateNewId().ToString();
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_prod = prod.ToBsonDocument();
                //var price_list = new price();
                //price_list = prod.price;
                // var whereclause = "{id:'" + prod.id + "'}"; //Atul
                var whereclause = "{id:'" + prod.Name_value + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();
                if (prod.Express != null)
                {
                    if (prod.have_child == 1)
                    {
                        var stock_list = new List<bulk_stock>();
                        update = MongoDB.Driver.Builders.Update.Set("id", prod.id).Set("Name", prod.Name).Set("sku", prod.sku).Set("description", prod.description).Set("active", prod.active).Set("brand", prod.brand).Set("shortdesc", prod.shortdesc).Set("Express", prod.Express).Set("Reorder_Level", prod.Reorder_Level).Set("have_child", prod.have_child).SetWrapped("stock", stock_list);
                    }
                    else
                    {
                        update = MongoDB.Driver.Builders.Update.Set("id", prod.id).Set("Name", prod.Name).Set("sku", prod.sku).Set("description", prod.description).Set("active", prod.active).Set("brand", prod.brand).Set("shortdesc", prod.shortdesc).Set("Express", prod.Express).Set("Reorder_Level", prod.Reorder_Level).Set("have_child", prod.have_child);
                    }
                }
                else
                {
                    if (prod.have_child == 1)
                    {
                        var stock_list = new List<bulk_stock>();
                        update = MongoDB.Driver.Builders.Update.Set("id", prod.id).Set("Name", prod.Name).Set("sku", prod.sku).Set("description", prod.description).Set("active", prod.active).Set("brand", prod.brand).Set("shortdesc", prod.shortdesc).Set("have_child", prod.have_child).Set("Reorder_Level", prod.Reorder_Level).Set("Express", BsonNull.Value).SetWrapped("stock", stock_list);
                    }
                    else
                    {
                        update = MongoDB.Driver.Builders.Update.Set("id", prod.id).Set("Name", prod.Name).Set("sku", prod.sku).Set("description", prod.description).Set("active", prod.active).Set("brand", prod.brand).Set("shortdesc", prod.shortdesc).Set("have_child", prod.have_child).Set("Reorder_Level", prod.Reorder_Level).Set("Express", BsonNull.Value);
                    }
                }

                string result = dal.mongo_update("product", whereclause, bd_prod, update);
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
		
		

        public string update_prod_price_list(product prod)
        {
            try
            {
                // evoke mongodb connection method in dal
                prod._id = ObjectId.GenerateNewId().ToString();
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_prod = prod.ToBsonDocument();
                var price_list = new List<price>();
                if (prod.price[0].mrp != 0)
                {
                    price_list = prod.price;
                }
                var whereclause = "{id:'" + prod.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();

                update = MongoDB.Driver.Builders.Update.SetWrapped("price", price_list);

                string result = dal.mongo_update("product", whereclause, bd_prod, update);
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }


        public string update_prod_stock_list(product prod)
        {
            try
            {
                // evoke mongodb connection method in dal
                prod._id = ObjectId.GenerateNewId().ToString();
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_prod = prod.ToBsonDocument();
                var stock_list = new List<bulk_stock>();
                if (prod.stock[0].warehouse != "" && prod.stock[0].warehouse != null)
                {
                    stock_list = prod.stock;
                }
                var whereclause = "{id:'" + prod.id + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();
                update = MongoDB.Driver.Builders.Update.SetWrapped("stock", stock_list);
                string result = dal.mongo_update("product", whereclause, bd_prod, update);
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }



        public string update_stock_qty(string Product_ID, double Quantity, string Cond)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_ID + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product prod = new product();
                    prod.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    //prod.stock = check_field(c.ToBsonDocument(), "stock") ? convertToInt(c.ToBsonDocument()["stock"]) : 0;
                    products.Add(prod);
                }

                var PrevStock = convertToDouble(products[0].stock.ToString());

                var Curr_Stock = 0.00;

                if (Cond == "Add")
                {
                    Curr_Stock = PrevStock + Quantity;
                }
                else
                {
                    Curr_Stock = PrevStock - Quantity;
                }


                BsonDocument bd_prod = products[0].ToBsonDocument();
                var whereclause = "{id:'" + Product_ID + "'}";
                MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();
                update = MongoDB.Driver.Builders.Update.Set("stock", Curr_Stock);

                string result = dal.mongo_update("product", whereclause, bd_prod, update);
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string update_brand(brand brnd)
        {
            try
            {
                // evoke mongodb connection method in dal
                var whereclause = "{name:'" + brnd._id + "'}";
                string result = "";
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db("brand");
                var brands = new List<brand>();
                foreach (var c in cursor)
                {
                    brand brd = new brand();
                    brd.name = check_field(c.ToBsonDocument(), "name") ? convertToString(c.ToBsonDocument()["name"]) : null;
                    brands.Add(brd);

                }
                int Match = 0;

                if (brnd.name.ToString().ToUpper() != brnd._id.ToString().ToUpper())
                {
                    for (var i = 0; i < brands.Count(); i++)
                    {
                        if (brands[i].name.ToString().ToUpper() == brnd.name.ToString().ToUpper())
                        {
                            Match++;
                        }
                    }

                }
                else
                {
                    Match = 0;
                }



                if (Match == 0)
                {
                    brnd._id = ObjectId.GenerateNewId().ToString();
                    BsonDocument bd_brnd = brnd.ToBsonDocument();
                    MongoDB.Driver.Builders.UpdateBuilder update = MongoDB.Driver.Builders.Update.Set("name", brnd.name);
                    result = dal.mongo_update("brand", whereclause, bd_brnd, update);

                }
                else
                {
                    result = "Brand Name Already Exists";

                }

                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }


        public string insert_filter(copyfilter catecopyfilter)
        {
            // evoke mongodb connection method in dal

            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument doc = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + catecopyfilter.copyto_catid + "'}");
                QueryDocument qryDoc = new QueryDocument(doc);
                MongoCursor cur = dal.execute_mongo_db("category", qryDoc);

                var Master_Cat = new List<category>();
                foreach (var c in cur)
                {
                    category cate = new category();
                    cate.id = c.ToBsonDocument()["id"].ToString();
                    cate.filters = JsonConvert.DeserializeObject<List<filter>>(c.ToBsonDocument()["filters"].ToString());
                    Master_Cat.Add(cate);
                }

                var Master_Cat_Filters = new List<filter>();
                Master_Cat_Filters = Master_Cat[0].filters;


                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + catecopyfilter.copyfrom_catid + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc);

                var catagories = new List<category>();
                foreach (var c in cursor)
                {
                    category cate = new category();
                    cate.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    cate.filters = JsonConvert.DeserializeObject<List<filter>>(c.ToBsonDocument()["filters"].ToString());
                    catagories.Add(cate);
                }

                var Category_Filters = new List<filter>();
                Category_Filters = catagories[0].filters;

                var whereclause = "{id:'" + catecopyfilter.copyto_catid + "'}";
                category categ = new category();
                BsonDocument bd_cat = categ.ToBsonDocument();
                var bsonArray = new BsonArray();
                for (int i = 0; i < Master_Cat_Filters.Count; i++)
                {
                    bsonArray.Add(Master_Cat_Filters[i].ToBsonDocument());
                }
                MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();
                update.PullAll("filters", bsonArray);
                string result = dal.mongo_update("category", whereclause, bd_cat, update);


                var New_Cat_Filters = new List<filter>();

                if (Master_Cat_Filters.Count == 0)
                {
                    New_Cat_Filters = Category_Filters;
                }
                else
                {
                    New_Cat_Filters = Master_Cat_Filters;

                    for (var i = 0; i < Category_Filters.Count; i++)
                    {
                        int match = 0;
                        for (var j = 0; j < Master_Cat_Filters.Count; j++)
                        {
                            if (Category_Filters[i].name == Master_Cat_Filters[j].name)
                            {
                                match = 1;
                            }
                        }
                        if (match == 0)
                        {

                            filter fl = new filter();
                            fl.name = Category_Filters[i].name;
                            fl.values = Category_Filters[i].values;
                            New_Cat_Filters.Add(fl);
                        }
                    }
                }

                whereclause = "{id:'" + catecopyfilter.copyto_catid + "'}";
                var bsonNewArray = new BsonArray();
                for (int i = 0; i < New_Cat_Filters.Count; i++)
                {
                    bsonNewArray.Add(New_Cat_Filters[i].ToBsonDocument());
                }
                MongoDB.Driver.Builders.UpdateBuilder upd = new MongoDB.Driver.Builders.UpdateBuilder();
                upd.PushAll("filters", bsonNewArray);
                result = dal.mongo_update("category", whereclause, bd_cat, upd);
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string insert_child_product(product prd)
        {
            //evoke mongodb connection method in dal

            try
            {
                prd._id = ObjectId.GenerateNewId().ToString();

                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{parent_prod:'" + prd.parent_prod + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();

                foreach (var c in cursor)
                {
                    product prod = new product();
                    prod.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    products.Add(prod);
                }
                if (products.Count() == 0)
                {
                    if (prd.parent_prod.Contains("."))
                    {
                        string[] Parent_Prod = prd.parent_prod.Split('.');
                        prd.id = Parent_Prod[0].ToString() + ".1";
                    }
                    else
                    {
                        prd.id = prd.parent_prod.ToString() + ".1";
                    }
                }
                else
                {
                    int[] Prod_Id = new int[products.Count()];
                    for (int i = 0; i < products.Count(); i++)
                    {
                        string[] ConsId = products[i].id.Split('.');
                        if (ConsId.Count() == 2)
                        {
                            Prod_Id[i] = convertToInt(ConsId[1].ToString());
                        }
                    }
                    var Max_Child_Id = Prod_Id.Max() * 1 + 1;
                    if (prd.parent_prod.Contains("."))
                    {
                        string[] Parent_Prod = prd.parent_prod.Split('.');
                        prd.id = Parent_Prod[0].ToString() + "." + Max_Child_Id.ToString();
                    }
                    else
                    {
                        prd.id = prd.parent_prod.ToString() + "." + Max_Child_Id.ToString();
                    }

                }
                prd.image_urls = new List<imgurl>();
                prd.video_urls = new List<url>();
                prd.ad_urls = new List<adurl>();
                prd.cat_id = new List<category_id>();
                prd.brand = "";
                prd.Name = "";
                //BsonDocument bd_cat = prd.ToBsonDocument();

                childproduct chldprod = new childproduct();
                chldprod._id = ObjectId.GenerateNewId().ToString();
                chldprod.id = prd.id;
                chldprod.parent_prod = prd.parent_prod;
                chldprod.size = prd.size;
                chldprod.sku = prd.sku;
                chldprod.stock = new List<bulk_stock>();
                chldprod.Express = prd.Express;
                BsonDocument bd_cat = chldprod.ToBsonDocument();
                string result = dal.mongo_write("product", bd_cat);
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }


       

        #endregion
    }
}